class ActionDispatch::Routing::Mapper
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end
end

Rails.application.routes.draw do


  devise_for :user, :controllers => {:sessions => "sessions"}
  devise_scope :user do
    match 'choose-school' => 'sessions#choose_school', via: [:get, :post]
    get 'sign-in' => 'sessions#who_are_you', as: :who_are_you
    get 'sign-in-teachers' => 'sessions#new', user: {teacher: true}
    post 'sign-in-teachers' => 'sessions#create'
    get 'sign-in-pupils' => 'sessions#new', user: {pupil: true}
    post 'sign-in-pupils' => 'sessions#create'
    get 'sign-out' => 'sessions#destroy'
  end

  resources :enquiries, only: [:new, :create] do
    collection { get :thanks }
  end

  resources :messages

  #regular routes
  resources :pages, only: :show do
    collection do
      get :product
      get :research
      get :blogs
      get :sample_missions
    end
  end

  get    'presentation/:presentation_id',               to: 'presentations#show', as: 'presentation'
  get    'presentation/:presentation_id/:slide_number', to: 'presentations#show', as: 'presentation_slide'

  get    ':course_id/presentation/:presentation_id', to: 'presentations#show', as: 'course_presentation'
  get    ':course_id/presentation/:presentation_id/:slide_number', to: 'presentations#show', as: 'course_presentation_slide'
  get    'presentation/:slide_id', to: 'presentations#slide_path', as: 'slide'

  draw :faculty

  namespace :pupil, path: 'ppl' do
    resources :lessons, only: [:index, :show] do
      member do
        get '/big_preview', to: 'lessons#big_preview'
        get '/quiz-start', to: 'quizzes#start_quiz', as: :start_quiz
        get '/question', to: 'quizzes#question', as: :question
        post '/question', to: 'quizzes#submit_answer', as: :submit_answer
      end
    end
    resources :results, only: [:index, :show]
    resources :achievements, path: 'my-achievements', only: [:index] do
      get '/set_avatar', action: :set_avatar, on: :member
    end
    get '/my-class', to: 'pupil#my_class'
    get '/my-library', to: 'lessons#library'
    get '/charts/:chart_type', to: 'pupil#charts'
    root 'pupil#home'
  end

  draw :admin
  draw :api

  root 'pages#home'

end
