
scope module: :faculties, path: 'teacher', as: :faculty do
  resources :courses, path_names: {edit: 'settings'} do
    get :duplicate, on: :member
    collection { get :select_years }
  end
  resources :teachers
  resources :klasses, path: 'classes' do
    get 'graph_json', action: :graph_json, on: :collection, as: :graph_json
    member do
      get 'graph_json', action: :show_graph_json, as: :show_graph_json
    end
  end
  resources :messages
  resources :lessons, only: [:show, :edit, :update] do
    member do
      get :mark
      patch :update_marks
    end
  end
  resources :pupils do#
    member do
      get 'graph_json/:chart_type', action: :graph_json, as: :graph_json
      get :add_to_class
      get :leave_class
    end
    collection do
      post :import
    end
  end
  resources :reports
  get '/settings' => 'pages#settings'
  get '/search' => 'pages#search'
  root 'pages#home'

end
