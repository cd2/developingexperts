#namespaced system admin routes
namespace :admin, path: '' do
  constraints subdomain: 'admin' do

    resources :study_years
    resources :units
    resources :lesson_templates do
      resources :lesson_plans, except: :index
      resources :risk_assessments do
        resources :risk_assessment_safeties
        resources :risk_assessment_apparatus
      end
      resources :rocket_words
      resources :quiz_questions
      resources :resources
      member do
        get :formative_questions
        get :summative_questions
      end
      resources :slides do
        collection do
          post :sort
        end
      end
    end

    resources :enquiries

    resources :schools
    resources :user_faculties
    resources :courses
    resources :pupils
    resources :klasses

    get '/soon', to: 'pages#soon'
    root 'pages#home'
  end
end
