scope module: 'api', constraints: {subdomain: 'api'} do
    scope '/v1' do
      scope '/study_years' do
        get '/' => 'api_study_years#index'
        scope '/:id' do
          get '/' => 'api_study_years#show'
        end
      end
      scope '/units' do
        get '/' => 'api_units#index'
        scope '/:id' do
          get '/' => 'api_units#show'
        end
      end
      scope '/lessons' do
        get '/' => 'api_lessons#index'
        scope '/:id' do
          get '/' => 'api_lessons#show'
        end
      end
      scope '/presentations' do
        get '/' => 'api_presentations#index'
        scope '/:id' do
          get '/' => 'api_presentations#show'
        end
      end
      scope '/slides' do
        get '/' => 'api_slides#index'
        scope '/:id' do
          get '/' => 'api_slides#show'
        end
      end
      scope '/schools' do
        get '/' => 'api_schools#index'
        get '/enrollments' => 'api_schools#enrollments'
        scope '/:id' do
          get '/' => 'api_schools#show'
        end
      end
    end
  end
