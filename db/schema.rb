# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161226210450) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "avatars", force: :cascade do |t|
    t.string   "image"
    t.string   "category"
    t.string   "challenge"
    t.string   "progress_text"
    t.string   "total_score"
    t.string   "completed_text"
    t.string   "completed_color"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.integer  "school_id"
    t.datetime "start_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_id"], name: "index_courses_on_school_id", using: :btree
  end

  create_table "enquiries", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.text     "body"
    t.string   "position"
    t.string   "school"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "pupil_id"
    t.integer  "klass_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["klass_id"], name: "index_enrollments_on_klass_id", using: :btree
    t.index ["pupil_id", "klass_id"], name: "index_enrollments_on_pupil_id_and_klass_id", unique: true, using: :btree
    t.index ["pupil_id"], name: "index_enrollments_on_pupil_id", using: :btree
  end

  create_table "klasses", force: :cascade do |t|
    t.string   "name"
    t.integer  "school_id"
    t.integer  "faculty_id"
    t.integer  "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_klasses_on_course_id", using: :btree
    t.index ["faculty_id"], name: "index_klasses_on_faculty_id", using: :btree
    t.index ["school_id"], name: "index_klasses_on_school_id", using: :btree
  end

  create_table "lesson_plans", force: :cascade do |t|
    t.integer  "lesson_template_id"
    t.text     "resources"
    t.text     "sentence_structure"
    t.text     "activities"
    t.text     "assessment"
    t.text     "learning_outcomes"
    t.text     "teacher_mastery"
    t.text     "pupil_mastery"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["lesson_template_id"], name: "index_lesson_plans_on_lesson_template_id", using: :btree
  end

  create_table "lesson_results", force: :cascade do |t|
    t.integer  "pupil_id"
    t.integer  "lesson_id"
    t.decimal  "elapsed_time"
    t.integer  "assessed_score"
    t.text     "teacher_comments"
    t.boolean  "absent",           default: false
    t.integer  "in_class_mark"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["lesson_id"], name: "index_lesson_results_on_lesson_id", using: :btree
    t.index ["pupil_id", "lesson_id"], name: "index_lesson_results_on_pupil_id_and_lesson_id", unique: true, using: :btree
    t.index ["pupil_id"], name: "index_lesson_results_on_pupil_id", using: :btree
  end

  create_table "lesson_templates", force: :cascade do |t|
    t.string   "machine_name"
    t.text     "body"
    t.integer  "unit_id"
    t.string   "song"
    t.string   "cover_image"
    t.integer  "lesson_number"
    t.boolean  "published",     default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["unit_id"], name: "index_lesson_templates_on_unit_id", using: :btree
  end

  create_table "lessons", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "lesson_template_id"
    t.integer  "lesson_number"
    t.boolean  "disabled"
    t.datetime "start_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["course_id"], name: "index_lessons_on_course_id", using: :btree
    t.index ["lesson_template_id"], name: "index_lessons_on_lesson_template_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipent_id"
    t.string   "subject"
    t.text     "message"
    t.boolean  "read"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["recipent_id"], name: "index_messages_on_recipent_id", using: :btree
    t.index ["sender_id"], name: "index_messages_on_sender_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.text     "message"
    t.integer  "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_id"], name: "index_notifications_on_school_id", using: :btree
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "presentations", force: :cascade do |t|
    t.integer  "lesson_template_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["lesson_template_id"], name: "index_presentations_on_lesson_template_id", using: :btree
  end

  create_table "pupil_avatars", force: :cascade do |t|
    t.integer  "pupil_id"
    t.integer  "avatar_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["avatar_id"], name: "index_pupil_avatars_on_avatar_id", using: :btree
    t.index ["pupil_id"], name: "index_pupil_avatars_on_pupil_id", using: :btree
  end

  create_table "quiz_attempt_questions", force: :cascade do |t|
    t.integer  "attempt_id"
    t.integer  "question_number"
    t.integer  "question_id"
    t.integer  "answer_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["answer_id"], name: "index_quiz_attempt_questions_on_answer_id", using: :btree
    t.index ["attempt_id"], name: "index_quiz_attempt_questions_on_attempt_id", using: :btree
    t.index ["question_id"], name: "index_quiz_attempt_questions_on_question_id", using: :btree
  end

  create_table "quiz_attempts", force: :cascade do |t|
    t.integer  "lesson_result_id"
    t.boolean  "completed",        default: false
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["lesson_result_id"], name: "index_quiz_attempts_on_lesson_result_id", using: :btree
  end

  create_table "quiz_questions", force: :cascade do |t|
    t.integer  "quiz_id"
    t.integer  "question_type"
    t.text     "question"
    t.text     "answer"
    t.string   "answer_image"
    t.integer  "slide_id"
    t.integer  "rocket_word_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["quiz_id"], name: "index_quiz_questions_on_quiz_id", using: :btree
    t.index ["rocket_word_id"], name: "index_quiz_questions_on_rocket_word_id", using: :btree
    t.index ["slide_id"], name: "index_quiz_questions_on_slide_id", using: :btree
  end

  create_table "quiz_quizzes", force: :cascade do |t|
    t.integer  "lesson_template_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["lesson_template_id"], name: "index_quiz_quizzes_on_lesson_template_id", using: :btree
  end

  create_table "resources", force: :cascade do |t|
    t.string   "file"
    t.string   "name"
    t.integer  "lesson_template_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["lesson_template_id"], name: "index_resources_on_lesson_template_id", using: :btree
  end

  create_table "risk_assessment_apparatus", force: :cascade do |t|
    t.string   "name"
    t.string   "amount"
    t.string   "practical"
    t.integer  "risk_assessment_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["risk_assessment_id"], name: "index_risk_assessment_apparatus_on_risk_assessment_id", using: :btree
  end

  create_table "risk_assessment_safeties", force: :cascade do |t|
    t.string   "procedure"
    t.text     "hazard"
    t.string   "level_of_risk"
    t.string   "severity"
    t.string   "number_affected"
    t.text     "safety_points"
    t.integer  "risk_assessment_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["risk_assessment_id"], name: "index_risk_assessment_safeties_on_risk_assessment_id", using: :btree
  end

  create_table "risk_assessments", force: :cascade do |t|
    t.integer  "lesson_template_id"
    t.string   "practical_number"
    t.string   "practical_outline"
    t.string   "day"
    t.string   "date"
    t.string   "period"
    t.string   "room"
    t.string   "teacher"
    t.string   "year_group"
    t.string   "lesson_number"
    t.boolean  "class_practical"
    t.boolean  "fume_cupboard"
    t.boolean  "working_in_pairs"
    t.boolean  "demo"
    t.boolean  "safety_screen"
    t.boolean  "individual_assessment"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["lesson_template_id"], name: "index_risk_assessments_on_lesson_template_id", using: :btree
  end

  create_table "rocket_words", force: :cascade do |t|
    t.integer  "lesson_template_id"
    t.string   "word"
    t.string   "image"
    t.text     "description"
    t.integer  "legacy_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["lesson_template_id"], name: "index_rocket_words_on_lesson_template_id", using: :btree
  end

  create_table "schools", force: :cascade do |t|
    t.string   "name"
    t.string   "subdomain"
    t.string   "logo"
    t.string   "twitter"
    t.string   "facebook"
    t.integer  "country_id"
    t.integer  "course_credits"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "sidebar_image"
  end

  create_table "slides", force: :cascade do |t|
    t.integer  "presentation_id"
    t.integer  "slide_number"
    t.integer  "slide_type",       default: 1
    t.text     "upper_text"
    t.text     "lower_text"
    t.text     "on_slide_text"
    t.string   "background_image"
    t.string   "on_slide_image"
    t.string   "video"
    t.string   "narration_video"
    t.integer  "legacy_id"
    t.boolean  "class_only",       default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "new_video_id"
    t.index ["presentation_id"], name: "index_slides_on_presentation_id", using: :btree
  end

  create_table "study_years", force: :cascade do |t|
    t.string   "sidebar_background"
    t.string   "lesson_outro_video"
    t.string   "lesson_intro_mascot"
    t.string   "rocket_thumbnail"
    t.string   "sixty_count_video"
    t.string   "ten_count_video"
    t.string   "mascot_name"
    t.string   "certificate"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "name"
    t.string   "year_number"
  end

  create_table "units", force: :cascade do |t|
    t.string   "name"
    t.string   "ib"
    t.integer  "study_year_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["study_year_id"], name: "index_units_on_study_year_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "type"
    t.integer  "school_id"
    t.string   "hash_id"
    t.integer  "age"
    t.integer  "gender"
    t.integer  "study_year_id"
    t.datetime "dob"
    t.integer  "working_days"
    t.string   "pupil_identifier"
    t.integer  "pupil_avatar_id"
    t.boolean  "demo",                   default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "administrator",          default: false
    t.string   "specified_class"
    t.integer  "fun_quiz_lesson_id"
    t.integer  "fun_quiz_attempt_id"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["fun_quiz_attempt_id"], name: "index_users_on_fun_quiz_attempt_id", using: :btree
    t.index ["hash_id"], name: "index_users_on_hash_id", using: :btree
    t.index ["pupil_avatar_id"], name: "index_users_on_pupil_avatar_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["school_id", "pupil_identifier"], name: "index_users_on_school_id_and_pupil_identifier", unique: true, using: :btree
    t.index ["study_year_id"], name: "index_users_on_study_year_id", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

end
