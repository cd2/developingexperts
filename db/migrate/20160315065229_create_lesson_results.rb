class CreateLessonResults < ActiveRecord::Migration[5.0]
  def change
    create_table :lesson_results do |t|

      t.references :pupil
      t.references :lesson
      
      t.decimal :elapsed_time
      t.integer :assessed_score

      t.text :teacher_comments
      t.boolean :absent, default: false
      t.integer :in_class_mark

      t.timestamps
    end
    add_index :lesson_results, [:pupil_id, :lesson_id], unique: true
  end
end
