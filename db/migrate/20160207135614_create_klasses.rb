class CreateKlasses < ActiveRecord::Migration[5.0]
  def change
    create_table :klasses do |t|
      
      t.string :name

      t.references :school
      t.references :teacher
      t.references :course

      t.timestamps
    end
  end
end
