class ChangeColumnsToFacultyId < ActiveRecord::Migration[5.0]
  def change

    rename_column :klasses, :teacher_id, :faculty_id

  end
end
