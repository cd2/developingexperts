class CreatePupilAvatars < ActiveRecord::Migration[5.0]
  def change
    create_table :pupil_avatars do |t|

      t.references :pupil
      t.references :avatar

      t.timestamps
    end
  end
end
