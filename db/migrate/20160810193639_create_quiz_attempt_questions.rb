class CreateQuizAttemptQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_attempt_questions do |t|

      t.references :attempt

      t.integer :question_number

      t.references :question
      t.references :answer

      t.timestamps
    end
  end
end
