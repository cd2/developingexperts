class CreateLessonPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :lesson_plans do |t|
      t.integer :lesson_template_id
      t.text :resources
      t.text :sentence_structure
      t.text :activities
      t.text :assessment
      t.text :learning_outcomes

      t.text :teacher_mastery
      t.text :pupil_mastery

      t.timestamps
    end
  end
end
