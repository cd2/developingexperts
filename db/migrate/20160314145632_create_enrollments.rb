class CreateEnrollments < ActiveRecord::Migration[5.0]
  def change
    create_table :enrollments do |t|

      t.references :pupil
      t.references :klass

      t.timestamps
    end
    add_index :enrollments, [:pupil_id, :klass_id], unique: true
  end
end
