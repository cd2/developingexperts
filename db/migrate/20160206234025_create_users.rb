class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|

      t.string :name

      # Different types of user
      t.string :type

      #IDs relating to groups
      t.integer :school_id

      #Obscure UID
      t.string :hash_id

      #pupil_attr
      t.integer :age
      t.integer :gender
      t.integer :study_year_id
      t.datetime :dob

      #teacher attr
      t.integer :working_days

      #pupil specific fields
      t.string :pupil_identifier
      t.references :pupil_avatar

      #demo
      t.boolean :demo, default: false

      t.timestamps
    end
    add_index :users, [:school_id, :pupil_identifier], unique: true
  end
end
