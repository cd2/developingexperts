class CreateAvatars < ActiveRecord::Migration[5.0]
  def change
    create_table :avatars do |t|
      
      t.string :image
      t.string :category
      t.string :challenge
      t.string :progress_text
      t.string :total_score
      t.string :completed_text
      t.string :completed_color

      t.timestamps
    end
  end
end
