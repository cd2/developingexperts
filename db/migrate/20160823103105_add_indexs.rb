class AddIndexs < ActiveRecord::Migration[5.0]
  def change

    add_index :lesson_plans, :lesson_template_id
    add_index :lesson_templates, :unit_id
    add_index :lessons, :course_id
    add_index :messages, :sender_id
    add_index :messages, :recipent_id
    add_index :notifications, :school_id
    add_index :quiz_questions, :slide_id
    add_index :quiz_questions, :rocket_word_id
    add_index :resources, :lesson_template_id
    add_index :risk_assessment_apparatus, :risk_assessment_id
    add_index :risk_assessment_safeties, :risk_assessment_id
    add_index :risk_assessments, :lesson_template_id
    add_index :slides, :presentation_id
    add_index :units, :study_year_id
    add_index :users, :hash_id
    add_index :users, :study_year_id

  end

end
