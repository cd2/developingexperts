class CreateRiskAssessmentSafeties < ActiveRecord::Migration[5.0]
  def change
    create_table :risk_assessment_safeties do |t|
      t.string :procedure
      t.text :hazard
      t.string :level_of_risk
      t.string :severity
      t.string :number_affected
      t.text :safety_points
      t.integer :risk_assessment_id

      t.timestamps
    end
  end
end
