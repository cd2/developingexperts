class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :subdomain
      t.string :logo
      t.string :twitter
      t.string :facebook
      t.integer :country_id

      t.integer :course_credits

      t.timestamps
    end
  end
end
