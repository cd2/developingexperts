class CreateQuizQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_questions do |t|

      t.references :quiz

      t.integer :question_type

      t.text :question
      t.text :answer
      t.string :answer_image

      t.integer :slide_id
      t.integer :rocket_word_id

      t.timestamps
    end
  end
end
