class CreateRocketWords < ActiveRecord::Migration[5.0]
  def change
    create_table :rocket_words do |t|
      
      t.references :lesson_template

      t.string :word
      t.string :image
      t.text :description

      t.integer :legacy_id

      t.timestamps
    end
  end
end
