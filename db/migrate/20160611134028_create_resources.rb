class CreateResources < ActiveRecord::Migration[5.0]
  def change
    create_table :resources do |t|
      t.string :file
      t.string :name
      t.integer :lesson_template_id

      t.timestamps
    end
  end
end
