class CreateLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :lessons do |t|

      t.integer :course_id
      t.references :lesson_template
      t.integer :lesson_number

      t.boolean :disabled      
      t.datetime :start_date

      t.timestamps
    end
  end
end
