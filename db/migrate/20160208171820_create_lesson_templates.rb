class CreateLessonTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :lesson_templates do |t|
      t.string :machine_name

      t.text :body
      t.integer :unit_id

      t.string :song
      t.string :cover_image

      t.integer :lesson_number

      t.boolean :published, default: false

      t.timestamps
    end
  end
end
