class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|

      t.string :name
            
      t.references :school

      t.datetime :start_date

      t.timestamps
    end
  end
end
