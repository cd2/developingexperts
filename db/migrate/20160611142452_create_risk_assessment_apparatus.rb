class CreateRiskAssessmentApparatus < ActiveRecord::Migration[5.0]
  def change
    create_table :risk_assessment_apparatus do |t|
      t.string :name
      t.string :amount
      t.string :practical
      t.integer :risk_assessment_id

      t.timestamps
    end
  end
end
