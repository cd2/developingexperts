class CreatePresentations < ActiveRecord::Migration[5.0]
  def change
    create_table :presentations do |t|
      t.references :lesson_template

      t.timestamps
    end
  end
end
