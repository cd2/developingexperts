class CreateQuizQuizzes < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_quizzes do |t|

      t.references :lesson_template

      t.timestamps
    end
  end
end
