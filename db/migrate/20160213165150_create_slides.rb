class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|

      t.integer :presentation_id

      t.integer :slide_number
      t.integer :slide_type, default: 1

      t.text :upper_text
      t.text :lower_text
      t.text :on_slide_text

      t.string :background_image
      t.string :on_slide_image
      t.string :video
      t.string :narration_video

      t.integer :legacy_id

      t.boolean :class_only, default: false

      t.timestamps
    end
  end
end
