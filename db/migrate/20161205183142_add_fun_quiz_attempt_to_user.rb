class AddFunQuizAttemptToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :fun_quiz_lesson_id, :integer
    add_reference :users, :fun_quiz_attempt
  end
end
