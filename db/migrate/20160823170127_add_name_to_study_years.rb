class AddNameToStudyYears < ActiveRecord::Migration[5.0]
  def change
    add_column :study_years, :name, :string
    add_column :study_years, :year_number, :string
  end
end
