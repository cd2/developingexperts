class CreateQuizAttempts < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_attempts do |t|

      t.references :lesson_result

      t.boolean :completed, default: false

      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
