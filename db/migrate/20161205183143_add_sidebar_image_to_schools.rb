class AddSidebarImageToSchools < ActiveRecord::Migration[5.0]
  def change

    add_column :schools, :sidebar_image, :string

  end
end
