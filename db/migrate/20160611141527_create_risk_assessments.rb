class CreateRiskAssessments < ActiveRecord::Migration[5.0]
  def change
    create_table :risk_assessments do |t|
      t.integer :lesson_template_id
      t.string :practical_number
      t.string :practical_outline
      t.string :day
      t.string :date
      t.string :period
      t.string :room
      t.string :teacher
      t.string :year_group
      t.string :lesson_number
      t.boolean :class_practical
      t.boolean :fume_cupboard
      t.boolean :working_in_pairs
      t.boolean :demo
      t.boolean :safety_screen
      t.boolean :individual_assessment

      t.timestamps
    end
  end
end
