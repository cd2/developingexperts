class CreateStudyYears < ActiveRecord::Migration[5.0]
  def change
    create_table :study_years do |t|

      t.string :sidebar_background
      t.string :lesson_outro_video
      t.string :lesson_intro_mascot
      t.string :rocket_thumbnail
      t.string :sixty_count_video
      t.string :ten_count_video
      t.string :mascot_name
      t.string :certificate

      t.timestamps
    end
  end
end
