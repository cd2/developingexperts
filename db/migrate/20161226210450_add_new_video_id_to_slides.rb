class AddNewVideoIdToSlides < ActiveRecord::Migration[5.0]
  def change
    add_column :slides, :new_video_id, :string, default: ""
    add_column :slides, :video_quality_override, :string, default: ""
  end
end
