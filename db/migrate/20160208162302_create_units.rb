class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
      t.string :name
      t.string :ib
      t.integer :study_year_id

      t.timestamps
    end
  end
end
