class AddSpecClassToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :specified_class, :string
  end
end
