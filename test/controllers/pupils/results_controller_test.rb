require 'test_helper'

class Pupils::ResultsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get pupils_results_index_url
    assert_response :success
  end

  test "should get show" do
    get pupils_results_show_url
    assert_response :success
  end

end
