desc "Create schools"
task super_school: :environment do

  School.find_by(subdomain: 'demo')&.destroy!

  school = School.create!(name: 'Blakehill Primary School', subdomain: 'demo', twitter: nil, facebook: nil, logo: File.open(File.join(Rails.root, 'seeds/blakehill.jpg')), country_id: 1, course_credits: 6)

  administrator = school.administrators.create!(name: "Jane Administrator", email: 'jane@kings.com', password: 'drowssap')
  teacher = school.teachers.create!(name: 'Jon Teacher', email: 'jon@kings.com', password: 'drowssap')

  course = school.courses.new(name: "Year One Science")
  course.build_lessons_for_year StudyYear.first if StudyYear.count > 0
  course.save!

  c = school.courses.new(name: "The Super Class")
  c.lessons << Lesson.all

  klass = school.klasses.create!(name: 'Mr Man\'s Science')
  klass.course = course
  klass.update(teacher: school.teachers.first)



  pupil = school.pupils.create!(name: 'Shane Morgan', age: 24, pupil_identifier: '000000')
  100.times do |i|
    school.pupils.create!(name: Faker::Name.name, age: rand(4..8))
  end

  klass.pupils << school.pupils.order('RANDOM()').first(29)
  klass.pupils << pupil

end
