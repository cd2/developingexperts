task demo_sample_marks: :environment do

  school = School.find_by(subdomain: 'demo')
  school.pupils.each_with_index do |pupil, i|
    begin
      puts "Pupil #{i}/#{school.pupils.count} - "
      pupil.lessons.first(10).each_with_index do |lesson, i|
        puts "    #{i}/10"
        lesson_result = lesson.get_result_for pupil
        #lesson_result.in_class_mark = raemail: nd(5)

        lesson_result.attempt.start!
        questions = lesson_result.attempt.questions
        question_ids = questions.pluck(:question_id)

        questions.each do |q|
          ans = question_ids + ([q.question_id] * 2)
          q.update(answer_id: ans.shuffle.first)
        end

        lesson_result.save
      end
    rescue
    end
  end


end
