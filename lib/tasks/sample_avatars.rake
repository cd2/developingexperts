task sample_avatars: :environment do

  Avatar.destroy_all

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Attempt 10 tasks.',
    progress_text: '{{current_score}}/10 completed.',
    completed_text: 'You completed 10 tasks!',
    completed_color: '#ffc107',
    category: 'complete_tasks',
    total_score: 10
  )
  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Attempt 30 tasks.',
    progress_text: '{{current_score}}/30 completed.',
    completed_text: 'You completed 30 tasks!',
    completed_color: '#ff9800',
    category: 'complete_tasks',
    total_score: 30
  )

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Attempt 50 tasks.',
    progress_text: '{{current_score}}/50 completed.',
    completed_text: 'You completed 50 tasks!',
    completed_color: '#ff5722',
    category: 'complete_tasks',
    total_score: 50
  )


  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Complete 5 tasks with no mistakes',
    progress_text: '{{current_score}} perfected.',
    completed_text: 'You perfected 5 tasks',
    completed_color: '#8bc34a',
    category: 'perfect_tasks',
    total_score: 5
  )

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Complete 10 tasks with no mistakes',
    progress_text: '{{current_score}} perfected.',
    completed_text: 'You perfected 10 tasks',
    completed_color: '#4caf50',
    category: 'perfect_tasks',
    total_score: 10
  )

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Finish the course',
    progress_text: '{{current_score}}/{{total_score}} completed',
    completed_text: 'You perfected {{total_score}} tasks',
    completed_color: '#673ab7',
    category: 'lessons'
  )




  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Watch a presentations',
    completed_text: 'You watched a presentation!',
    completed_color: '#00bcd4',
    category: 'presentations',
    total_score: 1
  )

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Watch 5 presentations',
    progress_text: '{{current_score}}/5',
    completed_text: '5 Presentations down',
    completed_color: '#2196f3',
    category: 'presentations',
    total_score: 5
  )

  Avatar.create!(
    remote_image_url: Faker::Avatar.image,
    challenge: 'Watch 25 presentations',
    progress_text: '{{current_score}}/25',
    completed_text: 'You watched 25 Presentations!',
    completed_color: '#3f51b5',
    category: 'presentations',
    total_score: 25
  )



end
