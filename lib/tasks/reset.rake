desc "Reset and Reinitialise site"
task reset_all: :environment do
  
  Rake::Task['db:environment:set']

  Rake::Task["db:drop"].invoke
  Rake::Task["db:create"].invoke
  Rake::Task["db:migrate"].invoke
  Rake::Task["user_setup"].invoke

  STDOUT.puts "Pull from api? (Ynq)"
  input = STDIN.gets.strip
  next if input == 'q'
  Rake::Task['api_update'].invoke unless input == 'n'
  
  STDOUT.puts "Sample School? (Ynq)"
  input = STDIN.gets.strip || 'y'
  next if input == 'q'
  Rake::Task['sample_school'].invoke unless input == 'n'

  STDOUT.puts "Sample Avatars? (Ynq)"
  input = STDIN.gets.strip || 'y'
  next if input == 'q'
  Rake::Task['sample_avatars'].invoke unless input == 'n'

end
