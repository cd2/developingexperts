desc "Import Data"
task api_update: :environment do

  require 'net/http'
  study_years
  units
  import_lessons

end

def study_years
  puts 'Fetching Study Years'
  source = "http://api.developingexperts.com/v1/study_years"
  resp = Net::HTTP.get_response(URI.parse(source))
  data = resp.body
  result = JSON.parse(data)

  result.each_with_index do |study_year, i|
    year = StudyYear.find_or_initialize_by(id: study_year["id"])

    # year.update_attributes(remote_sidebar_background_url: study_year["sidebar_background"]["url"])
    # year.update_attributes(remote_lesson_intro_mascot_url: study_year["mission_intro_mascot"]["url"])
    # year.update_attributes(remote_rocket_thumbnail_url: study_year["rocket_thumbnail"]["url"])
    # year.update_attributes(remote_certificate_url: study_year["certificate"]["url"])
    # year.update_attributes(remote_lesson_outro_video_url: study_year["mission_outro_video"]["url"])
    # year.update_attributes(remote_sixty_count_video_url: study_year["sixty_count_video"]["url"])
    # year.update_attributes(remote_ten_count_video_url: study_year["ten_count_video"]["url"])
    year.save!
    puts "#{i+1}/#{result.length} study years"
  end
end

def units
  puts 'Fetching Units'

  source = "http://api.developingexperts.com/v1/units"
  resp = Net::HTTP.get_response(URI.parse(source))
  data = resp.body
  result = JSON.parse(data)

  result.each_with_index do |u, i|

    unit = Unit.find_or_initialize_by(id: u["id"])
    unit.update(u)
    puts "#{i+1}/#{result.length} units"
  end
end

def import_lessons
  puts 'Fetching Lessons'

  source = "http://api.developingexperts.com/v1/lessons"
  resp = Net::HTTP.get_response(URI.parse(source))
  data = resp.body
  result = JSON.parse(data)
  mission_ids = result.pluck("id")
  mission_ids.each_with_index do |lesson, i|
    import_lesson lesson
  end
end



def import_lesson lesson_id
  source = "http://api.developingexperts.com/v1/lessons/#{lesson_id}"
  resp = Net::HTTP.get_response(URI.parse(source))
  data = resp.body
  result = JSON.parse(data)
  lesson_data = result["mission"]
  @lesson = LessonTemplate.find_or_create_by(machine_name: lesson_data["machine_name"])#

  lesson_params = {
      body: lesson_data["mission"],
      unit_id: lesson_data["unit_id"]
  }
  @lesson.update(lesson_params)
  # @lesson.update_attributes(remote_song_url: lesson_data["song"]["url"]) rescue nil
  # @lesson.update_attributes(remote_cover_image_url: lesson_data["cover_image"]['url']) rescue nil
  puts "Importing Lesson: #{@lesson.machine_name} - Slides"
#
  ############ PRESENTATION
#
  @presentation = Presentation.find_or_create_by!(lesson_template_id: @lesson.id)

  ############ SLIDES

  result["slides"].each_with_index do |slide_data, i|

    @slide = @presentation.slides.find_or_create_by(slide_number: slide_data["slide_number"]) do |slide|
      slide.presentation_id = slide_data["presentation_id"]
      slide.slide_number = slide_data["slide_number"]
      slide.slide_type = slide_data["slide_type"]
      slide.upper_text = slide_data["upper_text"]
      slide.lower_text = slide_data["lower_text"]
      slide.on_slide_text = slide_data["on_slide_text"]
      slide.legacy_id = slide_data["id"]
      slide.class_only = slide_data["class_only"]
    end

   # @slide.update_attributes(remote_background_image_url: slide_data["background_image"]["url"]) rescue nil
   # @slide.update_attributes(remote_on_slide_image_url: slide_data["on_slide_image"]["url"]) rescue nil
   # @slide.update_attributes(remote_video_url: slide_data["video"]["url"]) rescue nil
   # @slide.update_attributes(remote_narration_video_url: slide_data["narration_video"]["url"]) rescue nil
  print " #{@slide.slide_number}" end

  result["rocket_words"].each do |word_data|
    @word = @lesson.rocket_words.find_or_create_by!(word: word_data["word"]) do |word|
      word.description = word_data["description"]
      word.legacy_id = word_data["id"]
    end
    # @word.update_attributes(remote_image_url: word_data["image"]["url"]) rescue nil
  end

  quiz = Quiz::Quiz.find_or_create_by!(lesson_template_id: @lesson.id)
  result["formative_questions"].each do |question_data|
    quiz.questions.create!(
        question_type: "formative",
        question: question_data[1],
        answer: question_data[3],
        slide_id: Slide.find_by(legacy_id: question_data[2]).try(:id),
        rocket_word_id: RocketWord.find_by(legacy_id: question_data[5]).try(:id)
    )
  end
   lesson_plan_data = result["mission_plan"]
   lesson_plan = @lesson.create_lesson_plan(
       resources: lesson_plan_data["resources"],
       sentence_structure: lesson_plan_data["sentence_structure"],
       activities: lesson_plan_data["activities"],
       assessment: lesson_plan_data["assessment"],
       learning_outcomes: lesson_plan_data["learning_outcomes"],
       teacher_mastery: lesson_plan_data["teacher_mastery"],
       pupil_mastery: lesson_plan_data["pupil_mastery"],
   )

  if @lesson.id > 0
    result["resources"].each do |resource_data|
      @resource = @lesson.resources.create!
      @resource.update_attributes(name: resource_data["name"]) rescue nil
      # @resource.update_attributes(remote_file_url: resource_data["file"]["url"], name: resource_data["name"]) rescue nil
    end
  end
end

