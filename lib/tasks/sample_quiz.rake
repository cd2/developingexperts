task build_quizs: :environment do

  LessonTemplate.all.each do |lesson|
    unless lesson.quiz
      lesson.create_quiz!
    end
  end

end
