desc "Lesson"
task t1m14: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune wants you to look up in the sky. He asks, ‘Is it a <span class='rw_highlight'>clear</span> day?’ On a <span class='rw_highlight'>clear day</span>, the sun shines in a bright blue sky.",
      },
      7 => {
          upper_text_field: "Captain Neptune wants to know if it is a <span class='rw_highlight'>cloudy day</span>? Or a <span class='rw_highlight'>partly cloudy</span> day? This means that there are some <span class='rw_highlight'>clouds</span> but also",
          lower_text_field: "some blue sky."
      },
      8 => {upper_text_field: "Captain Neptune explains that <span class='rw_highlight'>clouds</span> are made of tiny drops of water or tiny bits of ice. <span class='rw_highlight'>Clouds</span> come in different shapes, sizes",
            lower_text_field: "and colours."
      },
      9 => {
        upper_text_field: "Captain Neptune asks, ‘Are there any <span class='rw_highlight'>clouds</span> in the sky now?’ Are they big, white and puffy? Or are they white streaks? Or dark",
        lower_text_field: "grey stripes?’"
      },
      10 => {
          upper_text_field: "‘Or is the sky covered over with a <span class='rw_highlight'>blanket of grey clouds</span> so thick that you can’t see through to the sun?’",
      },
      11 => {
          upper_text_field: "Captain Neptune explains that thick <span class='rw_highlight'>cloud</span> sometimes means that rain is coming. Here’s a <span class='rw_highlight'>blanket of cloud</span> moving across the sky.",
      },
      12 => {
          upper_text_field: "Today’s Mission Activity: Making a <span class='rw_highlight'>cloud</span> in a bottle",
          main_video_slide: ""
      },
      13 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
  }


end
