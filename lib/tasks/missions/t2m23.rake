desc "Lesson"
task t2m23: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains that today, children around the world receive <span class='rw_highlight'>vaccinations</span>. The injection may hurt a little, but it",
          lower_text_field: "helps to keep you safe from serious disease. <span class='rw_highlight'>Vaccinations</span> work in a way that might surprise you."
      },
      7 => {
          upper_text_field: "Major Saturn explains that most vaccinations stop you from getting a disease by placing a small amount of the disease germs",
          lower_text_field: "into your body. Your body fights these germs, and from then on your body protects you from catching the disease."
      },
      8 => {
          upper_text_field: "Major Saturn explains that over two hundred years ago, before anyone knew about <span class='rw_highlight'>vaccinations</span>, many people died of a disease",
          lower_text_field: "called smallpox. They would get terrible sores on their bodies, and those who survived would end up blind, weak and scarred."
      },
      9 => {
          upper_text_field: "Those who survived knew they would never catch the disease again – just the way you know that after you have had chicken",
          lower_text_field: "pox, you will never catch it again."
      },
      10 => {
          upper_text_field: "Doctors in those days started thinking that maybe, just maybe, they could figure out a way to make people catch a mild case of",
          lower_text_field: "smallpox. Then they might get a little poorly, but they would never catch smallpox again."
      },
      11 => {
          upper_text_field: "So doctors tried taking a little fluid from a sick person’s smallpox sore and putting it into a cut on a well person’s arm. Sometimes",
          lower_text_field: "this worked. The person would get a little sick but soon feel better, and then he would never catch smallpox again."
      },
      12 => {
          upper_text_field: "Sometimes the people got very sick, and even died. Major Saturn learns in those cases, it seemed as if the doctor was actually",
          lower_text_field: "spreading the smallpox disease. This problem concerned <span class='rw_highlight'>Edward Jenner</span>, an English doctor."
      },
      13 => {
          upper_text_field: "One day he met a woman who was working on a dairy farm. He noticed that her hands were covered with the marks of old sores",
          lower_text_field: "from a previous <span class='rw_highlight'>infection</span>. ‘Have you ever had smallpox?’ he asked. She replied, ‘I can’t get smallpox. I’ve already had <span class='rw_highlight'>cowpox</span>.’"
      },
      14 => {
          upper_text_field: "Everybody knows you never get smallpox after you’ve had <Span class='rw_highlight'>cowpox</span>.’ Major Saturn explains that this made <span class='rw_highlight'>Jenner</span> think very",
          lower_text_field: "carefully. He had seen cattle with the disease <span class='rw_highlight'>cowpox</span>. The cows got sores on their body, just as humans with smallpox did."
      },
      15 => {
          upper_text_field: "Major Saturn explains that <span class='rw_highlight'>Jenner</span> had seen people with smallpox, too. They got a little poorly, with a few sores, but then they got",
          lower_text_field: "better. <span class='rw_highlight'>Jenner</span> started to study and think that it might be true: by catching <span class='rw_highlight'>cowpox</span>, a person could be kept from catching smallpox."
      },
      16 => {
          upper_text_field: "Finally <span class='rw_highlight'>Jenner</span> was ready to try an experiment. Major Saturn learns that on the 14th May 1796, he took fluid from a <Span class='rw_highlight'>cowpox</span> sore",
          lower_text_field: "and put it into a cut on the arm of and eight-year-old boy named James Phipps. James caught <span class='rw_highlight'>cowpox</span>."
      },
      17 => {
          upper_text_field: "Major Saturn explains that he came down with a fever, headaches and sores, but he soon got better. Now came the important and",
          lower_text_field: "risky next step: on the 1st July, <span class='rw_highlight'>Jenner</span> <span class='rw_highlight'>infected</span> the boy with fluid from a smallpox sore."
      },
      18 => {
          upper_text_field: "Major Saturn explains that for days and weeks, Jenner watched and waited. James stayed healthy. The <span class='rw_highlight'>cowpox</span> kept him from",
          lower_text_field: "catching smallpox. <span class='rw_highlight'>Jenner</span> told other doctors about his discovery, but at first they didn’t believe him."
      },
      19 => {
          upper_text_field: "But <span class='rw_highlight'>Jenner</span> believed in his idea, and kept giving people <span class='rw_highlight'>cowpox</span>. Major Saturn explains that he believed so much, he even gave his",
          lower_text_field: "own baby son the <span class='rw_highlight'>cowpox</span> germs. Finally he wrote a book explaining his discovery."
      },
      20 => {
          upper_text_field: "He called it ‘<Span class='rw_highlight'>accination</span>’, from the Latin word vacca for ‘cow’. Major Saturn explains that as <span class='rw_highlight'>Jenner</span> vaccinated more people",
          lower_text_field: "successfully, his work became well known."
      },
      21 => {
          upper_text_field: "Major Saturn learns that King George IV asked to be <Span class='rw_highlight'>vaccinated</span> by him and also made <span class='rw_highlight'>Jenner</span> his Physician Extraordinary. Soon it",
          lower_text_field: "became the fashion to be injected by <span class='rw_highlight'>Jenner</span>!"
      },
      22 => {
          upper_text_field: "Major Saturn learns that other scientists learned from <span class='rw_highlight'>Edward Jenner’s</span> ideas, and they worked to make <span class='rw_highlight'>vaccinations</span> even safer",
          lower_text_field: "and more reliable. Today <span class='rw_highlight'>vaccinations</span> protect us against many serious diseases, such as polio, tuberculosis and <span class='rw_highlight'>measles</span>."
      },
      23 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      24 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      25 => {complete_investigation_slide: ""},
      26 => {rocket_word_slide: ""},
      27 => {quiz_slide: ""},
      28 => {outro_slide: ""}
}

  $rocket_words = {
          "Vaccination" => {
              description: "Substance that is injected into a person or animal to protect against a particular disease.",
              image: "" },
          "Edward Jenner" => {
              description: "An English doctor and scientist who created the smallpox vaccine.",
              image: "" },
          "Cowpox" => {
              description: "Mild skin disease, that can be caught by people when milking an infected cow.",
              image: "" },
          "Infection" => {
              description: "When bacteria attacks your body.",
              image: "" },
          "Measles" => {
              description: "Viral infection, which causes illness. One of the signs of <span class='rw_highlight'>measles</span> is a skin rash.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>vaccination</span>?" => {answer: "Substance that is injected into a person or animal to protect against a particular disease.",
                           image: "",
                           slide_id: 7},
      "Who was <span class='rw_highlight'>Edward Jenner</span>?" => {answer: "An English doctor and scientist who created the smallpox vaccine.",
                                image: "",
                                slide_id: 12},
      "What is <span class='rw_highlight'>cowpox</span>?" => {answer: "Mild skin disease, that can be caught by people when milking an infected cow.",
                           image: "",
                           slide_id: 13},
      "What is an <span class='rw_highlight'>infection</span>?" => {answer: "When bacteria attacks your body.",
                              image: "",
                              slide_id: 13},
      "What are <span class='rw_highlight'>measles</span>?" => {answer: "Viral infection, which causes illness. One of the signs of <span class='rw_highlight'>measles</span> is a skin rash.",
                              image: "",
                              slide_id: 22},
  }

end
