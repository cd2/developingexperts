desc "Lesson"
task t1m19: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune asks if you have ever started digging a hole and wondered how far you could go? What would it be like to dig",
          lower_text_field: "deep down into the <span class= 'rw_highlight'>earth</span>?"
      },
      7 => {
          upper_text_field: "Captain Neptune explains that <span class= 'rw_highlight'>scientists</span>, have used special <Span class= 'rw_highlight'>equipment</span> to drill deep down into the <span class= 'rw_highlight'>earth</span>. They have found out",
          lower_text_field: "that the <span class= 'rw_highlight'>earth</span> has many layers."
      },
      8 => {upper_text_field: "Captain Neptune explains that if you could slice right through the <span class= 'rw_highlight'>earth</span>, you would see the different layers, all the way to the",
            lower_text_field: "centre, like the picture."
      },
      9 => {
          upper_text_field: "Captain Neptune explains that the outermost layer is called the <span class= 'rw_highlight'>crust</span> which is a bit like the peel on an orange. That’s the",
          lower_text_field: "surface of the <span class= 'rw_highlight'>Earth</span>, the part we live on."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that mountains and valleys, rivers and deserts, oceans and continents make up the <span class= 'rw_highlight'>Earth’s crust</span>. The",
          lower_text_field: "<span class= 'rw_highlight'>crust</span> is a thin outer coating compared to the other layers inside."
      },
      11 => {
          upper_text_field: "Captain Neptune explains that beneath the <span class= 'rw_highlight'>crust</span> lies a thick layer of hot, melted rock called a <span class= 'rw_highlight'>mantle</span>. At the centre of the <span class= 'rw_highlight'>Earth</span>",
          lower_text_field: "burns a core of hot, melted rock."
      },
      12 => {
          upper_text_field: "Captain Neptune explains that no one has ever travelled through the <span class= 'rw_highlight'>Earth’s crust</span>. No one would ever want to, because it is so hot.",
          lower_text_field: "Hotter than you could ever imagine!"
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment Option 1: Read the handout to make a cookie which is decorated to show the layers of the <span class= 'rw_highlight'>Earth</span>.",
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment Option 2: Using plasticine to create a model of the <span class= 'rw_highlight'>Earth’s</span> layers.",
      },
      15 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }



end
