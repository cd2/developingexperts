desc "Lesson"
task t4m1: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {course_intro_slide: ""},
      3 => {ten_count_launch: ""},
      4 => {rocket_word_slide: ""},
      5 => {
          upper_text_field: "Commander Mars explains, how a simple <span class='rw_highlight'>electric circuit</span> allows a light <span class='rw_highlight'>bulb</span> to light. It’s easy, an <span class='rw_highlight'> electric circuit</span> is a route",
          lower_text_field: "through which electricity or an <span class='rw_highlight'>electrical current</span> can flow. All <span class='rw_highlight'>electrical appliances</span> are powered by <span class='rw_highlight'>electric circuits</span>."
      },
      6 => {
          upper_text_field: "Commander Mars explains, electricity flows from the negative side of a <span class='rw_highlight'>battery</span> to conducting material on the base of the <span class='rw_highlight'>bulb</span>.",
          lower_text_field: "The electricity flows up a wire that is inside the <span class='rw_highlight'>bulb</span> and across the filament, which is the part of the <span class='rw_highlight'>bulb</span> that actually lights."
      },
      7 => {
          upper_text_field: "Commander Mars discovers that when the electricity passes through the filament, some of the electrical energy is changed to",
          lower_text_field: "heat and light energy. The electricity continues down another small wire inside the <span class='rw_highlight'>bulb</span>, to another wire at the base of the <span class='rw_highlight'>bulb</span>."
      },
      8 => {
          upper_text_field: "What is happening in the picture? Explain it to your talk partner.",
      },
      9 => {
          upper_text_field: "Commander Mars explains that the electricity finally makes its way to the positive side of the <span class='rw_highlight'>battery</span>.",
      },
      10 => {
          upper_text_field: "If the electricity did not travel to the positive side of the <span class='rw_highlight'>battery</span> there would not be a complete circuit and the <span class='rw_highlight'>bulb</span> would not light.",
      },
      11 => {
          upper_text_field: "Commander Mars finds out that when there is an unbroken path on which electrons flow, as in this example, it is called a",
          lower_text_field: "complete circuit. The unbroken path that the electrons follow is called a <span class='rw_highlight'>closed circuit</span>."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film. Make a lemon powered <span class='rw_highlight'>electrical circuit</span>.",
          main_video_slide: ""
      },
      13 => {
          upper_text_field: "Writing up your experiment: Complete the investigation sheet.",
          lower_text_field: "Now explain to your talk partners why these <span class='rw_highlight'>electrical appliances</span> need <span class='rw_highlight'>electricity to work</span>. Mission Timer: 2 Minutes"
      },
      14 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s Mission Assignment. Where is Commander Mars?",
          sixty_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
