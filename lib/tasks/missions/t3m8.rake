desc "Lesson"
task t3m8: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains the <span class='rw_highlight'>cycle of the seasons</span>. Nature seems to wake up and come alive after the cold winter. In spring the seeds of plants begin to",
          lower_text_field: "sprout as the earth grows warmer. A <span class='rw_highlight'>sunflower</span> seedling will become a new <span class='rw_highlight'>sunflower</span> plant."
      },
      7 => {
          upper_text_field: "Colonel Jupiter describes how plants send roots down from the seed into the warm soil, kept healthy by worms. Soon little green",
          lower_text_field: "plant shoots appear. Colonel Jupiter explains how maple and oak trees are bare and leafless during winter."
      },
      8 => {
          upper_text_field: "They then begin to send <span class='rw_highlight'>sap</span> up to their branches to help new leaves sprout and grow. <span class='rw_highlight'>Sap</span> is a sugary liquid that carries",
          lower_text_field: "nutrients. You can eat the <span class='rw_highlight'>sap</span> from some maple trees – it’s where maple syrup comes from. Colonel Jupiter uses it on his pancakes!"
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains how in spring, many baby animals are born or become active. Squirrels scurry about, and young",
          lower_text_field: "badgers, born during the winter, now have grown so that they can see, hear and start exploring outside their <span class='rw_highlight'>badger sett</span>."
      },
      10 => {
          upper_text_field: "Colonel Jupiter describes how birds that have flown south for the winter, now return and build nests where they will lay their",
          lower_text_field: "eggs. Why do you think they fly south?"
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains how other birds like swans also lay eggs and their babies – called <span class='rw_highlight'>cygnets</span> – hatch. Insect eggs that lay",
          lower_text_field: "quietly all winter now begin to hatch. From some eggs, out come tiny grasshoppers that feed on the just-budding leaves of plants."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment:",
      },
      13 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

  $rocket_words = {
          "Cycle of the Seasons" => {
              description: "A group of events that happen in a order, <b>one after the other seasonally then repeated</b>.",
              image: "" },
          "Sap" => {
              description: "A <b>sugary liquid</b> that carries nutrients.",
              image: "" },
          "Sunflower" => {
              description: "A <b>plant</b> with a <b>very tall stem</b> and a single large, round, flat, <b>yellow flower</b> with petals.",
              image: "" },
          "Badger sett" => {
              description: "<b>Where the badger lives.</b> It usually has a network of tunnels and entrances.",
              image: "" },
          "Cygnet" => {
              description: "A <b>young swan</b>.",
              image: "" },
  }

  $questions = {
      "What is the <span class='rw_highlight'>cycle of the seasons</span>?" => {answer: "A group of events that happen in a order, one after the other seasonally then repeated.",
                           image: "",
                           slide_id: 6},
      "What is <span class='rw_highlight'>sap</span>?" => {answer: "A sugary liquid that carries nutrients.",
                                image: "",
                                slide_id: 8},
      "What is a <span class='rw_highlight'>sunflower</span>?" => {answer: "A plant with a very tall stem and a single large, round, flat, yellow flower with petals.",
                           image: "",
                           slide_id: 6},
      "What is a <span class='rw_highlight'>badger sett</span>?" => {answer: "Is where the badger lives which usually has a network of tunnels and entrances.",
                              image: "",
                              slide_id: 9},
      "What is a <span class='rw_highlight'>cygnet</span>?" => {answer: "A young swan.",
                              image: "",
                              slide_id: 11},
  }

end
