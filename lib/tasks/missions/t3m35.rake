desc "Lesson"
task t3m35: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }  

  $rocket_words = {
          "Mirror" => {
              description: "A piece of glass or plastic that reflects images.",
              image: "" },
          "Concave" => {
              description: "Having a shape like the inside of a bowl: curving inward.",
              image: "" },
          "Convex" => {
              description: "Having a shape like the outside of a bowl: curving outward.",
              image: "" },
          "Reflection" => {
              description: "An image that is seen in a mirror or on a shiny surface.",
              image: "" },
          "Telescope" => {
              description: "A device shaped like a long tube that you look through to see things that are far away.",
              image: "" },
  }



  end