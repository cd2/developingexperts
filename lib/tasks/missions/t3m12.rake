desc "Lesson"
task t3m12: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter wonders if you know the word ‘cycle’, or a word with ‘cycle’ in it? How about words like ‘bicycle’ or ‘tricycle’?",
          lower_text_field: "Think about a bicycle’s wheel - can you tell where it begins or ends?"
      },
      7 => {
          upper_text_field: "You can’t really find a beginning or end, can you? It’s a cycle that goes round and round. Colonel Jupiter explains that’s the",
          lower_text_field: "way it is with cycles in nature, too."
      },
      8 => {
          upper_text_field: "In nature, all living things are part of the cycle of life, a process that keeps going around and around. Colonel Jupiter explains",
          lower_text_field: "that all living things are <span class='rw_highlight'>born</span>, <span class='rw_highlight'>grow</span> and eventually <span class='rw_highlight'>die</span>."
      },
      9 => {
          upper_text_field: "To keep life going, living things need to <span class='rw_highlight'>reproduce</span>, which means to make young like themselves. Colonel Jupiter wants you to",
          lower_text_field: "imagine a farmyard with lots of <span class='rw_highlight'>chickens</span>. A <span class='rw_highlight'>chicken</span> lays an egg. Out of the egg hatches a little baby chick."
      },
      10 => {
          upper_text_field: "The chick grows up to be a hen. The hen mates with a cockerel, then soon the hen lays an egg. Out of the egg hatches a chick.",
          lower_text_field: "That chick grows up and the cycle continues."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that it’s a cycle – with no beginning or end – that keeps going around and around. The cycle of life has",
          lower_text_field: "four parts: <span class='rw_highlight'>birth</span>, <span class='rw_highlight'>growth</span>, <span class='rw_highlight'>reproduction</span>, and <span class='rw_highlight'>death</span>."
      },
      12 => {
          upper_text_field: "Here’s an old question that no one has ever answered. ‘Which came first, the <span class='rw_highlight'>chicken</span> or the egg?’ You can’t tell.",
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: Create your own cycle of life poster, using the worksheet provided.",
      },
      14 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
