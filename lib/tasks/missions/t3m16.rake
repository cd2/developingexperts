desc "Lesson"
task t3m16: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter reminds you that you’ve been learning about different cycles in nature, such as the cycle of the seasons and",
          lower_text_field: "the life cycles of plants and animals. Well, here’s another cycle. It’s called the <span class='rw_highlight'>water cycle</span> and it has a lot to do with the weather."
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains that water can exist in three states of matter: as a <span class='rw_highlight'>solid</span>, <span class='rw_highlight'>liquid</span>, or <span class='rw_highlight'>gas</span>. The water you drink is a <span class='rw_highlight'>liquid</span>.",
      },
      8 => {
          upper_text_field: "Colonel Jupiter expects that you already know what we call water when it’s a <span class='rw_highlight'>solid</span>, yes ice. Then when you boil water, it",
          lower_text_field: "turns into a <span class='rw_highlight'>gas</span> called <span class='rw_highlight'>steam</span> or water vapour."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that water vapour is a <span class='rw_highlight'>gas</span> in the air all around you. As you go on to learn about the <span class='rw_highlight'>water cycle</span> in your",
          lower_text_field: "next mission, keep in mind that ice, water and water vapour are all water, just in different states of matter."
      },
      10 => {
          upper_text_field: "So always remember, water comes in three different forms either: a <span class='rw_highlight'>solid</span>, a <span class='rw_highlight'>liquid</span> or a <span class='rw_highlight'>gas</span>. If you fill a plastic beaker with",
          lower_text_field: "water then place it in the freezer, the <span class='rw_highlight'>liquid</span> will become solid ice. If you then boil the ice, it will then become <span class='rw_highlight'>steam</span> (<span class='rw_highlight'>gas</span>)."
      },
      11 => {
          upper_text_field: "Watch this short film which explains the difference between a <span class='rw_highlight'>liquid</span>, <span class='rw_highlight'>solid</span> and <span class='rw_highlight'>gas</span>.",
          main_video_slide: ""
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment 1:",
          main_video_slide: ""
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment 2:",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
