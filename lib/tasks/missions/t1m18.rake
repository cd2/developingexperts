desc "Lesson"
task t1m18: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune wants you to take a closer look at the third planet from the Sun: our home, the Earth. You already know",
          lower_text_field: "some things about the Earth. You know that its surface is covered with land and water."
      },
      7 => {
          upper_text_field: "Captain Neptune wants to know what the biggest chunks of land are called? Yes, <span class= 'rw_highlight'>continents</span>. And the biggest bodies of water?",
          lower_text_field: "Yes, the <span class= 'rw_highlight'>oceans</span>. Imagine drawing a line around the middle of the Earth, as though you were putting a big belt around it."
      },
      8 => {upper_text_field: "Captain Neptune explains that the imaginary line is called the <span class= 'rw_highlight'>equator</span>. Many world maps and globes have this imaginary line",
            lower_text_field: "drawn on them; see if you can find it."
      },
      9 => {
        upper_text_field: "Captain Neptune explains that the sun always shines strongly at the <span class= 'rw_highlight'>equator</span>, so the land and <span class= 'rw_highlight'>oceans</span> near the <span class= 'rw_highlight'>equator</span> stay warm",
        lower_text_field: "all year round. Lots of rainforests are located near the <span class= 'rw_highlight'>equator</span>. It is hot and it rains a lot in the forest."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that the top and the bottom of the Earth are called poles. What’s on top? The <span class= 'rw_highlight'>North Pole</span>,",
          lower_text_field: "where polar bears live. At the bottom of the Earth is the <span class= 'rw_highlight'>South Pole</span>."
      },
      11 => {
          upper_text_field: "Captain Neptune explains that the sun never shines as strongly at the poles as it does at the <span class= 'rw_highlight'>equator</span>. In fact, the water at the <span class= 'rw_highlight'>North</span>",
          lower_text_field: "and <span class= 'rw_highlight'>South Poles</span> stays frozen all year round."
      },
      12 => {
          upper_text_field: "Captain Neptune explains that if you look at the globe, you might think that people living near the <span class= 'rw_highlight'>South Pole</span> must feel as if they",
          lower_text_field: "were standing upside down."
      },
      13 => {
          upper_text_field: "Captain Neptune explains that in fact, standing up near the <span class= 'rw_highlight'>South Pole</span> feels just like standing up in your own neighbourhood – feet",
          lower_text_field: "down, head up, only colder!"
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: <span class= 'rw_highlight'>Ocean</span> in a bottle.",
          main_video_slide: ""
      },
      15 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }



end
