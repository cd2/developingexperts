desc "Lesson"
task t3m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter wants you to think of some places on the Earth where you can find water. Did you think of rivers and most of all,",
          lower_text_field: "oceans and seas? There’s a lot of water on this planet: almost two-thirds of the surface of the Earth is covered with water!"
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains that the water in rivers, lakes and oceans is liquid. But every day, some of this liquid turns to gas.",
          lower_text_field: "Every day, as the sun shines down, some of the water <span class='rw_highlight'>evaporates</span>: this means it turns into <span class='rw_highlight'>water vapour</span> (gas) and mixes with the air."
      },
      8 => {
          upper_text_field: "Colonel Jupiter wants to know if you recognise the word ‘<span class='rw_highlight'>vapour</span>’ inside the word ‘<span class='rw_highlight'>evaporation</span>’? Here’s a question to think about.",
          lower_text_field: "Where do you think most of the <span class='rw_highlight'>water vapour</span> in the air comes from? It comes from oceans and the seas."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that there’s <span class='rw_highlight'>water vapour</span> in the air around you. Try this. Put a few centimetres of water in a glass.",
          lower_text_field: "Mark a line where the water comes to. Then put the glass where it won’t be disturbed. Check the amount of water daily."
      },
      10 => {
          upper_text_field: "What has happened to the water? It has <span class='rw_highlight'>evaporated</span>. It has turned into <span class='rw_highlight'>water vapour</span> and become part of the air around you.",
          lower_text_field: "Maybe you are breathing it in right now!"
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that at different times, there are different amounts of <span class='rw_highlight'>water vapour</span> in the air. When we talk about",
          lower_text_field: "the amount of <span class='rw_highlight'>water vapour</span> in the air, we talk about <span class='rw_highlight'>humidity</span>. A day with a lot of <span class='rw_highlight'>moisture</span> in the air has ‘high <span class='rw_highlight'>humidity’</span>."
      },
      12 => {
          upper_text_field: "Colonel Jupiter explains that a day with very little water in the air has ‘low <span class='rw_highlight'>humidity</span>’. On a hot, summer day, have you ever heard",
          lower_text_field: "someone complain: ‘It’s not the heat, it’s the <span class='rw_highlight'>humidity</span>’?"
      },
      13 => {
          upper_text_field: "This means that on a hot day there is a lack of <span class='rw_highlight'>water vapour</span> in the air which leaves you feeling hot and uncomfortable.",
          lower_text_field: "When it rains hard, puddles of water form on the ground."
      },
      14 => {
          upper_text_field: "Colonel Jupiter asks, ‘What happens to the puddles when it stops raining and the sun starts to shine’? Slowly, they get smaller and",
          lower_text_field: "then they go away. Where does the water go? Well, some of it <span class='rw_highlight'>evaporates</span>."
      },
      15 => {
          upper_text_field: "Colonel Jupiter explains that it turns to <span class='rw_highlight'>water vapour</span> and goes up into the air, just as steam rises from a pan of water that you",
          lower_text_field: "heat on a stove. But in nature, it’s the sun that heats the water and turns it into vapour."
      },
      16 => {
          upper_text_field: "Colonel Jupiter explains that the water that doesn’t go up into the air soaks down into the earth. It becomes <span class='rw_highlight'>groundwater</span> which",
          lower_text_field: "is the name for water found under the ground. When people drill a well, they are drilling down to find <span class='rw_highlight'>groundwater</span>."
      },
      17 => {
          upper_text_field: "Colonel Jupiter explains that once they dig deep enough to find this underground water, they can put long pipes into the hole",
          lower_text_field: "and then pump the water up to use in their homes, schools and other places."
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Following the instructions on your handout model the water cycle process.",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
