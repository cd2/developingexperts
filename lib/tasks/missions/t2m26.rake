desc "Lesson"
task t2m26: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants you to imagine you are taking a rocket ship Journey. You pass the Earth, you notice a shape like a ball circling",
          lower_text_field: "around it. What is it? It’s something you know very well: the <span>Moon</span>. The <span>moon</span> that you see in the sky travels around the Earth."
      },
      7 => {
          upper_text_field: "Major Saturn explains that the <span>Moon</span> orbits the Earth, just as the Earth and the other eight planets orbit the Sun. Have you noticed",
          lower_text_field: "that the <psan>Moon</span> seems to change shape? Sometimes it’s a full <span>moon</span>, with a big round face."
      },
      8 => {
          upper_text_field: "Sometimes it’s a <Span>half-moon</span>, shaped like half of a pizza. Sometimes it’s only a thin <span>crescent</span>. Some days, the <span>moon</span>",
          lower_text_field: "crosses the sky near the sun. We can’t see it because the half facing us is not as bright as the sun."
      },
      9 => {
          upper_text_field: "Major Saturn explains that when this happens we call it a new <Span>moon</span>. We call these different shapes the phases of the <span>moon</span>.",
          lower_text_field: "The <span>moon</span> only appears to change shape. It’s always a big round ball."
      },
      10 => {
          upper_text_field: "Sometimes we see the whole ball, and sometimes only see part of it. We see the part that is lit by the sun. Major Saturn asks, ‘Do",
          lower_text_field: "you know that the moon itself does not shine?’ When the <span>moon</span> seems to shine, it’s only reflecting light from the sun."
      },
      11 => {
          upper_text_field: "Major Saturn notices that sometimes when the <span>moon</span> is bright and big in the sky, it seems as if a face is looking down at you.",
          lower_text_field: "People even jokingly refer to ‘the man in the <span>moon</span>’."
      },
      12 => {
          upper_text_field: "However, there are no people on the <span>moon</span> – no animals or plants no trees, grass or clouds. There is no water or air. <span>Astronauts</span> like",
          lower_text_field: "Buzz Aldrin who visited the <span>moon</span> had to wear space suits to give them air to breathe."
      },
      13 => {
          upper_text_field: "Major Saturn explains that the <Span>moon</span> may look lovely but it’s not a friendly place to visit. So far, <span>astronauts</span> have visited the <span>moon</span>,",
          lower_text_field: "but no <span>astronaut</span> has landed on another planet. Not yet, at least. Who knows what may happen when you grow up?"
      },
      14 => {
          upper_text_field: "Perhaps by then <span>astronauts</span> will have made a trip to Mars. Maybe you will be one of them! How exciting!",
      },
      15 => {
          upper_text_field: "Today’s Film: The different phases of the <span>moon</span>.",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: How to make the moon!",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
