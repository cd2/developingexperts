desc "Lesson"
task t5m21: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that scientists are always trying to discover new things. This is difficult to do. Firstly, you have to",
          lower_text_field: "make a discovery in the first place. Secondly, you have to <span class='rw_highlight'>persuade</span> people to agree with your ideas."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains that <span class='rw_highlight'>Galileo Galilei</span> was a man who made many discoveries but had trouble <span class='rw_highlight'>persuading</span> people that",
          lower_text_field: "he was right. He was born in Pisa, Italy in 1564 and became a professor of Mathematics there."
      },
      8 => {
          upper_text_field: "The town is famous for its beautiful leaning tower, which was where <span class='rw_highlight'>Galileo</span> conducted one of his experiments. Brigadier",
          lower_text_field: "Venus explains that experiments help scientists to <span class='rw_highlight'>persuade</span> other people of the truth of their ideas."
      },
      9 => {
          upper_text_field: "In an experiment you try something out and predict the result. Better still, you can repeat the test and get other people to try it",
          lower_text_field: "out for themselves, with the same results. Try this. Does a heavy object fall faster than a lighter object?"
      },
      10 => {
          upper_text_field: "Brigadier Venus explains that <span class='rw_highlight'>Galileo</span> climbed the tower of Pisa and dropped a heavy object and a light object from the top.",
          lower_text_field: "He dropped the weights at the same time and they hit the ground at the same time."
      },
      11 => {
          upper_text_field: "Was that what you expected? Brigadier Venus explains that <span class='rw_highlight'>Galileo</span> knew some people with influence. They helped him get",
          lower_text_field: "a job in Padua in Italy, where he learnt about the telescope."
      },
      12 => {
          upper_text_field: "He designed his own telescope that was better than those traditionally used, and he could even see mountains on the face",
          lower_text_field: "of the Moon and the moons of Jupiter."
      },
      13 => {
          upper_text_field: "Seeing that the moons were circling another planet convinced <span class='rw_highlight'>Galileo</span> to believe Copernicus’s theory: the Earth was not at",
          lower_text_field: "the centre of the universe. Brigadier Venus explains that <span class='rw_highlight'>Galileo</span> had a lot of enemies who campaigned against his work."
      },
      14 => {
          upper_text_field: "One of his friends, Pope Urban VIII was head of the Catholic church, in 1623. The Pope encouraged <span class='rw_highlight'>Galileo</span> to discuss",
          lower_text_field: "<span class='rw_highlight'>Copernicus’</span> theory, so long as he remembered to call his work a ‘<span class='rw_highlight'>hypothesis</span>’, meaning other explanations were still possible."
      },
      15 => {
          upper_text_field: "This worked nicely until 1630, when <span class='rw_highlight'>Galileo</span> wrote a book stating that the Sun was at the centre of the solar system and that the",
          lower_text_field: "Earth orbited it."
      },
      16 => {
          upper_text_field: "The Pope then turned on <span class='rw_highlight'>Galileo</span> and he was made to say that <span class='rw_highlight'>Copernicus</span>’ theory was wrong (although it wasn’t).",
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment 1",
          main_video_slide: ""
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment 2",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "",
          sixty_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}


  $rocket_words = {
          "Galileo Galilei" => {
              description: "An Italian astronomer, physicist, engineer, and mathematician.",
              image: "" },
          "Tower of Pisa" => {
              description: "A bell tower in Pisa, Italy which leans unintentionally.",
              image: "" },
          "Nicolaus Copernicus" => {
              description: "An astronomer who identified that the Sun is at the centre of our universe.",
              image: "" },
          "Hypothesis" => {
              description: "An idea or theory that is not proven.",
              image: "" },
          "Persuade" => {
              description: "To cause (someone) to do something by asking, arguing, or giving reasons.",
              image: "" },
  }

  $questions = {
      "Who was <span class='rw_highlight'>Galileo Galilei</span>?" => {answer: "An Italian astronomer, physicist, engineer, and mathematician.",
                           image: "",
                           slide_id: 7},
      "What is the <span class='rw_highlight'>Tower of Pisa</span>?" => {answer: "A bell tower in Pisa, Italy which leans unintentionally.",
                                image: "",
                                slide_id: 10},
      "Who was <span class='rw_highlight'>Nicolaus Copernicus</span>?" => {answer: "An astronomer who identified that the Sun is at the centre of our universe.",
                           image: "",
                           slide_id: 13},
      "What is an <span class='rw_highlight'>hypothesis</span>?" => {answer: "An idea or theory that is not proven.",
                              image: "",
                              slide_id: 14},
      "What does <span class='rw_highlight'>persuade</span> mean?" => {answer: "To cause (someone) to do something by asking, arguing, or giving reasons.",
                              image: "",
                              slide_id: 8},
  }

end
