desc "Lesson"
task t6m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      18 => {
          upper_text_field: "",
          sixty_count: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}

end
