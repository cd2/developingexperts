desc "Lesson"
task t2m19: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains that your blood carries good things from the food you eat to all parts of your body. But how did your food",
          lower_text_field: "get into your blood?"
      },
      7 => {
          upper_text_field: "It got there because your body <Span class='rw_highlight'>digested</span> it. Whatever you eat – an apple, slice of bread, a slice of ham, a carrot, a piece of cake,",
          lower_text_field: "a glass of milk or sausage roll – your body has to <span class='rw_highlight'>digest</span> it. Here is how that happens."
      },
      8 => {
          upper_text_field: "Major Saturn explains that when you put a piece of food in your mouth, you chew it with your teeth, which breaks the food into",
          lower_text_field: "little pieces. There’s a watery fluid in your mouth called <span class='rw_highlight'>saliva</span> that also helps break down the food into smaller soft pieces."
      },
      9 => {
          upper_text_field: "When you <span class='rw_highlight'>swallow</span>, the food goes down a tube into your <span class='rw_highlight'>stomach</span>. When you drink something, you <span class='rw_highlight'>swallow</span>, and the liquid you’re",
          lower_text_field: "drinking goes down the same tube to your <span class='rw_highlight'>stomach</span>."
      },
      10 => {
          upper_text_field: "Major Saturn wants to know if you ever heard your <span class='rw_highlight'>stomach</span> make some squishy, <span class='rw_highlight'>gurgling</span> noises after you eat? These noises",
          lower_text_field: "show that your <span class='rw_highlight'>stomach</span> and other body parts are continuing the process of <span class='rw_highlight'>digesting</span> your food."
      },
      11 => {
          upper_text_field: "Major Saturn explains that once your food is <span class='rw_highlight'>digested</span>, it’s broken into very tiny pieces. The most valuable parts go into your",
          lower_text_field: "blood and give you the energy you need to do all the things you like to do."
      },
      12 => {
          upper_text_field: "Major Saturn explains that your body cannot use some parts of the food you eat; you get rid of these when you go to the toilet.",
          lower_text_field: "Your <span class='rw_highlight'>digestive system</span> breaks your food down so your body can use it."
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}
end
