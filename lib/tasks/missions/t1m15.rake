desc "Lesson"
task t1m15: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that he is going to ask you the same question in two different ways. Here’s the first way: ‘What’s it",
          lower_text_field: "like outside?’ Here’s the second way: ‘What’s the weather like?’"
      },
      7 => {
          upper_text_field: "The weather is what it is like outside. Did you think about the weather today? Maybe you did without even realising it. What",
          lower_text_field: "kind of clothes did you wear today? The weather had a lot to do with your choice."
      },
      8 => {upper_text_field: "Captain Neptune explains that no matter where people live they talk about the weather. It’s something everybody shares. When",
            lower_text_field: "it’s cold outside, we <span class= 'rw_highlight'>shiver</span>. When it’s <span class= 'rw_highlight'>pouring</span> with rain, we need <span class= 'rw_highlight'>raincoats</span> and <span class= 'rw_highlight'>umbrellas</span>."
      },
      9 => {
        upper_text_field: "Captain Neptune explains that rain falls from clouds and soaks into the earth, filling up the lakes and streams. It helps plants",
        lower_text_field: "to grow."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that sometimes you may not welcome the rain: you know the poem, ‘Rain, rain go away, come again",
          lower_text_field: "another day’. But without rain, the ground becomes hard and dry."
      },
      11 => {
          upper_text_field: "When that happens the roots of plants cannot soak up enough water and the plants die. Rain can fall in fine, trickly <span class= 'rw_highlight'>droplets</span>,",
          lower_text_field: "called mist or drizzle. It can come down in a short friendly shower or in a heavy torrent."
      },
      12 => {
          upper_text_field: "Captain Neptune wants to know if you have you ever heard the funny expression people sometimes use to describe a really",
          lower_text_field: "heavy rain? They say, ‘Its raining cats and dogs!’ Have you ever seen a rainbow?"
      },
      13 => {
          upper_text_field: "Today’s Mission Activity 1:",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Today’s Mission Activity 2:",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Today’s Mission Activity 3:",
          main_video_slide: ""
      },
      16 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
  }

end
