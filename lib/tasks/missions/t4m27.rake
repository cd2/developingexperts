desc "Lesson"
task t4m27: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span> was a great <span class='rw_highlight'>philosopher</span> and scientist who lived in Ancient Greece. A <span class='rw_highlight'>philosopher</span> and",
          lower_text_field: "scientist is someone who asks questions about why people do things and why we think certain things are true."
          },
      7 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span>’s father was a doctor. He taught <span class='rw_highlight'>Aristotle</span> about the life cycle and the human body, which",
          lower_text_field: "you have been learning about."
          },
      8 => {
          upper_text_field: "Commander Mars explains that Aristotle travelled around Ancient Greece with his father and looked after people who were ill. He",
          lower_text_field: "started to learn how to become a doctor. Sadly, when <span class='rw_highlight'>Aristotle</span> was still young, his father died."
          },
      9 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span> then lived with his uncle who taught him poetry and other things. When <span class='rw_highlight'>Aristotle</span> was 17,",
          lower_text_field: "he began studying philosophy with an older <span class='rw_highlight'>philosopher</span>, <span class='rw_highlight'>Plato</span>, at <span class='rw_highlight'>Plato</span>’s school in Athens."
          },
      10 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span> enjoyed learning about philosophy so much that he even became a teacher! He enjoyed",
          lower_text_field: "asking questions about life and people."
          },
      11 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span>’s father and uncle both taught him different things that helped him to become a great",
          lower_text_field: "<span class='rw_highlight'>philosopher</span>, writer and scientist."
          },
      12 => {
          upper_text_field: "Commander Mars explains that in Ancient Greece, <span class='rw_highlight'>philosophers</span> and scientists were very similar. They both tried to explain why",
          lower_text_field: "things happened. <span class='rw_highlight'>Aristotle</span> believed in watching and measuring things to find out about them."
          },
      13 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span> made a system of organising things into lists. He looked for patterns. For example,",
          lower_text_field: "we know that labradors are animals. <span class='rw_highlight'>Aristotle</span> made groups and lists for lots of different types of living things."
          },
      14 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>Aristotle</span> liked sorting things and using a set process for working things out. Through his",
          lower_text_field: "experiments, he created something called the ‘<span class='rw_highlight'>scientific method</span>’."
          },
      15 => {
          upper_text_field: "Commander Mars explains that the ‘<span class='rw_highlight'>scientific method</span>’ is what you might use when doing <span class='rw_highlight'>investigations</span> in science. His",
          lower_text_field: "‘<span class='rw_highlight'>scientific method</span>’ starts with finding a problem, thinking of an answer, then testing out the answer to see if it is right or wrong."
          },
      16 => {
          upper_text_field: "Because <span class='rw_highlight'>Aristotle</span> was the first person to <span class='rw_highlight'>investigate</span> things in this way, he is sometimes called the ‘Father of Science’.",
          lower_text_field: ""
          },

      17 => {upper_text_field: "Today’s First Mission Assignment: ",
             main_video_slide: "",
             },
      18 => {upper_text_field: "Today’s Second Mission Assignment: "},
      19 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
  }



  $rocket_words = {
          "Aristotle" => {
              description: "One of the greatest thinkers of all time. He was an ancient Greek <span class='rw_highlight'>philosopher</span>.",
              image: "" },
          "Plato" => {
              description: "Ancient Athenian <span class='rw_highlight'>philosopher</span>; who was a pupil of Socrates and teacher of <span class='rw_highlight'>Aristotle</span>",
              image: "" },
          "Scientific Method" => {
              description: "A process used by scientists to study the world around them.",
              image: "" },
          "Investigate" => {
              description: "To explore facts about something.",
              image: "" },
          "Philosopher" => {
              description: "A person who studies ideas about knowledge, truth, the nature and meaning of life.",
              image: "" },
  }

  $questions = {
      "Who was <span class='rw_highlight'>Aristotle</span>?" => {answer: "One of the greatest thinkers of all time. He was an ancient Greek <span class='rw_highlight'>philosopher</span>.",
                           image: "",
                           slide_id: 6},
      "Who was <span class='rw_highlight'>Plato</span>?" => {answer: "Ancient Athenian <span class='rw_highlight'>philosopher</span>; who was a pupil of Socrates and teacher of <span class='rw_highlight'>Aristotle</span>",
                                image: "",
                                slide_id: 9},
      "What is the <span class='rw_highlight'>scientific method</span>?" => {answer: "A process used by scientists to study the world around them.",
                           image: "",
                           slide_id: 14},
      "What does <span class='rw_highlight'>investigate</span> mean?" => {answer: "To explore facts about something.",
                              image: "",
                              slide_id: 15},
      "What is <span class='rw_highlight'>philosopher</span>?" => {answer: "A person who studies ideas about knowledge, truth, the nature and meaning of life.",
                              image: "",
                              slide_id: 6},
  }

end
