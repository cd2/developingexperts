desc "Lesson"
task t5m8: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that weather happens because our planet is wrapped in layers of air, called Earth’s atmosphere,",
          lower_text_field: "and because the Sun is constantly bombarding the Earth with energy."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains that without an atmosphere, Earth would look like the Moon – a waterless, lifeless hunk of rock. Our",
          lower_text_field: "atmosphere is constantly absorbing energy from the Sun, and that energy moves around from place to place, creating weather."
      },
      8 => {
          upper_text_field: "Brigadier Venus explains the Earth’s atmosphere is made up of five main layers of air. In height order starting with the lowest",
          lower_text_field: "layer they are: the <Span>troposphere</span>; <span>stratosphere<s/pan>; <span>mesosphere</span>; <Span>thermosphere</span> and <span>exophere</span>."
      },
      9 => {
          upper_text_field: "Brigadier Venus describes how the ozone layer in the <span>stratosphere</span> absorbs ultraviolet radiation from the Sun, which",
          lower_text_field: "in high quantities can be very harmful, even deadly, to many creatures on Earth."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains how in the twentieth century, scientists discovered that a hole had developed in the ozone layer over",
          lower_text_field: "Antarctica. This allowed even more unhealthy amounts of ultraviolet radiation to reach Earth."
      },
      11 => {
          upper_text_field: "Brigadier Venus describes how these changes mainly affected people living in the Southern Hemisphere. The further south you",
          lower_text_field: "go, the greater the amount of harmful ultraviolet radiation you may encounter."
      },
      12 => {
          upper_text_field: "Brigadier Venus explains how this hole in the ozone layer was caused by some chemicals produced by humans, such as",
          lower_text_field: "chemicals in old fridges and spray cans."
      },
      13 => {
          upper_text_field: "Brigadier Venus discovers that in 1987 those chemicals were banned in 197 countries (the largest international agreement in",
          lower_text_field: "history!) and since then the ozone layer has been recovering."
      },
      14 => {
          upper_text_field: "Scientists are keeping a close eye on it and predict that it will have fully repaired itself within 50 years or so. Brigadier Venus",
          lower_text_field: "explains when you breathe, you are inhaling air from the <span>troposphere</span>. Clouds can be formed in the <span>troposhere</span>."
      },
      15 => {
          upper_text_field: "Brigadier Venus explains that the <span>stratosphere</span>, is about 10 or 30 kilometres above the Earth and contains a tiny amount of gas",
          lower_text_field: "called ozone, which protects us from ultraviolet radiation, part of the energy that comes from the Sun."
      },
      16 => {
          upper_text_field: "Brigadier Venus explains that the <span>mesosphere</span> is 30 to 50km above the Earth and acts as a protective shield in another way.",
          lower_text_field: "Perhaps you have seen shooting stars streaking across the night. These are meteors, hunks of rock streaking through space."
      },
      17 => {
          upper_text_field: "Sometimes they come so close to Earth that they enter the atmosphere. But when they rub against air in the <Span>mesosphere</span>,",
          lower_text_field: "all but the largest meteors heat up so intensely that they burn to cinders before reaching the Earth."
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then complete the experiment with your partner.",
          main_video_slide: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}

  $rocket_words = {
          "Stratosphere" => {
              description: "Upper atmosphere extending upward from the troposphere to about 30 miles above the Earth.",
              image: "" },
          "Mesosphere" => {
              description: "Below the exosphere, 250–650 miles above the Earth.",
              image: "" },
          "Thermosphere" => {
              description: "Upper atmosphere; all of the atmosphere above the mesosphere.",
              image: "" },
          "Exosphere" => {
              description: "Highest region of the atmosphere.",
              image: "" },
          "Troposphere" => {
              description: "Lowest layer of the atmosphere, 6 to 12 miles high from the Earth.",
              image: "" },
  }


end
