desc "Lesson"
task t2m7: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn discovers that <span class='rw_highlight'>Charles Macintosh</span> was born in 1766. His father originally came from the Highlands. He moved to",
          lower_text_field: "Glasgow to set up a factory in Dennistoun in 1777 to manufacture a violet-red dyeing powder."
      },
      7 => {
          upper_text_field: "Major Saturn learns that <span class='rw_highlight'>Macintosh</span> had a strong interest in chemistry. In 1818, while analysing the by-products of a works",
          lower_text_field: "making coal gas, he discovered dissolved indiarubber."
      },
      8 => {
          upper_text_field: "Major Saturn learns that <span class='rw_highlight'>Macintosh</span> joined two sheets of fabric together with this solution and then allowed them to dry.",
          lower_text_field: "<span class= 'rw_highlight'>Macintosh</span> discovered that the new material was not <span class='rw_highlight'>absorbent</span> and could not be <span class='rw_highlight'>penetrated</span> by water - the first rainproof cloth!"
      },
      9 => {
          upper_text_field: "Major Saturn finds out that chemists George Hancock and <span class='rw_highlight'>Macintosh</span> worked together to solve issues with the fabric and",
          lower_text_field: "finally succeeded in producing reliable <span class='rw_highlight'>waterproofed</span> sheets and coats."
      },
      10 => {
          upper_text_field: "<span class='rw_highlight'>Macintosh</span> founded his own <span class='rw_highlight'>waterproofing</span> company in Glasgow in 1834 - mainly because of the opposition he faced from tailors,",
          lower_text_field: "who wanted nothing to do with his new cloth – but he later moved to Manchester in 1840 to exploit the material further."
      },
      11 => {
          upper_text_field: "Major Saturn is surprised by the coincidence that the factory is now owned by the Dunlop Rubber Company you learnt about",
          lower_text_field: "in the last mission. The picture shows a <span class='rw_highlight'>Macintosh</span> coat."
      },
      12 => {
          upper_text_field: "Major Saturn learns that materials must be tested to ensure their suitability for making a particular product. If the wrong material",
          lower_text_field: "is used to make a product it could be unsafe for the person using it. You can’t use wool for an umbrella because it doesn’t <span class='rw_highlight'>repel</span> water."
      },
      13 => {
          upper_text_field: "Major Saturn wants to know what you think would happen if a saucepan handle was made out of metal or if a table was made",
          lower_text_field: "out of ice? Discuss with your talk partners."
      },
      14 => {
          upper_text_field: "Major Saturn explains that altering the ‘properties’ of materials can change the performance of a material.",
          lower_text_field: "Watch this short film. Think about how a material’s properties have been changed. Discuss with your talk partner.",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Do you think an orange would sink or swim in a bowl of water? Discuss with your talk partners. Now watch the film.",
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Complete the impossible scenarios experiment. Can you have a towel made out of foil?",
          lower_text_field: "Will a paper chair work? Could you have a metal jumper? Can you have a wooden bottle?"
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Identify the links between the properties of the material chosen and its use. Complete the grid.",
      },
      18 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations and be ready to start today’s mission.",
          sixty_second_count: ""
      },
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}


  $rocket_words = {
          "Charles Macintosh" => {
              description: "A chemist born in 1766. He <b>invented <span class='rw_highlight'>waterproof</span> fabric</b>.",
              image: "" },
          "Penetrate" => {
              description: "Spread or <b>diffuse through</b> a substance.",
              image: "" },
          "Repel" => {
              description: "Cause to <b>move back by force</b> or influence.",
              image: "" },
          "Absorbent" => {
              description: "A material having capacity or tendency to <b>absorb or soak up another substance</b>.",
              image: "" },
          "Waterproof" => {
              description: "Able to resist water so its <b>surface repels liquid</b>.",
              image: "" },
  }

  $questions = {
      "Who was <span class='rw_highlight'>Charles Macintosh</span>?" => {answer: "A chemist born in 1766. He invented <span class='rw_highlight'>waterproof</span> fabric.",
                           image: "",
                           slide_id: 1},
      "What does <span class='rw_highlight'>penetrate</span> mean?" => {answer: "Spread or diffuse through a substance.",
                                image: "",
                                slide_id:8 },
      "What does <span class='rw_highlight'>repel</span> mean?" => {answer: "Cause to move back by force or influence.",
                           image: "",
                           slide_id: 12},
      "What does <span class='rw_highlight'>absorbent</span> mean?" => {answer: "A material having capacity or tendency to absorb or soak up another substance.",
                              image: "",
                              slide_id: 8},
      "What does <span class='rw_highlight'>waterproof</span> mean?" => {answer: "Able to resist water so its surface repels liquid.",
                              image: "",
                              slide_id: 9},
  }

end
