desc "Lesson"
task t1m30: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that <span class='rw_highlight'>Joseph Banks</span> was born in 1743. He was a particular type of scientist known as a ‘<span class='rw_highlight'>botanist</span>’, one",
          lower_text_field: "who studies plants. <span class='rw_highlight'>Joseph Banks’</span> love of plants began at school."
          },
      7 => {
          upper_text_field: "He dreamed of travelling the world in search of new and different types of plants that no one had ever seen before. Captain",
          lower_text_field: "Neptune explains that <span class='rw_highlight'>Banks</span> was particularly interested in plants that could be used for practical purposes."
          },
      8 => {
          upper_text_field: "Captain Neptune asks if you know that plants are not only used as food but can also be used in medicines and to make clothes? It",
          lower_text_field: "was not very easy to travel around in those days, however. The picture show cotton plants before they are harvested."
          },
      9 => {
          upper_text_field: "Captain Neptune explains that transport was very limited and it took a very long time to get anywhere – there were no",
          lower_text_field: "aeroplanes and not even cars! Most people never left the town in which they were born."
          },
      10 => {
          upper_text_field: "But, after he finished his studies at Oxford University, the opportunity arose for <span class='rw_highlight'>Joseph</span> to travel with one of the most",
          lower_text_field: "famous explorers of the day, <span class='rw_highlight'>Captain James Cook</span>. The picture show the <span class='rw_highlight'>Captain James Cook</span> Memorial in Whitby, England."
          },
      11 => {
          upper_text_field: "Captain Neptune explains that in 1766, they set off in a great sailing ship, not knowing if they were ever to return. The three",
          lower_text_field: "year journey had already taken the adventurers to Madeira, Rio de Janeiro and New Zealand."
          },
      12 => {
          upper_text_field: "Captain Neptune explains that many new and exciting plants had been collected before the ship even reached the yet unexplored",
          lower_text_field: "land of Australia. <span class='rw_highlight'>Joseph</span> collected an enormous number of plants on the way. He discovered many new types."
          },
      13 => {
          upper_text_field: "Captain Neptune explains that in New Zealand he saw that the local people used a particular type of plant called ‘<span class='rw_highlight'>flax</span>’ to make",
          lower_text_field: "their clothes, so he brought some back to England and it is now commonly found in people’s gardens."
          },
      14 => {
          upper_text_field: "Captain Neptune explains that in Australia, <span class='rw_highlight'>Joseph</span> collected over a thousand different types of plants, in what must have been a",
          lower_text_field: "thrilling 70 days – with new discoveries being made at every turn. Much of what they found had never been seen or heard of before."
          },
      15 => {
          upper_text_field: "On his return to England, he became president of the <span class='rw_highlight'>Royal Society</span>. The <span class='rw_highlight'>Royal Society</span> is a group of the world’s most",
          lower_text_field: "important and famous scientists and is the oldest scientific academy still in existence. So this was a very great honour."
          },
      16 => {
          upper_text_field: "Captain Neptune explains that by this time, <span class='rw_highlight'>Joseph Banks</span> was held in such high regard that he even advised the King of England",
          lower_text_field: "on which plants would be best for the new gardens the King was making for the public, now known as ‘Kew Gardens’."
          },
      17 => {
          upper_text_field: "Captain Neptune explains that you can still visit Kew Gardens in London today and see different types of plants from all over the",
          lower_text_field: "world. The plants <span class='rw_highlight'>Banks</span> discovered, can still be seen in the Natural History Museum in London."
          },
      18 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: This weaving craft shows how <span class='rw_highlight'>flax</span> would be woven to make linen clothes."},
      19 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
  }

end
