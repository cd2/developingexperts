desc "Lesson"
task t3m4: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {
          upper_text_field: "Colonel Jupiter wants to know how your celery transporter experiments turned out? Explain what happened with your talk partners.",
      },
      5 => {ten_count_launch: ""},
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "Colonel Jupiter explains that some plants and animals can reproduce by themselves <span class='rw_highlight'>asexually</span>. In plants, the most familiar",
          lower_text_field: "example of such reproduction is called <span class='rw_highlight'>cloning</span>."
      },
      8 => {
          upper_text_field: "<span class='rw_highlight'>Cloning</span> happens when a piece of a plant - a leaf or stem cutting - is taken and placed in moist material or soil. Then a whole new",
          lower_text_field: "plant grows, a new plant that is <span class='rw_highlight'>identical</span> to the parent. We call it a <span class='rw_highlight'>clone</span>."
      },
      9 => {
          upper_text_field: "Colonel Jupiter learns that some plants do this naturally, like strawberries that send out a ‘runner’ or daffodil bulbs that",
          lower_text_field: "divide into two underground. Some plants like <span class='rw_highlight'>algae</span> can even reproduce using more than one way. How clever is that!"
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that only simple animals like sponges, flatworms or jellyfish reproduce <span class='rw_highlight'>asexually</span>. Some simple",
          lower_text_field: "animals just split in two, and some grow a new, smaller copy on their side that eventually falls off and grows on its own."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains most animals do not reproduce <span class='rw_highlight'>asexually</span>. However, many animals have the ability to replace",
          lower_text_field: "lost cells or even lost body parts. This is called <span class='rw_highlight'>regeneration</span>."
      },
      12 => {
          upper_text_field: "Colonel Jupiter learns that animals <span class='rw_highlight'>regenerate</span> in different ways and amounts. You <span class='rw_highlight'>regenerate</span> skin cells when you cut your finger",
          lower_text_field: "and the wound heals. But if you cut off your whole finger, it won’t grow back."
      },
      13 => {
          upper_text_field: "Colonel Jupiter describes how a starfish can grow a whole new arm if one is cut off. If the lost arm still has a piece of the centre",
          lower_text_field: "of the starfish, it can even grow into a new starfish! When some kinds of worms are cut in half, each half grows into a new worm."
      },
      14 => {
          upper_text_field: "Colonel Jupiter describes how <span class='rw_highlight'>salamanders</span> can <span class='rw_highlight'>regenerate</span> a leg if they lose one. But the leg can’t <span class='rw_highlight'>regenerate</span> a new <span class='rw_highlight'>salamander</span>.",
          lower_text_field: "In general, more complex animals like <span class='rw_highlight'>salamanders</span> and humans have a more limited ability to <span class='rw_highlight'>regenerate</span>."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment: Colonel Jupiter would like you to learn how to <span class='rw_highlight'>clone</span> a potato!",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Colonel Jupiter announces that you have 60 seconds to move to your work stations for today’s Mission Assignment.",
          sixty_second_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

  $rocket_words = {
          "Asexual Plants" => {
              description: "Plants which reproduce on their own. An example is bacteria.",
              image: "" },
          "Algae" => {
              description: "Simple, usually small plants that grow in or near water and <b>do not have ordinary leaves or roots.",
              image: "" },
          "Cloning" => {
              description: "Creating a plant or animal from the same genes as an original.",
              image: "" },
          "Regeneration" => {
              description: "To improve a place or system, especially by making it more active.",
              image: "" },
          "Salamander" => {
              description: "Small animal that <b>looks like a lizard</b> but has soft skin and lives both on land and in water.",
              image: "" },
  }

  $questions = {
      "What are <span class='rw_highlight'>asexual plants</span>?" => {answer: "Plants which reproduce on their own. An example is bacteria.",
                           image: "",
                           slide_id: 7},
      "What is <span class='rw_highlight'>algae</span>?" => {answer: "Simple, usually small plants that grow in or near water and do not have ordinary leaves or roots.",
                                image: "",
                                slide_id: 9},
      "What is <span class='rw_highlight'>cloning</span>?" => {answer: "Creating a plant or animal from the same genes as an original.",
                           image: "",
                           slide_id: 1 },
      "What is <span class='rw_highlight'>regeneration</span>?" => {answer: "To improve a place or system, especially by making it more active.",
                              image: "",
                              slide_id: 1 },
      "What is a <span class='rw_highlight'>salamander</span>?" => {answer: "Small animal that looks like a lizard but has soft skin and lives both on land and in water.",
                              image: "",
                              slide_id: 14},
  }

end
