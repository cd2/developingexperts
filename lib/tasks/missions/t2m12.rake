desc "Lesson"
task t2m12: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants you to pretend that you’re at the supermarket. You need to buy some milk. How much milk do you need to buy?",
          lower_text_field: "What units of measurement could you use to answer that question?"
      },
      7 => {
          upper_text_field: "Major Saturn reminds us that metres and <span class='rw_highlight'>centimetres</span> are some of the units we use to measure length. But if you want to know how much",
          lower_text_field: "milk to buy, do you think about how long the milk is?"
      },
      8 => {
          upper_text_field: "Do you think, ‘I need 15 <span class='rw_highlight'>centimetres</span> of milk’? No: You’re not thinking about how long the milk is, but how much space it takes up.",
      },
      9 => {
          upper_text_field: "Major Saturn explains that we can use many different units to measure how much space something takes up. For example,",
          lower_text_field: "when you shop for milk, you can buy a pint bottle."
      },
      10 => {
          upper_text_field: "Or you can buy a <span class='rw_highlight'>litre</span> bottle. Major Saturn explains you could buy two pint bottles, but it might be easier to buy a one <span class='rw_highlight'>litre</span> carton:",
          lower_text_field: "that’s because two pints is about the same a one <span class='rw_highlight'>litre</span> (actually it is a bit more)."
      },
      11 => {
          upper_text_field: "Major Saturn explains how every <span class='rw_highlight'>litre</span> bottle holds the same amount of milk. Even if <span class='rw_highlight'>litre</span> containers have different labels and",
          lower_text_field: "come from different companies, they hold the same amount – they are measured <span class='rw_highlight'>accurately</span> in every factory in the world."
      },
      12 => {
          upper_text_field: "Major Saturn explains another unit of measuring is a <span class='rw_highlight'>tablespoon</span>. <span class='rw_highlight'>Tablespoons</span> are a particular size. When you bake a cake that needs",
          lower_text_field: "a <span class='rw_highlight'>tablespoon</span> of baking powder, the cake will rise by just the right amount if you use exactly a <span class='rw_highlight'>tablespoon</span>."
      },
      13 => {
          upper_text_field: "This is because you, and the person who wrote the recipe, both use the same measuring standard.",
      },
      14 => {
          upper_text_field: "Major Saturn wants to find out how many <span class='rw_highlight'>tablespoons</span> are needed to fill the space of 100 <span class='rw_highlight'>millilitres</span>. A <span class='rw_highlight'>millilitre</span> is another unit of measuring.",
          lower_text_field: "<span class='rw_highlight'>Millilitres</span> are much, much smaller than <span class='rw_highlight'>litres</span> and so <span class= 'rw_highlight'>tablespoons</span> are useful for measuring smaller amounts."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment 1: Measure out three quantities of water and place into three different containers to compare what happens.",
      },
      16 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
