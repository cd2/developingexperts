desc "Lesson"
task t1m8: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune wants you to imagine that you’re climbing a high tower. When you reach the top, you look out over a great",
          lower_text_field: "<span class= 'rw_highlight'>forest</span>. You see trees stretching for miles, like a big blanket of green – so many trees that it might seem as though we could never run out of them."
      },
      8 => {
          upper_text_field: "Captain Neptune wants you to think about the <span class= 'rw_highlight'>forest</span> and the trees and wants to know why you think they are important? If you were",
          lower_text_field: "an animal in the <span class= 'rw_highlight'>forest</span> – a fox, a badger, an owl or a deer – the <span class= 'rw_highlight'>forest</span> would be important to you because it’s your home!"
      },
      9 => {upper_text_field: "Captain Neptune explains that many people like to hike through <Span class= 'rw_highlight'>forests</span> and to camp out in the woods, where the air is fresh and",
            lower_text_field: "clean. These are some reasons why <span class= 'rw_highlight'>forests</span> are important."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that people cut down trees. Think how many ways we use wood from trees. We burn wood in fireplaces.",
          lower_text_field: "We use wood to build houses. We use it to build furniture: chairs, tables, doors and more."
      },
      11 => {
          upper_text_field: "Captain Neptune describes how paper comes from wood. Twenty million people read a Sunday newspaper in Britain, and all those",
          lower_text_field: "papers started life as trees. Think of all the other paper we use,"
      },
      12 => {
          upper_text_field: "paper towels, cardboard, the paper you use for writing and drawing and the paper that makes the pages of books.",
      },
      13 => {
          upper_text_field: "Captain Neptune explains why trees are important to us - we cut them down to make things we need: houses, furniture,",
          lower_text_field: "paper and more. But trees are also important to us when they’re standing tall in <span class= 'rw_highlight'>forests</span>."
      },
      14 => {
          upper_text_field: "Captain Neptune explains that we should grow new trees to take the place of the trees we’ve cut down. This is called <span class= 'rw_highlight'>reforestation</span>.",
          lower_text_field: "Nowadays <span class= 'rw_highlight'>logging</span> companies need to think carefully about <span class= 'rw_highlight'>conservation</span> and protecting our <span class= 'rw_highlight'>natural resources</span>."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment: Create and bury your own time capsule.",
      },
      16 => {upper_text_field: "Captain Neptune would like you get ready for today’s mission assignment and report to your work stations.", sixty_count: ""},
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
  }

end
