desc "Lesson"
task t4m21: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars can’t see <span class='rw_highlight'>vitamins</span> and <span class='rw_highlight'>minerals</span> but he knows they are important nutrients that come to you in good",
          lower_text_field: "foods, like vegetables and fruits."
      },
      7 => {
          upper_text_field: "He knows that to be healthy, his body needs the right combination of <span class='rw_highlight'>vitamins</span> and <span class='rw_highlight'>minerals</span> everyday.",
          lower_text_field: "Without enough <span class='rw_highlight'>vitamins</span> and <span class='rw_highlight'>minerals</span>, he could get very sick."
      },
      8 => {
          upper_text_field: "Commander Mars discovers that <span class='rw_highlight'>vitamin A</span> helps his skin, tissues and eyes grow strong; he can get plenty of <span class='rw_highlight'>vitamin A</span> by",
          lower_text_field: "eating carrots, sweet potatoes and by drinking milk."
      },
      9 => {
          upper_text_field: "Hand the carrot samples round. What do they taste like? What do we know about carrots?",
      },
      10 => {
          upper_text_field: "Commander Mars finds out there are eight different <span class='rw_highlight'>vitamins</span> called <span class='rw_highlight'>vitamin B</span>, each one has its own role in keeping his body",
          lower_text_field: "healthy. Most come from meat and may come in vegetables too."
      },
      11 => {
          upper_text_field: "<span class='rw_highlight'>Vitamin C</span> helps Commander Mars’ cells grow strong, and it helps his body fight against disease. He can get plenty of",
          lower_text_field: "<span class='rw_highlight'>vitamin C</span> by eating oranges, tomatoes and broccoli, ‘Yum!’"
      },
      12 => {
          upper_text_field: "Commander Mars knows <span class='rw_highlight'>vitamin D</span> is important because it helps his teeth and bones grow strong. His skin absorbs some",
          lower_text_field: "vitamin D from the sunshine, and he can also get <span class='rw_highlight'>vitamin D</span> from tuna, egg yolks and milk too."
      },
      13 => {
          upper_text_field: "Commander Mars knows that besides <span class='rw_highlight'>vitamins</span> his body needs <span class='rw_highlight'>minerals</span>. The <span class='rw_highlight'>minerals</span> his body needs are actually tiny amounts",
          lower_text_field: "of the same <span class='rw_highlight'>minerals</span> that can be found in the earth. Does that mean he has to eat rocks? No!"
      },
      14 => {
          upper_text_field: "His body only needs small amounts of these <span class='rw_highlight'>minerals</span>, which he can get from the right food. Commander Mars’ body needs iron",
          lower_text_field: "to build his blood cells, otherwise he will feel tired and unwell."
      },
      15 => {
          upper_text_field: "Foods with plenty of iron include meat, green leafy vegetables, whole grain cereals, raisins and dried beans.",
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then try the experiment with your talk partners.",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}



  $rocket_words = {
          "Vitamin A" => {
              description: "<span class='rw_highlight'>Vitamin A</span> helps the skin, tissues and eyes grow strong and can be found in vegetables.",
              image: "" },
          "Vitamin B" => {
              description: "There are 8 different <span class='rw_highlight'>vitamins</span> called <span class='rw_highlight'>vitamin B</span> which keep you healthy. Most come from meat.",
              image: "" },
          "Vitamin C" => {
              description: "<span class='rw_highlight'>Vitamin C</span> helps cells grow strong, and your body fight against disease. Oranges have <span class='rw_highlight'>vitamin C</span>.",
              image: "" },
          "Vitamin D" => {
              description: "<span class='rw_highlight'>Vitamin D</span> is important because it helps teeth and bones grow strong; the sun provides <span class='rw_highlight'>vitamin D</span>.",
              image: "" },
          "Mineral" => {
              description: "A chemical that your body needs to stay healthy found in the ground.",
              image: "" },
  }

  $questions = {
      "What does <span class='rw_highlight'>vitamin A</span> do?" => {answer: "<span class='rw_highlight'>Vitamin A</span> helps the skin, tissues and eyes grow strong and can be found in vegetables.",
                           image: "",
                           slide_id: 8},
      "Where does <span class='rw_highlight'>vitamin B</span> do?" => {answer: "There are 8 different <span class='rw_highlight'>vitamins</span> called <span class='rw_highlight'>vitamin B</span> which keep you healthy. Most come from meat.",
                                image: "",
                                slide_id: 10},
      "What does <span class='rw_highlight'>vitamin C</span> do?" => {answer: "<span class='rw_highlight'>Vitamin C</span> helps cells grow strong, and your body fight against disease. Oranges have <span class='rw_highlight'>vitamin C</span>.",
                           image: "",
                           slide_id: 11},
      "What does <span class='rw_highlight'>vitamin D</span> do?" => {answer: "<span class='rw_highlight'>Vitamin D</span> is important because it helps teeth and bones grow strong; the sun provides <span class='rw_highlight'>vitamin D</span>.",
                              image: "",
                              slide_id: 12},
      "What is a <span class='rw_highlight'>mineral</span>?" => {answer: "A chemical that your body needs to stay healthy found in the ground.",
                              image: "",
                              slide_id: 13},
  }

end
