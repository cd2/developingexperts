desc "Lesson"
task t3m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {
          upper_text_field: "Colonel Jupiter wants to know how your potato cloning experiment is progressing? Explain what happened with your talk partners.",
      },
      5 => {ten_count_launch: ""},
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "Colonel Jupiter explains that many plants produce flowers, including most trees, shrubs, vines, grasses, and garden plants.",
          lower_text_field: "Flowers can be as large and showy as sunflowers or tiny like the flowers on a grass plant, but most flowers have similar parts."
      },
      8 => {
          upper_text_field: "Colonel Jupiter wants us to take a look at a diagram of a typical flower to see how seeds are formed.",
          lower_text_field: "Colonel Jupiter asks if you can see the seeds somewhere? Can you see the stigma?"
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that the outer part is made up of <span class='rw_highlight'>sepals</span>, which are usually green and look like leaves attached to the",
          lower_text_field: "stem at the base of the flower."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that inside the <span class='rw_highlight'>sepals</span>, the petals make the next ring. The colourful petals attract insects, which are",
          lower_text_field: "often important for bringing pollen carrying the male gamete to the egg."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that the reproductive parts lie inside the petals, in the centre of the flower. The <span class='rw_highlight'>stamens</span> are the",
          lower_text_field: "reproductive organs. Each <span class='rw_highlight'>stamen</span> has an <span class='rw_highlight'>anther</span> on its tip, where millions of tiny pollen grains, each with a male gamete, are made."
      },
      12 => {
          upper_text_field: "Colonel Jupiter wonders if you know what an <span class='rw_highlight'>ovary</span> is? In Latin it means a ‘place for eggs’. This place for the egg or eggs is",
          lower_text_field: "completely protected. Colonel Jupiter wants to know why this is a great advantage for a flower."
      },
      13 => {
          upper_text_field: "Colonel Jupiter explains that when the <span class='rw_highlight'>ovary</span> develops into fruit, there will be seeds inside, just like in a tomato, orange or apple.",
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Separate the flower parts and label them. Use the hand out provided.",
      },
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

end
