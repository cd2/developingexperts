desc "Lesson"
task t3m5: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {
          upper_text_field: "Colonel Jupiter wants to know how your potato cloning experiment is progressing? Discuss what happened with your talk partners.",
      },
      5 => {ten_count_launch: ""},
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "Colonel Jupiter explains that most large plants reproduce by combining a male and female cell to make a fertilised egg which",
          lower_text_field: "grows into an embryo. This embryo, or baby plant, is protected inside a seed."
      },
      8 => {
          upper_text_field: "Colonel Jupiter discovers that some of the simplest of these seed plants are the <span class='rw_highlight'>conifers</span>, a name that means ‘<span class='rw_highlight'>cone</span>-carriers’.",
          lower_text_field: "He wonders if you know what a pine <span class='rw_highlight'>cone</span> is for? It is the reproductive part of the pine tree."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that you can find big and small <span class='rw_highlight'>cones</span> on <span class='rw_highlight'>conifer</span> trees. These two kinds of <span class='rw_highlight'>cones</span> are usually found on the",
          lower_text_field: "same plant. The small <span class='rw_highlight'>cone</span> carries the male cells."
      },
      10 => {
          upper_text_field: "That’s because it doesn’t take much space to store millions of tiny grains of pollen, each of which carries a male cell.",
      },
      11 => {
          upper_text_field: "Colonel Jupiter describes how pollen from the male pine <span class='rw_highlight'>cone</span> is carried by the wind and sticks to the larger female pine <span class='rw_highlight'>cones</span>.",
      },
      12 => {
          upper_text_field: "Colonel Jupiter explains how the tubes grow from the grains of pollen to reach the eggs inside the female <span class='rw_highlight'>cone</span>. Then a male cell",
          lower_text_field: "joins with the egg to fertilise it. The fertilised egg divides and grows the embryo."
      },
      13 => {
          upper_text_field: "The seed also contains a supply of food on the inside and a seed coat on the outside.",
      },
      14 => {
          upper_text_field: "Colonel Jupiter describes how the seed coat keeps the embryo from dying out. The seed drops to the ground when the <span class='rw_highlight'>cone</span>",
          lower_text_field: "opens. If the soil is moist, the seed germinates, using the stored food to help the embryo grow."
      },
      15 => {
          upper_text_field: "Colonel Jupiter explains how the embryo grows into a new tree if there is enough water and enough nutrients in the soil. The",
          lower_text_field: "seed from the <span class='rw_highlight'>conifer</span> is called a naked seed, because it has nothing on except its own seed coat."
      },
      16 => {
          upper_text_field: "There is no fruit that surrounds it. <span class='rw_highlight'>Conifers</span> belong to the group of plants called <span class='rw_highlight'>gymnosperms</span>, which means naked seeds.",
      },
      17 => {
          upper_text_field: "Colonel Jupiter describes how most plants clothe the seeds they make with some sort of covering. The fruit of a cherry is",
          lower_text_field: "the covering for the seed inside. Tomatoes have seeds inside, and so do cucumbers, green peppers and oranges."
      },
      18 => {
          upper_text_field: "Colonel Jupiter explains that all of these are fruit. You’re probably more interested in the fruit than the seeds, but for the",
          lower_text_field: "purpose of reproduction, the tomato or the orange are just coverings for seeds."
      },
      19 => {
          upper_text_field: "Colonel Jupiter describes how these plants with covered seeds are called <span class='rw_highlight'>angiosperms</span>, which in Greek means ‘covered seeds’.",
          lower_text_field: "All these plants have <span class='rw_highlight'>flowers</span>. The seed covers, hard or soft, big or small, sweet or sour, all come from the same place – a <span class='rw_highlight'>flower</span>."
      },
      20 => {
          upper_text_field: "Watch today’s film to learn more about wind pollination.",
          main_video_slide: ""
      },
      21 => {
          upper_text_field: "Colonel Jupiter wants you to move to your work stations for today’s mission assignment.",
          sixty_second_count: ""
      },
      22 => {upper_text_field: "Today’s Mission Assignment: Complete the wind pollination Game. Follow the instructions on the handout provided."},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}

end
