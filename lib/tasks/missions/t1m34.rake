desc "Lesson"
task t1m34: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that the weather is constantly changing but it is possible to predict the type of weather you might expect",
          lower_text_field: "given the time of year. People who sell plant seeds use this information to predict the best time to plant them."
      },
      7 => {
          upper_text_field: "Major Saturn explains that plants make their own food out of sunshine, air, water and nutrients from the soil in which they grow.",
          lower_text_field: "For example, peas are a cool season crop, so they are well suited to the UK climate. Peas can be directly sown outdoors from"
          },
      8 => {
          upper_text_field: "Captain Neptune explains that farmers need to <span class='rw_highlight'>monitor</span> weather trends to see if the summer weather is lasting longer to predict",
          lower_text_field: "how well their crops might grow."
          },
      9 => {
          upper_text_field: "Captain Neptune explains that weather stations are used to <span class='rw_highlight'>monitor</span> the weather. Weather stations are centres which house",
          lower_text_field: "a range of equipment which record what the weather is like. People who study the weather are called meteorologists."
          },
      10 => {
          upper_text_field: "Captain Neptune explains that they use the information they gather to forecast what the weather will be like. It is important",
          lower_text_field: "to <span class='rw_highlight'>monitor</span> weather so other people can be warned about the type of weather they can expect."
          },
      11 => {
          upper_text_field: "Captain Neptune explains that satellites in space are used to monitor the weather. If the weather is too dry and hot it might",
          lower_text_field: "not be the ideal growing conditions to grow a crop."
          },
      12 => {
          upper_text_field: "On the other hand if the weather is too windy and wet, plants might not be strong enough to grow outside. Captain Neptune",
          lower_text_field: "explains that knowing what the weather is going to be like helps the farmer make the necessary preparations for growing crops."
          },
      13 => {
          upper_text_field: "Captain Neptune explains that one of the many jobs meteorologists carry out on a daily basis is measuring the",
          lower_text_field: "amount of rainwater. Discuss with your talk partner why this is an important job for a meteorologist to complete."
          },
      14 => {
          upper_text_field: "Captain Neptune explains that meteorologists measure the daily amount of <span class='rw_highlight'>rainfall</span> using <span class='rw_highlight'>graphs</span>. The information entered",
          lower_text_field: "into a <span class='rw_highlight'>graph</span> is referred to as <span class='rw_highlight'>data</span>."
          },
      15 => {
          upper_text_field: "Captain Neptune explains that the <span class='rw_highlight'>data</span> helps them to compare the amount of <span class='rw_highlight'>rainfall</span> gathered on different days of the week and",
          lower_text_field: "different weeks of the year. Using <span class='rw_highlight'>data</span>, helps meteorologists compare how much rain falls over a period of time."
          },
      16 => {
          upper_text_field: "Captain Neptune explains that they can compare how much rain fell in January this year, compared with January last year.",
          lower_text_field: "If you have enough years to compare you can <span class='rw_highlight'>monitor</span> <span class='rw_highlight'>weather patterns</span> and trends."
          },
      17 => {
          upper_text_field: "Today’s Film",
          main_video_slide: ""

          },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Complete the task on your hand out. Then create your own rain catcher and <span class='rw_highlight'>monitor</span> your results.",
          lower_text_field: ""
          },
      19 => {
          upper_text_field: "Today’s Mission Assignment",
          main_video_slide: ""
          },
      20 => {
          upper_text_field: "",
          sixty_count: ""
      },
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
}



  $rocket_words = {
          'Data' => {
              description: "Facts and statistics collected together for reference.",
              image: "" },
          'Graph' => {
              description: "A diagram of values, usually shown as lines or bars.",
              image: "" },
          'Weather Patterns' => {
              description: "The average weather conditions in a certain place or during a certain season.",
              image: "" },
          'Monitor' => {
              description: "To watch and observe something for a special purpose over a period of time.",
              image: "" },
          'Rainfall' => {
              description: "The amount of rain that falls on a particular area.",
              image: "" },
  }

  $questions = {
      "What is <span class='rw_highlight'>data</span>?" => {answer: "Facts and statistics collected together for reference.",
                           image: "",
                           slide_id: 14},
      "What is a <span class='rw_highlight'>graph</span>?" => {answer: "A diagram of values, usually shown as lines or bars.",
                                image: "",
                                slide_id: 14},
      "What are <span class='rw_highlight'>weather patterns</span>?" => {answer: "The average weather conditions in a certain place or during a certain season.",
                           image: "",
                           slide_id: 16},
      "What does <span class='rw_highlight'>monitor</span> mean?" => {answer: "To watch and observe something for a special purpose over a period of time.",
                              image: "",
                              slide_id: 9},
      "What is <span class='rw_highlight'>rainfall</span>?" => {answer: "The amount of rain that falls on a particular area.",
                              image: "",
                              slide_id: 14},
  }

end
