desc "Lesson"
task t3m22: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter wants to know if you have you ever made a sandcastle? Did you fill a bucket with wet sand and then turn it",
          lower_text_field: "upside down to make a tower? You used the bucket as a mould. That tells you something about how <span class='rw_highlight'>fossils</span> were made."
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains that <span class='rw_highlight'>fossils</span> are made out of rock, not just sand. The sandcastle only lasts until the sea comes over it.",
          lower_text_field: "A <span class='rw_highlight'>fossil</span> takes a long time to form into a rock, in fact it takes millions of years; it lasts a very long time."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that living things have soft parts, which decay quickly after they die, and hard parts, which last longer.",
          lower_text_field: "You know that there are hard bones under your softer skin, muscles and blood vessels."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that if an ancient body is dug up it’s usually just the skeleton and teeth that have survived. Similarly,",
          lower_text_field: "the shells of snails and limpets last longer than the living creatures inside them."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that when these creatures die, sand and mud can cover the shell and start the long, slow process of",
          lower_text_field: "turning into rock or stone. The shell is a mould, like the bucket for a sandcastle."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that in time, the sand and mud turn into rock that is formed around the shell and takes on its shape. This",
          lower_text_field: "is a <span class='rw_highlight'>fossil</span>. It is like a three-dimensional picture in stone of something that was alive before the rock formed."
      },
      12 => {
          upper_text_field: "Colonel Jupiter explains that most <span class='rw_highlight'>fossils</span> are small. On beaches along the <span class='rw_highlight'>Jurassic Coast</span> in Dorset you have a good chance of",
          lower_text_field: "finding a stone with a print of a broken <span class='rw_highlight'>sea shell</span> from long, long ago. Have you ever found a <span class='rw_highlight'>fossil</span>?"
      },
      13 => {
          upper_text_field: "Colonel Jupiter explains that you have a small chance of finding a whole shell <span class='rw_highlight'>fossil</span>, a dinosaur bone or even a whole dinosaur!",
          lower_text_field: "The animals are <span class='rw_highlight'>extinct</span> but these <span class='rw_highlight'>fossils</span> help us learn about them."
      },
      14 => {
          upper_text_field: "Colonel Jupiter explains that ammonites are <span class='rw_highlight'>sea shells</span> from animals that are <span class='rw_highlight'>extinct</span>. If you found an ammonite like the one",
          lower_text_field: "below, someone could cut it open and you’d see how beautiful it is inside."
      },
      15 => {
          upper_text_field: "Plants can become <span class='rw_highlight'>fossils</span> too. Colonel Jupiter explains that coal is a plant <span class='rw_highlight'>fossil</span>. <span class='rw_highlight'>Amber</span> is particularly beautiful, formed",
          lower_text_field: "when sticky resin went solid and eventually hardened into stone."
      },
      16 => {
          upper_text_field: "Colonel Jupiter explains that sometimes the resin caught an insect, which was then preserved for thousands of years.",
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Have a go at creating your own <span class='rw_highlight'>fossil</span>.",
          main_video_slide: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
