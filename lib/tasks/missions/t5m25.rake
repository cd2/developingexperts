desc "Lesson"
task t5m25: :environment do

 slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that the third law of motion states that for every action, there is an equal and <span class='rw_highlight'>opposite</span> <span class='rw_highlight'>reaction</span>. This",
          lower_text_field: "means that there are always two forces that are the same."
          },
      7 => {
          upper_text_field: "Brigadier Venus explains that if you kick a football, there is the force of your foot on the ball, but there is also the same amount",
          lower_text_field: "of force that the ball puts on your foot. This force is in the exact <span class='rw_highlight'>opposite</span> direction."
          },
      8 => {
          upper_text_field: "Brigadier Venus wants you to remember, for every action there is always an equal and <span class='rw_highlight'>opposite</span> <span class='rw_highlight'>reaction</span>. Here’s an example:",
          lower_text_field: "You have 4 marbles dangling by a string that are touching each other. You lift the one on the far right, then let it go."
          },
      9 => {
          upper_text_field: "Brigadier Venus explains that the marble on the far left moves approximately the same distance (approximately because of",
          lower_text_field: "energy used up due to friction). So, the <span class='rw_highlight'>reaction</span> moves about the same distance but in the <span class='rw_highlight'>opposite</span> direction."
          },
      10 => {
          upper_text_field: "Brigadier Venus explains that the friction which used up some energy in the action is also equal to the friction in the <span class='rw_highlight'>reaction</span>,",
          lower_text_field: "but in the <span class='rw_highlight'>opposite</span> direction. Therefore, everything is equal and <span class='rw_highlight'>opposite</span>."
          },
      11 => {
          upper_text_field: "Brigadier Venus explains that this is a <span class='rw_highlight'>metronome</span>. It’s an example of Newton third law of motion in action. Its pendulum",
          lower_text_field: "swings back and forth to help keep the beat. The best way to understand Newton’s third law of motion is to see it in action."
          },
      12 => {
          upper_text_field: "Brigadier Venus explains that this film shows what happens when an <span class='rw_highlight'>input</span> force is applied, the same <span class='rw_highlight'>output</span> force is",
          lower_text_field: "achieved. If one ball bearing is the <span class='rw_highlight'>input</span>, the <span class='rw_highlight'>output</span> is one ball bearing and so on."
          },
      13 => {
          upper_text_field: "Today’s Film: Watch the recap film on Newton’s 3 Laws of Motion",
          main_video_slide: ""
          },
      14 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      15 => {upper_text_field: "Today’s Second Mission Assignment: "},
      16 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
  }



  $rocket_words = {
          "Input" => {
              description: "What is put in or taken in, by a process or system.",
              image: "" },
          "Output" => {
              description: "The amount of something produced by a person, machine or industry.",
              image: "" },
          "Reaction" => {
              description: "Something done, felt, or thought in response to a situation or event.",
              image: "" },
          "Opposite" => {
              description: "Completely different.",
              image: "" },
          "Metronome" => {
              description: "A rhythm setting device used in music.",
              image: "" },
  }

  $questions = {
      "What is <span class='rw_highlight'>input</span>?" => {answer: "What is put in or taken in, by a process or system.",
                           image: "",
                           slide_id: 12},
      "What is <span class='rw_highlight'>output</span>?" => {answer: "The amount of something produced by a person, machine or industry.",
                                image: "",
                                slide_id: 12},
      "What is a <span class='rw_highlight'>reaction</span>?" => {answer: "Something done, felt, or thought in response to a situation or event.",
                           image: "",
                           slide_id: 6},
      "What does <span class='rw_highlight'>opposite</span> mean?" => {answer: "Completely different.",
                              image: "",
                              slide_id: 9},
      "What is a <span class='rw_highlight'>metronome</span>?" => {answer: "A rhythm setting device used in music.",
                              image: "",
                              slide_id: 11},
  }

end
