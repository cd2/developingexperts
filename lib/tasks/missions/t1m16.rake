desc "Lesson"
task t1m16: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that weather can change very quickly. You might be playing out on a sunny day and then suddenly",
          lower_text_field: "a storm will blow in, it could rain or even snow."
      },
      7 => {
          upper_text_field: "Captain Neptune asks if you know what a <span class= 'rw_highlight'>thunderstorm</span> sounds like? Rumble! Boom! Snap! Crack! Boom again! You hear the",
          lower_text_field: "<span class= 'rw_highlight'>thunder</span> roaring?"
      },
      8 => {upper_text_field: "Captain Neptune asks if you can see the bright, jagged streaks of <span class= 'rw_highlight'>lightning</span> in the sky. <span class= 'rw_highlight'>Thunder</span> and <span class= 'rw_highlight'>lightning</span> might scare you,",
            lower_text_field: "but they’re part of nature during a <span class= 'rw_highlight'>thunderstorm</span>."
      },
      9 => {
        upper_text_field: "Captain Neptune explains that you don’t have to be scared of <span class= 'rw_highlight'>lightning</span> if you follow some simple rules. If you’re outside when",
        lower_text_field: "you hear <span class= 'rw_highlight'>thunder</span> and see <span class= 'rw_highlight'>lightning</span>, go inside quickly. Never stand under a tree during a <span class= 'rw_highlight'>lightning storm</span>."
      },
      10 => {
          upper_text_field: "Never stay in a swimming pool or lake during a <span class= 'rw_highlight'>lightning</span> storm. Find your way to a dry shelter and wait for the storm to pass by.",
          lower_text_field: "<span class= 'rw_highlight'>Lightning</span> storms usually don’t last long. Cars are <span class= 'rw_highlight'>safe</span> places to be because they have rubber tyres."
      },
      11 => {
          upper_text_field: "Captain Neptune explains that rain can fall fiercely during a <span class= 'rw_highlight'>thunderstorm</span>. Sometimes the rain can even freeze before it",
          lower_text_field: "comes down. Then it is called <span class= 'rw_highlight'>hail</span> – balls of ice that can be as small as peas or as big as golf balls!"
      },
      12 => {
          upper_text_field: "Mission Assignment: Create your own <span class= 'rw_highlight'>hail</span> bouncy ball. Decorate it with paint to make it look like a snowflake.",
          main_video_slide: ""
      },
      13 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
  }

end
