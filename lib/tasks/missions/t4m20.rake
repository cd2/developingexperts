desc "Lesson"
task t4m20: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that your digestive system breaks down food for your body. For your digestive system to do a",
          lower_text_field: "good job, you need to give it healthy food to work with."
      },
      7 => {
          upper_text_field: "Commander Mars explains that to be sure you’re eating enough of the food that’s good for you, take a look at the picture, called",
          lower_text_field: "the <span>food pyramid</span>. Can you work out the names of the five basic food groups by looking at the picture?"
      },
      8 => {
          upper_text_field: "Commander Mars explains that the <span>food pyramid</span> is divided into four sections with different kinds of foods in each section: the",
          lower_text_field: "bigger section, the more of that kind of food you should be eating."
      },
      9 => {
          upper_text_field: "Most of all you need to eat grains, like bread, cereal, rice and pasta. You also need to eat plenty of <span>fruit and vegetables</span>. And",
          lower_text_field: "you need to eat foods that give you proteins. These foods include <span>dairy products</span>, like milk, cheese and yoghurt."
      },
      10 => {
          upper_text_field: "Commander Mars explains that you also get protein from <Span>meat</span>, fish, eggs and nuts. See how much smaller the section at the top",
          lower_text_field: "of the pyramid is compared with all the other sections?"
      },
      11 => {
          upper_text_field: "This smallest section includes fats, oils and sweets, which means we need very little of these. In fact, if you are eating foods shown",
          lower_text_field: "in the bigger sections of the food pyramid, you are already getting some fats and sugars."
      },
      12 => {
          upper_text_field: "For example, if you eat a sausage, you’re getting some protein, but you’re also eating a lot of fat, too. Commander Mars explains",
          lower_text_field: "that if you eat fried food like chips and fried chicken, you are eating added fat."
      },
      13 => {
          upper_text_field: "That’s why the <span>food pyramid</span> says you should eat very few extra fats and sweets. Commander Mars explains that fats and oils",
          lower_text_field: "include things like mayonnaise, salad dressing and butter."
      },
      14 => {
          upper_text_field: "Commander Mars explains that your body needs some fats or oils, but just a little amount. Too much fat can do bad things, such",
          lower_text_field: "as damage your heart. Does anyone not like sweets? They taste so good it’s hard to resist them."
      },
      15 => {
          upper_text_field: "Commander Mars explains that you need to be careful not to eat too many sweets, fizzy drinks, biscuits and ice cream because",
          lower_text_field: "they contain too much sugar."
      },
      16 => {
          upper_text_field: "Commander Mars explains that your body does need sugar for energy. But many of the good foods in the <span>food pyramid</span>, such as",
          lower_text_field: "fruit and bread, already give your body good, <span>natural sugar</span>."
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then follow the instructions on the handout to make your own pizza.",
          main_video_slide: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}



}

  $rocket_words = {
          "Food pyramid" => {
              description: "Diagram representing the number of servings to be eaten each day from each basic food group.",
              image: "" },
          "Natural Sugar" => {
              description: "Sugar derived from plant materials, or sugars naturally present in plant products.",
              image: "" },
          "Dairy Product" => {
              description: "Food produced from the milk of mammals.",
              image: "" },
          "Meat" => {
              description: "The flesh of an animal, typically a mammal or bird used as food.",
              image: "" },
          "Fruit and Vegetables" => {
              description: "A <span class='rw_highlight'>fruit</span> is the mature ovary of a plant. A <span class='rw_highlight'>vegetable</span> is part of a plant used as food.",
              image: "" },
  }


end
