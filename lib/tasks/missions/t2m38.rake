desc "Lesson"
task t2m38: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Catching too many fish in an area of the sea so that there are not many fish left.",
          lower_text_field: "Tuna comes from the ocean. So do prawns. Many people around the world enjoy eating <span class='rw_highlight'>marine</span> food such as seaweed."
          },
      7 => {
          upper_text_field: "Major Saturn explains that the sea helps people on every <span class='rw_highlight'>continent</span> of the world. It has many different habitats for many",
          lower_text_field: "different plants and animals. It helps planet earth stay healthy. That’s why people have to be careful to protect the sea."
          },
      8 => {
          upper_text_field: "People can harm the sea by putting the wrong things in it, like rubbish and <span class='rw_highlight'>litter</span>. Sometimes people put things in by accident.",
          lower_text_field: "When an <span class='rw_highlight'>oil tanker</span> leaks oil into the sea, the oil slick kills many fish and birds."
          },
      9 => {
          upper_text_field: "Major Saturn explains that it also kills plankton, which may be too small to see but it is important to the habitat. When an accident",
          lower_text_field: "like an oil spill happens, people may come quickly to the rescue, but such a big mess is very hard to clean up."
          },
      10 => {
          upper_text_field: "People can also do harm to the sea by taking too much from it. For example, fishermen can ‘<span class='rw_highlight'>overfish</span>’ if they catch the same",
          lower_text_field: "kind of fish for a long time in one small region."
          },
      11 => {
          upper_text_field: "Major Saturn explains that sometimes, if fishermen catch too many of one kind of fish, not enough fish are left behind to have",
          lower_text_field: "babies, and then that kind of fish can become extinct."
          },
      12 => {
          upper_text_field: "When one kind of fish disappears, it can affect the lives of other animal and plants living in that part of the sea. How?"
          },
      13 => {
          upper_text_field: "Major Saturn explains that today many countries have laws telling people how many fish they are allowed to catch and during what",
          lower_text_field: "time of the year people can catch them. These laws are one way to keep <span class='rw_highlight'>overfishing</span> from damaging the habitat forever."
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }  

  $rocket_words = {
          "Marine" => {
              description: "Related to the sea or sea transport.",
              image: "" },
          "Continent" => {
              description: "One of seven large land masses on the Earth’s surface, mainly surrounded by sea.",
              image: "" },
          "Litter" => {
              description: "Small pieces of rubbish that have been left lying on the ground in public places.",
              image: "" },
          "Oil Tanker" => {
              description: "A ship that carries a large amount of oil.",
              image: "" },
          "Overfish" => {
              description: "Catching too many fish in an area of the sea so that there are not many fish left.",
              image: "" },
  }

  $questions = {
      "What does <span class='rw_highlight'>marine</span> mean?" => {answer: "Related to the sea or sea transport.",
                           image: "",
                           slide_id: 6},
      "What is a <span class='rw_highlight'>continent</span>?" => {answer: "One of seven large land masses on the Earth’s surface, mainly surrounded by sea.",
                                image: "",
                                slide_id: 7},
      "What is <span class='rw_highlight'>litter</span>?" => {answer: "Small pieces of rubbish that have been left lying on the ground in public places.",
                           image: "",
                           slide_id: 8},
      "What is an <span class='rw_highlight'>oil tanker</span>?" => {answer: "A ship that carries a large amount of oil.",
                              image: "",
                              slide_id: 8},
      "What does <span class='rw_highlight'>overfish</span> mean?" => {answer: "Catching too many fish in an area of the sea so that there are not many fish left.",
                              image: "",
                              slide_id: 10},
  }


  end