desc "Lesson"
task t1m5: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune says, ‘Look up. Look down. What part of your body do you use? Your <span class= 'rw_highlight'>eyes</span>. Point to your <span class= 'rw_highlight'>eyes</span>!’",
      },
      8 => {
          upper_text_field: "Captain Neptune says, ‘Sniff. What do you smell? What part of your body do you use? Your nose. Point to your nose!’",
      },
      9 => {upper_text_field: "Captain Neptune says, ‘Lick your lips. Can you <span class= 'rw_highlight'>taste</span> anything? What part of your body do you use? Your <span class= 'rw_highlight'>tongue</span>. Point to your <span class= 'rw_highlight'>tongue</span>!’",
      },
      10 => {
          upper_text_field: "Captain Neptune says, ‘Rub the top of your head and use your fingertips to <span class= 'rw_highlight'>touch</span> your hair. What does your hair feel like?’",
      },
      11 => {
          upper_text_field: "Captain Neptune explains that your five senses make your body aware of the things around you. You can see with your <span class= 'rw_highlight'>eyes</span>.",
          lower_text_field: "With your <span class= 'rw_highlight'>eyes</span> you can see how big things are, and what colours they are."
      },
      12 => {
          upper_text_field: "Did you know that some animals can’t see colours? For example, cats can’t see colours. But their <span class= 'rw_highlight'>sight</span> is better in the dark. At night",
          lower_text_field: "their eyes can see better than your <span class= 'rw_highlight'>eyes</span> can."
      },
      13 => {
          upper_text_field: "Captain Neptune explains that you can hear with your ears. You hear pleasant sounds, like a bird singing or someone reading to you.",
      },
      14 => {
          upper_text_field: "Captain Neptune describes how you hear loud sounds, like a siren of a fire engine. You hear quiet sounds, like a whisper.",
      },
      15 => {
          upper_text_field: "Captain Neptune explains that you can hear a lot, though some animals can hear more than you: a dog, for example, can hear",
          lower_text_field: "a special whistle that puts out a very high-pitched squealing sound that you may not be able to hear."
      },
      16 => {
          upper_text_field: "Captain Neptune explains how some animals can’t hear at all: you may hear a bee buzz, but the bees never hear their own",
          lower_text_field: "buzzing, because they don’t hear anything."
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Place the blindfold on your talk partner and ask them to guess the items on the tray by tasting them.",
      },
      18 => {complete_investigation_slide: ""},
      19 => {
          upper_text_field: "Today’s Mission Assignment: Place the blindfold on your talk partner. Ask them to guess the items on the tray by feeling them.",
      },
      20 => {complete_investigation_slide: ""},
      21 => {upper_text_field: "Today’s Mission Assignment: Put on the blindfold. Draw the shapes your partner calls out."},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }

end
