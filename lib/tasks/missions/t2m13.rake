desc "Lesson"
task t2m13: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains the importance of testing the <span class='rw_highlight'>temperature</span> of your bath water before you get in, you might stick your finger",
          lower_text_field: "in the water to see whether it’s too cold, too hot or just right."
      },
      7 => {
          upper_text_field: "Major Saturn warns that fingers aren’t really a very good way to measure <span class='rw_highlight'>temperature</span>. Some things, like a hot oven, are too hot",
          lower_text_field: "to touch. Major Saturn explains that when we want to know how hot or cold something is, we use a <span class='rw_highlight'>thermometer</span>."
      },
      8 => {
          upper_text_field: "<span class='rw_highlight'>Thermometers</span> help us measure the <span class='rw_highlight'>temperature</span> in units called <span class= 'rw_highlight'>degrees</span>. Major Saturn explains that when you go to a doctor’s",
          lower_text_field: "<span class='rw_highlight'>surgery</span>, the doctor or a nurse might use a special kind of <span class='rw_highlight'>thermometer</span> to take your <span class='rw_highlight'>temperature</span>."
      },
      9 => {
          upper_text_field: "Major Saturn explains that if you’re healthy, your <span class='rw_highlight'>temperature</span> will be 37 <span class='rw_highlight'>degree</span>, or close to that. If you’re ill and have a fever,",
          lower_text_field: "your <span class='rw_highlight'>temperature</span> might be 38 or 39 <span class='rw_highlight'>degrees</span>."
      },
      10 => {
          upper_text_field: "We use a different kind of <span class='rw_highlight'>thermometer</span> to tell us the <span class='rw_highlight'>temperature</span> of the air around us. Look at the picture of the weather",
          lower_text_field: "<span class='rw_highlight'>thermometer</spam>. A weather <span class='rw_highlight'>thermometer</span> has a tube running up and down its centre, usually with red or silver liquid in it."
      },
      11 => {
          upper_text_field: "This liquid goes higher in the tube when the <span class='rw_highlight'>temperature</span> gets warmer. Major Saturn explains when the <span class='rw_highlight'>temperature</span> gets cooler,",
          lower_text_field: "the liquid goes... what do you think? That’s right, lower."
      },
      12 => {
          upper_text_field: "In some countries the <span class= 'rw_highlight'>temperature</span> can be high. More than 30 <span class= 'rw_highlight'>degrees</span> means it is very hot outside, it can be difficult to work or",
          lower_text_field: "sleep and uncomfortable for your body. To cool down healthy bodies automatically <span class='rw_highlight'>perspire</span>."
      },
      13 => {
          upper_text_field: "Major Saturn explains that to read the <span class='rw_highlight'>thermometer</span>, you find where the liquid has stopped, then you look over to see the",
          lower_text_field: "number nearest the top of the liquid. Major Saturn wants you to find 18 <span class='rw_highlight'>degrees</span> on the <span class='rw_highlight'>thermometer</span>."
      },
      14 => {
          upper_text_field: "18 <span class='rw_highlight'>degrees</span> is a comfortable <span class='rw_highlight'>temperature</span> for most people, it’s not too hot and it’s not too cold. Older people often prefer a",
          lower_text_field: "slightly warmer room <span class='rw_highlight'>temperature</span>, perhaps around 21. Can you find that on the <span class='rw_highlight'>thermometer</span>? Now can you find 30?"
      },
      15 => {
          upper_text_field: "Can you find 0 (zero)? If you put a glass of water outside on a day when the outside <span class='rw_highlight'>temperature</span> is 0 <span class='rw_highlight'>degrees</span> or lower, do you",
          lower_text_field: "know what will happen to the water? The water will freeze: it will turn to ice."
      },
      16 => {
          upper_text_field: "So, if the <span class= 'rw_highlight'>temperature</span> outside is 0 <span class= 'rw_highlight'>degrees</span>, wrap up before you go out because it’s freezing outside. Can you find -5 on the",
          lower_text_field: "<span class='rw_highlight'>thermometer</span>?"
      },
      17 => {
          upper_text_field: "Today’s film explains how to make your own <span class='rw_highlight'>thermometer</span>.",
          main_video_slide: ""
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
