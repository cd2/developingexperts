desc "Lesson"
task t1m9: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that it’s not just trees that we have to be <span class='rw_highlight'>careful</span> about. We have to be <span class='rw_highlight'>careful</span> about how we use other",
          lower_text_field: "riches of the earth. We need to <span class='rw_highlight'>protect</span> our world."
      },
      7 => {
          upper_text_field: "For example, the earth has only so much fresh water for us to drink and use. So it’s important not to <span class='rw_highlight'>waste</span> water. You can help <span class='rw_highlight'>save</span> water",
          lower_text_field: "if you don’t let the water run in the sink when you’re not using it."
      },
      8 => {
          upper_text_field: "Captain Neptune explains that when you turn the tap off make sure to turn it firmly, you’re helping to <span class='rw_highlight'>conserve</span> water.",
          lower_text_field: "To ‘<span class='rw_highlight'>conserve</span>’ means to use something <span class='rw_highlight'>carefully</span>. It means to <span class='rw_highlight'>save</span> and not to <span class='rw_highlight'>waste</span>."
      },
      9 => {
          upper_text_field: "Captain Neptune explains that, if we’re <span class='rw_highlight'>careful</span> not to <span class='rw_highlight'>waste</span> what nature gives us, then we will have enough of what",
          lower_text_field: "we need. But if we <span class='rw_highlight'>waste</span> what nature gives us, then someday we may want something – like trees or fresh water – but be unable to find it."
      },
      10 => {
          upper_text_field: "Today’s Film:",
          main_video_slide: ""
      },
      11 => {
          upper_text_field: "Today’s Mission Assignment: Water collecting experiment",
      },
      12 => {
        upper_text_field: "Captain Neptune would like you get ready for today’s mission assignment and report to your work stations.", 
        sixty_count: ""},
      13 => {
          upper_text_field: "Today’s Mission Assignment: On your handout put a tick next to the picture that shows how people should save water.",
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Record how many times you use water at home. Put a tick in the frequency column every time you use water for each activity.",
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
  }

end
