desc "Lesson"
task t1m2: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune discovers that wild animals can take care of themselves. But pets need special care. You can learn a lot about",
          lower_text_field: "animals by taking care of a pet. Owning a pet is almost like being a parent. Captain Neptune thinks about what his pet hamster Freddie needs."
      },
      8 => {
          upper_text_field: "Captain Neptune has to provide food, water and a safe home for Freddie. Captain Neptune likes teaching and caring for Freddie.",
          lower_text_field: "If you do all of these things well, you’ll have a healthy pet. Who owns a pet and what are their needs?"
      },
      9 => {upper_text_field: "Captain Neptune explains that some jobs you need to do as a pet owner are less pleasant. When your dog does a poo you need to clean up after",
            lower_text_field: "it, otherwise it can be dangerous for other people. Discuss why you need to do this with your talk partner."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that cats and dogs have a special diet which is different from humans, it is called <span class= 'rw_highlight'>pet food</span>. They need",
          lower_text_field: "to be given the correct food otherwise they will become ill."
      },
      11 => {
          upper_text_field: "Captain Neptune explains that some people prefer dogs. They give them food and water and a place to sleep. Some dogs might have a",
          lower_text_field: "<span class= 'rw_highlight'>kennel</span>. Dogs love to take walks and to learn new tricks like fetching a ball, rolling over or catching a frisbee in mid-air!"
      },
      12 => {
          upper_text_field: "Captain Neptune explains that some people prefer cats and give them food and water and a place to sleep. Captain Neptune learns",
          lower_text_field: "that cats curl up on the sofa or at the foot of the bed. When cats feel happy, they purr."
      },
      13 => {
          upper_text_field: "When they’re frightened, cats arch their backs, their hair stands on end and their tails puff out like a big brush!"
      },
      14 => {
          upper_text_field: "Captain Neptune explains that some people prefer birds. They give them food and water and a clean, dry cage. Some birds sing. Some birds",
          lower_text_field: "even ‘talk’. Captain Neptune explains that people teach parrots to say funny things like ‘Hello, Sugar!’ and ‘What’s up, Doc?’"
      },
      15 => {
          upper_text_field: "Captain Neptune is surprised to discover that some people like to keep more unusual animals as pets, such as tropical fish, <span class= 'rw_highlight'>snakes</span>, <span class= 'rw_highlight'>iguanas</span>,",
          lower_text_field: "<span class= 'rw_highlight'>ferrets</span> or monkeys. No matter what kind of pet a person chooses, the animal will always need food, water, a safe home and loving care."
      },
      16 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: To make your own homemade dog biscuits. Watch the film to see how to do this."},
      17 => {upper_text_field: "Today’s Second Mission Assignment: Guest speaker from either the RSPCA or Guide Dogs for the Blind."},
      18 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
  }

end
