desc "Lesson"
task t2m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {
          upper_text_field: "Major Saturn wants to know how your broad bean planting is going? Explain how to plant broad beans to your talk partners.",
      },
      4 => {previous_rocket_word_slide: ""},
      5 => {ten_count_launch: ""},
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "Major Saturn is impressed that you’ve learnt that a baby plant sprouts from a seed and then uses the seed for food as it grows.",
          lower_text_field: "Did you know that you get food from seeds, too?"
      },
      8 => {
          upper_text_field: "Major Saturn explains that the corn in the picture is a type of seed you might eat. Corn is the seed of the corn plant that grows tall",
          lower_text_field: "in the farmer’s field. Did you know that you are eating rows of seed when you eat corn on the cob?"
      },
      9 => {
            upper_text_field: "Major Saturn learns that <span class= 'rw_highlight'>wheat</span> is the seed of a <span class='rw_highlight'>wheat</span> plant. <span class='rw_highlight'>Wheat</span> seeds are so hard that if you tried to eat them, they would",
            lower_text_field: "almost break your teeth. Major Saturn decides not to eat one! He learns that <span class='rw_highlight'>wheat</span> seed is ground into flour to make bread."
      },
      10 => {
        upper_text_field: "Major Saturn explains that peas are the seeds of a pea plant. Peas grow into long green pods and taste delicious!",
        
      },
      11 => {
          upper_text_field: "Major Saturn explains that green beans are the seed pods of a bean plant. When you eat a green bean, you are eating a pod of",
          lower_text_field: "a seed. If you pull one apart very carefully, you can see the little seeds inside."

      },
      12 => {
          upper_text_field: "Major Saturn learns that peanuts are the seeds of the peanut plant. He loves peanut butter so is keen to learn more! He finds",
          lower_text_field: "out that if you eat a peanut, you need to pull it apart very carefully because you may see the start of a tiny new peanut plant inside."
      },
      13 => {
          upper_text_field: "Major Saturn discovers that seeds aren’t the only part of plants that we like to eat because we eat roots such as onions, carrots",
          lower_text_field: "and <span class='rw_highlight'>radishes</span> too. We also eat plant stems like celery."
      },
      14 => {
          upper_text_field: "Major Saturn explains that we eat leaves like lettuce and cabbage. We even eat flowers. For example, when we eat <span class='rw_highlight'>broccoli</span> we are",
          lower_text_field: "eating the flower of a <span class='rw_highlight'>broccoli</span> plant, just before it blooms. Did you know <span class='rw_highlight'>broccoli</span> was a flower?"
      },
      15 => {
          upper_text_field: "Major Saturn expects that you already know that we eat the <span class='rw_highlight'>fruit</span> of many plants. Apples, pears and oranges are <span class='rw_highlight'>fruits</span>. For us,",
          main_video_slide: "these <span class='rw_highlight'>fruits</span> are food. As it grows, the <span class='rw_highlight'>fruit</span> protects the seeds that grow inside the <span class='rw_highlight'>fruit</span>. What <span class='rw_highlight'>fruit</span> is the girl eating?"
      },
      16 => {
          upper_text_field: "Major Saturn explains that in the plant world, some plants are a little confusing because tomatoes, green peppers and pumpkins",
          main_video_slide: "are <span class='rw_highlight'>fruit</span>, even though most of us call them <span class='rw_highlight'>vegetables</span>. They are <span class='rw_highlight'>fruit</span> because they hold the seeds inside them as they grow."
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Create plasticine models of <span class='rw_highlight'>fruit</span> and <span class='rw_highlight'>vegetables</span> then place them into their correct family on your worksheet.",
          main_video_slide: ""
      },
      18 => {
        upper_text_field: "Major Saturn wants you to place your created <span class='rw_highlight'>fruit</span> and <span class='rw_highlight'>vegetables</span> into the right grouping.", 
      },
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
  }

end
