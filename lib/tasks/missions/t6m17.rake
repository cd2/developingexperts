desc "Lesson"
task t6m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      11 => {
          upper_text_field: "",
          sixty_count: ""
      },
      12 => {complete_investigation_slide: ""},
      13 => {rocket_word_slide: ""},
      14 => {quiz_slide: ""},
      15 => {outro_slide: ""}
}

end
