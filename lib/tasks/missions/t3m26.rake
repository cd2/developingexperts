desc "Lesson"
task t3m26: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that the earth is like a great big magnet! The magnetic fields surrounding the earth are strongest near",
          lower_text_field: "the North Pole and the South Pole."
          },
      7 => {
          upper_text_field: "Colonel Jupiter explains that one end of your magnet is pointing north and the other end is pointing south. That’s why the poles",
          lower_text_field: "of your magnet are called north and south."
          },
      8 => {
          upper_text_field: "Now that you know that magnets have different poles, Colonel Jupiter wants you to go back and think about what happened",
          lower_text_field: "when you brought the ends of two bar magnets together."
          },
      9 => {
          upper_text_field: "Colonel Jupiter reminds us that once they pulled towards each other, but once they push apart. They were following a rule of",
          lower_text_field: "magnetic force that says: unlike poles <span class='rw_highlight'>attract</span> but like poles <span class='rw_highlight'>repel</span>."
          },
      10 => {
          upper_text_field: "Colonel Jupiter explains that <span class='rw_highlight'>repel</span> means push away. The north pole of a magnet pulls or <span class='rw_highlight'>attracts</span> the south pole of another",
          lower_text_field: "magnet. But if you bring poles together, they <span class='rw_highlight'>repel</span> each other – they push apart."
          },
      11 => {
          upper_text_field: "Colonel Jupiter explains that China specialises in building <span class='rw_highlight'>high speed</span> state of the art <span class='rw_highlight'>maglev trains</span>. One of their <span class='rw_highlight'>maglev</span>",
          lower_text_field: "<span class='rw_highlight'>trains</span> recently set a world record of just over 600km/h (373mph), just days after it broke its previous 12-year-old record."
          },
      12 => {
          upper_text_field: "The maglev train uses magnetic levitation to move vehicles without touching the ground. The <span class='rw_highlight'>maglev train</span> travels along",
          lower_text_field: "a guideway using magnets to create both lift and <span class='rw_highlight'>propulsion</span>, which reduces friction and enables very high speeds."
          },
      13 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      14 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      15 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }  



  $rocket_words = {
          "Attract" => {
              description: "To pull to or draw toward oneself or itself.",
              image: "" },
          "Repel" => {
              description: "To force back or push away.",
              image: "" },
          "Propulsion" => {
              description: "The action of driving or pushing forwards.",
              image: "" },
          "Maglev Train" => {
              description: "A <span class='rw_highlight'>high speed train</span> used in China, which is powered by magnetic fields.",
              image: "" },
          "High Speed Train" => {
              description: "A train which travels very fast. Their top speeds are at least 250 km/h (155 mph).",
              image: "" },
  }

  $questions = {
      "What does <span class='rw_highlight'>attract</span> mean?" => {answer: "To pull to or draw toward oneself or itself.",
                           image: "",
                           slide_id: 9},
      "What does <span class='rw_highlight'>repel</span> mean?" => {answer: "To force back or push away.",
                                image: "",
                                slide_id: 9},
      "What is <span class='rw_highlight'>propulsion</span>?" => {answer: "The action of driving or pushing forwards.",
                           image: "",
                           slide_id: 12},
      "What is the <span class='rw_highlight'>Maglev</span>?" => {answer: "A <span class='rw_highlight'>high speed train</span> used in China, which is powered by magnetic fields.",
                              image: "",
                              slide_id: 11},
      "What is a <span class='rw_highlight'>high speed train</span>?" => {answer: "A train which travels very fast. Their top speeds are at least 250 km/h (155 mph).",
                              image: "",
                              slide_id: 11},
  }

  end