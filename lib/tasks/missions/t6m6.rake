desc "Lesson"
task t6m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "General Mercury explains that <span class='rw_highlight'>Charles Darwin</span> was born in Shrewsbury, where his father was a doctor. He studied at",
          lower_text_field: "Cambridge, where he became interested in all of the different species of plants and animals."
      },
      7 => {
          upper_text_field: "At aged 22, he was invited to sail to South America, working as a naturalist on the ship <span class='rw_highlight'>HMS Beagle</span>. For five years he sailed",
          lower_text_field: "the world and collected samples of rocks and animals, just as he had loved collecting shells when he was a little boy."
          },
      8 => {
          upper_text_field: "General Mercury explains how <span class='rw_highlight'>Charles Darwin</span> found rocks containing fossils of things that had lived millions of years ago",
          lower_text_field: "but had become extinct."
          },
      9 => {
          upper_text_field: "General Mercury informs us that the <span class='rw_highlight'>HMS Beagle</span> reached the <span class='rw_highlight'>Galapagos Islands</span>. in the Pacific Ocean, far off the South",
          lower_text_field: "American coast, <span class='rw_highlight'>Darwin</span> found lots of birds like finches, he noticed that they were very similar apart from their beaks."
          },
      10 => {
          upper_text_field: "General Mercury explains that some birds had big, powerful beaks whilst others had thinner, sharper beaks which varied in",
          lower_text_field: "length and width like shoe sizes in a shoe shop."
          },
      11 => {
          upper_text_field: "General Mercury explains how <span class='rw_highlight'>Darwin</span> considered whether one species from long ago had gradually separated into several",
          lower_text_field: "different forms. He discovered that different species, each with its own characteristics, had evolved from a shared <span class='rw_highlight'>ancestor</span>."
          },
      12 => {
          upper_text_field: "General Mercury explains how <span class='rw_highlight'>Darwin</span> explored the different islands of the <span class='rw_highlight'>Galapagos</span>, and he found that the tortoises were",
          lower_text_field: "gigantic, but they were not all the same. On some hot islands they had long necks for feeding on tall, spiky cactus plants."
          },
      13 => {
          upper_text_field: "General Mercury explains that on cooler islands where there was plenty of grass to eat at ground level, the tortoises had short",
          lower_text_field: "necks. It suggested to <span class='rw_highlight'>Darwin</span> that the tortoises had slowly adapted to suit the islands’ different habitats."
          },
      14 => {
          upper_text_field: "The different species of giant tortoises of the <span class='rw_highlight'>Galapagos</span> all shared a common parent many generations back, but this",
          lower_text_field: "original species of tortoise evolved to have different characteristics in the different habitats of the different islands."
          },
      15 => {
          upper_text_field: "When he came home, <span class='rw_highlight'>Darwin</span> spent several years sorting out his idea. Only when another scientist, Alfred Russell Wallace,",
          lower_text_field: "described some similar ideas did <span class='rw_highlight'>Darwin</span> publish his great book on the Origin of Species by Means of <span class='rw_highlight'>Natural Selection</span>."
          },
      16 => {
          upper_text_field: "General Mercury explains that ‘<span class='rw_highlight'>Natural selection</span>’ was <span class='rw_highlight'>Charles Darwin’s</span> term for the idea that those animals and plants best",
          lower_text_field: "suited to their environment are more likely to survive and reproduce. Other people call it ‘survival of the fittest’."
          },
      17 => {
          upper_text_field: "<span class='rw_highlight'>Charles Darwin</span> wrote a book entitled ‘Origin of the Species’ to explain his theory of evolution. It took many years for people to",
          lower_text_field: "accept the idea. Some did not like the thought that we shared common <span class='rw_highlight'>ancestors</span> with apes."
          },
      18 => {
          upper_text_field: "With so many facts to support it, <span class='rw_highlight'>Darwin’s</span> theory was gradually accepted by many people. He joined the Royal Society and was",
          lower_text_field: "eventually honoured with burial in Westminster Abbey, very near to Isaac Newton."
          },
      19 => {
          upper_text_field: "Today’s Mission Assignment",
          main_video_slide: ""
          },
      20 => {
          upper_text_field: "",
          sixty_count: ""
      },
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
}




  $rocket_words = {
          "Charles Darwin" => {
              description: "An English naturalist and geologist, best known for his theory of evolution.",
              image: "" },
          "Galapagos Islands" => {
              description: "Volcanic islands distributed on either side of the Equator in the Pacific Ocean.",
              image: "" },
          "Ancestor" => {
              description: "A person from whom one is descended.",
              image: "" },
          "Natural Selection" => {
              description: "Is the survival and reproduction of the fittest.",
              image: "" },
          "HMS Beagle" => {
              description: "The ship in which naturalist <span class='rw_highlight'>Charles Darwin</span> set sail, it’s one of the most famous ships in history.",
              image: "" },
  }

  $questions = {
      "Who was <span class='rw_highlight'>Charles Darwin</span>?" => {answer: "An English naturalist and geologist, best known for his theory of evolution.",
                           image: "",
                           slide_id: 6},
      "What are the <span class='rw_highlight'>Galapagos Islands</span>?" => {answer: "Volcanic islands distributed on either side of the Equator in the Pacific Ocean.",
                                image: "",
                                slide_id: 9},
      "What are <span class='rw_highlight'>ancestors</span>?" => {answer: "A person from whom one is descended.",
                           image: "",
                           slide_id: 11},
      "What is <span class='rw_highlight'>natural selection</span>?" => {answer: "Is the survival and reproduction of the fittest.",
                              image: "",
                              slide_id: 15},
      "What was the <span class='rw_highlight'>HMS Beagle</span>?" => {answer: "The ship in which naturalist <span class='rw_highlight'>Charles Darwin</span> set sail, it’s one of the most famous ships in history.",
                              image: "",
                              slide_id: 7},
  }

end
