desc "Lesson"
task t3m36: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter asks ‘Why is it dangerous to cross the road when it is <span class='rw_highlight'>dark</span> at night?’ Did you think of your size compared to a car?",
          lower_text_field: "Did you think about how <span class='rw_highlight'>dark</span> it is at night?"
          },
      7 => {main_video_slide: "",
          upper_text_field: "Today’s Film: Take a look at this film about how to ride your bike at night safely.",
          },
      8 => {
          upper_text_field: "Colonel Jupiter asks ‘What safety steps can you take to improve your <span class='rw_highlight'>road safety</span> at night?’ <span class='rw_highlight'>Reflective</span> strips can be applied on",
          lower_text_field: "bags and coats which help improve your visibility at night. Discuss how <span class='rw_highlight'>reflective material</span> works with your talk partner."
          },
      9 => {
          upper_text_field: "Colonel Jupiter explains that, in addition to wearing the right type of clothing at night, you can also improve your <span class='rw_highlight'>road safety</span>",
          lower_text_field: "by following simple rules. 1. Find a safe place to cross. 2. Stop just before you get to the kerb."
          },
      10 => {
          upper_text_field: "3. Look all around for traffic and listen. 4. If traffic is coming, let it pass. ",
          lower_text_field: "5. When it is safe, go straight across the road – do not run."
          },
      11 => {
          upper_text_field: "Colonel Jupiter explains that different <span class='rw_highlight'>materials</span> <span class='rw_highlight'>reflect</span> light. The pretty colours you see in bubbles are caused by light that is",
          lower_text_field: "<span class='rw_highlight'>reflected</span> off the walls of the bubble."
          },
      12 => {
          upper_text_field: "Colonel Jupiter explains that when light is <span class='rw_highlight'>reflected</span>, light touches the outside of the bubble and <span class='rw_highlight'>bounces</span> off it, then it goes to your",
          lower_text_field: "eye and you see colour! As the surface of a bubble gets thinner, you will see different colours."
          },
      13 => {
          upper_text_field: "Colonel Jupiter explains that when you first blow a bubble, you will probably see green and blue, then <span class='rw_highlight'>magenta</span> or purple, and",
          lower_text_field: "then just before the bubble pops, most of it will be a dark golden yellow colour, or almost black."
          },
      14 => {
          upper_text_field: "Colonel Jupiter explains that sometimes the surface of the bubble is very thin on some parts, and thicker on other parts.",
          lower_text_field: "When that happens, you will see lots of different colours on the bubble at once making it look like a rainbow!"
          },
      15 => {
          upper_text_field: "Colonel Jupiter suggests that next time you are outside blowing bubbles, see if you can tell when they are about to pop.",
          lower_text_field: "Remember most of the bubble will probably be dark yellow or black just before it pops!"
          },
      16 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      17 => {upper_text_field: "Today’s Second Mission Assignment: "},
      18 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
  }



  $rocket_words = {
          "Reflective Material" => {
              description: "<span class='rw_highlight'>Reflective</span> materials help you to be more visible in the dark.",
              image: "" },
          "Road Safety" => {
              description: "Steps you need to follow in order to make yourself safer on the road.",
              image: "" },
          "Bounce" => {
              description: "Move quickly up, back, or away from a surface after hitting it.",
              image: "" },
          "Dark" => {
              description: "With little or no light.",
              image: "" },
          "Magenta" => {
              description: "A colour.",
              image: "" },
  }

  $questions = {
      "What is a reflective material?" => {answer: "<span class='rw_highlight'>Reflective</span> materials help you to be more visible in the dark.",
                           image: "",
                           slide_id: 8},
      "What is road safety?" => {answer: "Steps you need to follow in order to make yourself safer on the road.",
                                image: "",
                                slide_id: 8},
      "What does bounce mean?" => {answer: "Move quickly up, back, or away from a surface after hitting it.",
                           image: "",
                           slide_id: 12},
      "What does dark mean?" => {answer: "With little or no light.",
                              image: "",
                              slide_id: 6},
      "What is magenta?" => {answer: "A colour.",
                              image: "",
                              slide_id: 13},
  }

end
