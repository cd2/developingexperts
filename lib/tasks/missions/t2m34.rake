desc "Lesson"
task t2m34: :environment do

 slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants to know if learning about different habitats, and the food animals eat makes you hungry? You’ve got to eat",
          lower_text_field: "to live. Not just you, but every living thing needs food to survive."
          },
      7 => {
          upper_text_field: "Major Saturn explains that plants make their own food out of sunshine, air, water and nutrients from the soil in which they grow.",
          lower_text_field: "Animals eat other living things, including plants and other animals. Big animals may eat little ones."
          },
      8 => {
          upper_text_field: "When the big animal dies, it may be eaten by little animals. Major Saturn tells us that all this eating is called the <span class='rw_highlight'>food chain</span>.",
          },
      9 => {
          upper_text_field: "Major Saturn wants you to imagine a green plant growing by the side of a river. A <span class='rw_highlight'>caterpillar</span> comes along and chews on the",
          lower_text_field: "leaves. Later the <span class='rw_highlight'>caterpillar</span> grows into a flying <span class='rw_highlight'>insect</span>."
          },
      10 => {
          upper_text_field: "Major Saturn explains that the <span class='rw_highlight'>insect</span> flies across the river, when suddenly, swoosh, a <span class='rw_highlight'>fish</span> leaps out of the water and swallows it.",
          lower_text_field: "The <span class='rw_highlight'>fish</span> splashes back into the water, feeling full and happy – but not for long."
          },
      11 => {
          upper_text_field: "Major Saturn describes how a big bear reaches into the river and grabs the <span class='rw_highlight'>fish</span> in his paw. The bear has caught a tasty supper.",
          },
      12 => {
          upper_text_field: "Major Saturn describes how later that year the bear dies, and through the winter its body rots away. The rotting body turns into",
          lower_text_field: "nutrients that soak into the soil by the side of the river. When spring comes, the nutrients help green plants grow."
          },
      13 => {
          upper_text_field: "Major Saturn explains that one of those green plants grows by the side of the river. A <span class='rw_highlight'>caterpillar</span> comes along and chews on the",
          lower_text_field: "leaves and... do you see? It’s a <span class='rw_highlight'>life cycle</span>, starting again, and going round and round."
          },
      14 => {
          upper_text_field: "Major Saturn explains that it’s a cycle of one creature feeding on another, a cycle of life and death and life again. People call this",
          lower_text_field: "cycle the <span class='rw_highlight'>food chain</span> because it seems to link together the plants and animals in nature."
          },
      15 => {
          upper_text_field: "Major Saturn explains that animals eat plants, and these animals are sometimes eaten by other animals. Plants and animals die",
          lower_text_field: "and rot, which returns nutrients to the soil, which helps more plants grow."
          },
      17 => {
          upper_text_field: "It’s all a part of the <span class='rw_highlight'>food chain</span> that keeps nature alive, and it all starts with plants growing from sunshine, air, water and nutrients.",
          },
      18 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      19 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
  }

    $rocket_words = {
          "Food Chain" => {
              description: "A series of living things that are connected because each group eats the group below it.",
              image: "" },
          "Caterpillar" => {
              description: "A small, long grub with many legs and develops into a butterfly or moth.",
              image: "" },
          "Insect" => {
              description: "A type of small animal with six legs, a body divided into three parts and usually two pairs of wings.",
              image: "" },
          "Fish" => {
              description: "An animal that lives in water, is covered with scales, and breathes through its gills.",
              image: "" },
          "Life Cycle" => {
              description: "The different stages a living thing goes through during its life.",
              image: "" },
  }

  $questions = {
      "What is the <span class='rw_highlight'>food chain</span>?" => {answer: "A series of living things that are connected because each group eats the group below it.",
                           image: "",
                           slide_id: 8},
      "What is a <span class='rw_highlight'>caterpillar</span>?" => {answer: "A small, long grub with many legs and develops into a butterfly or moth.",
                                image: "",
                                slide_id: 9},
      "What is an <span class='rw_highlight'>insect</span>?" => {answer: "A type of small animal with six legs, a body divided into three parts and usually two pairs of wings.",
                           image: "",
                           slide_id: 9},
      "What is a <span class='rw_highlight'>fish</span>?" => {answer: "An animal that lives in water, is covered with scales, and breathes through its gills.",
                              image: "",
                              slide_id: 10},
      "What is the <span class='rw_highlight'>life cycle</span>?" => {answer: "The different stages a living thing goes through during its life.",
                              image: "",
                              slide_id: 13},
  }

end
