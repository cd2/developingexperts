desc "Lesson"
task t5m7: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that the study of weather is called Meteorology, coming from a Greek word meteoros, meaning",
          lower_text_field: "‘high in the air’ and the suffix–ology, meaning ‘study of’. The
people who study weather are called meteorologists."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains what happens when it feels damp outside. Some water vapour mixes with the air near the ground",
          lower_text_field: "and some of it rises high into the sky, way up where the air is
cooler."
      },
      8 => {
          upper_text_field: "In this coolness, the water vapour turns back into little droplets of liquid water. Brigadier Venus explains that when water vapour",
          lower_text_field: "turns from gas back into liquid, we say it <Span>condenses</span>."
      },
      9 => {
          upper_text_field: "Here’s a way to see <span>condensation</span> happen. Fill a glass with ice and water. Make sure the outside of the glass stays nice and dry.",
          lower_text_field: "Let it sit for a little while, maybe five or ten minutes."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains that pretty soon, the outside of the glass will develop a thin coating of water. Feel it – it’s wet. Now",
          lower_text_field: "where did the water come from? Your glass didn’t leak. No the water came from the air."
      },
      11 => {
          upper_text_field: "Brigadier Venus explains that the ice water made the glass cold, which made the air around the glass cool, just like the air high",
          lower_text_field: "in the sky. Then the water vapour in the air <Span>condensed</span> – it turned into liquid – on the outside of your glass."
      },
      12 => {
          upper_text_field: "Brigadier Venus explains that when water vapour <Span>condenses</span> into droplets of liquid up in the sky it forms <span>clouds</span>. Even though",
          lower_text_field: "they may look like candyfloss, <Span>clouds</span> are made of billions of water droplets."
      },
      13 => {
          upper_text_field: "Brigadier Venus explains that wispy feathery <Span>clouds</span> are <span>cirrus clouds</span>. They form high in the sky and are made of tiny ice",
          lower_text_field: "crystals. In the <Span>clouds</span>, the water droplets bump up against each other and become bigger drops."
      },
      14 => {
          upper_text_field: "Brigadier Venus explains that when the drops get heavy enough, they fall from the <Span>clouds</span> – it’s raining! Or, if it’s cold enough,",
          lower_text_field: "instead of rain, snow will fall."
      },
      15 => {
          upper_text_field: "Brigadier Venus explains that snow is water frozen into tiny crystals that fall as snowflakes. Big, puffy <span>clouds</span> are <span>cumulus</span>",
          lower_text_field: "<span>clouds</span>. They are usually signs of fair weather. When a dark layer of <span>stratus clouds</span> cover the sky, it often means rain is on its way."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then complete the experiment with your partner.",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "Brigadier Venus wants you to report to your work stations for today’s mission assignment.",
          sixty_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
