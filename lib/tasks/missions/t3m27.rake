desc "Lesson"
task t3m27: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter reminds us that we use magnets in many ways. One of the most useful things a magnet can do is tell us what",
          lower_text_field: "<span class='rw_highlight'>direction</span> we’re going – north, south, east or west. When a magnet is used in this way, we call it a <span class='rw_highlight'>compass</span>."
          },
      7 => {
          upper_text_field: "Colonel Jupiter explains that <span class='rw_highlight'>compasses</span> help sailors find their way at sea. They help ramblers find their way through forests.",
          lower_text_field: "They can help you find your way, too."
          },
      8 => {
          upper_text_field: "Colonel Jupiter explains that if you tie a bar magnet to a string, you have made a kind of <span class='rw_highlight'>compass</span>. Most <span class='rw_highlight'>compasses</span> have a",
          lower_text_field: "small magnet in the shape of an arrow, called a <span class='rw_highlight'>needle</span>."
          },
      9 => {
          upper_text_field: "Colonel Jupiter explains that this <span class='rw_highlight'>magnetic needle</span> can spin around When it stops spinning, the <span class='rw_highlight'>needle</span> always points north.",
          lower_text_field: "Take a <span class='rw_highlight'>compass</span> and turn it around. You’ll see that the <span class='rw_highlight'>needle</span> always points the same way – north."
          },
      10 => {
          upper_text_field: "But what if you want to go east? Colonel Jupiter wants to know how would you use the compass to do that? First, face in the",
          lower_text_field: "<span class='rw_highlight'>direction</span> of the <span class='rw_highlight'>needle</span> – north. So, what <span class='rw_highlight'>direction</span> is behind you?"
          },
      11 => {
          upper_text_field: "Colonel Jupiter says, ‘That’s right, south. Which way is east?’ Is it to your right or left? East is to your right, and west is to your",
          lower_text_field: "left. That’s the way the map works, remember? The top is north, the bottom is south, the right is east and the left is west."
          },
      12 => {
          upper_text_field: "(‘Left’ and ‘west’ sound alike, remember?) So, if you need to go east, Colonel Jupiter wants to know what do you do? Your",
          lower_text_field: "<span class='rw_highlight'>compass</span> <span class='rw_highlight'>needle</span> points north. East is to the right of where the <span class='rw_highlight'>needle</span> points."
          },
      13 => {
          upper_text_field: "Colonel Jupiter explains that people use <span class='rw_highlight'>compasses</span> to work out the <span class='rw_highlight'>direction</span> to take when <span class='rw_highlight'>orienteering</span>. If you are on a mountain",
          lower_text_field: "top surrounded by fog, your <span class='rw_highlight'>compass</span> will help you find your way back down the mountain safely."
          },
      14 => {main_video_slide: "",
          upper_text_field: "Today’s Film: Roller coasters use magnetic forces to create propulsion. Do you know any other modes of transport which do",
          lower_text_field: "this too?"
          },
      15 => { main_video_slide: "",
              upper_text_field: "Today’s First Mission Assignment: Today’s Mission Assignment: (Option 1 school outing) To complete an <span class='rw_highlight'>orienteering</span> course."},
      16 => { main_video_slide: "",
              upper_text_field: "Today’s Second Mission Assignment: Today’s Mission Assignment: (Option 2) Create your own magnetic <span class='rw_highlight'>compass</span>, then use it to navigate to a set point."},
      17 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
  }




  $rocket_words = {
          "Compass" => {
              description: "An instrument containing a magnetized pointer which shows direction.",
              image: "" },
          "Magnetic Needle" => {
              description: "A piece of magnetised steel used as an indicator on the dial of a compass.",
              image: "" },
          "Direction" => {
              description: "A course along which someone or something moves.",
              image: "" },
          "Orienteering" => {
              description: "A sport where you have to find your way across a route with the aid of a map and compass.",
              image: "" },
          "Roller Coaster" => {
              description: "Fairground ride that consists of a railway track which has many tight turns and steep slopes.",
              image: "" },
  }

end
