desc "Lesson"
task t2m37: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }  

  $rocket_words = {
          "Plankton" => {
              description: "Tiny plants and animals that which are eaten by other sea creatures.",
              image: "" },
          "Blue Whale" => {
              description: "A very large sea mammal that breathes air through a hole at the top of its head.",
              image: "" },
          "Valley" => {
              description: "An area of low land between hills or mountains, above or below water.",
              image: "" },
          "Mountain" => {
              description: "A raised part of the Earth’s surface, much larger than a hill, above or below water.",
              image: "" },
          "Trench" => {
              description: "A narrow hole that is dug or formed in the ground.",
              image: "" },
  }



  end