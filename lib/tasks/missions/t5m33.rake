desc "Lesson"
task t5m33: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }  

  $rocket_words = {
          "Elasticity" => {
              description: "The quality or state of being elastic makes a material adaptable.",
              image: "" },
          "Plasticity" => {
              description: "The quality of being able to be made into different shapes.",
              image: "" },
          "Absorbency" => {
              description: "The quality or state of being able to draw in or soak up.",
              image: "" },
          "Waterproof" => {
              description: "To treat with a material (as a solution of rubber) to prevent permeation by water.",
              image: "" },
          "Flexibility" => {
              description: "Capable of bending or being bent.",
              image: "" },
  }

end