desc "Lesson"
task t2m18: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants you to <span class='rw_highlight'>squeeze</span> one of your arms. Can you feel the solid bone inside? You feel something firm around",
          lower_text_field: "it, too, that’s a <span class='rw_highlight'>muscle</span>."
      },
      7 => {
          upper_text_field: "Major Saturn explains that <span class='rw_highlight'>muscles</span> wrap around bones and <span class='rw_highlight'>stretch</span> from one bone to another. Hold one hand under your",
          lower_text_field: "arm, between your wrist and your elbow."
      },
      8 => {
          upper_text_field: "Major Saturns wants you to make a <span class='rw_highlight'>tight</span> fist with the arm that you are holding. Do you feel something <span class='rw_highlight'>tighten</span> up inside?",
      },
      9 => {
          upper_text_field: "Major Saturn explains that’s your <span class='rw_highlight'>muscle</span>. <span class='rw_highlight'>Muscles</span> make you move. You use your <span class='rw_highlight'>muscles</span> to walk, run jump, draw, <span class='rw_highlight'>stretch</span>",
          lower_text_field: "and lift. You even use your <span class='rw_highlight'>muscles</span> to talk, yawn, laugh, <span class='rw_highlight'>wink</span> and sing."
      },
      10 => {
          upper_text_field: "Today’s Film on <span class= 'rw_highlight'>Muscles</span>:",
          main_video_slide: ""
      },
      11 => {
          upper_text_field: "Today’s Mission Assignment:"
      },
      12 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },      
      13 => {complete_investigation_slide: ""},
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
  }

end
