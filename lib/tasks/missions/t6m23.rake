desc "Lesson"
task t6m23: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "General Mercury wants to know if you have you ever tasted sweet, golden honey? Do you know where honey comes from?",
          lower_text_field: "From the nests of honey bees! Where do honey bees get the honey?"
          },
      7 => {
          upper_text_field: "General Mercury explains that they make it from sweet liquid, called nectar that they gather from hundreds of flowers. In their",
          lower_text_field: "homes, called a <span class='rw_highlight'>beehive</span>, the bees feed the sweet honey to the young larvae."
          },
      8 => {
          upper_text_field: "General Mercury explains that like ants, honey bees can communicate with each other by giving off chemical scent. But",
          lower_text_field: "honey bees can communicate in another way, too."
          },
      9 => {
          upper_text_field: "General Mercury explains that when a honey bee finds a field of flowers with lots of nectar, she can tell the other bees in her",
          lower_text_field: "<span class='rw_highlight'>beehive</span> where to find those flowers. How? By doing a <span class='rw_highlight'>‘waggle dance’</span>!"
          },
      10 => {
          upper_text_field: "Captain Mercury explains that she turns from side to side, flaps her wings and buzzes. Her dances imitates how she flew to the",
          lower_text_field: "flowers."
          },
      11 => {
          upper_text_field: "Captain Mercury explains that the other bees understand her message and they fly to the flowers. One of the most important",
          lower_text_field: "things that bees do is to take pollen from one flower and put it on another flower. Pollination enables the flowers to make seeds."
          },
      12 => {
          upper_text_field: "Captain Mercury explains that in a honey bee hive, there are three different kinds of bees. Each hive has one <span class='rw_highlight'>queen bee</span>.",
          lower_text_field: "She is female, and she grows bigger than the rest. Her job is to lay all of the eggs for the <span class='rw_highlight'>beehive</span>."
          },
      13 => {
          upper_text_field: "She is the mother of every bee in the hive. In the spring and summer, she can lay thousands of eggs in a single day! In a",
          lower_text_field: "<span class='rw_highlight'>beehive</span>, the queen is surrounded by many bees that are constantly feeding her and cleaning her."
          },
      14 => {
          upper_text_field: "General Mercury explains that these are the <span class='rw_highlight'>worker bees</span>. Most of the bees in a hive are <span class='rw_highlight'>worker bees</span>. They are female, but they",
          lower_text_field: "do not lay eggs (only the <span class='rw_highlight'>queen</span> does that). The workers do a lot more than take care of the <span class='rw_highlight'>queen</span>."
          },
      15 => {
          upper_text_field: "General Mercury explains that they make a special wax from their own bodies. They use the wax to make thousands of little",
          lower_text_field: "six-sided compartments in which honey is stored. The honey is food for each baby bee growing in one of the little larvae."
          },
      16 => {
          upper_text_field: "General Mercury explains that they fly out from the hive and find flowers with sweet nectar, so they can make honey. Besides the",
          lower_text_field: "one <span class='rw_highlight'>queen bee</span> and the many <span class='rw_highlight'>worker bees</span>, a honey bee hive has many <span class='rw_highlight'>drone bees</span>. The drones are male."
          },
      17 => {
          upper_text_field: "General Mercury explains that compared to the female <span class='rw_highlight'>worker bees</span>, <span class='rw_highlight'>drones</span> seem pretty lazy. They do not gather nectar or",
          lower_text_field: "make honey or build the hive."
          },
      18 => {
          upper_text_field: "Captain Mercury explains that their only job is to mate with the queen to help her make more bees. They are the fathers of",
          lower_text_field: "every bee in the hive."
          },
      19 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      20 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      21 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
  }  

  $rocket_words = {
          "Waggle Dance" => {
              description: "A waggle movement performed by a honey bee at the hive, to show a source of food.",
              image: "" },
          "Beehive" => {
              description: "A box-like or dome-shaped structure in which bees are kept.",
              image: "" },
          "Queen Bee" => {
              description: "The single reproductive female in a hive or colony of honey bees.",
              image: "" },
          "Worker Bee" => {
              description: "Any female bee that lacks the full reproductive capacity of the colony's <span class='rw_highlight'>queen bee</span>.",
              image: "" },
          "Drone Bee" => {
              description: "Stingless male bee in a colony of bees whose sole function is to mate with the <span class='rw_highlight'>queen bee</span>.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>waggle dance</span>?" => {answer: "A waggle movement performed by a honey bee at the hive, to show a source of food.",
                           image: "",
                           slide_id: 10},
      "What is a <span class='rw_highlight'>beehive</span>?" => {answer: "A box-like or dome-shaped structure in which bees are kept.",
                                image: "",
                                slide_id: 7},
      "What is a <span class='rw_highlight'>queen bee</span>?" => {answer: "The single reproductive female in a hive or colony of honey bees.",
                           image: "",
                           slide_id: 12},
      "What is a <span class='rw_highlight'>worker bee</span>?" => {answer: "Any female bee that lacks the full reproductive capacity of the colony's <span class='rw_highlight'>queen bee</span>.",
                              image: "",
                              slide_id: 14},
      "What is a <span class='rw_highlight'>drone bee</span>?" => {answer: "Stingless male bee in a colony of bees whose sole function is to mate with the <span class='rw_highlight'>queen bee</span>.",
                              image: "",
                              slide_id: 16},
  }

end