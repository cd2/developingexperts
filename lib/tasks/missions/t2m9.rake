desc "Lesson"
task t2m9: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants to know ‘What do these things have in common: an apple, a river and the <span class='rw_highlight'>air</span> we breathe?’ They may",
          lower_text_field: "seem very different, but they’re alike in one way: they are all made of <span class='rw_highlight'>matter</span>."
      },
      7 => {
          upper_text_field: "Major Saturn explains how <span class='rw_highlight'>matter</span> is the stuff that makes up all the things in the world: your shoes, a flower, an egg, a dog, a rock,",
          lower_text_field: "a tyre, a book, a cloud, a goldfish, an aeroplane, a pencil. <span class='rw_highlight'>Matter</span> makes up each of these things and everything else as well."
      },
      8 => {
          upper_text_field: "<span class='rw_highlight'>Matter</span> even makes up your body. Let’s go back to our first three examples: an apple, a river and the <span class= 'rw_highlight'>air</span> we breathe."
      },
      9 => {
          upper_text_field: "You can see and touch the <span class='rw_highlight'>matter</span> in an apple. It’s <span class='rw_highlight'>solid</span>. Can you think of some other <span class= 'rw_highlight'>matter</span> that’s <span class= 'rw_highlight'>solid</span>? How about a rock?",
          lower_text_field: "A football? Your shoes?"
      },
      10 => {
          upper_text_field: "Major Saturn explains how you can see and touch the <span class='rw_highlight'>matter</span> in a river, too. It’s not <span class='rw_highlight'>solid</span>, though, or hard like a rock. It’s <span class='rw_highlight'>liquid</span>.",
          lower_text_field: "Can you think of some other <span class='rw_highlight'>matter</span> that’s <span class='rw_highlight'>liquid</span>? Such as milk? Or the saliva in your mouth?"
      },
      11 => {
          upper_text_field: "Major Saturn asks, ‘What about the <span class='rw_highlight'>air</span> we breathe?’ It’s different: it’s not a <span class='rw_highlight'>solid</span> or a <span class='rw_highlight'>liquid</span>. You can’t see it. You can’t reach out and",
          lower_text_field: "touch it. But sometimes you can feel it, like when the wind blows. When you feel the wind on your face, you are feeling the <span class='rw_highlight'>matter</span> in the <span class='rw_highlight'>air</span>."
      },
      12 => {
          upper_text_field: "Major Saturn wants you to think about blowing up a balloon. What goes inside? Some stuff goes into the balloon and makes it",
          lower_text_field: "bigger. That stuff is <span class='rw_highlight'>air</span>, and <span class='rw_highlight'>air</span> is <span class='rw_highlight'>matter</span>. But <span class= 'rw_highlight'>air</span> is a different kind of <span class='rw_highlight'>matter</span>: it’s not a <span class='rw_highlight'>solid</span>. <span class='rw_highlight'>Air</span> is a <span class='rw_highlight'>gas</span>."
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: How to turn a <span class='rw_highlight'>liquid</span> into a <span class='rw_highlight'>solid</span>.",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Report to your work stations for today’s Mission Assignment. You have 60 seconds.", 
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}



end
