desc "Lesson"
task t4m16: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars suspects that you’ve probably seen the telephone lines running from pole to pole, carrying messages",
          lower_text_field: "back and forth from homes and businesses around the world."
      },
      7 => {
          upper_text_field: "Commander Mars wants you to imagine your brain as being the central communications headquarters, then you can think of",
          lower_text_field: "your <Span>nerves</span> as the wires running throughout your body."
      },
      8 => {
          upper_text_field: "Commander Mars explains that the medulla connects your brain is a thick bundle of nerve fibres called your <span>spinal cord</span>. The",
          lower_text_field: "<span>spinal cord</span> runs through your backbone, through a hole in each vertebra."
      },
      9 => {
          upper_text_field: "Commander Mars explains that your <span>spinal cord</span> connects to many <span>nerves</span> that stretch throughout your body, branching out to",
          lower_text_field: "your legs, arms, toes and fingers. Your <Span>nerves</span> carry messages back and forth from your brain."
      },
      10 => {
          upper_text_field: "Commander Mars wants you to explore what happens in the nervous system when you lean down towards a rose. First your",
          lower_text_field: "eyes send signals along special <span>nerves</span> to the brain."
      },
      11 => {
          upper_text_field: "Commander Mars explains that your brain recognises the image of a flower, then compares the image of a flower with others in",
          lower_text_field: "your memory and recalls that this kind of flower often has a pleasant smell."
      },
      12 => {
          upper_text_field: "Your brain sends signals through your <Span>spinal cord</span> and nerves to many muscles, giving the orders that make you bend closer to the",
          lower_text_field: "flower. Your brain then sends a message to breathe in deeply."
      },
      13 => {
          upper_text_field: "Commander Mars explains that the scent of the rose comes through your nose, then signals of that scent travel through",
          lower_text_field: "nerves to your brain. Ah, the sweet smell of a beautiful rose!"
      },
      14 => {
          upper_text_field: "Commander Mars wants you to imagine that you have just sniffed that lovely rose. You reach to bring the rose closer to you when",
          lower_text_field: "suddenly a sharp thorn pricks your thumb. Without thinking, you jerk your hand away. When this happens, it is called a <span>reflex</span> action."
      },
      15 => {
          upper_text_field: "Commander Mars explains that <span>reflex</span> actions happen almost instantly, without the brain sending messages to perform the",
          lower_text_field: "action."
      },
      16 => {
          upper_text_field: "When you touch the sharp thorn, a signal of pain raced from your finger to your <span>spinal cord</span>, which then sent back an immediate",
          lower_text_field: "command to your muscles, saying, ‘Get back!’"
      },
      17 => {
          upper_text_field: "Your body didn’t wait for your brain to receive the pain message and respond to it. Instead, your <span>reflexes</span> took over and saved you",
          lower_text_field: "from feeling even more pain."
      },
      18 => {
          upper_text_field: "Commander Mars explains that your <span>reflexes</span> will work in the same way if, for example, you accidentally touch something hot, such as",
          lower_text_field: "a dish that’s just been taken out of the oven. Try sitting on a chair and cross one leg over the other."
      },
      19 => {
          upper_text_field: "Ask someone to give you a gentle tap just below the <span>kneecap</span>. If the tap comes gently in just the right place, your leg will kick out",
          lower_text_field: "automatically. You didn’t have to think about it – it’s a <span>reflex</span>. Sneezing and <span>blinking</span> are <span>reflex</span> actions too!"
      },
      20 => {
          upper_text_field: "Today’s films takes a look at how reflexes work.",
          main_video_slide: ""
      },
      21 => {
          upper_text_field: "Mission Activity 1: In today’s experiment record your reaction times. See how quickly you can catch the ruler!",
          main_video_slide: ""
      },
      22 => {
          upper_text_field: "Mission Activity 2: Today’s task is an experiment to which records your reaction times. See how quickly you can catch the balls!",
          main_video_slide: ""
      },
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}

end
