desc "Lesson"
task t4m13: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that muscles make you move by pulling on the bones of your <span class='rw_highlight'>skeleton</span>. To do this, the muscles",
          lower_text_field: "must be attached to the bones. Throughout your body, strong fibres called <span class='rw_highlight'>tendons</span> connect muscles to bones."
      },
      7 => {
          upper_text_field: "Commander Mars explains that your largest and strongest <span class='rw_highlight'>tendon</span> is called the Achilles <span class='rw_highlight'>tendon</span>. It connects your calf muscle",
          lower_text_field: "to your heel bone. It’s easy to find your Achilles <span class='rw_highlight'>tendon</span>."
      },
      8 => {
          upper_text_field: "Gently pinch the back of your foot, just above your heel. Commander Mars wants to know if you feel something like a",
          lower_text_field: "strong, tough rope? Wag your foot up and down and feel how the Achilles <span class='rw_highlight'>tendon</span> stretches and relaxes."
      },
      9 => {
          upper_text_field: "Commander Mars wants to know why is it called the Achilles <span class='rw_highlight'>endon</span>? He learns that it comes from a myth from ancient Greece",
          lower_text_field: "which tells the story of a boy names Achilles who was destined to become a great warrior."
      },
      10 => {
          upper_text_field: "When he was a baby, his mother dipped him in the river Styx. She believed its water would protect him from harm. But when she",
          lower_text_field: "dipped him in, part of his body never touched the water. It was his heel, by which his mother was holding him upside down."
      },
      11 => {
          upper_text_field: "Commander Mars explains that Achilles grew up to become the mightiest warrior in the Trojan War. It seemed that no sword,",
          lower_text_field: "spear or arrow could harm him. However, when a poisoned arrow pierced his heel, he died."
      },
      12 => {
          upper_text_field: "Today people still use the phrase, ‘Achilles heel’ to refer to a person’s special weakness. Commander Mars explains that",
          lower_text_field: "<span class='rw_highlight'>tendons</span> can sometimes be weak and can break, but they do grow back together."
      },
      13 => {
          upper_text_field: "Commander Mars explains that to examine a broken bone, a doctor looks at a special kind of picture taken with an X-ray",
          lower_text_field: "machine. X-ray machines use a sort of invisible light, called X-rays, that can travel through muscle but not bone."
      },
      14 => {
          upper_text_field: "X-rays create a picture showing the bones inside your body. This X-ray image shows a boy’s broken arm. The doctor looked at the",
          lower_text_field: "X-ray and operated on the boy. She put a cast around his arm, which he wore for six weeks to protect the bone while it healed."
      },
      15 => {
          upper_text_field: "Once the cast is removed the arm will be as good as new. Don’t you think that bones are AMAZING? They contain <span class='rw_highlight'>marrow</span> which",
          lower_text_field: "produces new blood cells to keep us healthy. They are padded by <span class='rw_highlight'>cartilage</span>. <span class='rw_highlight'>Ligaments</span> help to keep bones in place."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Use the handout to create your own <span class='rw_highlight'>skeleton</span> puppet.",
          main_video_slide: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
