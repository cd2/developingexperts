desc "Lesson"
task t5m4: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explores Mount <span>Vesuvius</span>. It’s a volcano on the western coast of Italy which erupted in A.D.79. The volcano",
          lower_text_field: "buried the ancient Roman cities of Pompeii and Herculaneum in ash and cinders."
      },
      7 => {
          upper_text_field: "Tragically twenty thousand people were killed. The tons of ash almost perfectly preserved Pompeii for centuries. Brigadier",
          lower_text_field: "Venus learns archaeologists digging in the area more than 1500 years later unearthed the entire town, complete with houses,"
      },
      8 => {
          upper_text_field: "Shops, restaurants, temples, signs and paintings. Even the bodies of people who had lived in Pompeii were well preserved, trapped",
          lower_text_field: "and hardened by ash."
      },
      9 => {
          upper_text_field: "Brigadier Venus learns that <Span>Krakatoa</span> is a volcano on an Indonesian island which erupted in 1883 with an explosion so",
          lower_text_field: "powerful that it was heard almost 3,000 miles away."
      },
      10 => {
          upper_text_field: "Volcanic ash shot 17 miles up into the Earth’s atmosphere, darkening the skies for almost 20 years. The force of the eruption",
          lower_text_field: "created a 30-metre-high tsunami that took the lives of 36,000 cpeople on the nearby islands of Java and Sumatra."
      },
      11 => {
          upper_text_field: "The ash sent into the atmosphere by <span>Krakatoa</span> changed the colour of sunsets around the world for months after the eruption.",
      },
      12 => {
          upper_text_field: "Brigadier Venus discovers <Span>Mount St Helens</span>, in Washington State in the United States, erupted in 1980. 230 square miles of forest",
          lower_text_field: "was destroyed. Rocks as big as a hilltop tumbled down, filling up a valley 150 feet deep – the largest landslide in recorded history."
      },
      13 => {
          upper_text_field: "Hot ash, a metre thick, blanketed the area nearest the volcano. Despite all this devastation, life has returned to <Span>St Helens</span> in",
          lower_text_field: "the years following the eruption. Plants have sprouted through the ash."
      },
      14 => {
          upper_text_field: "These plants have created a little bit of soil in which more seeds, brought by the wind, have taken root and begun to grow.",
          lower_text_field: "Scientists predict that by 2200, a forest will cover <Span>Mount St Helens</span> once again."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment: Experiment to simulate how lava sets over objects in its path and leaves a perfect cast.",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Brigadier Venus would like you to report to your work stations for today’s mission assignment!",
          sixty_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}




  $rocket_words = {
          "Active" => {
              description: "Being in a state of existence, progress, or motion.",
              image: "" },
          "Dormant" => {
              description: "Not erupting.",
              image: "" },
          "Krakatoa" => {
              description: "A volcanic island situated near the islands of Java and Sumatra in Indonesia.",
              image: "" },
          "Vesuvius" => {
              description: "<span class= 'rw_highlight'>Mount Vesuvius</span> in Italy, is still one of the most dangerous volcanoes on Earth.",
              image: "" },
          "Mount St. Helens" => {
              description: "<span class= 'rw_highlight'>Mount St. Helens</span> is a volcano located in Washington State.",
              image: "" },
  }


end
