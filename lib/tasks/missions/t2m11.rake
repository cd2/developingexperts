desc "Lesson"
task t2m11: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn reminds us that everything is made of matter. But sometimes we need to know things about matter. We might need",
          lower_text_field: "to know how much something weighs or what size it is or what temperature it might be."
      },
      7 => {
          upper_text_field: "Major Saturn explains how important <span class='rw_highlight'>measurements</span> are. If you’ve ever helped somebody bake a cake, you have to <span class='rw_highlight'>measure</span>",
          lower_text_field: "out each <span class='rw_highlight'>ingredient</span> carefully."
      },
      8 => {
          upper_text_field: "Major Saturn explains that when making a cake you use a cake tin that’s a certain size, and you heat the oven to a certain temperature.",
          lower_text_field: "Every step of the way, you use numbers and make <span class='rw_highlight'>measurements</span>. You might use grams and kilograms or pounds and ounces."
      },
      9 => {
          upper_text_field: "Major Saturn explains that to tell how tall you are, you’ll probably use centimetres or metres to measure. Centimetres and metres measure",
          lower_text_field: "length. You might also use a different set of <span class='rw_highlight'>units</span> of <span class='rw_highlight'>measurement</span> called inches and feet. Inches and feet measure length too."
      },
      10 => {
          upper_text_field: "Mission Assignment 1: How tall are you? Work with your talk partner. Take each other’s height <span class='rw_highlight'>measurement</span> then record it on your handout.",
      },
      11 => {
          upper_text_field: "Mission Assignment 2: Use the <span class='rw_highlight'>measuring</span> scales. Make three sets of modelling dough, then compare the differences",
          lower_text_field: "between the batches. Follow the <span class='rw_highlight'>recipe</span> on the cards provided."
      },
      12 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s Mission Assignment.",
          sixty_second_count: ""
      },      
      13 => {complete_investigation_slide: ""},
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
  }

end
