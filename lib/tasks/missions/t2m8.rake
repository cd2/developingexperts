desc "Lesson"
task t2m8: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn is fascinated to learn that <span class='rw_highlight'>John Loudon McAdam</span> was a Scottish engineer who modernised the way we build roads.",
          lower_text_field: "<span class='rw_highlight'>McAdam</span> was born in Scotland in 1756 but moved to New York in 1790 to make his fortune."
      },
      7 => {
          upper_text_field: "Major Saturn finds out that when he returned to Scotland, <span class= 'rw_highlight'>John McAdam</span> bought his own estate. He became a road trustee in a",
          lower_text_field: "part of Scotland called Ayrshire."
      },
      8 => {
          upper_text_field: "Major Saturn discovers that at the time some roads were dirt paths which were easily ruined by rain and turned into mud.",
          lower_text_field: "Other roads were made out of huge slabs of stone which often broke."
      },
      9 => {
          upper_text_field: "Major Saturn learns that <span class='rw_highlight'>McAdam</span> was convinced that massive stone slabs were not needed to carry the weight of passing",
          lower_text_field: "carriages, as long as the road was kept dry. <span class='rw_highlight'>McAdam</span> came up with the idea of raising roadbeds to ensure adequate drainage."
      },
      10 => {
          upper_text_field: "Major Saturn learns that <span class='rw_highlight'>John McAdam</span> designed these roadbeds using broken stones laid in tight patterns covered with small",
          lower_text_field: "stones to create a hard surface."
      },
      11 => {
          upper_text_field: "<span class= 'rw_highlight'>McAdam</span> discovered that the best stone or gravel for road surfacing had to be broken or crushed, and then graded to a",
          lower_text_field: "constant size of chippings."
      },
      12 => {
          upper_text_field: "<span class='rw_highlight'>McAdam's</span> design, called ‘<span class='rw_highlight'>macadam</span> roads’, represented a revolutionary advancement in road construction at the time.",
      },
      13 => {
          upper_text_field: "Major Saturn learns that the water-bound <span class='rw_highlight'>macadam</span> roads were the forerunners of the tar and bitumen based binding that was",
          lower_text_field: "to become <span class='rw_highlight'>tarmacadam</span>. The word <span class='rw_highlight'>tarmacadam</span> was shortened to the now-familiar name: <span class='rw_highlight'>tarmac</span>."
      },
      14 => {
          upper_text_field: "Major Saturn discovers that by making roads both significantly cheaper and more hardwearing, <span class='rw_highlight'>McAdam</span> triggered an explosion",
          lower_text_field: "in road building with roads made out of his material everywhere."
      },
      15 => {
          upper_text_field: "Major Saturn learns that the right materials need to be selected to create a product. If the wrong materials are used it could harm",
          lower_text_field: "the person using it or it may not be able to be used to complete the task it was designed to do."
      },
      16 => {
          upper_text_field: "Major Saturn asks ‘What would happen if your knives and forks were made from <span class='rw_highlight'>rubber</span> rather than <span class='rw_highlight'>metal</span>?’",
          lower_text_field: "Discuss with your talk partners."
      },
      17 => {
          upper_text_field: "Mission Assignment: To investigate which paper is the most water absorbent.",
      },
      18 => {
          upper_text_field: "Mission Assignment: Test the different papers again but this time apply wax to the surface before you place the samples in the water.",
      },
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

  $rocket_words = {
          'Force' => {
              description: "A <b>strength</b> or power <b>placed upon an object</b>.",
              image: "" },
          'Pushing' => {
              description: "The <b>force applied</b> to an object to push it <b>away from</b> you.",
              image: "" },
          'Properties' => {
              description: "The <b>qualities something might own.</b>",
              image: "" },
          'John Dunlop' => {
              description: "Born in 1888, <b>John</b> Boyd <b>Dunlop invented the rubber tyre</b>.",
              image: "" },
          'Material' => {
              description: "<b>What things are made of.</b>",
              image: "" },
  }


end
