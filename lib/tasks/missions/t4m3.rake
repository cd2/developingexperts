desc "Lesson"
task t4m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants you to take a look at a <span class='rw_highlight'>parallel circuit</span>. A <span class='rw_highlight'>parallel circuit</span> is a closed circuit in which the electricity divides",
          lower_text_field: "into two or more paths before rejoining to create a complete circuit."
      },
      7 => {
          upper_text_field: "Commander Mars explains that the lights in your classroom are powered using a <span class='rw_highlight'>parallel circuit</span>.",
      },
      8 => {
          upper_text_field: "Commander Mars explains how the electricity needs to flow through several routes before it returns to the common path so",
          lower_text_field: "that the power gets to all of the lights in the room."
      },
      9 => {
          upper_text_field: "Commander Mars explains that if one light breaks in a <span class='rw_highlight'>parallel circuit</span> the electricity can still keep flowing through the",
          lower_text_field: "alternative route which means that not all of the lights go off."
      },
      10 => {
          upper_text_field: "Commander Mars is wondering if you can work out why a series circuit wouldn’t work in your classroom if a bulb broke? In a",
          lower_text_field: "<span class='rw_highlight'>parallel circuit</span> the bulbs remain bright regardless of how many bulbs you use."
      },
      11 => {
          upper_text_field: "Today’s Film: Explaining <span class='rw_highlight'>Parallel Circuits</span>.",
          main_video_slide: ""
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Create you own <span class='rw_highlight'>parallel circuit</span> using a bulb, battery and switch",
          lower_text_field: "using the kit and handout provided. Then draw your circuit diagram."
      },
      13 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where is Commander Mars?",
          sixty_count: ""
      },
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
}

  $rocket_words = {
          "Parallel Circuit" => {
              description: "A <b>circuit with two or more pathways</b> for the current to flow through.",
              image: "" },
          "Open Circuit" => {
              description: "A <b>circuit with a break</b> in it.",
              image: "" },
          "Loop" => {
              description: "A <b>complete circuit</b>.",
              image: "" },
          "Push Switch" => {
              description: "A switch <b>which is pushed down in order to complete the circuit</b>.",
              image: "" },
          "Slide Switch" => {
              description: "A switch which <b>slides to the on or off</b> position to turn the circuit on or off.",
              image: "" },
  }


end
