desc "Lesson"
task t4m11: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars want you to close your eyes. What do you see? Nothing, of course! But why? You might say, ‘Because my eyes",
          lower_text_field: "are closed, silly!’ Or you could answer: ‘Because no light is coming into my eyes.’"
      },
      7 => {
          upper_text_field: "Commander Mars explains that you see things because light bounces off them, and then this light enters your eyes. Another",
          lower_text_field: "way of saying this is that things reflect light, and we see the reflected light. Let’s find out what happens when you see."
      },
      8 => {
          upper_text_field: "Commander Mars wants you to look at the picture of the eyeball and its parts. He explains that rays of light pass through your",
          lower_text_field: "<span>cornea</span>, a transparent covering on the outside of your eye. Remember, ‘transparent’ means ‘see-through’, like glass."
      },
      9 => {
          upper_text_field: "Next the light goes through the <Span>pupil</span>, which is a hole in the middle of the <span>iris</span>. The <span>iris</span>, the coloured part of your eye, helps",
          lower_text_field: "the <span>pupil</span> open and close."
      },
      10 => {
          upper_text_field: "Commander Mars describes how on a bright sunny day, the <span>iris</span> makes the <span>pupil</span> grow smaller to let in less light. In a dark room,",
          lower_text_field: "such as a theatre, the iris makes the <span>pupil</span> grow wider to let in more light."
      },
      11 => {
          upper_text_field: "Commander Mars explains that after the light rays pass through the <span>iris</span>, they go through the <span>lens</span>. Muscles attached to the <span>lens</span>",
          lower_text_field: "change its shape just a little bit, to help the <span>lens</span> focus."
      },
      12 => {
          upper_text_field: "The <span>lens</span> focuses the light rays onto the surface at the back of the eyeball, called the <span>retina</span>. Inside the <span>retina</span>, light rays",
          lower_text_field: "change the electrical signals in the nerve endings. These signals travel along the optic nerve to your brain."
      },
      13 => {
          upper_text_field: "Commander Mars explains that the brain makes sense of the signals and recognises the image as, for example, a tree, cat, car",
          lower_text_field: "or letters on the page of a book. It all happens so fast you don’t even notice."
      },
      14 => {
          upper_text_field: "Commander Mars explains that in some people’s eyes, the <span>lenses</span> don’t change and focus as well as they should. Those are the",
          lower_text_field: "people who need glasses. People who see things close-up but need glasses to focus on things far away are called ‘near-sighted’."
      },
      15 => {
          upper_text_field: "Commander Mars explains that a person who can see things far away but needs glasses to focus on things close-by – in order to",
          lower_text_field: "read, for example – is called ‘far-sighted’. Glasses made of differently shaped lenses correct each of those seeing problems."
      },
      16 => {
          upper_text_field: "Commander Mars explains that there are even glasses designed to help people see both far away and close-up. They are called",
          lower_text_field: "bifocals. Bifocal glasses are made from two differently shaped lenses."
      },

      17 => {
          upper_text_field: "They help a person’s eyes focus in two different ways, to see close-up and to see far away. The <Span>iris</span>, the colourful part of your",
          lower_text_field: "eye, gets its name Iris, from the goddess of the rainbow in ancient Greek mythology. What colour are your eyes?"
      },

      18 => {
          upper_text_field: "Mission Activity: Watch today’s films then have a go at creating your own <span>lens</span>.",
          main_video_slide: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}

end
