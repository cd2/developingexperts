desc "Lesson"
task t3m10: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that during <span class='rw_highlight'>autumn</span>, many plants become mature, which means fully grown. On an apple tree, the",
          lower_text_field: "apples grow heavy on the branches and, if you don’t pick them first, they fall to the ground."
      },
      7 => {
          upper_text_field: "Colonel Jupiter describes how <span class='rw_highlight'>acorns</span> fall from the <span class='rw_highlight'>oak trees</span>. <span class='rw_highlight'>Courgettes</span> swell and turn into huge <span class='rw_highlight'>marrows</span>. In the fields, the",
          lower_text_field: "crops are harvested."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that stalks of wheat in fields, turn brown and bend over, weighed down by plump heads of grain.",
          lower_text_field: "On many trees, the leaves turn from green to red, gold, yellow or brown, and then fall to the ground."
      },
      9 => {
          upper_text_field: "Colonel Jupiter describes that many animals prepare for the coming of changes as the weather gets cooler in the <span class='rw_highlight'>autumn</span>.",
          lower_text_field: "Squirrels scurry about gathering nuts and storing them for the cold months ahead."
      },
      10 => {
          upper_text_field: "Colonel Jupiter describes how otters eat as much as they can to build up extra fat, and they look for a den to protect them",
          lower_text_field: "from the cold."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains how some birds like greylag geese make a long journey, or <span class='rw_highlight'>migrate</span>, by flying south to warmer weather. In",
          lower_text_field: "oceans, big whales also <span class='rw_highlight'>migrate</span> to warmer waters. Some grey whales swim for thousands of miles to warmer water."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Make a calendar and decorate it with dried leaves and flowers.",
          main_video_slide: ""
      },
      13 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

end
