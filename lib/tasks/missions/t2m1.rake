desc "Lesson"
task t2m1: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {course_intro_slide: ""},
      3 => {rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {
          upper_text_field: "Major Saturn is wondering what the differences are between plants and a person like you? Well, both you and a plant start out",
          lower_text_field: "small and then keep on growing. But you can walk around. Most plants stay put."
      },
      6 => {
          upper_text_field: "Major Saturn discovers that as plants grow, their roots sink deep down into the <span class= 'rw_highlight'>soil</span> and hold on tight. A plant may sway in the wind,",
          lower_text_field: "but it stays rooted, growing in one place for its whole life."
      },      
      7 => {
          upper_text_field: "Major Saturn discovers another big difference between you and a plant is that you eat your breakfast, lunch and dinner. Have you",
          lower_text_field: "ever seen a plant eat breakfast? Plants don’t sit down for a meal, but they do need food. In fact, plants make their own food."
      },      
      8 => {
          upper_text_field: "Major Saturn learns that plants need air, light, water and minerals from the <span class= 'rw_highlight'>soil</span> to make their own food. The plant soaks up water",
          lower_text_field: "and minerals through its roots (these minerals are dissolved in the water, in the same way that you dissolve sugar in a cup of water)."
      },      
      9 => {
         upper_text_field: "Major Saturn discovers you can grow a plant in a pot of <span class= 'rw_highlight'>soil</span> if you give it enough water, air and light. What do you think would",
          lower_text_field: "happen to a plant if it didn’t get enough water or light? Let’s find out."
      },      
      10 => {
          upper_text_field: "In today’s Mission Assignment we are going to carry out an <span class= 'rw_highlight'>experiment</span>. The <span class= 'rw_highlight'>experiment</span> we are going to do will take several",
          lower_text_field: "days. In this <span class= 'rw_highlight'>experiment</span>, we are going to plant some seeds and see what they need to grow."
      },      
      11 => {
          upper_text_field: "Major Saturn explains that you need to poke a tiny hole in the bottom of each of the three paper cups. Fill each cup with potting",
          lower_text_field: "<span class= 'rw_highlight'>soil</span> to about 2cm from the top. Use your finger to poke three holes in the <span class= 'rw_highlight'>soil</span> in each cup, half a finger deep."
      },      
      12 => {
          upper_text_field: "Major Saturn explains that you need to put one seed in each hole and cover them all with <span class= 'rw_highlight'>soil</span>. Look at these beans in a <span class= 'rw_highlight'>pod</span>.",
          lower_text_field: "We only need three <span class= 'rw_highlight'>bean</span> plants for this <span class= 'rw_highlight'>experiment</span>, but we are going to plant more in case the seeds do not <span class= 'rw_highlight'>germinate</span>."
      },      
      13 => {
          upper_text_field: "Major Saturn explains that you need to take the tray to a place near a sunny window and you will need a class member to",
          lower_text_field: "water the seeds every day until the seeds sprout and the first leaves begin to spread."
      },      
      14 => {
          upper_text_field: "If more than one seed has sprouted in a cup, then carefully pull out the other plants and leave only one plant per cup in order",
          lower_text_field: "to continue with the <span class= 'rw_highlight'>experiment</span>."
      },      
      15 => {
          upper_text_field: "Major Saturn needs you to have a plant growing well in three cups which are labelled cups 1, 2, and 3.",
          lower_text_field: "You are going to do something different with each of these plants."
      },
      16 => {
          upper_text_field: "Major Saturn would like you to leave <b>plant 1</b> where it is and continue to water it everyday.",
      },
      17 => {
          upper_text_field: "Major Saturn would like you to leave <b>plant 2</b> where it is but do not water it anymore.",
          lower_text_field: "Major Saturn wants you to understand that the plant won’t be getting water but it will be getting light and air."
      },
      18 => {
         upper_text_field: "Major Saturn explains that you will need to place <b>plant 3</b> in a dark place but will still need to keep giving the plant a little water daily.",
         lower_text_field: "Major Saturn wants you to know that the plant will be getting air, water and minerals but no light."
      },
      19 => {
        upper_text_field: "You have one minute to move to your work stations for today’s mission assignment.", 
        sixty_count: ""},
      20 => {upper_text_field: "",},
      21 => {upper_text_field: "",},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }

    $rocket_words = {
          "Experiment" => {
              description: "A test done in order to learn something or to discover if something works or is true.",
              image: "" },
          "Germinate" => {
              description: "When a seed starts to grow.",
              image: "" },
          "Soil" => {
              description: "The material on the surface of the ground in which plants grow.",
              image: "" },
          "Bean" => {
              description: "A seed (or the pod containing seeds) of various climbing plants. Eaten as a vegetable.",
              image: "" },
          "Pod" => {
              description: "Long, narrow, flat part of some plants, that contains the seeds and usually has a thick skin.",
              image: "" },
  }



end
