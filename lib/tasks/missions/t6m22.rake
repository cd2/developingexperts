desc "Lesson"
task t6m22: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "",
          sixty_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}



  $rocket_words = {
          "Social Insects" => {
              description: "A number of species of insects that live in colonies.",
              image: "" },
          "Colony" => {
              description: "Individual organisms of the same species who live closely together.",
              image: "" },
          "Ant Hill" => {
              description: "A nest in the form of a mound built by ants or termites.",
              image: "" },
          "Honey Bee" => {
              description: "The honey producing bee which live in colonies.",
              image: "" },
          "Wasp" => {
              description: "Social winged insect which has a narrow waist and a sting. It is yellow with black stripes.",
              image: "" },
  }


end
