desc "Lesson"
task t2m20: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants you to put your hand on your chest. Do you feel something beating inside? If you don’t, run around",
          lower_text_field: "quickly and then try again."
      },
      7 => {
          upper_text_field: "Major Saturn explains that beating you feel is a very important muscle: it’s your heart. Your heart is beating all of the time, day",
          lower_text_field: "and night. When your heart beats, it pumps blood."
      },
      8 => {
          upper_text_field: "It pumps blood through tubes that go all around your body and then come back to your heart.",
      },
      9 => {
          upper_text_field: "By beating, your heart keeps the blood circulating, which means going round and round to every part of your body.",
      },
      10 => {
          upper_text_field: "Major Saturn explains that the heart is one of the most important <span class= 'rw_highlight'>organs</span> in the human body, continuously pumping",
          lower_text_field: "blood aroung our body through <span class= 'rw_highlight'>blood vessels</span>."
      },
      11 => {
          upper_text_field: "Your heart is located in your chest and is well protected by your <span class= 'rw_highlight'>rib cage</span>.The study of the human heart and its various disorders",
          lower_text_field: "is known as <span class= 'rw_highlight'>cardiology</span>."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      13 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

  $rocket_words = {
          "Circulatory System" => {
              description: "System that <b>circulates blood around the body</b>.",
              image: "" },
          "Organ" => {
              description: "Part of a living thing, that <b>carries out a specific job</b>.",
              image: "" },
          "Blood Vessel" => {
              description: "A <b>tube like structure that carries blood around</b> the body.",
              image: "" },
          "Rib Cage" => {
              description: "The <b>bony frame</b> formed by the ribs round the chest.",
              image: "" },
          "Cardiology" => {
              description: "The field of medicine that deals with <b>diseases of the heart</b>.",
              image: "" },
  }


end
