desc "Lesson"
task t4m7: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants to know if you remember how fast light travels? Faster than anything else – an amazing 186,000 miles per",
          lower_text_field: "second. That’s about 300,000 kilometres per second! Sound travels much more slowly than light."
      },
      7 => {
          upper_text_field: "Commander Mars tells us that, in warm air, sound travels at about 770 miles per hour. Of course, that’s still very fast",
          lower_text_field: "compared to a car going 70 miles per hour! But there are jet aeroplanes that can fly as fast as the speed of sound."
      },
      8 => {
          upper_text_field: "Commander Mars says this is known as ‘breaking the <span class='rw_highlight'>sound barrier</span>’. In 2012 a man completed a <span class='rw_highlight'>parachute</span> jump: he fell",
          lower_text_field: "39,045 metres (128,100 feet), faster than the speed of sound! He survived as he had a special space suit and <span class='rw_highlight'>parachute</span>."
      },
      9 => {
          upper_text_field: "Commander Mars explains that during a distant storm, you see lightning before you hear the thunder. Why? Because light",
          lower_text_field: "travels much faster to your eyes than sound does to your ears. Can you remember how to determine the distance of the storm?"
      },
      10 => {
          upper_text_field: "In 1962, British and French designers began a plan to take people as fast as possible around the world. They chose to build",
          lower_text_field: "a <span class='rw_highlight'>supersonic</span> airliner, which they called <span class='rw_highlight'>Concorde</span>. It had an unusual design."
      },
      11 => {
          upper_text_field: "Commander Mars explains how the sleek shape became very popular and easily recognisable. People would often stop what",
          lower_text_field: "they were doing to watch one fly by. Did you know that <span class='rw_highlight'>parachutes</span> are sometimes used to slow planes down when landing?"
      },
      12 => {
          upper_text_field: "Commander Mars wants to know why <span class='rw_highlight'>Concorde</span> was called <span class='rw_highlight'>supersonic</span>. It actually broke the <span class='rw_highlight'>sound barrier</span>. It could travel",
          lower_text_field: "faster than the Earth spins, so passengers flying west to New York would arrive earlier in the day than when they left London."
      },
      13 => {
          upper_text_field: "They seemed to be travelling back in time! The flight took about three hours, which was under half the time that it took an",
          lower_text_field: "ordinary aeroplane. <span class='rw_highlight'>Concorde’s</span> design was stylish too, with graceful curves to help it move through the air swiftly."
      },
      14 => {
          upper_text_field: "Commander Mars discovers that the last <span class='rw_highlight'>Concorde</span> passenger flight was in 2003. Why did it stop? Going so fast meant burning",
          lower_text_field: "such a lot of fuel and the planes were also very expensive to maintain! The remaining <span class='rw_highlight'>Concordes</span> are now in museums."
      },
      15 => {
          upper_text_field: "Commander Mars describes how, when you’re listening to your favourite song on the radio, you might say, ‘Turn it up!’ When you",
          lower_text_field: "turn up the volume, you are making the volume louder. A scientist might say that you are increasing the sound intensity."
      },
      16 => {
          upper_text_field: "Commander Mars explains that a quiet sound, like a whisper, does not travel very far but a really loud sound can travel",
          lower_text_field: "hundreds of miles. The higher the intensity of the sound, the further it travels."
      },
      17 => {
          upper_text_field: "Commander Mars learns that more than a hundred years ago, when a volcano exploded on the island of <span class='rw_highlight'>Krakatoa</span>, the sound",
          lower_text_field: "could be heard in Australia, almost three hundred miles away!"
      },      
      18 => {
          upper_text_field: "Today’s Mission Assignment: Watch today’s film then have a go at creating your own singing glasses.",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "Please report to your work stations for today’s mission assignment.",
          sixty_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
