desc "Lesson"
task t4m23: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants to know, can there be a <span>volume</span>, or space, in which there is no matter at all, not even air? Yes. It is",
          lower_text_field: "called a <Span>vacuum</span> and no we are not talking about a <span>vacuum</span> cleaner!"
      },
      7 => {
          upper_text_field: "Commander Mars explains that it is rare to find a perfect vacuum in the real world. Even in outer space, a few atoms float around.",
          lower_text_field: "When a certain space contains many fewer atoms than normal, we call it a <span>partial vacuum</span>."
      },
      8 => {
          upper_text_field: "Here is an experiment that will show you the effect of a <Span>partial vacuum</span>. Commander Mars wants you to ask an adult to help you",
          lower_text_field: "try it. Find an old empty <span>plastic bottle</span> and put it somewhere where it does not matter if it gets a bit wet."
      },
      9 => {
          upper_text_field: "Remove the cap, boil a <span>kettle</span> and – be very careful – pour in some boiling water, enough to cover the bottom. Notice the",
          lower_text_field: "inside of the bottle goes misty. It is now full of water and water <span>vapour</span>, which pushes the air out of the bottle."
      },
      10 => {
          upper_text_field: "Commander Mars wants you to screw the cap back on tightly and wait for about thirty minutes. What do you think will happen?",
          lower_text_field: "As the water <Span>vapour</span> turns back into water, a <span>partial vacuum</span> is created."
      },
      11 => {
          upper_text_field: "Commander Mars explains that air cannot get back into the closed bottle, but it can push on the bottle so that the sides go in.",
      },
      12 => {
          upper_text_field: "If you had a metal container with stronger sides, it could hold the <span>partial vacuum</span> without collasping but you would find it",
          lower_text_field: "hard to get the top off again and it might break."
      },
      13 => {
          upper_text_field: "Today’s Activity: Here is an experiment that will show the effect of a <span>partial vacuum</span>. Change film!!",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}




}

  $rocket_words = {
          "Plastic Bottle" => {
              description: "A bottle made of plastic which is used to store liquids such as water, oil or shampoo.",
              image: "" },
          "Vapour" => {
              description: "Substance that is in the form of a gas that consists of very small droplets mixed with air.",
              image: "" },
          "Kettle" => {
              description: "A container used for heating or boiling liquid.",
              image: "" },
          "Vacuum" => {
              description: "A space which contains no matter – no liquids, solids or gases.",
              image: "" },
          "Partial Vacuum" => {
              description: "A space which contains a small amount of matter (liquids, solids or gases).",
              image: "" },
  }

end
