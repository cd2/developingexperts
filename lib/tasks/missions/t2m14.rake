desc "Lesson"
task t2m14: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn discovers that <span class='rw_highlight'>Thomas Eddison</span> was not the inventor of the <span class='rw_highlight'>electric</span> light bulb, but he did produce the first light bulbs",
          lower_text_field: "people could buy."
      },
      7 => {
          upper_text_field: "Before the light bulb, people burned lamp oils or used manufactured natural gas for illumination, a rather dangerous",
          lower_text_field: "way to provide light."
      },
      8 => {
          upper_text_field: "Major Saturn asks you to look around the room to see how many things can be turned on and off. Is there a light with a <span class='rw_highlight'>light switch</span>?",
          lower_text_field: "Is there a television, or maybe a computer? Is there a toy that uses batteries and moves or makes a noise when you turn it on?"
      },
      9 => {
          upper_text_field: "Major Saturn explains that all of these things use <span class='rw_highlight'>electricity</span>. <span class='rw_highlight'>Electricity</span> is the <span class= 'rw_highlight'>power</span> that makes them work. <span class='rw_highlight'>Electricity</span> makes",
          lower_text_field: "them shine, beep, show pictures or move around."
      },
      10 => {
          upper_text_field: "Major Saturn learns that everything in the world, even your body, carries a little bit of <span class='rw_highlight'>electricity</span> in it. Have you ever combed your",
          lower_text_field: "hair and noticed the hairs on your head standing up as you pull the comb away?"
      },
      11 => {
          upper_text_field: "That’s <span class='rw_highlight'>electricity</span>. Have you ever rubbed a balloon against your shirt, then pressed it against the wall and watched it stick?",
          lower_text_field: "That’s <span class='rw_highlight'>electricity</span>, too."
      },
      12 => {
          upper_text_field: "Major Saturn explains there’s a special name for this <span class='rw_highlight'>electrical</span> attraction between a comb and your hair, or between the balloon",
          lower_text_field: "and the wall. It’s called <span class='rw_highlight'>static electricity</span>. Maybe you have walked across a room and touched somebody’s hand and felt a zap!"
      },
      13 => {
          upper_text_field: "Major Saturn explains that this happens because of <span class='rw_highlight'>static electricity</span>. If it happens in a dark room, you might see a little",
          lower_text_field: "flash of light."
      },
      14 => {
          upper_text_field: "That little zap of <span class='rw_highlight'>static electricity</span> works the same way as a lightning bolt streaking through the sky in a thunderstorm. But",
          lower_text_field: "the lightning bolt is a lot more <span class= 'rw_highlight'>powerful</span>!"
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
