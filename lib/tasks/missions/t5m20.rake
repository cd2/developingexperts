desc "Lesson"
task t5m20: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      18 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "",
          ten_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}



  $rocket_words = {
          "Satellite" => {
              description: "Device designed to be launched into orbit around the earth, planets or Sun.",
              image: "" },
          "Apollo" => {
              description: "U.S. spacecraft, designed to carry astronauts to the moon.",
              image: "" },
          "Space Shuttle" => {
              description: "U.S. space vehicle which is a reusable manned orbiter.",
              image: "" },
          "Observatory" => {
              description: "A place equipped with a powerful telescope for observing the planets and stars.",
              image: "" },
          "Rocket" => {
              description: "A space capsule or vehicle put into orbit.",
              image: "" },
  }


end
