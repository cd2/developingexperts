desc "Lesson"
task t4m18: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that when you bite an apple, the piece in your mouth is still too big to swallow. So you chew it up. Your",
          lower_text_field: "teeth are important players on the digestive team as well."
      },
      7 => {
          upper_text_field: "They cut, munch and crunch the food into smaller pieces. Commander Mars that your front eight teeth, four on the top and",
          lower_text_field: "four on the bottom, are called your <span>incisors</span>. To incise means to cut."
      },
      8 => {
          upper_text_field: "Growing right next to your <span>incisors</span> are four pointy teeth called your <span>canines</span>. They tear the food into pieces, by the way, ‘<span>canine</span>’",
          lower_text_field: "is the old Latin word for ‘dog!’ Commander Mars wants to know why you think your sharp, pointy teeth are called ‘<span>canines</span>’?"
      },
      9 => {
          upper_text_field: "Commander Mars explains that after your <span>incisors</span> and <span>canines</span> bite and tear the apple, your tongue pushes the pieces of food",
          lower_text_field: "to the teeth in the back of your mouth. Your <span>molars</span> have flat tops for grinding the food into pieces small enough to swallow."
      },
      10 => {
          upper_text_field: "Commander Mars explain that when you’ve chewed enough, the apple pieces are small enough to swallow. Gulp! Down they go.",
          lower_text_field: "Go where? Down your throat, through a tube called the <span>oesophagus</span>."
      },
      11 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      12 => {
          upper_text_field: "Please report to your work stations for today’s mission assignment.",
          sixty_count: ""
      },
      13 => {complete_investigation_slide: ""},
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
}

end
