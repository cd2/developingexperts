desc "Lesson"
task t6m14: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "General Mercury explains that if you take a look inside your chest, on either side of your heart, you can find two inflatable",
          lower_text_field: "sacs called ‘lungs’. They are like warm, wet sponges inside."
      },
      7 => {
          upper_text_field: "They expand and contract as your breathing fills them with air, then pushes the air out again. General Mercury wants you to",
          lower_text_field: "take a deep breath and imagine the air filling those two warm, wet sacs inside your chest."
      },
      8 => {
          upper_text_field: "General Mercury explains how as you take a breath, air flows through your nose or your mouth and travels down the windpipe,",
          lower_text_field: "or <span class='rw_highlight'>trachea</span>. It moves past the voice box and into tubes inside your lungs called <span class='rw_highlight'>bronchi</span>."
      },
      9 => {
          upper_text_field: "<span class='rw_highlight'>Bronchi</span> branch into smaller and smaller tubes. At the very ends of the tiniest <span class='rw_highlight'>bronchi</span> are air sacs called <span class='rw_highlight'>alveoli</span>. General",
          lower_text_field: "Mercury explains that these air sacs look a little bit like small balloons filled with water."
      },
      10 => {
          upper_text_field: "These <span class='rw_highlight'>alveoli</span> contain tiny <span class='rw_highlight'>capillaries</span> where the respiratory and the circulatory systems meet. General Mercury explains how",
          lower_text_field: "haemoglobin in red blood cells absorbs the oxygen from the breath you took and carries it to all the cells in your body."
      },
      11 => {
          upper_text_field: "General Mercury explains how the process happens in reverse, too. As blood circulates through your body, it picks up carbon",
          lower_text_field: "dioxide, which is of no use to the body."
      },
      12 => {
          upper_text_field: "When the red blood cell carrying the carbon dioxide reach the <span class='rw_highlight'>capillaries</span> in the <span class='rw_highlight'>alveoli</span>, they unload the waste products into the",
          lower_text_field: "lungs. Then you breathe out and get rid of the unneeded gas."
      },
      13 => {
          upper_text_field: "General Mercury explains that breathing in and out happens thanks to the <span class='rw_highlight'>diaphragm</span>, which is a stretchy sheet of muscle",
          lower_text_field: "underneath your lungs. When the <span class='rw_highlight'>diaphragm</span> arches down, it opens up space in your lungs and air rushes in to fill them."
      },
      14 => {
          upper_text_field: "When the <span class='rw_highlight'>diaphragm</span> arches up, it pushes the lungs together and forces air out of them, through the windpipe.",
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment",
          main_video_slide: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}




  $rocket_words = {
          "Trachea" => {
              description: "Windpipe.",
              image: "" },
          "Bronchi" => {
              description: "Two tubes that branch from the <span class='rw_highlight'>trachea</span> and carry air into the lungs.",
              image: "" },
          "Alveoli" => {
              description: "The many small air bags in the lungs, with thin walls that allow oxygen to enter the blood.",
              image: "" },
          "Capillary" => {
              description: "A very thin tube, especially one of the smaller tubes that carry blood around the body.",
              image: "" },
          "Diaphragm" => {
              description: "The muscle that separates the chest from the lower part of the body.",
              image: "" },
  }

  $questions = {
      "What is the trachea?" => {answer: "Windpipe.",
                           image: "",
                           slide_id: 8},
      "What are bronchi?" => {answer: "Two tubes that branch from the <span class='rw_highlight'>trachea</span> and carry air into the lungs.",
                                image: "",
                                slide_id: 8},
      "What are alveoli?" => {answer: "The many small air bags in the lungs, with thin walls that allow oxygen to enter the blood.",
                           image: "",
                           slide_id: 9},
      "What are capillaries?" => {answer: "A very thin tube, especially one of the smaller tubes that carry blood around the body.",
                              image: "",
                              slide_id: 10},
      "What is the diaphragm?" => {answer: "The muscle that separates the chest from the lower part of the body.",
                              image: "",
                              slide_id: 13},
  }
end
