desc "Lesson"
task t4m5: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars is woken up by the <span class='rw_highlight'>sound</span> of an alarm clock. He hears a dog bark and a voice call ‘Time to get up.’ He explains",
          lower_text_field: "that we hear familiar <span class='rw_highlight'>sounds</span> every day but wants to know what exactly is <span class='rw_highlight'>sound</span>?"
      },
      7 => {
          upper_text_field: "Commander Mars learns that <span class='rw_highlight'>sound</span> is caused by a back and forth movement called <span class='rw_highlight'>vibration</span>. Commander Mars wants you to",
          lower_text_field: "close your lips and hum. While you’re humming, feel your throat under your chin. Do you feel it tingling?"
      },
      8 => {
          upper_text_field: "Commander Mars learns that it is caused by something moving back and forth very fast. When you hum, the vocal cords in your",
          lower_text_field: "throat are <span class='rw_highlight'>vibrating</span> back and forth, which makes the air around them <span class='rw_highlight'>vibrate</span>."
      },
      9 => {
          upper_text_field: "Commander Mars discovers these <span class='rw_highlight'>vibrations</span> of air strike your eardrums and make them <span class='rw_highlight'>vibrate</span>, to create the <span class='rw_highlight'>sound</span> you hear.",
      },
      10 => {
          upper_text_field: "Commander Mars explains a way you can see how <span class='rw_highlight'>sound</span> makes the air <span class='rw_highlight'>vibrate</span>. Stretch a piece of cling film over the surface of a",
          lower_text_field: "bowl and fasten it tightly with an elastic band."
      },
      11 => {
          upper_text_field: "Sprinkle a few grains of dry rice, salt or sugar on the film. Now take a big pan, hold it near the bowl and strike it with a spoon",
          lower_text_field: "a few times."
      },
      12 => {
          upper_text_field: "Do you see the grains jump when you hit the pan? That is because the pan is <span class='rw_highlight'>vibrating</span>, which causes the air and then the plastic to <span class='rw_highlight'>vibrate</span>.",
      },
      13 => {
          upper_text_field: "Commander Mars learns that this is caused by <span class='rw_highlight'>vibrating</span> air <span class='rw_highlight'>sound waves</span>. When you hit the pan, <span class='rw_highlight'>sound waves</span> travel through the",
          lower_text_field: "air and cause the plastic to <span class='rw_highlight'>vibrate</span>, which in turn makes the grains jump."
      },
      14 => {
          upper_text_field: "Commander Mars learns how <span class='rw_highlight'>sound waves</span> move out from a <span class='rw_highlight'>vibrating</span> object in all directions, making the air move back and",
          lower_text_field: "forth in a way that we can’t see. <span class='rw_highlight'>Sounds</span> <span class='rw_highlight'>compress</span> and <span class='rw_highlight'>decompress</span> the air, pushing and then relaxing, making invisible <span class='rw_highlight'>vibrations</span>."
      },
      15 => {
          upper_text_field: "Those back and forth <span class='rw_highlight'>vibrations</span> spread out from the source that made them, getting weaker as they get farther away. That’s why",
          lower_text_field: "you hear your friend standing right next to your more clearly than you hear someone calling from across the street."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: How to make a chicken cup!",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment 1: Create the product below using the materials provided. Complete the tests on the handout.",
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment 2: Create a rice drum using the materials provided. Test <span class='rw_highlight'>vibrating</span> <span class='rw_highlight'>sounds</span>.",
      },
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}



}
  $rocket_words = {
          "Compress" => {
              description: "To flatten by pressure; squeeze or press.",
              image: "" },
          "Decompress" => {
              description: "To relieve of <span class='rw_highlight'>compressing</span> forces.",
              image: "" },
          "Sound" => {
              description: "An audible vibrating wave of pressure, through a medium such as air or water.",
              image: "" },
          "Sound Waves" => {
              description: "A mechanical wave that results from the back and forth <span class='rw_highlight'>vibration</span>.",
              image: "" },
          "Vibrating" => {
              description: "Moving continuously and rapidly to and fro.",
              image: "" },
  }

  $questions = {
      "What does <span class='rw_highlight'>compress</span> mean?" => {answer: "To flatten by pressure; squeeze or press.",
                           image: "",
                           slide_id: 14},
      "What does <span class='rw_highlight'>decompress</span> mean?" => {answer: "To relieve of <span class='rw_highlight'>compressing</span> forces.",
                                image: "",
                                slide_id: 14},
      "What is <span class='rw_highlight'>sound</span>?" => {answer: "An audible vibrating wave of pressure, through a medium such as air or water.",
                           image: "",
                           slide_id: 7},
      "What are <span class='rw_highlight'>sound waves</span>?" => {answer: "A mechanical wave that results from the back and forth <span class='rw_highlight'>vibration</span>.",
                              image: "",
                              slide_id: 13},
      "What does <span class='rw_highlight'>vibrating</span> mean?" => {answer: "Moving continuously and rapidly to and fro.",
                              image: "",
                              slide_id: 8},
  }
end
