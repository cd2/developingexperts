desc "Lesson"
task t6m13: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      16 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "",
          sixty_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}



  $rocket_words = {
          "Atrium" => {
              description: "One of two spaces at the top part of heart that receive blood from the veins.",
              image: "" },
          "Ventricle" => {
              description: "One of two chambers that collect and expel blood received from an atrium in the heart.",
              image: "" },
          "Aorta" => {
              description: "Main artery or thick tube that takes blood to the other parts of the body from the heart.",
              image: "" },
          "Respiratory" => {
              description: "Relating to breathing.",
              image: "" },
          "Bloodstream" => {
              description: "The flow of blood around the body.",
              image: "" },
  }


end
