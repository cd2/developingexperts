desc "Lesson"
task t5m10: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that dark, massive clouds signal the approach of one of nature’s loudest and most dazzling sound",
          lower_text_field: "and light shows, a thunderstorm."
      },
      7 => {
          upper_text_field: "As a cold front moves through an area, especially during spring and summer, it pushes up warm and humid air. The moist air",
          lower_text_field: "piles up higher and higher into towering, miles-high, flat-topped clouds called <span>cumulonimbus</span>."
      },
      8 => {
          upper_text_field: "Brigadier Venus describes how the <span>cumulonimbus</span> clouds’, condensed droplets quickly cool into icy crystals. Strong air",
          lower_text_field: "currents jiggle the ice crystals up and down inside the cloud."
      },
      9 => {
          upper_text_field: "As the ice crystals crash, bump and rub against each other, electrons loosen and jostle around the static electricity, just as",
          lower_text_field: "they do when your socks and pyjamas tumble against each other in the clothes dryer."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains that a huge amount of stored energy is released as the negative electrons rush down towards the",
          lower_text_field: "positively-charged ground. The white flash of <Span>lightning</span> that can be seen, is a powerful electrical current flowing through the air."
      },
      11 => {
          upper_text_field: "Brigadier Venus describes how a <Span>lightning</span> bolt heats up the air around it so that it is five times hotter than the surface of the",
          lower_text_field: "sun. That air expands quickly, and it violently vibrates the air all around it and a rumbling clap of <Span>thunder</span> can be heard."
      },
      12 => {
          upper_text_field: "Light travels much faster than sound. The flash of a bolt of <Span>lightning</span> travels to our eyes at 186,000 miles per second",
          lower_text_field: "(the speed of light). The clap of <span>thunder</span> accompanying the <span>lightning</span> bolt – travels only one fifth of a mile per second."
      },
      13 => {
          upper_text_field: "Brigadier Venus explains that you can estimate how far away <span>lightning</span> has struck. As soon as you see a flash of <span>lightning</span>,",
          lower_text_field: "count the seconds until you hear the <span>thunder</span>. Divide this number by five, giving you the distance in miles to the bolt of <span>lightning</span>."
      },
      14 => {
          upper_text_field: "Brigadier Venus explains that the most dangerous thunderstorms are the ones that create <Span>tornadoes</span> – whirling",
          lower_text_field: "funnel shaped clouds that reach down to the surface of the Earth and suck things up like houses!"
      },
      15 => {
          upper_text_field: "Brigadier Venus explains that <Span>hurricanes</span> form over tropical oceans in areas of low pressure. Warm, moist air rises rapidly",
          lower_text_field: "from the warm water and forms clouds. More warm, moist air rushes in to replace it."
      },
      16 => {
          upper_text_field: "Air gets sucked up faster and faster, creating storm clouds and a tall, spiralling column of wind. The column of wind pulls more",
          lower_text_field: "and more moisture from the ocean, growing larger and picking up speed."
      },
      17 => {
          upper_text_field: "The eye of the <Span>hurricane</span> is the hole in the storm at the centre of the spiral. In the eye, the air pressure is very low and the winds",
          lower_text_field: "are, for the moment, calm."
      },
      18 => {
          upper_text_field: "Brigadier Venus explains how meteorologists arrange to fly in jets equipped with instruments designed to photograph",
          lower_text_field: "measure the storm from high above."
      },
      19 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then complete the experiment with your partner.",
          main_video_slide: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
