desc "Lesson"
task t5m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus discovers that <span>volcanoes</span> are like safety valves for the planet’s furnace. They release built-up pressure from",
          lower_text_field: "inside the Earth."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains that <span>volcanoes</span> form when hot <span>magma</span> from the mantle squeezes up through weak spots",
          lower_text_field: "in the Earth’s crust, usually where plates meet."
      },
      8 => {
          upper_text_field: "Brigadier Venus explains that when a <Span>volcano</span> erupts incredibly hot liquid comes gushing out of the Earth. Once it <span>flows</span> out",
          lower_text_field: "of a <span>volcano</span>, <span>magma</span> is called <span>lava</span>. A <span>volcanic</span> eruption can keep going for hours, days, even months."
      },
      9 => {
          upper_text_field: "Brigadier Venus explains how ash and hunks of fiery rock spew from the opening in the Earth, and over time they pile up and",
          lower_text_field: "harden into a mountain."
      },
      10 => {
          upper_text_field: "He explains that at other times <span>volcanoes</span> erupt violently, flinging hot <span>lava</span>, gases and pieces of rock into the air. <span>Volcanoes</span> often",
          lower_text_field: "erupt many times over the course of centuries, like <span>Mount Stromboli</span> in Italy."
      },
      11 => {
          upper_text_field: "He expands by saying how sometimes <span>volcanoes</span> erupt quietly, without a lot of noise and explosions. Glowing hot <span>lava</span>",
          lower_text_field: "oozes out from the opening on top and runs down the sides of the <span>volcano</span>."
      },
      12 => {
          upper_text_field: "Brigadier Venus explains that sometimes volcanoes erupt explosively, building up a mountain of layers of hardened <span>lava</span>.",
          lower_text_field: "When the <span>volcano</span> Eyjafjllajokull in Iceland erupted in 2010 it sent cinders and ash billowing into the sky. Airlines were disrupted."
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: Create your own erupting Volcano!",
          main_video_slide: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

end
