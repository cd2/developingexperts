desc "Lesson"
task t3m7: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {
          upper_text_field: "Colonel Jupiter wants to know how your potato cloning experiment is progressing? Explain what has happened with your talk partners.",
      },
      5 => {ten_count_launch: ""},
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "The first step of flower <span class='rw_highlight'>fertilisation</span> is <span class='rw_highlight'>pollination</span>, the movement of <span class='rw_highlight'>pollen</span> from the anther to the sticky top of the <span class='rw_highlight'>pistil</span>. But how",
          lower_text_field: "does the <span class='rw_highlight'>pollen</span> make that trip? <span class='rw_highlight'>Insects</span> or birds are responsible for <span class='rw_highlight'>pollinating</span> many flowers."
      },
      8 => {
          upper_text_field: "But wind and rain also help. Look at the bee in the film. When the bee sips nectar from the flower, it also picks up some <span class='rw_highlight'>pollen</span>",
          lower_text_field: "that sticks to its back or legs. When it visits another flower to get more nectar, the <span class='rw_highlight'>pollen</span> can fall off and stick to the <span class='rw_highlight'>pistil</span>."
      },
      9 => {
          upper_text_field: "In the second step of <span class='rw_highlight'>fertilisation</span>, a tube grows out of the <span class='rw_highlight'>pollen</span> grain that is stuck to the <span class='rw_highlight'>pistil</span>. A <span class='rw_highlight'>pollen</span> tube cannot grow unless",
          lower_text_field: "the <span class='rw_highlight'>pollen</span> comes from the same kind of plant, so a bee needs to choose the same plant to land on!"
      },
      10 => {
          upper_text_field: "In the last step, the male and the female cells join together and the <span class='rw_highlight'>fertilisation</span> occurs. The <span class='rw_highlight'>fertilised</span> egg cell begins to divide",
          lower_text_field: "and form an <span class='rw_highlight'>embryo</span>, or young plant."
      },
      11 => {
          upper_text_field: "As the seed forms in the parent plant, the flower changes. The sepals and petals die and fall off, and flowers starts to grow.",
          lower_text_field: "The seed is covered by fruit that we eat, for example cherries and strawberries."
      },
      12 => {
          upper_text_field: "The covering protects the seed or seeds inside and it also helps to scatter the seed. These coverings are often juicy and",
          lower_text_field: "nutritious. Animals eat fruits, but they often do not digest the seeds."
      },
      13 => {
          upper_text_field: "When the seeds pass through the animal’s body, they may end up in a new location where they can germinate. If the fruit falls",
          lower_text_field: "from the plant but is not eaten, it starts to decay and the seed is uncovered."
      },
      14 => {
          upper_text_field: "This allows the seed to reach the soil, where it can germinate and grow into a new plant. What happens to a seed once it",
          lower_text_field: "reaches the soil?"
      },
      15 => {
          upper_text_field: "Let’s look at how a plant grows. This is a picture of a conker, which is a horse chestnut seed. When it is cut open, you can see",
          lower_text_field: "the young plant or <span class='rw_highlight'>embryo</span> inside. Notice the large area where food is stored."
      },
      16 => {
          upper_text_field: "This food store keeps the <span class='rw_highlight'>embryo</span> alive and helps it to grow until it is big enough to make its own food.",
          lower_text_field: "See the seed coat? It protects and keeps it from dying out."
      },
      17 => {
          upper_text_field: "When a seed falls to the ground, it is sometimes pushed into the soil by heavy rainfall. Certain seeds are buried by",
          lower_text_field: "animals, such as squirrels who want to eat them later."
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Making Bertie the Bee",
          main_video_slide: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}

  $rocket_words = {
          "Pistil" => {
              description: "The female parts of a flower e.g. the stigma, style, and ovary.",
              image: "" },
          "Embryo" => {
              description: "An unborn or unhatched offspring in the process of development.",
              image: "" },
          "Fertilization" => {
              description: "When a bee takes pollen from one flower to another to make more flowers.",
              image: "" },
          "Pollen" => {
              description: "A powder, produced by the male part of a flower, that causes the female part to produce seeds.",
              image: "" },
          "Insect" => {
              description: "A small animal that has six legs and generally one or two pairs of wings.",
              image: "" },
  }


end
