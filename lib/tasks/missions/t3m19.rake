desc "Lesson"
task t3m19: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that every day water evaporates up into air mainly from the oceans and seas. It then condenses into",
          lower_text_field: "cloud and falls back to the earth. We can draw all of these movements of water as a big cycle. This is called the water cycle."
      },
      7 => {
          upper_text_field: "Fill in the missing words with your talk partners. Every day water __________ up into air mainly from the ______ and ____. It then",
          lower_text_field: "__________ into _____ and falls back to the earth. This is called the water _____."
      },
      8 => {
          upper_text_field: "Colonel Jupiter wants to know what happens when the water droplets get big enough? They fall back to earth as some form of",
          lower_text_field: "precipitation, (rain, <span class='rw_highlight'>sleet</span>, <span class='rw_highlight'>hail</span> or snow). It fills rivers, lakes and oceans, some of it soaks through the earth to the groundwater."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that as water such as rain, <span class='rw_highlight'>sleet</span> or <span class='rw_highlight'>hail</span> falls from the sky, it <span class='rw_highlight'>infiltrates</span> the ground it falls onto. This means",
          lower_text_field: "it soaks into the ground. Water in plants is also soaked up by the sun. When this happens the process is called <span class='rw_highlight'>transpiration</span>."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that some water runs down the mountains and land, this is called ‘<span class='rw_highlight'>surface run off</span>’. It flows back",
          lower_text_field: "into the rivers or oceans."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that from the rivers, lakes and oceans, water evaporates and rises into the sky, and you know what",
          lower_text_field: "happens next! That’s the never-ending water cycle: on and on it goes, over and over again."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: To continue work on your Water Cycle display.",
      },
      13 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

end
