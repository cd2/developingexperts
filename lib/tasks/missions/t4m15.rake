desc "Lesson"
task t4m15: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that inside your body there’s an organ that you use to think, talk, listen, look, hear, smell, taste, dream,",
          lower_text_field: "feel, make decisions and remember. That sounds like a computer, phone, camera, television and scrapbook all rolled into one!"
      },
      7 => {
          upper_text_field: "Commander Mars explains that it’s your brain, which connects with your nerves to do all of these things. Your brain is in charge",
          lower_text_field: "of everything you do. It’s like the prime minister of your body. It keeps your heart beating."
      },
      8 => {
          upper_text_field: "Commander Mars explains that your brains makes sense of the information coming to it from your sensory organs – yours eyes,",
          lower_text_field: "ears, nose, tongue, and skin. It sends orders all over your body."
      },
      9 => {
          upper_text_field: "It stores information in memory, such as the aroma of orange peel, your best friend’s favourite colour or the answer to the",
          lower_text_field: "mathematics problem you learnt last week. The human brain is a pinkish-grey, wrinkled, spongy organ."
      },
      10 => {
          upper_text_field: "Commander Mars explains that an adult’s brain weighs about three pounds. The brain is divided into three main parts, which",
          lower_text_field: "you can see in the image. The <span>cerebrum</span> is by far the biggest section, about nine-tenths of your brain."
      },
      11 => {
          upper_text_field: "Commander Mars explains that most brain activity takes place in the outer layer of the <Span>cerebrum</span>, called the <span>cerebral cortex</span>. It’s",
          lower_text_field: "full of deep, wiggly grooves."
      },
      12 => {
          upper_text_field: "Different parts of the <span>cerebrum</span> do different things. Some parts understand speech, other parts store memories, other control",
          lower_text_field: "how hungry you feel, others control eye movement, and so on."
      },
      13 => {
          upper_text_field: "Commander Mars explains that a groove runs down the middle of your <span>cerebrum</span> and divides it into two halves: the right half of",
          lower_text_field: "your <span>cerebrum</span> controls the left half of your body, while your left half of your <span>cerebrum</span> controls the right half of your body."
      },
      14 => {
          upper_text_field: "Commander Mars explains that deep in the back of your brain lies the <span>cerebellum</span>. It coordinates your balance and movements.",
          lower_text_field: "When you first ride a bicycle, for example, you have to concentrate really hard."
      },
      15 => {
          upper_text_field: "Soon you learn to balance and move your body easily, without even thinking about what you’re doing. When that happens, your",
          lower_text_field: "<span>cerebellum</span> is in control."
      },
      16 => {
          upper_text_field: "Commander Mars explains that the <span>medulla</span> or <span>brain stem</span>, lies even deeper than the <span>cerebellum</span>. It controls involuntary body",
          main_video_slide: "functions. What are some involuntary body functions you can think of? Did you think of your heartbeat, breathing or digestion?"
      },
      17 => {
          upper_text_field: "Mission Activity: Now watch the film to test out how your brain can be tricked through hearing.",
          main_video_slide: ""
      },
      18 => {
          upper_text_field: "Mission Activity: Now watch the film to test out how your brain can be tricked through seeing.",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "Mission Activity: Use the handout to complete today’s experiments which explore the brain.",
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
