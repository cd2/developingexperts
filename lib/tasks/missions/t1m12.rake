desc "Lesson"
task t1m12: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that a year is divided into four parts, called the four <span class= 'rw_highlight'>seasons</span>. Do you know the names of the four",
          lower_text_field: "<span class= 'rw_highlight'>seasons</span>? They’re called <span class= 'rw_highlight'>spring, summer, autumn</span> and <span class= 'rw_highlight'>winter</span>. What is each <span class= 'rw_highlight'>season</span> like? That depends on where you live."
      },
      7 => {
          upper_text_field: "Captain Neptune explains that in many places, <span class= 'rw_highlight'>spring</span> is warm, and flowers bloom. Then comes a hot <span class= 'rw_highlight'>summer</span>, followed by",
          lower_text_field: "<span class= 'rw_highlight'>autumn</span> which is full of falling leaves. Then comes a cold <span class= 'rw_highlight'>winter</span>, and maybe a lot of snow."
      },
      8 => {upper_text_field: "In other places, the <span class= 'rw_highlight'>seasons</span> change in other ways. Some children live in places where it never snows.",
      },
      9 => {
        upper_text_field: "Captain Neptune explains that in some places, the leaves stay green all year round. But no matter what the weather is like,",
        lower_text_field: "where you live, there are still four <span class= 'rw_highlight'>seasons</span> – <span class= 'rw_highlight'>spring, summer, autumn</span>, winter – over and over, every year."
      },
      10 => {
          upper_text_field: "Captain Neptune wants to know what are the <span class= 'rw_highlight'>seasons</span> like where you live? Close your eyes. Think of each <span class= 'rw_highlight'>season</span>, what do you",
          lower_text_field: "hear or see or smell? Tell your talk partners."
      },
      11 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      12 => {
        upper_text_field: "Captain Neptune would like you get ready for today’s mission assignment and report to your work stations.", 
        sixty_count: ""},
      13 => {complete_investigation_slide: ""},
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
  }



    $rocket_words = {
          'Spring' => {
              description: "The <span class= 'rw_highlight'>season</span> <b>after <span class= 'rw_highlight'>Winter</span> and before <span class= 'rw_highlight'>Summer</span>.</b>",
              image: "" },
          'Summer' => {
              description: "<span class= 'rw_highlight'>Summer</span> begins in late June and last until late September.",
              image: "" },
          'Autumn' => {
              description: "<b><span class= 'rw_highlight'>Season</span> after <span class= 'rw_highlight'>Summer</span></b> and before <span class= 'rw_highlight'>Winter</span>. <span class= 'rw_highlight'>Autumn</span> begins in late September.",
              image: "" },
          'Winter' => {
              description: "The <b>coldest season of the year.</b> <span class= 'rw_highlight'>Winter</span> begins in late December.",
              image: "animal.jpg" },
          'Seasons' => {
              description: "<b>Four parts of the year.</b> Each <span class= 'rw_highlight'>season</span> has different weather.",
              image: "" },
    }




   $questions = {
      "What is <span class='rw_highlight'>Spring</span> ?" => {answer: "The <span class='rw_highlight'>season</span> after <span class='rw_highlight'>Winter</span> and before <span class='rw_highlight'>Summer</span>.",
                           image: "",
                           slide_id: 7},
      "What is <span class='rw_highlight'>Summer</span>?" => {answer: "<span class='rw_highlight'>Summer</span> begins in late June and last until late September.",
                                image: "",
                                slide_id: 7},
      "What is <span class='rw_highlight'>Autumn</span> pets?" => {answer: "<span class='rw_highlight'>Season</span> of the year after <span class='rw_highlight'>Summer</span> and before <span class='rw_highlight'>Winter</span>. <span class='rw_highlight'>Autumn</span> begins in late September.",
                           image: "",
                           slide_id: 7},
      "What is <span class='rw_highlight'>Winter</span>?" => {answer: "The coldest <span class='rw_highlight'>season</span> of the year. <span class='rw_highlight'>Winter</span> begins in late December.",
                              image: "",
                              slide_id: 7},
      "What are <span class='rw_highlight'>seasons</span>?" => {answer: "Four parts of the year. Each <span class='rw_highlight'>season</span> has different weather.",
                              image: "",
                              slide_id: 6},
    }


end
