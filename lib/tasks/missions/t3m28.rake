desc "Lesson"
task t3m28: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Colonel Jupiter explains that the world we live in today depends on many tools. With tools, we can build and dig, we lift and cut,",
          lower_text_field: "we grip and carry. What tools can you name? Did you think of a <span class='rw_highlight'>hammer</span>, <span class='rw_highlight'>screwdriver</span> or <span class='rw_highlight'>pliers</span>? What is a <span class='rw_highlight'>claw hammer</span> used for?"
          },
      8 => {
          upper_text_field: "Colonel Jupiter wants to know what a <span class='rw_highlight'>screwdriver</span> is used for? To twist a screw. What are <span class='rw_highlight'>pliers</span> for? To grip and pull. With the",
          lower_text_field: "right tool, you can do a lot more than you can do with your hands alone."
          },
      9 => {
          upper_text_field: "Colonel Jupiter explains that the simple <span class='rw_highlight'>machine</span> called a <span class='rw_highlight'>lever</span> is so simple you might not even think it is a <span class='rw_highlight'>machine</span>. But like",
          lower_text_field: "other <span class='rw_highlight'>machines</span>, <span class='rw_highlight'>levers</span> help us do work. <span class='rw_highlight'>Levers</span> can help us lift things."
          },
      10 => {
          upper_text_field: "Colonel Jupiter explains that you use a <span class='rw_highlight'>lever</span> when you use the <span class='rw_highlight'>claw</span> of a <span class='rw_highlight'>hammer</span> to pull a nail out of a board. You also use a",
          lower_text_field: "<span class='rw_highlight'>lever</span> when you use an old <span class='rw_highlight'>screwdriver</span> to prise open the lid of a tin of paint."
          },
      11 => {
          upper_text_field: "Colonel Jupiter explains that when you push down on one end of a tin opener, the other end is forced up, which brings the lid",
          lower_text_field: "up, too. Take a look at the picture here."
          },
      12 => {
          upper_text_field: "Colonel Jupiter explains that with a <span class='rw_highlight'>lever</span> you can lift something that you could not lift with just your hands. They",
          lower_text_field: "have used a <span class='rw_highlight'>lever</span> called a car jack to lift this car easily."
          },
      13 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      14 => {upper_text_field: "Today’s Second Mission Assignment: "},
      15 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }




  $rocket_words = {
          "Lever" => {
              description: "Bar used around a fixed point to control or move a heavy object.",
              image: "" },
          "Machine" => {
              description: "A moving piece of equipment equipment.",
              image: "" },
          "Claw Hammer" => {
              description: "Tool for hitting or removing nails.",
              image: "" },
          "Screwdriver" => {
              description: "Tool for turning screws.",
              image: "" },
          "Pliers" => {
              description: "Small tool with two handles for holding or pulling small objects such as nails, or cutting wire.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>lever</span>?" => {answer: "Bar used around a fixed point to control or move a heavy object.",
                           image: "",
                           slide_id: 9},
      "What is a <span class='rw_highlight'>machine</span>?" => {answer: "A moving piece of equipment.",
                                image: "",
                                slide_id: 9},
      "What is a <span class='rw_highlight'>claw hammer</span>?" => {answer: "Tool for hitting or removing nails.",
                           image: "",
                           slide_id: 7},
      "What is a <span class='rw_highlight'>scewdriver</span>?" => {answer: "Tool for turning screws.",
                              image: "",
                              slide_id: 8},
      "What is a pair of <span class='rw_highlight'>pliers</span>?" => {answer: "Small tool with two handles for holding or pulling small objects such as nails, or cutting wire.",
                              image: "",
                              slide_id: 8},
  }


end
