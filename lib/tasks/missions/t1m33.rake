desc "Lesson"
task t1m33: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }  

  $rocket_words = {
          'Deciduous' => {
              description: "A tree which loses its leaves in Autumn and grows new ones in Spring.",
              image: "" },
          'Evergreen' => {
              description: "A plant, bush, or tree which has leaves for the whole year.",
              image: "" },
          'Oak Tree' => {
              description: "A large <span class= 'rw_highlight'>deciduous</span> tree which comes from a seed called an acorn.",
              image: "" },
          'Pine Tree' => {
              description: "Is an <span class= 'rw_highlight'>evergreen</span> tree which grows in cooler areas.",
              image: "" },
          'Holly Bush' => {
              description: "<span class= 'rw_highlight'>Evergreen</span> plant which never loses its leaves with shiny, sharp leaves and red fruit.",
              image: "" },
  }


  $questions = {
      "What is a <span class= 'rw_highlight'>deciduous tree</span>?" => {answer: "A tree which loses its leaves in Autumn and grows new ones in Spring.",
                           image: "",
                           slide_id: ""},
      "What does <span class= 'rw_highlight'>evergreen</span> mean?" => {answer: "A plant, bush, or tree which has leaves for the whole year.",
                                image: "",
                                slide_id: ""},
      "What is an <span class= 'rw_highlight'>oak tree</span>?" => {answer: "A large <span class= 'rw_highlight'>deciduous</span> tree which comes from a seed called an acorn.",
                           image: "",
                           slide_id: ""},
      "What is a <span class= 'rw_highlight'>pine tree</span>?" => {answer: "Is an <span class= 'rw_highlight'>evergreen</span> tree which grows in cooler areas.",
                              image: "",
                              slide_id: ""},
      "What is a <span class= 'rw_highlight'>holly bush</span>?" => {answer: "<span class= 'rw_highlight'>Evergreen</span> plant which never loses its leaves with shiny, sharp leaves and red fruit.",
                              image: "",
                              slide_id: ""},
  }

end