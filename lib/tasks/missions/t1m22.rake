desc "Lesson"
task t1m22: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that just as it takes different ingredients to bake a cake, lots of different ingredients also go into making",
          lower_text_field: "rocks. Each of these ingredients is called a <span class= 'rw_highlight'>mineral</span>. Most rocks are made up of more than one kind of <span class= 'rw_highlight'>mineral</span>."
      },
      7 => {
          upper_text_field: "Captain Neptune explains that we can find lots of different <span class= 'rw_highlight'>minerals</span> in the earth. <span class= 'rw_highlight'>Gold</span> is a <span class= 'rw_highlight'>mineral</span>. When you think of <span class= 'rw_highlight'>gold</span>,",
          lower_text_field: "maybe you think of jewellery or pirate’s treasure."
      },
      8 => {upper_text_field: "Captain Neptune explains that for thousands of years, people have used <span class= 'rw_highlight'>gold</span> to make the things they consider most valuable,",
            lower_text_field: "such as coins, rings and crowns. All the <span class= 'rw_highlight'>gold</span> in the world comes from the earth."
      },
      9 => {
        upper_text_field: "Captain Neptune learns that <span class= 'rw_highlight'>diamonds</span> are <span class= 'rw_highlight'>minerals</span>. Just like <span class= 'rw_highlight'>gold</span>, all the <span class= 'rw_highlight'>diamonds</span> in the world come from the earth. In the",
        lower_text_field: "earth they don’t look like they do in a ring or necklace: it takes a lot of work to make them pretty and shiny."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that people use <span class= 'rw_highlight'>diamonds</span> for more than jewellery. <span class= 'rw_highlight'>Diamonds</span> are harder than other rock or <span class= 'rw_highlight'>mineral</span>,",
          lower_text_field: "so people use <span class= 'rw_highlight'>diamonds</span> to cut other rocks!"
      },
      11 => {
          upper_text_field: "Captain Neptune explains that <span class= 'rw_highlight'>quartz</span> is another <span class= 'rw_highlight'>mineral</span> which is more common than <span class= 'rw_highlight'>gold</span> and <span class= 'rw_highlight'>diamonds</span>. In many places, you can",
          lower_text_field: "find <span class= 'rw_highlight'>quartz</span> in the ground. <span class= 'rw_highlight'>Quartz</span> crystals can be used to make clocks and watches accurate."
      },
      12 => {
          upper_text_field: "Captain Neptune explains that people have learned how to find <span class= 'rw_highlight'>minerals</span> in the earth and how to dig them out, or mine them.",
          lower_text_field: "We mine the <span class= 'rw_highlight'>mineral</span> called halite, then use it to make salt for our food. People who work in mines are called ‘miners’."
      },
      13 => {
          upper_text_field: "Captain Neptune explains that we mine the <span class= 'rw_highlight'>mineral</span> iron ore, then use it to make steel, which is used to make cars, refrigerators and",
          lower_text_field: "bicycles. We mine the <span class= 'rw_highlight'>mineral</span> <span class= 'rw_highlight'>copper</span>, then use it to make cooking pots, electrical wiring and coins."
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      15 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }



end
