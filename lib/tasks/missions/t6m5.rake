desc "Lesson"
task t6m5: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      20 => {
          upper_text_field: "",
          sixty_count: ""
      },
      21 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}




  $rocket_words = {
          "Generation" => {
              description: "A group of individuals belonging to a specific category at the same time.",
              image: "" },
          "Species" => {
              description: "A class of individuals having some common characteristics or qualities.",
              image: "" },
          "Evolution" => {
              description: "Any process of formation or growth; development.",
              image: "" },
          "Genes" => {
              description: "The basic physical unit of heredity.",
              image: "" },
          "DNA" => {
              description: "The material in chromosomes that transfers genetic characteristics in all life forms.",
              image: "" },
  }



end
