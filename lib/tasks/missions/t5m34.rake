desc "Lesson"
task t5m34: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that you often <span class='rw_highlight'>separate</span> mixtures in everyday life. For example, when you <span class='rw_highlight'>separate</span> laundry you are",
          lower_text_field: "<span class='rw_highlight'>separating</span> a mixture. You can also <span class='rw_highlight'>sort</span> items; <span class='rw_highlight'>separate</span> things in a particular order. Can you give an example?"
          },
      7 => {
          upper_text_field: "Brigadier Venus explains that a mixture is a combination of substances that do not react chemically when they are mixed.",
          lower_text_field: "According to this definition, a solution such as sugar water is a mixture. Sugar and sand can also be a mixture."
          },
      8 => {
          upper_text_field: "Brigadier Venus explains that there are several separating <span class='rw_highlight'>techniques</span> which you can use to <span class='rw_highlight'>filter</span> a mixture apart. By",
          lower_text_field: "experimenting you can observe which <span class='rw_highlight'>techniques</span> are most suited for separating different combinations of mixtures."
          },
      9 => {
          upper_text_field: "Brigadier Venus explains that if you mix uncooked rice, kidney beans and flour in a mixing bowl, then spread the mixture onto",
          lower_text_field: "a large sheet of wax paper, you'll notice that the beans are easy to see. You can pick them out by hand and place them in a cup."
          },
      10 => {
          upper_text_field: "Brigadier Venus explains a suitable <span class='rw_highlight'>technique</span> to <span class='rw_highlight'>separate</span> the rice from the flour. Prepare a <span class='rw_highlight'>sieve</span> by cutting out a square",
          lower_text_field: "section of window screen, large enough to fit over a bowl. Place the screen over the mouth of the bowl."
          },
      11 => {
          upper_text_field: "Fix it with a large rubber band and create a funnel shape with wax paper. Pour the flour and rice mixture onto the screen. The",
          lower_text_field: "flour will pass through, leaving the rice on top."
          },
      12 => {
          upper_text_field: "Brigadier Venus explains that you can separate identical solids if you can identify a property that differentiates them. To observe",
          lower_text_field: "this <span class='rw_highlight'>technique</span>, you could gather a collection of aluminium and steel bolts, making sure that both sets look alike."
          },
      13 => {
          upper_text_field: "Mix the bolts well in a plastic bowl and lower a magnet towards them. Then something amazing happens! The steel bolts",
          lower_text_field: "contain iron which will be attracted to the magnet. As it gets close, the magnet will <span class='rw_highlight'>separate</span> the steel bolts from the pile."
          },
      14 => {
          upper_text_field: "Brigadier Venus explains that as space on the magnet fills, remove the attracted bolts and place them in a <span class='rw_highlight'>separate</span>",
          lower_text_field: "container. Keep passing the magnet over the bowl until you have removed all of the steel bolts"
          },
      15 => {
          upper_text_field: "Today’s film explains how to <span class='rw_highlight'>separate</span> a mixture.",
          main_video_slide: ""
          },
      17 => {
          upper_text_field: "Today’s Experiment. The ball <span class='rw_highlight'>separating</span> challenge.",
          main_video_slide: ""
          },
      18 => {
          upper_text_field: "Today’s First Mission Assignment",
          main_video_slide: ""
          },
      19 => {upper_text_field: "Today’s Second Mission Assignment: "},
      20 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }




  $rocket_words = {
          "Filter" => {
              description: "Separation of solid particles in a mixture by passing the mixture through a screen.",
              image: "" },
          "Separate" => {
              description: "To cause something to stop being joined together.",
              image: "" },
          "Sieve" => {
              description: "A tool with small holes used to <span class='rw_highlight'>separate</span> smaller particles from larger ones.",
              image: "" },
          "Technique" => {
              description: "A way of doing something by using special knowledge or skill.",
              image: "" },
          "Sort" => {
              description: "To separate things and put them in a particular order.",
              image: "" },
  }

  $questions = {
      "What does <span class='rw_highlight'>filter</span> mean?" => {answer: "Separation of solid particles in a mixture by passing the mixture through a screen.",
                           image: "",
                           slide_id: 8},
      "What does <span class='rw_highlight'>separate</span> mean?" => {answer: "To cause something to stop being joined together.",
                                image: "",
                                slide_id: 6},
      "What is a <span class='rw_highlight'>sieve</span>?" => {answer: "A tool with small holes used to <span class='rw_highlight'>separate</span> smaller particles from larger ones.",
                           image: "",
                           slide_id: 10},
      "What is a <span class='rw_highlight'>technique</span>?" => {answer: "A way of doing something by using special knowledge or skill.",
                              image: "",
                              slide_id: 8},
      "What does <span class='rw_highlight'>sort</span> mean?" => {answer: "To separate things and put them in a particular order.",
                              image: "",
                              slide_id: 6},
  }

end
