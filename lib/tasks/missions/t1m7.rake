desc "Lesson"
task t1m7: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune wants to know if you are ready to use your body? Ready? Then...put your arms over your head and reach",
          lower_text_field: "way up into the sky."
      },
      8 => {
          upper_text_field: "Captain Neptune wants you to stretch! Jump up and down! Bend at your waist and touch your toes. Curl up into a tiny ball. Lie",
          lower_text_field: "down and be absolutely still."
      },
      9 => {upper_text_field: "Captain Neptune explains that your body can do so many things. It can bend and jump and run and sleep. It can see and hear and",
            lower_text_field: "smell and taste and touch."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that it is important to take good care of your body. Your body needs special care. You need to keep",
          lower_text_field: "it clean by washing your hands. <span class= 'rw_highlight'>Cleanliness</span> is very important to stay <span class= 'rw_highlight'>healthy</span>."
      },
      11 => {
          upper_text_field: "Captain Neptune explains the importance of washing your hands before eating and after going to the bathroom.",
          lower_text_field: "Captain Neptune stays clean by taking baths regularly and by brushing his teeth."
      },
      12 => {
          upper_text_field: "Captain Neptune explains that you need to help your body grow stronger by eating <span class= 'rw_highlight'>healthy</span> <span class= 'rw_highlight'>foods</span>, by getting plenty of <span class= 'rw_highlight'>exercise</span>, and",
          lower_text_field: "by getting a good <span class= 'rw_highlight'>rest</span> every night."
      },
      13 => {
          upper_text_field: "Captain Neptune wants you to watch today’s film for your warm up workout!",
          main_video_slide: "",
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Invent your own hot school dinner menu for a week.",
      },
      15 => {upper_text_field: "Captain Neptune would like you to get ready for today’s mission assignment and report to your work stations.", sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }

end
