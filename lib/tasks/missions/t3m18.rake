desc "Lesson"
task t3m18: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter wants to know what happens to water vapour in the air? Some of it mixes with the air near the ground and",
          lower_text_field: "some of it rises high into the sky, way up where the air is cooler."
      },
      7 => {
          upper_text_field: "In this coolness, Colonel Jupiter explains that the water vapour turns back into little droplets of liquid water which can be called",
          lower_text_field: "<span class='rw_highlight'>precipitation</span>. When water vapour turns from gas back into liquid, we say it condenses. Here’s a way to see condensation happen."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that if you fill a glass which is nice and dry on the outside, with ice and water, after five or ten minutes",
          lower_text_field: "you will quickly see a thin coating of water develop on the outside of the glass. Feel it – it’s wet."
      },
      9 => {
          upper_text_field: "Colonel Jupiter wonders if you can work out where the water came from? Your glass didn’t leak. No, the water came from",
          lower_text_field: "the air. The ice water made the glass cold, which made the air around the glass cool, just like the air high in the sky."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that the water vapour in the air condenses – it turns into liquid – on the outside of your glass.",
          lower_text_field: "Up in the sky, when water vapour condenses into droplets of liquid, it forms <span class='rw_highlight'>clouds</span>."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that even though they may look like candy floss, <span class='rw_highlight'>clouds</span> are made of billions of water droplets (or",
          lower_text_field: "sometimes, if the air is very cold, billions of tiny ice particles)."
      },
      12 => {
          upper_text_field: "Wispy feathery <span class='rw_highlight'>clouds</span> are <span class='rw_highlight'>cirrus clouds</span>. They form high in the sky and are made of tiny ice crystals. Colonel Jupiter explains",
          lower_text_field: "that the water droplets bump up against each other in the <span class='rw_highlight'>clouds</span>."
      },
      13 => {
          upper_text_field: "But instead of saying, ‘Excuse me’ and getting out of each other’s way, they join and turn into bigger drops. Colonel Jupiter",
          lower_text_field: "explains that when the drops get heavy enough, they fall from the <span class='rw_highlight'>clouds</span> – it’s raining!"
      },
      14 => {
          upper_text_field: "Or, if it’s cold enough, instead of rain, snow will fall. Colonel Jupiter explains that snow is water frozen into tiny crystals that",
          lower_text_field: "fall as snowflakes. Big, puffy <span class='rw_highlight'>clouds</span> are <span class='rw_highlight'>cumulus clouds</span>. They are usually signs of good weather."
      },
      15 => {
          upper_text_field: "Colonel Jupiter explains that when a dark layer of <span class='rw_highlight'>stratus cloud</span> covers the sky, it often mean rain is on its way.",
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Create a Water Cycle display over the next two lessons. Create your <span class='rw_highlight'>clouds</span> and trees today.",
      },
      17 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
