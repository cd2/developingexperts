desc "Lesson"
task t3m15: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter is fascinated by nature and explains how it does some amazing things. For example, the life cycle of insects is",
          lower_text_field: "inspiring! Who would think that from a little, crawling <span class='rw_highlight'>caterpillar</span> would come a beautiful <span class='rw_highlight'>butterfly</span>?"
      },
      7 => {
          upper_text_field: "That’s a very big change and there’s a very big word to describe this kind of change. The word is <span class='rw_highlight'>metamorphosis</span>.",
          lower_text_field: "Colonel Jupiter explains that it comes from the Greek word for ‘changing shape’. Let’s take a closer look at the big change."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that a female <span class='rw_highlight'>butterfly</span> lays eggs. They look like tiny beads attached to the bark of a tree or the",
          lower_text_field: "underside of a leaf. When the eggs hatch, out comes – no not a <span class='rw_highlight'>butterfly</span>, not yet – but a <span class='rw_highlight'>caterpillar</span>."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that the <span class='rw_highlight'>caterpillar</span> is the <span class='rw_highlight'>larva</span>, or the baby insect. The <span class='rw_highlight'>caterpillar</span> crawls up and down the stalks of",
          lower_text_field: "plants finding fresh, green leaves to chew on."
      },
      10 => {
          upper_text_field: "The <span class='rw_highlight'>caterpillar</span> eats and eats - like a little eating machine! It grows longer and larger. Colonel Jupiter explains that when the",
          lower_text_field: "<span class='rw_highlight'>caterpillar</span> matures, it attaches itself to a leaf or a twig."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains it makes a tough, shiny covering that’s called a cocoon or <span class='rw_highlight'>chrysalis</span>, and wraps itself completely inside.",
          lower_text_field: "From the outside, it looks as if nothing is happening, but there is a <span class='rw_highlight'>pupa</span> inside!"
      },
      12 => {
          upper_text_field: "Colonel Jupiter explains that when the <span class='rw_highlight'>chrysalis</span> finally opens, the adult insect comes out, but it doesn’t look like a <span class='rw_highlight'>caterpillar</span>",
          lower_text_field: "anymore! Now you can tell it’s a <span class='rw_highlight'>butterfly</span>. When it first comes out, the <span class='rw_highlight'>butterfly</span> is weak, damp and crumpled."
      },
      13 => {
          upper_text_field: "Then slowly it spreads its wings. When the wings have dried, the <span class='rw_highlight'>butterfly</span> is ready to fly away and flutter from flower to flower to",
          lower_text_field: "look for food."
      },
      14 => {
          upper_text_field: "Today’s Film:",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment: Making a butterfly",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
