desc "Lesson"
task t2m15: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains that <span class='rw_highlight'>light bulbs</span> shine and mobile phones ring, thanks to <span class='rw_highlight'>electricity</span>. When you turn on a light at home or at",
          lower_text_field: "school, you are letting electricity flow through <Span class='rw_highlight'>wires</span> all the way to the <span class='rw_highlight'>light bulb</span>."
      },
      7 => {
          upper_text_field: "Major Saturn wants to know where electricity comes from. It probably comes from a <span class='rw_highlight'>power station</span> miles away, where big",
          lower_text_field: "machines generate the electricity and send it through <Span class='rw_highlight'>wires</span> to your home and school and lots of other places."
      },
      8 => {
          upper_text_field: "What happens when you turn on a light? Look at the model and find the following parts: <span class='rw_highlight'>Battery, wire, switch</span> and <span class='rw_highlight'>light bulb</span>.",
      },
      9 => {
          upper_text_field: "Major Saturn wants you to look at the <span class='rw_highlight'>battery</span> in the picture. The <span class='rw_highlight'>battery</span> uses chemicals to make electricity. Never try to open",
          lower_text_field: "a <span class= 'rw_highlight'>battery</span>! The chemicals inside could hurt you."
      },
      10 => {
          upper_text_field: "Do you see the <span class='rw_highlight'>wires</span>? Use your finger to follow the part of the <span class='rw_highlight'>wire</span> which goes from one end of the <span class='rw_highlight'>battery</span> to the <span class='rw_highlight'>switch</span>.",
          lower_text_field: "Then it goes from the <span class='rw_highlight'>switch</span> to the <span class='rw_highlight'>light bulb</span>."
      },
      11 => {
          upper_text_field: "Then it goes from the <span class='rw_highlight'>light bulb</span> back to the other end of the <span class='rw_highlight'>battery</span>, the <span class='rw_highlight'>wire</span> has to be connected to both ends of the <span class='rw_highlight'>battery</span>",
          lower_text_field: "to make the electricity flow. Let’s follow the electricity on its path from one end of the <span class='rw_highlight'>battery</span> through the <span class= 'rw_highlight'>switch</span> to the <span class='rw_highlight'>light bulb</span>."
      },
      12 => {
          upper_text_field: "The electricity from the <span class='rw_highlight'>battery</span> flows through the <span class='rw_highlight'>wire</span>, rather like water flowing through a hose. What happens when the",
          lower_text_field: "electricity gets to the <span class='rw_highlight'>switch</span>?"
      },
      13 => {
          upper_text_field: "Major Saturn explains that it depends on the whether the <span class='rw_highlight'>switch</span> is turned ‘off’ or ‘on’. If the <span class='rw_highlight'>switch</span> is ‘on’ then the electricity can",
          lower_text_field: "continue along its path to the <span class='rw_highlight'>bulb</span>. But if the <span class='rw_highlight'>switch</span> is ‘off’, then the path of electricity is broken."
      },
      14 => {
          upper_text_field: "Major Saturn wants you to pretend that you are walking a long path. You come to a river, and you find that there is no bridge.",
          lower_text_field: "Oh no! The path is broken, so you can’t keep going."
      },
      15 => {
          upper_text_field: "Now pretend there is a bridge but there’s no break in the path, and you can continue on your way. That’s what happens with",
          lower_text_field: "electricity: as it travels along the <span class= 'rw_highlight'>wire</span>, it comes to the <Span class='rw_highlight'>switch</span>."
      },
      16 => {
          upper_text_field: "If the <span class='rw_highlight'>switch</span> is turned ‘off’, you could say that there is no path: the electricity cannot continue along the path to the <span class='rw_highlight'>light bulb</span>.",
      },
      17 => {
          upper_text_field: "But when the <span class='rw_highlight'>switch</span> is turned ‘on’, you could say there is a bridge: the electricity can keep right on going from the <span class='rw_highlight'>battery</span>, through",
          lower_text_field: "the <span class='rw_highlight'>wire</span> and turned on <span class='rw_highlight'>switch</span>, through the <span class='rw_highlight'>wire</span>, and to the <span class='rw_highlight'>bulb</span>. When electricity flows through the <span class='rw_highlight'>light bulb</span>, the <span class='rw_highlight'>bulb</span> lights up."
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Do you think it is possible for vinegar to power a light? Watch the film then create your own light.",
          main_video_slide: ""
      },
      19 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
}

end
