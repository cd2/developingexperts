desc "Lesson"
task t4m2: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars is going to take a look at <span class='rw_highlight'>series circuits</span>. The picture shows a <span class='rw_highlight'>series circuit</span>. A set of Christmas tree lights runs",
          lower_text_field: "on a <span class='rw_highlight'>series circuit</span>. If one light bulb goes off, the whole set of lights stops working. He wants you to describe what you see."
      },
      7 => {
          upper_text_field: "Commander Mars explains that the flow of electricity (<span class='rw_highlight'>electrical current</span>) stops when the circuit has been broken. It’s like the",
          lower_text_field: "whole class joining hands to make one large circle. If your class represented an electric circuit, you would be a <span class='rw_highlight'>series circuit</span>."
      },
      8 => {
          upper_text_field: "Commander Mars explains that if one of you let go of someone’s hand, the circuit would be broken. When you are all holding",
          lower_text_field: "everyone’s hands and a complete circle has formed, the circuit is complete and is a closed circuit."
      },
      9 => {
          upper_text_field: "Commander Mars explains that when using a <span class='rw_highlight'>series circuit</span>, the bulbs become dimmer when more bulbs are used in the circuit.",
      },
      10 => {
          upper_text_field: "Commander Mars explains that special symbols are needed when creating a diagram for an electric circuit. The symbols represent",
          lower_text_field: "the different parts or components of the circuit. Which symbols have been used for the <span class='rw_highlight'>switch</span>, the bulb and <span class='rw_highlight'>buzzer</span>?"
      },
      11 => {
          upper_text_field: "Commander Mars explains that the picture shows the symbols used to represent the following components, a battery, bulb,",
          lower_text_field: "<span class='rw_highlight'>switch</span> and resistor and is called a <span class='rw_highlight'>circuit diagram</span>. The battery is the power source. The bulb is the light source."
      },
      12 => {
          upper_text_field: "Commander Mars explains that the <span class='rw_highlight'>switch</span> turns the power on and off. The resistor acts like a tap to slow down the amount of",
          lower_text_field: "current flowing from the battery to the bulb. It protects the bulb by preventing too much electricity flowing into the bulb."
      },
      13 => {
          upper_text_field: "Commander Mars wants to know what you think might happen if too much electricity flowed into the bulb?",
      },
      14 => {
          upper_text_field: "Today’s Film: Exploring the different types of electrical circuits.",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "You have 60 seconds to report to your work stations for Today’s Mission Assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Create your own <span class='rw_highlight'>series circuit</span> using a bulb, battery and <span class='rw_highlight'>switch</span>.",
          lower_text_field: "Use the handout and kit. Draw your <span class='rw_highlight'>circuit diagram</span>."
      },
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}

end
