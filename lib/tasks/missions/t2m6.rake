desc "Lesson"
task t2m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn discovered that in 1888 <span class='rw_highlight'>Dunlop’s</span> founder, <span class='rw_highlight'>John Boyd Dunlop</span>, was watching his young son riding his tricycle on",
          lower_text_field: "solid rubber tyres over cobbled ground."
      },
      7 => {
          upper_text_field: "He noticed that his little boy was not going very fast and did not seem very comfortable. In order to give his son a smoother ride",
          lower_text_field: "and better handling <span class='rw_highlight'>Dunlop</span> took the tricycle, wrapped its wheels in thin rubber sheets and glued them together."
      },
      8 => {
          upper_text_field: "<span class='rw_highlight'>Dunlop</span> then inflated them using a football pump. That way he developed the first air cushioning system in history, and laid the",
          lower_text_field: "foundation for the first pneumatic tyre. If your family has a car they may well have <span class='rw_highlight'>Dunlop</span> tyres on their car."
      },
      9 => {
          upper_text_field: "Major Saturn explains you may have noticed that the weight of your car will be <span class='rw_highlight'>pushing</span> down on the tyres.",
          lower_text_field: "It’s a <span class='rw_highlight'>pushing force</span> which is pressing down the weight of your car on the tyres. You can’t see them, but <span class='rw_highlight'>forces</span> make the world go round."
      },
      10 => {
          upper_text_field: "What would happen if your tyres were made of wood? How would your car journey feel? Discuss with your talk partners.",
      },
      11 => {
          upper_text_field: "Major Saturn learns that friction helps the tyres on your car to keep their grip on the road. Friction happens when two things rub",
          lower_text_field: "against each other. Friction makes things slow down. It can also warm them up. Try rubbing your hands together."
      },
      12 => {
          upper_text_field: "Major Saturn wants to know if your hands are warmer? Gravity is a natural <span class='rw_highlight'>force</span>. It pulls everything down towards the earth.",
      },
      13 => {
          upper_text_field: "Major Saturn explains that when you <span class='rw_highlight'>push</span> a friend on a swing, you are using another <span class='rw_highlight'>force</span>. <span class='rw_highlight'>Pushing</span> moves something in the",
          lower_text_field: "direction of the <span class='rw_highlight'>push</span>. The harder the <span class='rw_highlight'>push</span>, the further the item goes."
      },
      14 => {
          upper_text_field: "Major Saturn explains that pulling something has a similar action. The harder you pull, the faster something moves along.",
      },
      15 => {
          upper_text_field: "Major Saturn explains that pressure is another <span class='rw_highlight'>force</span>. Pressure is <span class='rw_highlight'>force</span> applied by weight. For example, if you press down on",
          lower_text_field: "a grape, the pressure squishes it."
      },
      16 => {
          upper_text_field: "Walk in snow and the pressure of your feet leaves footprints. Any kind of <span class='rw_highlight'>force</span> is really just a <span class='rw_highlight'>push</span> or a pull.",
      },
      17 => {
          upper_text_field: "Major Saturn explains that different <span class='rw_highlight'>materials</span> respond differently to <span class='rw_highlight'>forces</span>.We have different words to describe this.",
      },
      18 => {
          upper_text_field: "The description is called the <span class='rw_highlight'>material’s</span> <span class='rw_highlight'>properties</span>. Rulers are made from different <span class='rw_highlight'>materials</span>. This ruler is made from <span class='rw_highlight'>material</span>",
          lower_text_field: "which is flexible. If you tried to bend some rulers made from different <span class='rw_highlight'>materials</span> that are less flexible, what might happen?"
      },
      19 => {
          upper_text_field: "Major Saturn explains that a <span class='rw_highlight'>material</span> might be described as having <span class='rw_highlight'>properties</span> such as being springy, bendy, flexible, brittle,",
          lower_text_field: "hard, soft, stretchy or heat proof (which is handy when getting these yummy cakes out of the oven)."
      },
      20 => {
          upper_text_field: "Major Saturn discovers that springs and elastic are also types of <span class='rw_highlight'>force</span>. <span class='rw_highlight'>Push</span> against them and they resist. They spring back",
          lower_text_field: "with the same <span class='rw_highlight'>force</span> you gave them."
      },
      21 => {
          upper_text_field: "Mission Assignment One: Tally up the number of classroom objects made out of the following <span class='rw_highlight'>materials</span>.",
      },
      22 => {
          upper_text_field: "Mission Assignment Two: Compare the samples at your work stations and decide if the <span class='rw_highlight'>material</span> is fragile, strong, hard, soft, rough, smooth, flexible or rigid.",
      },
      23 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations and be ready to start today’s mission.",
          sixty_second_count: ""
      },
      24 => {complete_investigation_slide: ""},
      25 => {rocket_word_slide: ""},
      26 => {quiz_slide: ""},
      27 => {outro_slide: ""}
  }

end
