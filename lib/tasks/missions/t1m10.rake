desc "Lesson"
task t1m10: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune wonders if you have ever seen people dumping furniture and paper <span class= 'rw_highlight'>litter</span> on the street? We don’t need to do this",
          lower_text_field: "as we can often <span class= 'rw_highlight'>reuse</span> things we don’t want or need anymore."
      },
      7 => {
          upper_text_field: "Captain Neptune explains that one good way to waste less is to recycle. <span class= 'rw_highlight'>Recycling</span> means using things over and over instead of",
          lower_text_field: "throwing them away. Material such as paper, <span class= 'rw_highlight'>glass</span> and plastic can all be <span class= 'rw_highlight'>recycled</span>."
      },
      8 => {
          upper_text_field: "Captain Neptune describes how lots of families <span class= 'rw_highlight'>recycle</span> clothing: when one child grows up and gets too big for a shirt or a pair of",
          lower_text_field: "trousers, then the clothes are passed on to a smaller child – maybe a brother or sister or neighbour – to be worn again."
      },
      9 => {upper_text_field: "Captain Neptune describes how <span class= 'rw_highlight'>reusing</span> things instead of throwing them away, helps to conserve what the earth has given",
            lower_text_field: "us. Many cities and towns have <span class= 'rw_highlight'>recycling centres</span> to collect materials that can be used over and over. You can <span class= 'rw_highlight'>recycle</span>, too."
      },
      10 => {
        upper_text_field: "Captain Neptune wonders if there a <span class= 'rw_highlight'>recycling centre</span> in your town or city? Do you know where it is? Here are some things you",
        lower_text_field: "shouldn’t throw away. Take them to a <span class= 'rw_highlight'>recycling centre</span> instead."
      },
      11 => {
          upper_text_field: "Today’s film shows how Ealing council in London <span class= 'rw_highlight'>recycling centres</span> work.",
          main_video_slide: ""
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Complete a recycling investigation In your school. Identify opportunities to <span class= 'rw_highlight'>recycle</span> materials.",
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: Using some of the words below create your own <span class= 'rw_highlight'>recycling</span> poster to display around your school.",
      },
      14 => {
        upper_text_field: "Captain Neptune would like you to get ready for today’s mission assignment and report to your work stations.", 
        sixty_count: ""},
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
  }





end
