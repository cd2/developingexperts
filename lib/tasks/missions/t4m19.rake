desc "Lesson"
task t4m19: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants to know what your <span>stomach</span> does? The stomach is a muscular organ located on the left side of the",
          lower_text_field: "upper abdomen. Your <span>stomach</span> is designed to stir up the food you eat."
      },
      7 => {
          upper_text_field: "Commander Mars explains that as it stirs the stomach glands add liquids and chemicals to help digest the food. The piece of",
          lower_text_field: "apple you ate, has now been broken down and no longer looks anything like an apple."
      },
      8 => {
          upper_text_field: "All the grinding, stirring and chemicals make it look like soup. Commander Mars explains that now the food is almost small",
          lower_text_field: "enough for your body to use."
      },
      9 => {
          upper_text_field: "The soupy substance moves from your stomach to the <span>intestines</span>. Commander Mars explains that your <span>intestines</span> are a long,",
          lower_text_field: "coiled-up tube that winds around inside your tummy."
      },
      10 => {
          upper_text_field: "If you stretched them out, they would measure much longer than your height. An adult’s <span>intestines</span> are about eight metres long!",
          lower_text_field: "Commander Mars explains that most of the <span>intestine</span> is like a narrow rope. This is called the <span>small intestine</span>."
      },
      11 => {
          upper_text_field: "The last part of the tube is bigger. This is called the <span>large intestine</span>. Commander Mars explains that in your <span>small intestine</span>,",
          lower_text_field: "the soupy food is mixed with more liquids and chemicals that break down the food into bits too small to see."
      },
      12 => {
          upper_text_field: "Commander Mars explains that the good particles, called the nutrients are then absorbed into your blood. Your blood carries",
          lower_text_field: "the nutrients to all the cells of your body. As the digested food travels through the intestine it passes the <span>appendix</span>."
      },
      13 => {
          upper_text_field: "Commander Mars explains that the <span>appendix</span> is a little tube that grows where the <span>small intestine</span> joins the <span>large intestine</span>. Even",
          lower_text_field: "though it’s there, it doesn’t really have a job. Usually it doesn’t bother anyone, but sometimes the <span>appendix</span> can get infected."
      },
      14 => {
          upper_text_field: "Commander Mars explains that when this happens, a person has to have an operation to have their appendix removed. But",
          lower_text_field: "it’s okay to take it out because you don’t really need it."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}

end
