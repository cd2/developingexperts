desc "Lesson"
task t1m29: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that thousands of <span class= 'rw_highlight'>aeroplanes</span> and jets fly across the country and around the world every day. But it",
          lower_text_field: "wasn’t very long ago that people thought that human beings would never fly."
          },
      7 => {
          upper_text_field: "Captain Neptune explains that they thought that flying was something that birds, bats and some insects could do - but",
          lower_text_field: "people? ‘No’, they said, ‘that won’t happen’."
          },
      8 => {
          upper_text_field: "But it has happened; and it took two brothers, <span class= 'rw_highlight'>Wilbur and Orville Wright</span>, to prove that human beings could build a machine to fly.",
          lower_text_field: "The picture shows the <span class= 'rw_highlight'>Wright Brothers'</span> National Memorial, located in Kill Devil Hills, North Carolina, in the U.S.A."
          },
      9 => {
          upper_text_field: "Captain Neptune explains that when they were young, <span class= 'rw_highlight'>Wilbur and Orville Wright were known by their friends as boys who could",
          lower_text_field: "build and fix things. Together they opened a bicylce shop."
          },
      10 => {
          upper_text_field: "Captain Neptune explains that they built, sold and rented bicycles. In the 1880s, many people wanted one of those new two-wheeled",
          lower_text_field: "travelling machines. At night the <span class= 'rw_highlight'>Wright brothers</span> used their brains and tools to study the possibilities of flying."
          },
      11 => {
          upper_text_field: "Captain Neptune explains that they heard about a German engineer who had built a glider. A glider is a plane with no engine,",
          lower_text_field: "it's carried by the wind, like a kite."
          },
      12 => {
          upper_text_field: "Captain Neptune explains that even when the <span class= 'rw_highlight'>Wright brothers</span> heard the sad news that the German engineer had been killed",
          lower_text_field: "when his glider crashed, they kept on trying to find a way to fly."
          },
      13 => {
          upper_text_field: "Captain Neptune explains that they built their own glider. It had two big wings, one on top and one on the bottom and a place for",
          lower_text_field: "a person to lie down in the middle. That first glider flew low to the ground, then it crashed."
          },
      14 => {
          upper_text_field: "Captain Neptune explains that Wilbur was discouraged: he said, 'Man will not fly in a thousand years'. But he was wrong. He and",
          lower_text_field: "his brother kept working and soon built another plane. This plane had an engine and propellers."
          },
      15 => {
          upper_text_field: "On December 17th 1903, the <span class= 'rw_highlight'>Wright brothers</span> stood on the Kitty Hawk sand dunes and tossed a coin to see which one of them",
          lower_text_field: "would fly their plane first. <span class= 'rw_highlight'>Orville</span> won. <span class= 'rw_highlight'>Orville</span> climbed into their plane Flyer 1 and flew through the air for twelve seconds."
          },
      16 => {
          upper_text_field: "That doesn't sound like a very long <span class= 'rw_highlight'>flight</span> to us today, but to the <span class= 'rw_highlight'>Wright brothers</span>, twelve seconds meant success! Captain",
          lower_text_field: "Neptune explains that they flew Flyer 1 three more times that day."
          },
      17 => {
          upper_text_field: "On the fourth <span class= 'rw_highlight'>flight</span> the plane stayed in the air for 59 seconds - almost a minute - and flew 852 feet, almost the length of three",
          lower_text_field: "football fields. The <span class= 'rw_highlight'>Wright brother</span> had proved it: human beings could fly in a flying machine!"
          },
      18 => {
          upper_text_field: "Captain Neptune explains that only five other people saw them fly that December day at Kitty Hawk. Only a few newspapers even",
          lower_text_field: "wrote about it. No one seemed to understand how important the <span class= 'rw_highlight'>aeroplane</span> would be. But the <span class= 'rw_highlight'>Wright brothers</span> kept on flying."
          },
      19 => {
          upper_text_field: "Captain Neptune explains how important the <span class= 'rw_highlight'>design</span> of the <span class= 'rw_highlight'>aeroplane</span> and its structure is. The right materials need to be",
          lower_text_field: "used. A lighter <span class= 'rw_highlight'>aeroplane</span> will use less fuel so will be cheaper to fly."
          },
      20 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      21 => {main_video_slide: "",
            upper_text_field: "Today’s Second Mission Assignment: "},
      22 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      23 => {complete_investigation_slide: ""},
      24 => {rocket_word_slide: ""},
      25 => {quiz_slide: ""},
      26 => {outro_slide: ""}
  }

    $rocket_words = {
          'Aeroplane' => {
              description: "A powered flying vehicle with fixed wings.",
              image: "" },
          'Flight' => {
              description: "The action or process of flying through the air.",
              image: "" },
          'Wilbur Wright' => {
              description: "One of two brothers who developed the world's first <span class= 'rw_highlight'>aeroplane</span>.",
              image: "" },
          'Orville Wright' => {
              description: "The older brother of <span class= 'rw_highlight'>Wilbur</span>, together they invented the world's first <span class= 'rw_highlight'>aeroplane</span>.",
              image: "" },
          'Design' => {
              description: "To plan and make decisions about something that is being built or created.",
              image: "" },
    }


  $questions = {
      "What is an aeroplane" => {answer: "A powered flying vehicle with fixed wings.",
                           image: "",
                           slide_id: 6},
      "What is <span class= 'rw_highlight'>flight</span>?" => {answer: "The action or process of flying through the air.",
                                image: "",
                                slide_id: 7},
      "Who was <span class= 'rw_highlight'>Wilbur Wright</span>?" => {answer: "One of two brothers who developed the world's first <span class= 'rw_highlight'>aeroplane</span>.",
                           image: "",
                           slide_id: 9},
      "Who was <span class= 'rw_highlight'>Orville Wright</span>?" => {answer: "The older brother of <span class= 'rw_highlight'>Wilbur</span>, together they invented the world's first <span class= 'rw_highlight'>aeroplane</span>.",
                              image: "",
                              slide_id: 9},
      "What does <span class= 'rw_highlight'>design</span> mean?" => {answer: "To plan and make decisions about something that is being built or created.",
                              image: "",
                              slide_id: 19},
  }

end
