desc "Lesson"
task t1m20: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains how sometimes holes open in the Earth’s crust. When these holes open, hot gas and liquid can",
          lower_text_field: "escape from deep down in the earth."
      },
      7 => {
          upper_text_field: "In some places, boiling hot steam explodes out of a hole in the ground, spewing high up into the air. Captain Neptune asks if",
          lower_text_field: "you know what these are called?"
      },
      8 => {
          upper_text_field: "Yes, a <span class= 'rw_highlight'>geyser</span>. Captain Neptune explains that in the state of Wyoming in America, at least two hundred <span class= 'rw_highlight'>geysers</span> shoot up",
          lower_text_field: "out of the ground."
      },
      9 => {
         upper_text_field: "Captain Neptune learns that the most famous one shoots 50,000 litres of water – boiling hot, so it’s steam – 50 metres up, far",
         lower_text_field: "above the treetops around it."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that for a long time, this <span class= 'rw_highlight'>geyser</span> has shot up regularly, nearly once an hour, so people call it ‘Old",
          lower_text_field: "Faithful’. You can see Old Faithful <span class= 'rw_highlight'>erupt</span> in the film."
      },
      11 => {
          upper_text_field: "Captain Neptune explains that in other places, hot, <span class= 'rw_highlight'>molten</span>, liquid rock bursts out of the ground. What do we call this feature?",
          lower_text_field: "A <span class= 'rw_highlight'>volcano</span>."
      },
      12 => {
          upper_text_field: "Captain Neptune explains that <span class= 'rw_highlight'>volcanoes</span> give us an idea of what it’s like beneath the Earth’s crust. We know that the <span class= 'rw_highlight'>molten</span> rock",
          lower_text_field: "comes from deep down inside the Earth’s mantle layer."
      },
      13 => {
          upper_text_field: "When it spurts out through the top of a mountain we call it a <span class= 'rw_highlight'>volcano</span>. Captain Neptune explains that the <span class= 'rw_highlight'>molten</span> rock, called",
          lower_text_field: "<span class= 'rw_highlight'>lava</span>, can travel in fiery streams down the mountainside, burning everything in its path."
      },
      14 => {
          upper_text_field: "Sometimes a <span class= 'rw_highlight'>volcano</span> can <span class= 'rw_highlight'>erupt</span>, then remain quiet for many years, and then <span class= 'rw_highlight'>erupt</span> again. Captain Neptune explains that in",
          lower_text_field: "2010, a <span class= 'rw_highlight'>volcano</span> <span class= 'rw_highlight'>erupted</span> in Iceland."
      },
      15 => {
          upper_text_field: "It hadn’t <span class= 'rw_highlight'>erupted</span> for almost a hundred years, then threw so much ash up in the air that aeroplanes couldn’t fly in parts of Europe",
          lower_text_field: "for several days."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      17 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
  }


end
