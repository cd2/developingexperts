desc "Lesson"
task t1m21: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "The next time you have a chance, Captain Neptune wants you to pick up a handful of <span class='rw_highlight'>rocks</span> and look at them. You’ll notice that they",
          lower_text_field: "are all very different. Some are all one colour, while others are streaked with different colours."
      },
      7 => {
          upper_text_field: "Captain Neptune describes how some are dark, while others are pale and so light that you can almost see through them. It’s like",
          lower_text_field: "looking through a foggy glass."
      },
      8 => {upper_text_field: "Captain Neptune wants to know if you ever picked a pebble up from the beach? Some are so smooth that it almost makes you",
            lower_text_field: "want to rub them, while others are rough and jagged."
      },
      9 => {
        upper_text_field: "There are many different <span class='rw_highlight'>rocks</span>. Captain Neptune asks, ‘Have you heard of granite, coal and marble?’ Scientists group <span class='rw_highlight'>rocks</span>",
        lower_text_field: "by how they are made. There are three different types of <span class='rw_highlight'>rock</span> groups which you will learn about next year."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that there are many famous <span class='rw_highlight'>rocks</span> around the world. Can you name some? Here are some examples:",
          lower_text_field: "The Giant’s Causeway in Northern Ireland."
      },
      11 => {
          upper_text_field: "Then there is Ayers <span class='rw_highlight'>Rock</span> in Australia.",
      },
      12 => {
          upper_text_field: "Mount Everest, is the tallest mountain in the world and can be found in the mountain range of Nepal and Tibet.",
      },
      13 => {
          upper_text_field: "Captain Neptune’s favourite <span class='rw_highlight'>rock</span> formation is the <span class='rw_highlight'>Grand Canyon</span> in Arizona, U.S.A.",
      },
      14 => {
          upper_text_field: "Captain Neptune explains that <span class='rw_highlight'>rocks</span> are very useful. Sometimes we use <span class='rw_highlight'>rocks</span> to build things. Can you think of things that are",
          lower_text_field: "made from <span class='rw_highlight'>rock</span> in a town and the countryside?"
      },
      15 => {
          upper_text_field: "Captain Neptune explains that <span class='rw_highlight'>rocks</span> can provide shelter. We call these <span class='rw_highlight'>caves</span>. <span class='rw_highlight'>Caves</span> can be found above and below ground.",
      },
      16 => {
          upper_text_field: "Captain Neptune explains that people <span class='rw_highlight'>mine</span> or dig <span class='rw_highlight'>rock</span> from the ground. The place where this happens is called a <span class='rw_highlight'>mine</span>. Only",
          lower_text_field: "one type of <span class='rw_highlight'>rock</span> can be dug from each <span class='rw_highlight'>mine</span>. This is a marble <span class='rw_highlight'>mine</span> in Italy."
      },
      17 => {
          upper_text_field: "Captain Neptune explains how some artists like to make statues out of marble. Marble is often used for the walls and stairs of",
          lower_text_field: "big buildings."
      },
      18 => {
          upper_text_field: "The longest wall which has been built out of stone or pieces of <span class='rw_highlight'>rock</span>, brick, earth and wood is the <span class='rw_highlight'>Great Wall of China</span>. It is so",
          lower_text_field: "large that you can see it from outer space."
      },
      19 => {
          upper_text_field: "Captain Neptune explains that <span class='rw_highlight'>rocks</span> can be used for sporting activities. Some people like to climb <span class='rw_highlight'>rocks</span> and some people like",
          lower_text_field: "to go potholing. What do you think this is? Have you ever been to the beach to explore <span class='rw_highlight'>rock</span> pools?"
      },
      20 => {
          upper_text_field: "Today’s Mission Assignment: Make and decorate your own monster <span class='rw_highlight'>rocks</span>!",
      },
      21 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
  }




end
