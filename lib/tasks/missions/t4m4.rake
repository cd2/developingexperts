desc "Lesson"
task t4m4: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that electrical <span class='rw_highlight'>conductors</span> are materials which allow electricity to flow through them easily.",
          lower_text_field: "Most metals are very good <span class='rw_highlight'>conductors</span>."
      },
      7 => {
          upper_text_field: "Commander Mars explains that <span class='rw_highlight'>insulators</span> are materials that do not let electricity pass through them easily. Plastic, wood, glass",
          lower_text_field: "and rubber are all examples of good <span class='rw_highlight'>insulators</span>."
      },
      8 => {
          upper_text_field: "Commander Mars explains how wire which carries electricity to electrical appliances uses both types of material. Wire is used",
          lower_text_field: "to carry the electrical current. It is then wrapped in plastic to <span class='rw_highlight'>insulate</span> the material to prevent people from getting an <span class='rw_highlight'>electric shock</span>."
      },
      9 => {
          upper_text_field: "Commander Mars explains how humans can conduct electricity which means they need to be very careful when using any",
          lower_text_field: "electrical appliance. What is wrong in this picture?"
      },
      10 => {
          upper_text_field: "Commander Mars explains that we must always follow health and safety instructions carefully and ensure that all wires which carry",
          lower_text_field: "electricity are fully covered with a material which is an <span class='rw_highlight'>insulator</span>."
      },
      11 => {
          upper_text_field: "You can’t see him but Commander Mars is looking very serious. He explains that if we touch a mains wire which has electricity",
          lower_text_field: "running through it we will get an <span class='rw_highlight'>electrical shock</span>. A shock from electricity can be fatal. What does that mean?"
      },
      12 => {
          upper_text_field: "Commander Mars explains how water is a good <span class='rw_highlight'>conductor</span> which means we need to keep water away from electrical appliances.",
      },
      13 => {
          upper_text_field: "Commander Mars explains that electrons find it hard to pass through <span class='rw_highlight'>insulating</span> material that surrounds a wire. We should",
          lower_text_field: "always make sure that the wires on our plugs are not damaged!"
      },
      14 => {
          upper_text_field: "If an <span class='rw_highlight'>insulator</span> does not surround a wire which is carrying a current, it is possible for the electron flow to get directed to",
          lower_text_field: "another <span class='rw_highlight'>conductor</span> that comes into contact with the circuit."
      },
      15 => {
          upper_text_field: "Electricity will follow the path of least <span class='rw_highlight'>resistance</span>. If the electrons take a path short of the complete circuit, it is called a <span class='rw_highlight'>short circuit</span>.",
      },
      16 => {
          upper_text_field: "Today’s Film: The difference between an electrical <span class='rw_highlight'>conductor</span> and <span class='rw_highlight'>insulator</span>.",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "You have 60 seconds to report to your work stations for Today’s Mission Assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Which materials are <span class='rw_highlight'>conductors</span> and which ones are <span>insulators</span>. Predict and then test using the kit provided.",
      },
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

  $rocket_words = {
          "Conductor" => {
              description: "Electrical <span class='rw_highlight'>conductors</span> are materials which allow electricity to flow through them easily.",
              image: "" },
          "Insulators" => {
              description: "Materials that do not let electricity pass through them easily.",
              image: "" },
          "Resistance" => {
              description: "A measure of how much an object opposes the passage of electrons.",
              image: "" },
          "Electrical Shock" => {
              description: "An <span class='rw_highlight'>electric shock</span> can result in a minor or severe injury to a person.",
              image: "" },
          "Short Circuit" => {
              description: "An electrical circuit that allows a current to travel along an unintended path.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>conductor</span>?" => {answer: "Electrical <span class='rw_highlight'>conductors</span> are materials which allow electricity to flow through them easily.",
                           image: "",
                           slide_id: 6},
      "What is an <span class='rw_highlight'>insulator</span>?" => {answer: "Materials that do not let electricity pass through them easily.",
                                image: "",
                                slide_id: 7},
      "What is <span class='rw_highlight'>resistance</span>?" => {answer: "A measure of how much an object opposes the passage of electrons.",
                           image: "",
                           slide_id: 15},
      "What is an <span class='rw_highlight'>electric shock</span>?" => {answer: "An <span class='rw_highlight'>electric shock</span> can result in a minor or severe injury to a person.",
                              image: "",
                              slide_id: 8},
      "What is a <span class='rw_highlight'>short circuit</span>?" => {answer: "An electrical circuit that allows a current to travel along an unintended path.",
                              image: "",
                              slide_id: 15},
  }

end
