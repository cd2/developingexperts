desc "Lesson"
task t5m5: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that in some places in the world, a lot of volcanic activity is going on under the Earth’s surface.",
          lower_text_field: "Magma that is forcing its way through the crust heats up water that seeped down through cracks."
      },
      7 => {
          upper_text_field: "Brigadier Venus discovers that <span class='rw_highlight'>geysers</span> form when water collects in underground caves and chambers. The water gets hotter and",
          lower_text_field: "hotter, until finally it boils and turns to steam. What do you think will happen in the film?"
      },
      8 => {
          upper_text_field: "Brigadier Venus explains just like the steam pressure that makes a kettle on the hob whistle, the force of this underground steam",
          lower_text_field: "blasts through the cracks in the Earth’s crust."
      },
      9 => {
          upper_text_field: "Brigadier Venus explains how some areas of the Earth have famous <span class= 'rw_highlight'>geysers</span> and <span class= 'rw_highlight'>hot springs</span>. In Iceland, the Strokkur <span class= 'rw_highlight'>geyser</span>",
          lower_text_field: "erupts regularly about every four to eight minutes, and sometimes the water can go up to 40 metres high."
      },
      10 => {
          upper_text_field: "Brigadier Venus discovers that the <span class= 'rw_highlight'>Yellowstone National Park</span>, in the United States, sits on a hot spot of underground volcanic",
          lower_text_field: "activity. One famous <span class= 'rw_highlight'>geyser</span> at <span class= 'rw_highlight'>Yellowstone</span> erupts so regularly that it is called <span class= 'rw_highlight'>Old Faithful</span>."
      },
      11 => {
          upper_text_field: "Brigadier Venus explains that <span class= 'rw_highlight'>Rotorua</span> is a town in New Zealand known for its <span class= 'rw_highlight'>hot springs</span>, bubbling mud and <span class= 'rw_highlight'>geysers</span>.",
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Once you’ve watched today’s experiment you are going to pop outside to try it with your class!",
          main_video_slide: ""
      },
      13 => {
          upper_text_field: "Brigadier Venus would like you to report to your work stations for today’s Mission Assignment. Count down with the clock!",
          sixty_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}



  $rocket_words = {
          "Old Faithful" => {
              description: "One of the best known <span class= 'rw_highlight'>geysers</span> of <span class= 'rw_highlight'>Yellowstone National Park</span>.",
              image: "" },
          "Hot Springs" => {
              description: "Are thermal mineral springs heated by the core of the Earth.",
              image: "" },
          "Geyser" => {
              description: "A <span class= 'rw_highlight'>hot spring</span> that intermittently sends up fountain like jets of water and steam into the air.",
              image: "" },
          "Yellowstone National Park" => {
              description: "Is a national park located primarily in the U.S. state of Wyoming.",
              image: "" },
          "Rotorua" => {
              description: "The <span class= 'rw_highlight'>Rotorua</span> geysers are located in New Zealand and produce real high spouters.",
              image: "" },
  }

  $questions = {
      "What is Old Faithful?" => {answer: "One of the best known <span class= 'rw_highlight'>geysers</span> of <span class= 'rw_highlight'>Yellowstone National Park</span>.",
                           image: "",
                           slide_id: 10},
      "What are hot springs?" => {answer: "They are thermal mineral springs heated by the core of the Earth.",
                                image: "",
                                slide_id:9 },
      "What are geysers?" => {answer: "A <span class= 'rw_highlight'>hot spring</span> that intermittently sends up fountain like jets of water and steam into the air.",
                           image: "",
                           slide_id: 7},
      "What is Yellowstone National Park?" => {answer: "Is a national park located primarily in the U.S. state of Wyoming.",
                              image: "",
                              slide_id: 10},
      "What is Rotorua?" => {answer: "The <span class= 'rw_highlight'>Rotorua</span> geysers are located in New Zealand and produce real high spouters.",
                              image: "",
                              slide_id: 11},
  }



end
