desc "Lesson"
task t4m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that eating healthy foods such as apples helps you grow stronger. But how? In order for your body",
          lower_text_field: "to get what it needs from an apple, it needs to <span>digest</span> it."
      },
      7 => {
          upper_text_field: "Commander Mars explains that <Span>digesting</span> means breaking food down into little pieces – so little you can’t see them with just your",
          lower_text_field: "eyes – so that your body can take those pieces and use them for energy and for building its own cells, tissues and organs."
      },
      8 => {
          upper_text_field: "Commander Mars wants you to pretend you’re holding a big, crisp, juicy red apple. The <span>digestive</span> process begins even before",
          lower_text_field: "you take a bite."
      },
      9 => {
          upper_text_field: "Commander Mars explains that your eyes, your nose and your fingertips send signals to your brain, and your brain sends a",
          lower_text_field: "message to your mouth and stomach: ‘Get ready, food is coming!’"
      },
      10 => {
          upper_text_field: "When you take a bite of the apple, your tongue tastes the sweetness and tells your brain, ‘Mmm, here’s something good",
          lower_text_field: "and sweet’. Then your brain sends an order to the parts of the mouth called the <span>salivary glands</span>: ‘Get to work!’"
      },
      11 => {
          upper_text_field: "Commander Mars explains that they jump to it by making a watery liquid called ‘spit’ or <span>saliva</span>. It is an important team player in your",
          lower_text_field: "<Span>digestive</span> process. It helps make the food you eat wet and soft, and it has chemicals that help you <span>digest</span> your food."
      },
      12 => {
          upper_text_field: "Commander Mars explains that on the surface of your tongue there are many tiny <span>taste buds</span>. Different <span>taste buds</span> taste",
          lower_text_field: "different flavours, sweet, salty, sour and <span>bitter</span>."
      },
      13 => {
          upper_text_field: "The <span>taste buds</span> send messages to your brain, like ‘Yum, this apple is sweet and delicious’ or ‘What a sour lemon!’ or ‘Ugh,",
          lower_text_field: "this is too <span>bitter</span> – spit it out!’"
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Watch today’s film then try the experiment with your talk partner.",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Please report to your work stations for today’s mission assignment.",
          sixty_count: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}

end
