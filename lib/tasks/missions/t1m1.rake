desc "Lesson"
task t1m1: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {course_intro_slide: ""},
      3 => {ten_count_launch: ""},
      4 => {rocket_word_slide: ""},
      5 => {song_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune wants to know your favourite <span class='rw'>animal</span>? Is it a wild <span class='rw'>animal</span>, like an owl or a bunny? Is it an <span class='rw'>animal</span> at a <span class='rw'>zoo</span>, like a <span class='rw'>monkey</span> or",
          lower_text_field: "hippopotamus? Is it an <span class='rw'>animal</span> on a farm, like a cow or chicken? Or maybe an <span class='rw'>animal</span> that lives at home, like a <span class='rw'>pet</span> dog or cat, goldfish or hamster?"},
      7 => {
          upper_text_field: "Captain Neptune explains that we share the world with many different <span class='rw'>animals</span> such as lions, wolves and bears. Can you name some wild",
          lower_text_field: "<span class='rw'>animals</span> that live near you? Have you seen <span class='rw'>squirrels</span> in the park or the woods? Have you seen rabbits or foxes in the fields?"},
      8 => {upper_text_field: "Captain Neptune explains that some <span class='rw'>animals</span> are tame, like <span class='rw'>pet</span> dogs and cats. Whether <span class='rw'>animals</span> are wild or tame, they need certain things to live."},
      9 => {
          upper_text_field: "Captain Neptune wants you to think about what <span class='rw'>animals</span> might need in order to grow.",
          lower_text_field: "One thing <span class='rw'>animals</span> need is food. Some <span class='rw'>animals</span> such as rabbits, eat plants. Some <span class='rw'>animals</span>, such as lions, eat meat."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that <span class='rw'>animals</span> also need safe homes. Wild <span class='rw'>animals</span> find or make their own homes. Some rabbits make their homes",
          lower_text_field: "in holes in the ground. Bears live in caves. "},
      11 => {upper_text_field: "Today’s Film: How to care for a puppy.", main_video_slide: ""},
      12 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Captain Neptune?", sixty_count: ""},
      13 => {upper_text_field: "Today’s First Mission Assignment: the 4 <span class='rw'>animals</span> below. Place the adjective (describing word) on the <span class='rw'>animal</span> based on how you feel about the <span class='rw'>animal</span>."},
      14 => {upper_text_field: "Today’s Second Mission Assignment: Choose one of the <span class='rw'>pets</span> that you would like to care for. Tell your talk partner what you will need to buy for your <span class='rw'>pet</span>."},
      15 => {upper_text_field: "Captain Neptune would like you to clear away your work stations!", sixty_count: ""},
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
  }



  $rocket_words = {
          'Zoo' => {
              description: "A place where <b>wild <span class='rw'>animals</span> are housed</b> for people to see.",
              image: "zoo.jpg" },
          'Squirrel' => {
              description: "A <b>bushy-tailed</b> rodent.",
              image: "squirrel.jpg" },
          'Pet' => {
              description: "A <b>tamed <span class='rw'>animal</span></b> that is kept as a companion and cared for affectionately.",
              image: "pet.jpg" },
          'Animal' => {
              description: "A <b>living thing</b> that is not a human being or plant.",
              image: "animal.jpg" },
          'Monkey' => {
              description: "A type of <b><span class='rw'>animal</span></b> that is closely related to humans, it has a <b>long tail</b> and usually <b>lives in trees</b>.",
              image: "monkey.jpg" },
  }

  $questions = {
      'What is a <span class="rw_highlight">zoo</span>?' => {answer: "A place where wild animals are housed for people to see.",
                           image: "zoo.jpg",
                           slide_id: 6},
      'What is a <span class="rw_highlight">squirrel</span>?' => {answer: "A bushy-tailed rodent.",
                                image: "squirrel.jpg",
                                slide_id: 7},
      'What are <span class="rw_highlight">pets</span>?' => {answer: "A tamed animal that is kept as a companion and care for affectionately.",
                           image: "pet.jpg",
                           slide_id: 8},
      'What are <span class="rw_highlight">animals</span>?' => {answer: "A living thing that is not a human being or plant.",
                              image: "animal.jpg",
                              slide_id: 9},
      'What is a <span class="rw_highlight">monkey</span>?' => {answer: "A type of animal that is closely related to humanas and that has a long tail and usually lives in tree.",
                              image: "monkey.jpg",
                              slide_id: 14},
  }

end

