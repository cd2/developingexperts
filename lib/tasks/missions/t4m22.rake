desc "Lesson"
task t4m22: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that there are many ways to <Span>measure</span> something. If you are <span>measuring</span> the weight of your travel luggage",
          lower_text_field: "you use weighing scales. There are two measures used around the world. They are called Metric and Imperial."
      },
      7 => {
          upper_text_field: "The scale we use is determined by the country we live in. The unit we use to <span>measure</sapn>, is determined by what we are <span>measuring</span>. For",
          lower_text_field: "example, if you are <span>measuring</span> flour using the metric system, you would use kilos and grammes."
      },
      8 => {
          upper_text_field: "If you were <span>measuring</span> the size of your cake tin, you would use centimetres and if you are <span>measuring</span> the amount of <span>liquid</span> to be",
          lower_text_field: "added to a recipe you would use <span>litres</span> and <span>millilitres</span>."
      },
      9 => {
          upper_text_field: "Commander Mars is in the kitchen, he is going to make a chocolate cake. Yum! He needs to <span>measure</span> his ingredients accurately",
          lower_text_field: "otherwise his cake will not turn out very well. He needs weighing scales, a <span>measuring</span> jug and a tape <span>measure</span>."
      },
      10 => {
          upper_text_field: "He will use his weighing scales to <span>measure</span> the butter, sugar, flour and chocolate powder. He will need a <span>measuring</span>> jug to <span>measure</span>",
          lower_text_field: "the water or milk. He will use his tape <span>measure</span> to help him find the right sized cake tin."
      },
      11 => {
          upper_text_field: "Now Commander Mars wants you to explore how to <span>measure</span> the <span>volume</span> of <span>liquids</span> in more detail. Scientific experiments often use",
          lower_text_field: "<span>liquids</span> that need to be <span>measured</span> to exact amounts, if not being a few <span>millilitres</span> out could alter the result."
      },
      12 => {
          upper_text_field: "For small amounts of <span>liquids</span> we needs to measure using <span>millilitres</span>. Adults need to <span>measure</span> medicine very carefully so they do",
          lower_text_field: "not use too much. For larger amounts of liquids we needs to <Span>measure</span> using <span>litres</span>. Milk cartons can be brought in <span>litre</span> cartons."
      },
      13 => {
          upper_text_field: "The next time you walk around a supermarket Commander Mars wants you to take a look at the <span>measurement</span> used on packaging",
          lower_text_field: "for different <span>liquids</span> such as shampoo, milk and cooking oil. Fuel for your car is bought in <span>litres</span> from the pump."
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Making a chocolate cake.",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}

end
