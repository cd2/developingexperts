desc "Lesson"
task t3m14: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that animals go through the same life cycle as plants: birth, growth, reproduction, death and this is",
          lower_text_field: "repeated all over again. You can see this in the life cycle of a <span class='rw_highlight'>frog</span>. <span class='rw_highlight'>Frogs</span> are <span class='rw_highlight'>amphibians</span>."
      },
      7 => {
          upper_text_field: "Colonel Jupiter wants you to imagine a little pond where you can see two <span class='rw_highlight'>frogs</span>. The female <span class='rw_highlight'>frog</span> is laying eggs. The male <span class='rw_highlight'>frog</span>",
          lower_text_field: "fertilises them. The fertilised eggs float in the water like a blob of jelly balls with dark specks inside."
      },
      8 => {
          upper_text_field: "When you look at the pond, you see something floating on the greenish-brown water. It’s the bunch of <span class='rw_highlight'>frog’s eggs</span>, called",
          lower_text_field: "<span class='rw_highlight'>frogspawn</span>. It looks like jelly, all clumped together."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that if you look closely, you can see a dark speck inside each little ball. The speck grows bigger and",
          lower_text_field: "begins to take shape. When it hatches it has a broad face and a long, flat tail. This baby <span class='rw_highlight'>frog</span> is called a <span class='rw_highlight'>tadpole</span>."
      },
      10 => {
          upper_text_field: "It lives in water, and swims around the pond. The <span class='rw_highlight'>tadpole</span> grows, two little legs begin to sprout from the back of its body. Soon",
          lower_text_field: "two more legs begin to grow in the front. At the same time, the <span class='rw_highlight'>tadpole’s</span> body becomes bigger and its tail then shrinks."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that not many of these eggs will hatch into <span class='rw_highlight'>tadpoles</span> because there are so many other creatures in",
          lower_text_field: "the pond, such as fish, who like to eat <span class='rw_highlight'>frog’s eggs</span>. A fully grown <span class='rw_highlight'>frog</span> can live for 10 – 12 years!"
      },
      12 => {
          upper_text_field: "The specks which survive grow bigger and begin to take shape and the life cycle of a <span class='rw_highlight'>frog</span> keeps on going.",
      },
      13 => {
          upper_text_field: "Colonel Jupiter explains that it looks more and more like a <span class='rw_highlight'>frog</span>. The <span class='rw_highlight'>frog</span> grows bigger and matures until it is ready to reproduce.",
          lower_text_field: "And the cycle starts all over again. A female <span class='rw_highlight'>frog</span> lays about 1000 eggs at a time!"
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Create your own collage which illustrates the life cycle of a <span class='rw_highlight'>frog</span>.",
      },
      15 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
