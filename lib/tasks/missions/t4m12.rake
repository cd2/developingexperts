desc "Lesson"
task t4m12: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants you to tighten your <span>biceps</span>. He explains that you are using your <span>voluntary muscles</span>. <span>Voluntary muscles</span> are",
          lower_text_field: "ones you can control. You can choose whether to make a header, kick a ball or raise your hand or sit down."
      },
      7 => {
          upper_text_field: "Whenever you do these things, you are using <span>voluntary muscles</span>. Commander Mars explains that your body also depends on many",
          lower_text_field: "muscles that move whether or not you want them to. These are called <span>involuntary muscles</span>."
      },
      8 => {
          upper_text_field: "For example your heart is an <span>involuntary muscle</span>. Commander Mars explains that it keeps on pumping blood without you telling",
          lower_text_field: "it when to do so. <span>Involuntary muscles</span> in your intestines work automatically to help you digest the food you eat."
      },
      9 => {
          upper_text_field: "Commander Mars explains that <span>involuntary muscles</span> work all of the time, whether you’re awake or asleep, whether or not you",
          lower_text_field: "think about them. Let’s pretend that you’re a powerful Olympic athlete."
      },
      10 => {
          upper_text_field: "Commander Mars wants you to show him your muscles – stand up, stretch out your arms, then bend them at the elbow as you curl",
          lower_text_field: "your fists towards your head. When you do that, you are tightening the muscles on the upper part of each arm, called your <span>biceps</span>."
      },
      11 => {
          upper_text_field: "Commander Mars explains that you have about 650 muscles in your body. Some muscles, such as the ones in your ears, are as",
          lower_text_field: "tiny as a thread, other muscles such as the hamstring muscles in the back of your leg, are thick and wide."
      },
      12 => {
          upper_text_field: "Commander Mars wants to know where you think your biggest muscle is? It’s called the gluteus maximus, and it’s the muscle",
          lower_text_field: "you sit down on (your bottom). You use your muscles when you walk, run, jump, swim, skate, ride a bicycle, play tennis or soccer."
      },
      13 => {
          upper_text_field: "Commander Mars explains that every time you move, you use your muscles. Even when you are not exercising, you use muscles.",
          lower_text_field: "When you read, your neck muscles hold your head up and your eye muscles move your gaze across the page."
      },
      14 => {
          upper_text_field: "Commander Mars explains that when you smile, you use fifteen different face muscles. When you frown, you use more than forty",
          lower_text_field: "different face muscles. So smile – it’s easier than frowning! When you move, your muscles work in pairs."
      },
      15 => {
          upper_text_field: "When you bend your arm, you tighten your <Span>biceps</span>, while another muscle on the lower part of your arm, called <span>triceps</span>, relaxes.",
          lower_text_field: "Can you point to your <span>triceps</span> and <span>biceps</span>? Can you feel them on your arm?"
      },
      16 => {
          upper_text_field: "Commander Mars explains that when you stretch your arm out straight again, the opposite occurs: the <span>biceps</span> relaxes and the",
          lower_text_field: "<span>triceps</span> tightens. That’s how many muscles work in your body. When one pulls tight, a companion muscle relaxes."
      },
      17 => {
          upper_text_field: "Today’s film explores muscles.",
          main_video_slide: ""
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Explores how information is sent from the muscles to the brain.",
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}

end
