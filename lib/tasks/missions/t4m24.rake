desc "Lesson"
task t4m24: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wonders if you have noticed that oil and water don’t mix very well. Try pouring a little oil and water into a bottle",
          lower_text_field: "and shaking it. He explains that you will see that they separate again when you put the bottle down."
          },
      7 => {
          upper_text_field: "One way of explaining this is to say that water molecules stick to each other really well, but they don’t stick to oil molecules",
          lower_text_field: "very well."
          },
      8 => {
          upper_text_field: "Commander Mars wonders what would happen if you put a spoonful of sugar into a glass of warm water and stirred it up?",
          lower_text_field: "If you stir it long enough, the sugar seems to disappear. Unlike oil, sugar molecules blend well with water molecules."
          },
      9 => {
          upper_text_field: "Commander Mars explains that sugar molecules attract water molecules well, it pulls them apart and manages to mingle in",
          lower_text_field: "between the water molecules. There are many different types of molecules which behave in different ways."
          },
      10 => {
          upper_text_field: "Commander Mars explains that when this happens, we say the sugar is <span class='rw_highlight'>dissolved</span> in water. We call the water the <span class='rw_highlight'>solvent</span> and the",
          lower_text_field: "sugar the <span class='rw_highlight'>solute</span>. The sugar is still there, even though you can’t see it!"
          },
      11 => {
          upper_text_field: "Commander Mars explains that you can’t go on adding sugar to the solution forever. A <span class='rw_highlight'>solvent</span> (in this case water) can only",
          lower_text_field: "<span class='rw_highlight'>dissolve</span> a certain amount of <span class='rw_highlight'>solute<span> (in this case sugar)."
          },
      12 => {
          upper_text_field: "Commander Mars explains that when the maximum amount of solute is <span class='rw_highlight'>dissolved</span> in a <span class='rw_highlight'>solvent</span>, we say that the solution is",
          lower_text_field: "<span class='rw_highlight'>saturated</span>."
          },
      13 => {
          upper_text_field: "Commander Mars explains that the picture shows what can happen to a <span class='rw_highlight'>mixture</span> of sand, water and oil. They can be",
          lower_text_field: "separated out again because no chemical reaction has taken place."
          },
      14 => {main_video_slide: "",
             upper_text_field: "Today’s First Mission Assignment: "},
      15 => {upper_text_field: "Today’s Second Mission Assignment: "},
      16 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
  }



    $rocket_words = {
          "Solute" => {
              description: "A <span class='rw_highlight'>dissolved</span> substance.",
              image: "" },
          "Solvent" => {
              description: "A substance that <span class='rw_highlight'>dissolves</span>.",
              image: "" },
          "Saturate" => {
              description: "To make (something) very wet.",
              image: "" },
          "Dissolve" => {
              description: "To mix with a liquid and become part of the liquid.",
              image: "" },
          "Mixture" => {
              description: "Something made by combining two or more substances.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>solute</span>?" => {answer: "A <span class='rw_highlight'>dissolved</span> substance.",
                           image: "",
                           slide_id: 10},
      "What is a <span class='rw_highlight'>solvent</span>?" => {answer: "A substance that <span class='rw_highlight'>dissolves</span>.",
                                image: "",
                                slide_id: 10},
      "What does <span class='rw_highlight'>saturate</span> mean?" => {answer: "To make (something) very wet.",
                           image: "",
                           slide_id: 12},
      "What does <span class='rw_highlight'>dissolve</span> mean?" => {answer: "To mix with a liquid and become part of the liquid.",
                              image: "",
                              slide_id: 10},
      "What is a <span class='rw_highlight'>mixture</span>?" => {answer: "Something made by combining two or more substances.",
                              image: "",
                              slide_id: 13},
  }

end
