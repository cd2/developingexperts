desc "Lesson"
task t3m20: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains a <span class='rw_highlight'>mountain</span> is a landform that rises high above the surrounding area. They are made from rocks",
          lower_text_field: "and earth. Generally, <span class='rw_highlight'>mountains</span> are higher than 600 metres. Those less than 600 metres are called <span class='rw_highlight'>hills</span>."
      },
      7 => {
          upper_text_field: "A <span class='rw_highlight'>mountain</span> is ‘higher and steeper than a <span class='rw_highlight'>hill</span>’. <span class='rw_highlight'>Mountains</span> are made from different types of rock. There are many different",
          lower_text_field: "types of <span class='rw_highlight'>mountains</span> such as dome <span class='rw_highlight'>mountains</span> and folded <span class='rw_highlight'>mountains<span>."
      },
      8 => {
          upper_text_field: "Colonel Jupiter wants you to imagine a tea towel, spread on the table. Now picture what happens when you push the edges",
          lower_text_field: "towards the centre. The more you push, the more the tea towel wrinkles and crumples into folds."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that the same kind of thing has happened to the crust of the Earth in different places, making",
          lower_text_field: "formations called folded <span class='rw_highlight'>mountains</span>. Can you see the folds?"
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that most scientists think that the Earth’s tallest <span class='rw_highlight'>mountains</span>, the <span class='rw_highlight'>Himalayas</span>, were formed when two",
          lower_text_field: "sections (plates) of the Earth’s surface banged together underneath the ocean."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that the tremendous force of this impact, made the ocean floor between the sections of the Earth’s plates,",
          lower_text_field: "bend and fold into <span class='rw_highlight'>mountains</span> and valleys."
      },
      12 => {
          upper_text_field: "You may have a hard time believing that the <span class='rw_highlight'>Himalayas</span> were part of an ocean floor, but fossils of sea creatures can still be",
          lower_text_field: "found embedded in the rock on the <span class='rw_highlight'>mountains</span>’ peaks! The <span class='rw_highlight'>Alps</span> in <span class='rw_highlight'>Europe</span> are folded <span class='rw_highlight'>mountains</span>, too."
      },
      13 => {
          upper_text_field: "Dome <span class='rw_highlight'>mountains</span> are formed when a large amount of magma which erupts from a volcano, builds up below the Earth’s",
          lower_text_field: "surface, forcing the rock above to bulge out. The Sierra Nevada range in California have dome-shaped <span class='rw_highlight'>mountains</span>."
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}



  $rocket_words = {
          "Folded Mountains" => {
              description: "Formed mainly by the effects of folding on layers within the upper part of the Earth's crust.",
              image: "" },
          "Europe" => {
              description: "The world's second-smallest continent.",
              image: "" },
          "Hill" => {
              description: "A slope less than 600 metres high.",
              image: "" },
          "Himalayas" => {
              description: "The mountain range along the border between India and Tibet.",
              image: "" },
          "Alps" => {
              description: "A mountain range in Southern Europe.",
              image: "" },
  }


end
