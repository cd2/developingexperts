desc "Lesson"
task t2m16: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains that a little battery puts out a small amount of electricity, so you can use it safely for your experiments. But the",
          lower_text_field: "electricity that comes through the wires in your house or school is much more powerful, so you need to remember some <span class='rw_highlight'>safety rules</span>."
      },
      7 => {
          upper_text_field: "There are cables which carry electricity inside the walls of your home or school. When you plug in a light or radio or other",
          lower_text_field: "electrical appliance, you are putting the light or radio in the <span class='rw_highlight'>pathway of the electricity</span>."
      },
      8 => {
          upper_text_field: "You have heard adults say: “Don’t stick your finger in the socket on the wall!’ Now you know why. If you did, you would become",
          lower_text_field: "part of the <span class='rw_highlight'>electrical pathway</span>. Your body is a pretty good conductor (or pathway) for electricity to flow through."
      },
      9 => {
          upper_text_field: "The electrical current coming through the wires to the socket is so strong that it would hurt a lot if you stuck your finger in it.",
          lower_text_field: "What needs to happen to make everything safe in this picture?"
      },
      10 => {
          upper_text_field: "Major Saturn explains if you did, you would become part of the <span class='rw_highlight'>electrical pathway</span>. Your body is a pretty good conductor of",
          lower_text_field: "electricity. What is wrong in this picture? What is <span class='rw_highlight'>dangerous</span>? Discuss what needs to change with your talk partner."
      },
      11 => {
          upper_text_field: "The electrical current coming through the wires to the wall outlet is so strong that it would hurt a lot if you stuck your finger in the",
          lower_text_field: "socket. What if you held a piece of metal, like a fork or knife, and stuck it in the socket? Don’t do it!"
      },
      12 => {
          upper_text_field: "Why? Because metals conduct electricity, and you would get a terrible <span class='rw_highlight'>shock</span> which could kill you! Another <span class='rw_highlight'>safety rule</span>: ‘Don’t",
          lower_text_field: "touch any electrical appliance when you are wet.’ Can you think why? It is because water is a good conductor of electricity."
      },
      13 => {
          upper_text_field: "Major Saturn explains that when you hands are wet or when your body is in a bathtub full of water, the electricity would flow right",
          lower_text_field: "through you and give you an awful <span class='rw_highlight'>shock</span>, or even kill you."
      },
      14 => {
          upper_text_field: "Think: Why should you never put your fingers into the socket where the light bulb screws into the lamp? That’s the place",
          lower_text_field: "where the electricity flows into the bulb, isn’t it?"
      },
      15 => {
          upper_text_field: "Put your finger in there and it could be you, rather than the light bulb, that becomes part of the <span class='rw_highlight'>electrical pathway</span>. And that would",
          lower_text_field: "hurt! Electricity is very useful, but it can be <span class= 'rw_highlight'>dangerous</span>. Be careful, be safe, be clever. Let electricity help you, not hurt you."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
