desc "Lesson"
task t2m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants to know if you have ever thought about what happens inside your body when you breathe? When you eat?",
          lower_text_field: "When you stand up? When you jump? When you run? Discuss your answers with your talk partners."
      },
      7 => {
          upper_text_field: "Major Saturn explains that the different parts of your body work together to let you breathe, eat, stand up, jump, run and do lots",
          lower_text_field: "more. Let’s find out about what’s going on inside. Let’s learn about some of the systems in your body."
      },
      8 => {
          upper_text_field: "Major Saturn explains that if you hold up a jacket by the collar, it just falls limp. Now, put the jacket over a coat hanger. The hard",
          lower_text_field: "coat hanger gives the soft jacket a shape."
      },
      9 => {
          upper_text_field: "Major Saturn explains that inside your body there’s something that gives you a shape. No, not a bundle of coat hangers, but a",
          lower_text_field: "bundle of <span class= 'rw_highlight'>bones</span> – these <span class= 'rw_highlight'>bones</span> make up your <span class= 'rw_highlight'>skeleton</span>. Your <span class= 'rw_highlight'>skeleton</span> is the hard part inside your body."
      },
      10 => {
          upper_text_field: "It looks a bit like the picture below. Squeeze one of your fingers: do you feel the hard bone inside? Now, tap your head with",
          lower_text_field: "your knuckles – not too hard!"
      },
      11 => {
          upper_text_field: "Major Saturn explains the sound you hear is the sound of the bone inside your finger knocking against your <span class= 'rw_highlight'>skull</span>, which is the bone",
          lower_text_field: "inside your head. <span class= 'rw_highlight'>Bones</span> are hard, but they can break, such as when a person falls badly. Doctors can mend most broken <span class= 'rw_highlight'>bones<span>."
      },
      12 => {
          upper_text_field: "Major Saturn explains that they use a special machine called an <span class= 'rw_highlight'>X-ray machine</span>. An <span class= 'rw_highlight'>x-ray machine</span> takes a picture through your",
          lower_text_field: "skin and lets the doctor see the broken bone."
      },
      13 => {
          upper_text_field: "Often the doctor will wrap the injured part of the body in a hard <span class= 'rw_highlight'>cast</span>, which will protect the broken bone and keep it straight until",
          lower_text_field: "it grows back again."
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      15 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      16 => {complete_investigation_slide: ""},
      17 => {rocket_word_slide: ""},
      18 => {quiz_slide: ""},
      19 => {outro_slide: ""}
}

end
