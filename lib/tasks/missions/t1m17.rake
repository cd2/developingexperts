desc "Lesson"
task t1m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune says that if the temperature drops low enough, then instead of rain we get <span class='rw_highlight'>snow</span>. When just a little <span class='rw_highlight'>snow</span> falls, we call it a <span class='rw_highlight'>snow</span> flurry,",
          lower_text_field: "which are ideal conditions for building snowmen, but not great to drive in."
      },
      7 => {
          upper_text_field: "A heavy <span class='rw_highlight'>snowstorm</span> is called a <span class='rw_highlight'>blizzard</span>. When lots of <span class='rw_highlight'>snow</span> falls during a <span class='rw_highlight'>blizzard</span> it can be disastrous if you are not <span class='rw_highlight'>prepared</span>.",
          lower_text_field: "If you are a long way from home and not dressed correctly you could be in danger so you need to know about the weather."
      },
      8 => {upper_text_field: "In some countries there is <span class='rw_highlight'>snow</span> all of the time. People even use the <span class='rw_highlight'>snow</span> to build their homes. A group of people called Inuits",
            lower_text_field: "used to live in homes called igloos which are also known as <span class='rw_highlight'>snow</span> houses."
      },
      9 => {
        upper_text_field: "Did you know that every <span class='rw_highlight'>snowflake</span> is different? Each <span class='rw_highlight'>snowflake</span> has its own beautiful design of tiny icy <span class='rw_highlight'>crystals</span>.",
      },
      10 => {
          upper_text_field: "Take a look at today’s film. Discuss with your talk partner what you think it would be like to play outside in this type of weather.",
          main_video_slide: ""
      },
      11 => {
          upper_text_field: "Today’s Mission activity: Take a look at today’s film to see how you make your own <span class='rw_highlight'>snowflakes</span>.",
          main_video_slide: ""
      },
      12 => {
        upper_text_field: "Captain Neptune wants you to report to your work stations to complete today’s mission assignment.", 
        sixty_count: ""},
      13 => {complete_investigation_slide: ""},
      14 => {rocket_word_slide: ""},
      15 => {quiz_slide: ""},
      16 => {outro_slide: ""}
  }


end
