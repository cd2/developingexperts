desc "Lesson"
task t5m1: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus asks, ‘Have you ever wondered what it would be like to journey down, down to the centre of the Earth?’",
          lower_text_field: "To get there, you would have to dig a hole 3,872 miles deep. You would travel through four different layers of the Earth."
      },
      7 => {
          upper_text_field: "Bigadier Venus explains that the top layer - the layer on which you live – is called the <span>crust</span>. The <span>crust</span> is about 25 miles deep, made of solid rock.",
          lower_text_field: "Brigadier Venus explains that once you dig through the <Span>crust</span>, you reach a very hot second layer called a <span>mantle</span>."
      },
      8 => {
          upper_text_field: "He describes how it is so hot that instead of being hard rock, the Earth’s material flows like incredibly thick syrup. Brigadier Venus explains that",
          lower_text_field: "geologists call the most runny parts of the <span>mantle</span>, magma. In some plates, the <span>mantle</span> can be 1,800 miles deep."
      },
      9 => {
          upper_text_field: "Brigadier Venus explains that if you continue down towards the centre of the earth, you come to the third layer – the <span>outer core</span> which is a layer,",
          lower_text_field: "made of searing hot liquid metal, nearly as thick as the <span>mantle</span>."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains that when you travel deeper still, you reach the <Span>inner core</span> at the Earth’s centre. It is estimated to be",
          lower_text_field: "about the size of the Moon."
      },
      11 => {
          upper_text_field: "Brigadier Venus describes how the temperature at the <span>inner core</span> is hotter than the surface of the Sun! But fortunately, the",
          lower_text_field: "pressure from all the layers around the <span>inner core</span> stops the Earth melting."
      },
      12 => {
          upper_text_field: "Brigadier Venus discovers that <span>plate tectonics</span> is the theory that Earth's outer shell is divided into several plates that glide over the <span>mantle</span> (the",
          lower_text_field: "rocky inner layer above the core). The <span>tectonic plates</span> act like a hard and rigid shell compared to Earth's mantle."
      },
      13 => {
          upper_text_field: "Move to your workstations for today’s mission assignment. You have 60 seconds to get ready!",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Create your own model which shows the different layers of the earth. Use the handout provided.",
          sixty_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
