desc "Lesson"
task t4m9: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars explains that the eardrum is a delicate, airtight seal. Never poke anything long or sharp into your ear, because it",
          lower_text_field: "could damage your eardrum. Extremely loud sounds can damage the eardrum, too."
      },
      7 => {
          upper_text_field: "Commander Mars explains that if sounds are loud enough to feel uncomfortable, it’s time to move somewhere quieter or turn the",
          lower_text_field: "volume down. Damaged eardrums are hard to heal. Without healthy hearing eardrums, you won’t hear well."
      },
      8 => {
          upper_text_field: "Commander Mars explains that if you look at someone’s head, you see the <Span>outer ear</span>. The outer ear is made of cartilage.",
          lower_text_field: "Nature has cleverly designed the outer ear to catch and direct sound waves through an opening in the ear canal."
      },
      9 => {
          upper_text_field: "Commander Mars describes how vibrations travel through the air inside the ear canal to the eardrum. Like a drum, the eardrum is",
          lower_text_field: "made of a thin tissue stretched tightly across an opening. Each of your eardrums is only about as big as the nail on your little finger."
      },
      10 => {
          upper_text_field: "Commander Mars explains how sound waves enter the ear and make the eardrum vibrate. Next, those vibrations travel through",
          lower_text_field: "three bones deep into the ear. They’re called the <Span>hammer</span>, <span>anvil</span> and <span>stirrup</span>."
      },
      11 => {
          upper_text_field: "Commander Mars explains these are the tiniest bones in your body. They get their names from their shapes. The <Span>hammer</span>",
          lower_text_field: "looks like a tiny <span>hammer</span>. The anvil looks like an anvil, the heavy iron surface that a blacksmith uses."
      },
      12 => {
          upper_text_field: "The <span>stirrup</span> resembles the metal loop below a horse’s saddle for the rider’s foot. Commander Mars explains that vibrations are",
          lower_text_field: "passed from the <span>hammer</span> to the <span>anvil</span> to the <span>stirrup</span>, and then onto the <Span>cochlea</span>. What causes the sound on a guitar?"
      },
      13 => {
          upper_text_field: "Commander Mars explains the <Span>cochlea</span> is spiral. Can you find it in the picture? Does its shape remind you of a certain animal”",
          lower_text_field: "(‘<span>Cochlea</span>’ comes from the Latin word for ‘snail’.)"
      },
      14 => {
          upper_text_field: "Commander Mars explains that the <span>cochlea</span> is filled with liquid, which vibrates as sound enters. When the liquid vibrates, it",
          lower_text_field: "shakes tiny hairs inside the <span>cochlea</span>."
      },
      15 => {
          upper_text_field: "Commander Mars explains that the hairs are connected to nerves that send signals to a big nerve called the auditory nerve. The",
          lower_text_field: "auditory carries the signals to the brain and – ta-dah! – you hear the sound."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Watch today’s film then create your own screaming ballooons.",
          main_video_slide: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Commander Mars?"
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: Complete your worksheet to write up your experiment and explain the difference between the sounds.",
          sixty_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
