desc "Lesson"
task t3m1: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {course_intro_slide: ""},
      3 => {ten_count_launch: ""},
      4 => {rocket_word_slide: ""},
      5 => {
          upper_text_field: "Colonel Jupiter finds out that scientists divide plants into two categories: <span class='rw_highlight'>vascular</span> and <span class='rw_highlight'>non-vascular plants</span>.",
          lower_text_field: "Can you identify the main differences between plant A and B?"
      },
      6 => {
          upper_text_field: "Colonel Jupiter learns that <span class='rw_highlight'>vascular plants</span> have roots, stems and leaves. Can you name some <span class='rw_highlight'>vascular plants</span>?",
      },
      7 => {
          upper_text_field: "<span class='rw_highlight'>Non-vascular plants</span> do not have stems or leaves. Can you name some <span class='rw_highlight'>non-vascular plants</span>?",
      },
      8 => {
          upper_text_field: "In our world we use vehicles such as cars, buses and trains to <span class='rw_highlight'>transport</span> us from one place to another. In the plant world the",
          lower_text_field: "vehicles are called <span class='rw_highlight'>xylem</span> and <span class='rw_highlight'>phloem</span>, which make, store and transport food. Everyone say, ‘<span class='rw_highlight'>xylem</span>’ and ‘<span class='rw_highlight'>phloem</span>’."
      },
      9 => {
          upper_text_field: "The <span class='rw_highlight'>xylem</span> and <span class='rw_highlight'>phloem</span> tissues transport water and nutrients through the plant. They take these nutrients from the soil",
          lower_text_field: "through the roots. They take rainwater from the leaves and soil. Look at the celery plant."
      },
      10 => {
          upper_text_field: "It takes in water and nutrients from the soil through tiny hairs on its roots. The water and nutrients are <span class='rw_highlight'>transported</span> through the",
          lower_text_field: "root hairs to the roots, and then up the stem (or ‘stalk’) of the celery through tubes arranged in bundles."
      },
      11 => {
          upper_text_field: "Colonel Jupiter is fascinated by this tree. The <span class='rw_highlight'>phloem</span> tubes carry sugars from the leaves to every part of the plant that is not green",
          lower_text_field: "(like the roots, trunk and flowers) and also the growing tips and the fruits. He likes fruit!"
      },
      12 => {
          upper_text_field: "Colonel Jupiter learns that some plants can’t move nutrients well. These plants don’t have these tube-like <span class='rw_highlight'>xylem</span> and <span class='rw_highlight'>phloem</span>",
          lower_text_field: "structures. These are called <span class='rw_highlight'>non-vascular plants</span>."
      },
      13 => {
          upper_text_field: "<span class='rw_highlight'>Moss</span> is a <span class='rw_highlight'>non-vascular plant</span> which usually grows low to the ground; it is small. <span class='rw_highlight'>Moss</span> does not have the transport network",
          lower_text_field: "provided by stems and branches."
      },
      14 => {
          upper_text_field: "Colonel Jupiter wants you to explain the difference between a <span class='rw_highlight'>vascular</span> and <span class='rw_highlight'>non-vascular plant</span> to your talk partner.",
          lower_text_field: "You have 60 seconds."
      },
      15 => {
          upper_text_field: "Today’s Mission Assignment: This experiment shows how the <span class='rw_highlight'>vascular</span> tissues in plants work.",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Colonel Jupiter activates the 1 minute countdown clock. Please report to your workstations.",
          sixty_second_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
