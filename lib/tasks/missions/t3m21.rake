desc "Lesson"
task t3m21: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that mountains are made of rocks, but what are rocks made of? Rocks are made from chemicals called",
          lower_text_field: "<span class='rw_highlight'>minerals</span>. There are over 2,000 kinds of <span class='rw_highlight'>minerals</span> on Earth. Some rocks are made of a single <span class='rw_highlight'>mineral</span>, and others are combinations."
      },
      7 => {
          upper_text_field: "Colonel Jupiter learns that geologists sort different types of rock, based on how they were formed. Rocks that were made from the",
          lower_text_field: "cooling liquid of a volcanic eruption (<span class='rw_highlight'>magma</span>) are called <span class='rw_highlight'>igneous</span> rocks. The name ignis, is the Latin word for ‘fire’."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that some <span class='rw_highlight'>igneous</span> rock is called granite. It is speckled and heavy and can be used to make kitchen work",
          lower_text_field: "tops. Some people use <span class='rw_highlight'>pumice</span> stones, to soften hard skin in the bath. It too is an <span class='rw_highlight'>igneous rock</span>."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that other rocks were made when layer upon layer of sand and debris settled down together over a long",
          lower_text_field: "time. These are called <span class='rw_highlight'>sedimentary rocks</span>. Their name comes from the Latin word sedo, which means ‘settle down’."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that over millions of years, layers of sediment pressed down in the bottom of ancient oceans and",
          lower_text_field: "rivers. The pressure cemented tiny grains together into rock."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains that limestone is a <span class='rw_highlight'>sedimentary rock</span>, made mostly of the compressed bones and shells of millions of",
          lower_text_field: "tiny creatures. <span class='rw_highlight'>Sandstone</span> is another <span class='rw_highlight'>sedimentary rock</span>."
      },
      12 => {
          upper_text_field: "Colonel Jupiter explains that the last family of rocks is called <span class='rw_highlight'>metamorphic rock</span>. This family gets its name from the Greek",
          lower_text_field: "word <span class='rw_highlight'>metamorphosis</span>, which means a transformation or change of shape, because <span class='rw_highlight'>metamorphic</span> rocks have changed form."
      },
      13 => {
          upper_text_field: "Colonel Jupiter explains that some <span class='rw_highlight'>metamorphic rocks</span> have changed form through heat. When <span class='rw_highlight'>magma</span> collects underground,",
          lower_text_field: "it heats the surrounding rock to such high temperatures that the <span class='rw_highlight'>minerals</span> become cooked."
      },
      14 => {
          upper_text_field: "Colonel Jupiter explains that they change into new <span class='rw_highlight'>minerals</span>, and the rocks containing them change form. Others have changed",
          lower_text_field: "through pressure. Immense weight, like the weight of a mountain, can press down and change <span class='rw_highlight'>minerals</span>."
      },
      15 => {
          upper_text_field: "Underground heat and pressure turn limestone into a rock we know as marble. Marble can be used to make sculptures, floor",
          lower_text_field: "tiles, furniture and buildings. It is a <span class='rw_highlight'>metamorphic rock</span>."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Have a go at creating your own rock cycle!",
          main_video_slide: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}




  $rocket_words = {
          "Metamorphic Rocks" => {
              description: "Rocks are made by either heating up or squashing the earth's crust.",
              image: "" },
          "Igneous Rocks" => {
              description: "Formed deep within the Earth's crust where temperatures are very high.",
              image: "" },
          "Sedimentary Rocks" => {
              description: "Rocks are made when sand, mud and pebbles get laid down in layers.",
              image: "" },
          "Magma" => {
              description: "Hot liquid rock below the surface of the Earth which can be seen when a volcano erupts.",
              image: "" },
          "Mineral" => {
              description: "Rocks and stones are solids made up of minerals.",
              image: "" },
  }

  $questions = {
      "What is <span class='rw_highlight'>metamorphic rock</span>?" => {answer: "Rocks are made by either heating up or squashing the earth's crust.",
                           image: "",
                           slide_id: 12},
      "What is <span class='rw_highlight'>igneous rock</span>?" => {answer: "Formed deep within the Earth's crust where temperatures are very high.",
                                image: "",
                                slide_id: 1},
      "What is <span class='rw_highlight'>sedimentary rock</span>?" => {answer: "Rocks are made when sand, mud and pebbles get laid down in layers.",
                           image: "",
                           slide_id: 7},
      "What is <span class='rw_highlight'>magma</span>?" => {answer: "Hot liquid rock below the surface of the Earth which can be seen when a volcano erupts.",
                              image: "",
                              slide_id: 7},
      "What is a <span class='rw_highlight'>mineral</span>?" => {answer: "Rocks and stones are solids made up of minerals.",
                              image: "",
                              slide_id: 6},
  }

  
end
