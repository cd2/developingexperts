desc "Lesson"
task t2m21: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn wants you to tap your head – gently! That’s your skull. Do you know what’s inside it? A very important part of",
          lower_text_field: "your body; your <span class='rw_highlight'>brain</span>."
      },
      7 => {
          upper_text_field: "Major Saturn explains that your <span class='rw_highlight'>brain</span> is what you use to think, remember and learn. Your <span class='rw_highlight'>brain</span> tells the rest of your body what",
          lower_text_field: "to do. Your <span class='rw_highlight'>brain</span> is in charge: it’s like the captain of a ship or the pilot of an aeroplane."
      },
      8 => {
          upper_text_field: "Major Saturn explains that your <span class='rw_highlight'>brain</span> sends <span class='rw_highlight'>messages</span> to all parts of your body and gets <span class='rw_highlight'>messages</span> back. These <span class='rw_highlight'>messages</span>",
          lower_text_field: "are carried through the <span class='rw_highlight'>nerves</span>, which go from your <span class='rw_highlight'>brain</span> all through your body."
      },
      9 => {
          upper_text_field: "Major Saturn explains that the <span class='rw_highlight'>nerves</span> look something like the branches of a tree, but they’re much thinner. Your <span class='rw_highlight'>nerves</span> carry",
          lower_text_field: "<span class= 'rw_highlight'>messages</span> to your <span class='rw_highlight'>five senses</span>."
      },
      10 => {
          upper_text_field: "Sight, hearing, smell, taste, touch. When you feel an <span class= 'rw_highlight'>itch</span> on the tip of your nose, <span class= 'rw_highlight'>nerves</span> are sending a <span class='rw_highlight'>message</span> from your nose",
          lower_text_field: "to your <span class='rw_highlight'>brain</span>. Then your <span class='rw_highlight'>brain</span> sends a <span class='rw_highlight'>message</span> along the <span class='rw_highlight'>nerves</span> to your fingers."
      },
      11 => {
          upper_text_field: "The <span class='rw_highlight'>message</span> says: ‘Reach up and scratch my <span class='rw_highlight'>itchy</span> nose.’ Ahh, does that feel better? <span class='rw_highlight'>Nerves</span> sent that ‘feel better’ <span class='rw_highlight'>message</span>",
          lower_text_field: "from your nose back to you <span class='rw_highlight'>brain</span>."
      },
      12 => {
          upper_text_field: "Today’s First Mission Assignment: Create the following symbols to help you learn the <span class='rw_highlight'>five senses</span>.",
      },
      13 => {
          upper_text_field: "Today’s Second Mission Assignment: Complete this experiment to see how the <span class='rw_highlight'>brain</span> can be tricked!",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete Today’s Mission Assignment.",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

  $rocket_words = {
          "Brain" => {
              description: "<b>Organ in the skull</b>, which controls your body.",
              image: "" },
          "Nerves" => {
              description: "<b>Fibres in the body that send feelings to the <span class='rw_highlight'>brain</span>.</b>",
              image: "" },
          "Messages" => {
              description: "<b>Communication sent.</b>",
              image: "" },
          "Five Senses" => {
              description: "We learn about our surroundings, through <b>seeing, hearing, smelling, tasting, and touching</b>.",
              image: "" },
          "Itch" => {
              description: "An <b>irritating skin sensation</b> causing a desire to scratch.",
              image: "" },
  }

  $questions = {
      "What does the <span class='rw_highlight'>brain</span> do?" => {answer: "Organ in the skull, which controls your body.",
                           image: "",
                           slide_id: 6},
      "What are <span class='rw_highlight'>nerves</span>?" => {answer: "Fibres in the body that send feelings to the <span class='rw_highlight'>brain</span>.",
                                image: "",
                                slide_id: 9},
      "What are <span class='rw_highlight'>messages</span>?" => {answer: "Communication sent.",
                           image: "",
                           slide_id: 8},
      "What are the <span class='rw_highlight'>five senses</span>?" => {answer: "We learn about our surroundings, through seeing, hearing, smelling, tasting, and touching.",
                              image: "",
                              slide_id: 10},
      "What is an <span class='rw_highlight'>itch</span>?" => {answer: "An irritating skin sensation causing a desire to scratch.",
                              image: "",
                              slide_id: 11},
  }

end
