desc "Lesson"
task t3m11: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains how in <span class='rw_highlight'>winter</span>, the world of living things grows more quiet and still. Many small green plants have",
          lower_text_field: "<span class='rw_highlight'>shrivelled</span> up and died, leaving their seeds in the ground."
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains how the seeds will sit quietly through the <span class='rw_highlight'>winter</span>, ready to sprout when warm weather arrives again.",
          lower_text_field: "Trees that have dropped their leaves may look dead although they are really alive."
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that they’re just <span class='rw_highlight'>dormant</span>, not actively growing but, in a way, sleeping through the <span class='rw_highlight'>winter</span>. Some animals",
          lower_text_field: "sleep through the <span class='rw_highlight'>winter</span>, too, this is called <span class='rw_highlight'>hibernation</span>. Can you think of any animals that hibernate?"
      },
      9 => {
          upper_text_field: "He explains how <span class='rw_highlight'>hedgehogs</span> curl up into spiky balls and dormice sleep in their little nests through most of the winter, living off fat",
          lower_text_field: "they built up during the summer and autumn."
      },
      10 => {
          upper_text_field: "Colonel Jupiter explains that frogs hibernate too: they burrow into the cold mud at the bottom of a pond and wait for spring to",
          lower_text_field: "come again. Birds that migrated south in the autumn spend the <span class='rw_highlight'>winter</span> resting and eating."
      },
      11 => {
          upper_text_field: "Colonel Jupiter explains how they need to build up their strength for the long trip back north in the spring. And then, as surely as",
          lower_text_field: "the earth moves along in its orbit around the sun, spring comes again."
      },
      12 => {
          upper_text_field: "Colonel Jupiter describes how when the weather warms up, sap rises, seeds sprout, animals wake from <span class='rw_highlight'>hibernation</span>, baby",
          lower_text_field: "animals are born and the cycle of life on earth begins again."
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: Create a weather calendar for the months of winter in the Northern Hemisphere include.",
      },
      14 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
