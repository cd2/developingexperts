desc "Lesson"
task t2m2: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {
          upper_text_field: "Take a look at how your plants are doing from last week.",
      },
      6 => {rocket_word_slide: ""},
      7 => {
          upper_text_field: "Major Saturn discovers that plants are everywhere around us, but wants to know how plants grow. He finds out that there are many",
          lower_text_field: "things plants need to grow well. They need air, light, temperature, space and time. But they need even more."
      },
      8 => {
          upper_text_field: "Major Saturn discovers that like humans and animals, plants need water and nutrients (food) to survive. He is surprised to learn that",
          lower_text_field: "most plants use water to carry moisture and nutrients back and forth between the roots and leaves."
      },
      9 => {
        upper_text_field: "Major Saturn loves water, so is pleased to hear how plants feed. He finds out that they normally get their nutrients through the",
        lower_text_field: "roots in the soil. This is why it’s important to water plants when the soil becomes dry."
      },
      10 => {
          upper_text_field: "Major Saturn discovers that <span class='rw_highlight'>fertiliser</span> also provides plants with nutrients and is usually given to plants when watering. He learns",
          lower_text_field: "that the most important nutrients for plant growth are <span class='rw_highlight'>nitrogen</span>, <span class='rw_highlight'>phosphorus</span> and <span class='rw_highlight'>potassium</span>."
      },
      11 => {
          upper_text_field: "<span class='rw_highlight'>Nitrogen</span> is necessary for making green leaves, <span class='rw_highlight'>phosphorus</span> is needed for making big flowers and strong roots. <span class='rw_highlight'>Potassium</span>",
          lower_text_field: "helps the plants fight off disease. Too little or too much water or nutrients can also be harmful."
      },
      12 => {
          upper_text_field: "Major Saturn wonders what helps plants to grow, besides water and nutrients. He learns that they need fresh, clean air and",
          lower_text_field: "healthy soil. Dirty air caused by smoke, gases and other <span class='rw_highlight'>pollutants</span> can be harmful to plants."
      },
      13 => {
          upper_text_field: "Dirty air makes it hard for plants to take in carbon dioxide. Plants need carbon dioxide to make food. Major Saturn discovers that",
          lower_text_field: "dirty air can also block out sunlight, which is also needed for plants to grow well."
      },
      14 => {
          upper_text_field: "Major Saturn learns that healthy soil is vital to plants. In addition to essential nutrients found in soil (from organic",
          lower_text_field: "matter and micro-organisms), soil provides an anchor for plant roots and helps support the plants."
      },
            15 => {
          upper_text_field: "Major Saturn finds out that plants also need sunlight to grow. He learns light is used as energy for making food, a process",
          lower_text_field: "called photosynthesis. Too little light can make plants weak and leggy looking. They will also have fewer flowers and fruit."
      },
            16 => {
          upper_text_field: "Major Saturn discovers temperature is important too. He learns that most plants prefer cooler night-time temperatures and",
          lower_text_field: "warmer daytime temperatures. If it is too hot they may burn, if it is too cold they will freeze."
      },
            17 => {
          upper_text_field: "Major Saturn finds out that it is important for plants to have space. Both the roots and leaves need room to grow. Without enough",
          lower_text_field: "room, plants can become stunted or too small."
      },
            18 => {
          upper_text_field: "Plants can suffer from diseases when they don’t get enough air. Major Saturn learns that plants need time; they do not grow",
          lower_text_field: "overnight. Most plants need days, months, or even years to produce flowers and fruit."
      },
                  19 => {
          upper_text_field: "Today’s Mission Assignment: To plant broad beans - how exciting! Take a look at our film to learn more.",
          main_video_slide: ""
      },
      20 => {
        upper_text_field: "You have one minute to move to your work stations for today’s Mission Assignment.", 
        sixty_count: ""},
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }

    $rocket_words = {
          'Nitrogen' => {
              description: "Is a gas present in animals and vegetables. It is needed to make leaves green.",
              image: "" },
          'Phosphorus' => {
              description: "A nutrient which is needed for making big flowers and strong roots and is used in fertilizers.",
              image: "" },
          'Potassium' => {
              description: "A nutrient used in fertilizer which helps the plants fight off disease.",
              image: "" },
          'Fertilizer' => {
              description: "A substance added to soil to help keep it healthy so plants get what they need to grow.",
              image: "" },
          'Pollutants' => {
              description: "A substance, added to air, soil, water, or other natural resources which is harmful.",
              image: "" },
  }

  $questions = {
      "What is <span class='rw_highlight'>nitrogen</span>?" => {answer: "Is a gas present in animals and vegetables. It is needed to make leaves green.",
                           image: "",
                           slide_id: 6},
      "What is <span class='rw_highlight'>phosphorous</span>?" => {answer: "A nutrient which is needed for making big flowers and strong roots and is used in fertilizers.",
                                image: "",
                                slide_id: 7},
      "What is <span class='rw_highlight'>potassium</span>?" => {answer: "A nutrient used in fertilizer which helps the plants fight off disease.",
                           image: "",
                           slide_id: 8},
      "What is a <span class='rw_highlight'>fertilizer</span>?" => {answer: "A substance added to soil to help keep it healthy so plants get what they need to grow.",
                              image: "",
                              slide_id: 9},
      "What is a <span class='rw_highlight'>pollutant</span>?" => {answer: "A substance, added to air, soil, water, or other natural resources which is harmful.",
                              image: "",
                              slide_id: 14},
  }

end
