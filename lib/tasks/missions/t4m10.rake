desc "Lesson"
task t4m10: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars learns that as a boy, <span>Alexander Graham Bell</span> was fascinated by sound and wanted to know how vocal chords",
          lower_text_field: "make noise."
      },
      7 => {
          upper_text_field: "He and his brother dissected the larynx of a sheep, then built a machine of tin and rubber, designing it to work like the sheep’s",
          lower_text_field: "vocal chords. When they blew through the machine, it made a noise."
      },
      8 => {
          upper_text_field: "Commander Mars learns that <span>Bell</span> was born in 1847 in the United States. His father and his grandfather were teachers who taught",
          lower_text_field: "students who could not speak or hear. Many of their students had been deaf all their lives."
      },
      9 => {
          upper_text_field: "Commander Mars explains that <span>Bell’s</span> father and grandfather invented ‘visible speech’, which showed deaf people how to",
          lower_text_field: "move their mouths to pronounce different letters."
      },
      10 => {
          upper_text_field: "Commander Mars explains that the young <Span>Bells</span> learnt that when air vibrations come into the ear, we hear sounds. A friend of their",
          lower_text_field: "father’s demonstrated that principle to him by scattering sand on top of a drum, then playing the violin nearby."
      },
      11 => {
          upper_text_field: "The vibrations from the violin made the drumhead vibrate. The sand, in turn, vibrated and shifted around.",
      },
      12 => {
          upper_text_field: "Commander Mars explains that in 1860, people had two ways to communicate over long distances. They could write letters or use",
          lower_text_field: "the telegraph, which had been invented by Samuel Morse in 1840. The telegraph worked by sending electrical pulses through wires."
      },
      13 => {
          upper_text_field: "Commander Mars explains that by following a code, now called Morse code, the pulses spelt out words. <Span>Alexander Bell</span> wondered",
          lower_text_field: "whether wires could carry more complicated signals. Could they carry sounds of the human voice?"
      },
      14 => {
          upper_text_field: "Commander Mars learns how in an electrical shop in Boston, <span>Bell</span> and another inventor called Thomas Watson began building",
          lower_text_field: "machines to test the idea. <Span>Bell</span> designed a machine that worked like the voice box and the ear, connected by electrical wire."
      },
      15 => {
          upper_text_field: "Commander Mars that the machine had two parts, the <span>transmitter</span> which turned sound into electricity and sent them through the",
          lower_text_field: "wire and a <span>receiver</span>, which turned the electrical signals back into sound. In March 1876, the invention finally worked."
      },
      16 => {
          upper_text_field: "<span>Alexander Bell</span> spoke into the <span>transmitter</span>: ‘Mr Watson! Come here – I want to see you!’ Thomas Watson, 20 metres away in the next",
          lower_text_field: "room, heard the words quite clearly. In June 1876, Bell showed his invention at America’s Centennial Exhibition in Philadelphia."
      },
      17 => {
          upper_text_field: "The emperor of Brazil was there. He held the <span>receiver</span> to his ear, and from the far end of the hall, <span>Alexander Graham Bell</span> into the",
          lower_text_field: "into the <span>transmitter</span>, reciting words from Shakespeare’s play Hamlet: ‘To be, or not to be: that is the question.’"
      },
      18 => {
          upper_text_field: "For his new invention, <span>Bell</span> received the Centennial Prize. Later that year, <span>Bell</span> and Watson attached their instruments to wires",
          lower_text_field: "and to each other, they lived two miles apart. It didn’t take very long before people wanted <Span>telephones</span>."
      },
      19 => {
          upper_text_field: "Within a year, hundreds of households in Boston were connected by <span>telephone</span> wires. <span>Alexander Graham</span> Bell continued",
          lower_text_field: "experimenting. He worked on early versions of <span>phonograph</span> records, air conditioners and X-ray machines."
      },
      20 => {
          upper_text_field: "Listen to this short film which provides an account of the work of <span>Alexander Graham Bell</span>.",
          main_video_slide: ""
      },
      21 => {
          upper_text_field: "Today’s Mission Assignment: Write and record a song which summarizes the work of <span>Alexander Graham Bell</span>.",
      },
      22 => {
          upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. Where’s Commander Mars?",
          sixty_count: ""
      },
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}

end
