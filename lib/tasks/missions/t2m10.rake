desc "Lesson"
task t2m10: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn explains that matter doesn’t always stay the same <span class='rw_highlight'>state</span>. Now, ‘<span class='rw_highlight'>state</span>’ here doesn’t mean a place like the United",
          lower_text_field: "States of America. This is a different kind of state – the <span class='rw_highlight'>states of matter</span>, which you’ve just learned the names of: solid, liquid and gas."
      },
      7 => {
          upper_text_field: "Major Saturn learns that matter can change from one <span class='rw_highlight'>state</span> to another. A solid can become a liquid. A liquid can become a gas.",
          lower_text_field: "Let’s see how <span class='rw_highlight'>matter</span> can change state or form."
      },
      8 => {
          upper_text_field: "Major Saturn explains we can do this by looking at a cup of water. If you put some water in a cup, what <span class='rw_highlight'>state of matter</span> is in the",
          lower_text_field: "cup? Yes, it’s a liquid."
      },
      9 => {
          upper_text_field: "Major Saturn explains if you put that cup of water into a freezer and leave it overnight, what will be its <span class='rw_highlight'>state of matter</span>? The water",
          lower_text_field: "freezes and becomes ice. The liquid has turned into a solid."
      },
      10 => {
          upper_text_field: "Now let the cup of frozen water sit for a few hours. What happens? The ice melts: the solid has turned back into a liquid.",
          lower_text_field: "Now take the cup of water, pour it into a <span class='rw_highlight'>boiling</span> tube, and put it over the Bunsen burner. Don’t get too close, but watch what happens."
      },
      11 => {
          upper_text_field: "Major Saturn describes how the water begins to <span class='rw_highlight'>boil</span>, then soon it bubbles as steam. The liquid water has turned to gas. Careful –",
          lower_text_field: "steam is very hot. If you <span class= 'rw_highlight'>boil</span> the water for a long time, eventually all of the water will turn to steam and the <span class='rw_highlight'>boiling</span> tube will be empty."
      },
      12 => {
          upper_text_field: "Major Saturn asks, ‘Where has the water gone?’ It has been turned into a gas (steam) and has mixed with the air in the room. Now",
          lower_text_field: "the water is part of the matter that makes up the air in the room."
      },
      13 => {
          upper_text_field: "Major Saturn explains that all matter is made up of tiny building blocks called <span class='rw_highlight'>atoms</span> and <span class='rw_highlight'>molecules</span>. They are much too small to",
          lower_text_field: "see until you get many of them together. A <span class='rw_highlight'>molecule</span> is a group of <span class='rw_highlight'>atoms</span> arranged in a <span class='rw_highlight'>pattern</span>."
      },
      14 => {
          upper_text_field: "Major Saturn describes how even simple things like water, sand, air, gas for cooking, sugar and petrol need the <span class='rw_highlight'>atoms</span> to be in a",
          lower_text_field: "<span class='rw_highlight'>pattern</span>, making a <span class='rw_highlight'>molecule</span>, for you to tell what they are."
      },
      15 => {
          upper_text_field: "Major Saturn explains that the <span class='rw_highlight'>atoms</span> in a fork or the <span class='rw_highlight'>molecules</span> in sugar are held tightly together. That’s how you can pick up a",
          lower_text_field: "fork or a grain of sugar. Even one grain still contains many, many <Span class='rw_highlight'>molecules</span>."
      },
      16 => {
          upper_text_field: "Today’s Mission Assignment: Changing a liquid into a gas.",
          main_video_slide: ""
      },
      17 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s Mission Assignment.",
          sixty_second_count: ""
      },
      18 => {complete_investigation_slide: ""},
      19 => {rocket_word_slide: ""},
      20 => {quiz_slide: ""},
      21 => {outro_slide: ""}
}

end
