desc "Lesson"
task t1m13: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that when it is hot and <span class= 'rw_highlight'>humid</span>, we sweat and want a cold icy drink. What is your favourite type",
          lower_text_field: "of <span class= 'rw_highlight'>weather</span>?"
      },
      7 => {
          upper_text_field: "Captain Neptune wants to know if it is it hot or cold, cool or warm outside? Discuss what the <span class= 'rw_highlight'>weather</span> is like with your talk",
          lower_text_field: "partner. The <span class= 'rw_highlight'>temperature</span> goes up and down."
      },
      8 => {upper_text_field: "Captain Neptune describes how when the sun comes up, it warms the air and the <span class= 'rw_highlight'>temperature</span> goes up. When the sun goes",
            lower_text_field: "down, the air gets cooler and the <span class= 'rw_highlight'>temperature</span> goes down."
      },
      9 => {
        upper_text_field: "Captain Neptune explains that the <span class= 'rw_highlight'>temperature</span> changes with the seasons. In many places, summer days are usually warm or hot.",
        lower_text_field: "In most places, the <span class= 'rw_highlight'>temperature</span> in summer is much higher than in the winter. Winter days are usually cool or cold."
      },
      10 => {
          upper_text_field: "Captain Neptune explains that people use a <span class= 'rw_highlight'>thermometer</span> to find out what the <span class= 'rw_highlight'>temperature</span> is. Many <span class= 'rw_highlight'>thermometers</span>, like the one in",
          lower_text_field: "the picture here, have a coloured <span class= 'rw_highlight'>liquid</span> inside a tube."
      },
      11 => {
          upper_text_field: "Captain Neptune describes how as the <span class= 'rw_highlight'>temperature</span> goes higher, the <span class= 'rw_highlight'>liquid</span> rises in the tube. As the <span class= 'rw_highlight'>temperature</span> goes lower, the",
          lower_text_field: "<span class= 'rw_highlight'>liquid</span> goes down in the tube."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment:",
      },
      13 => {rocket_word_slide: ""},
      14 => {quiz_slide: ""},
      15 => {outro_slide: ""}
  }


end
