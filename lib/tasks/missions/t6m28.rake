desc "Lesson"
task t6m28: :environment do

 slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "General Mercury explains that if you put an apple in a bowl of water. What do you see? From the side, the part of the picture",
          lower_text_field: "that’s underwater looks a lot larger than the part above the surface, doesn’t it? Why?"
          },
      7 => {
          upper_text_field: "General Mercury explains that the apple in the bowl of water looks a bit odd because the light <span class='rw_highlight'>rays</span> speed up and change",
          lower_text_field: "course just a bit as they move between the water and the air."
          },
      8 => {
          upper_text_field: "General Mercury explains that whenever you see something, you are seeing light <span class='rw_highlight'>rays</span> bouncing off it. The light travels from",
          lower_text_field: "the apple, through the water and the glass, then through the air before it reaches your eyes."
          },
      9 => {
          upper_text_field: "General Mercury explains that the <span class='rw_highlight'>transparent</span> materials allow light to pass through them. But light moves less quickly through",
          lower_text_field: "<span class='rw_highlight'>transparent</span> materials than through air."
          },
      10 => {
          upper_text_field: "So, water and glass slow light down much more than air. General Mercury explains you can see this particularly well when your",
          lower_text_field: "apple is half in and half out of the water."
          },
      11 => {
          upper_text_field: "General Mercury explains that the light <span class='rw_highlight'>rays</span> change course just a little when they move from water to glass, then from glass to",
          lower_text_field: "water. It is called <span class='rw_highlight'>refraction</span>. Light leaving the water is bent closer to the surface."
          },
      12 => {
          upper_text_field: "General Mercury explains that those slight changes in the course of the light <span class='rw_highlight'>rays</span> travelling between the apple and your",
          lower_text_field: "eyes make the partly-submitted apple look crooked and misshaped."
          },
      13 => {
          upper_text_field: "General Mercury explains that if you now try putting a straw in a straight-sided glass that is half full of water. What do you see?",
          lower_text_field: "Does it also look crooked when you see it from the side?"
          },
      14 => {
          upper_text_field: "General Mercury explains that when looking from above, things in water appear shallower than they really are. The top of the",
          lower_text_field: "water is flat but the area where it touches the glass is curved. A curved, refracting surface is a kind of lens."
          },
      15 => {
          upper_text_field: "General Mercury explains that some days on an extremely hot day you can see ‘heat <span class='rw_highlight'>haze</span>’. This is when some air gets hotter",
          lower_text_field: "or more humid than the rest and changes the speed of light."
          },
      17 => {
          upper_text_field: "General Mercury explains that light <span class='rw_highlight'>rays</span> get bent as they pass through and things beyond the <span class='rw_highlight'>haze</span> look <span class='rw_highlight'>distorted</span> or ‘hazy’. It",
          lower_text_field: "doesn’t often become hot enough in the UK, but perhaps you can see heat <span class='rw_highlight'>haze</span> if you are in a hot country in Summer!"
          },
      18 => {
          upper_text_field: "General Mercury wants you to explain to your talk partner how a magnifying glass works and when it would be useful.",
          },
      19 => {main_video_slide: "",
             upper_text_field: "Today’s Mission Assignment: "
             },
      20 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }




  $rocket_words = {
          "Ray" => {
              description: "The lines in which light appears to radiate from a luminous body.",
              image: "" },
          "Transparent" => {
              description: "Able to be seen through.",
              image: "" },
          "Refraction" => {
              description: "The action of distorting an image by viewing through a medium.",
              image: "" },
          "Haze" => {
              description: "Dust, smoke or mist that has filled the air so that you cannot see clearly.",
              image: "" },
          "Distort" => {
              description: "To change (something) so that it is no longer true or accurate.",
              image: "" },
  }

  questions = {
      "What is a <span class='rw_highlight'>ray</span>?" => {answer: "The lines in which light appears to radiate from a luminous body.",
                           image: "",
                           slide_id: 7},
      "What is <span class='rw_highlight'>transparent</span>?" => {answer: "Able to be seen through.",
                                image: "",
                                slide_id: 9},
      "What does <span class='rw_highlight'>refraction</span> mean?" => {answer: "The action of distorting an image by viewing through a medium.",
                           image: "",
                           slide_id: 11},
      "What is <span class='rw_highlight'>haze,/span>?" => {answer: "Dust, smoke or mist that has filled the air so that you cannot see clearly.",
                              image: "",
                              slide_id: 15},
      "What does <span class='rw_highlight'>distort</span> mean?" => {answer: "To change (something) so that it is no longer true or accurate.",
                              image: "",
                              slide_id: 17},
  }

end
