desc "Lesson"
task t4m8: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars wants you to pretend that you’re an opera star. Sing the highest note you can sing. Now sing the lowest. When we",
          lower_text_field: "describe how high or low sound is, we are talking about the sound’s pitch. A bird singing makes a <span>high-pitched</span> sound."
      },
      7 => {
          upper_text_field: "Commander Mars explains that a dog growling makes a low- pitched sound. Think of a flute and a tuba. Which instrument",
          lower_text_field: "makes <span>high-pitched</span> sounds? Which makes low-pitched sounds?"
      },
      8 => {
          upper_text_field: "Commander Mars explains when you sing a high note, your <span>vocal chords</span> vibrate very fast, hundreds of times a second. When you",
          lower_text_field: "sing a low note, your <span>vocal chords</span> vibrate more slowly."
      },
      9 => {
          upper_text_field: "Faster vibrations make a sound with a <Span>higher pitch</span>. Slower vibrations make a sound with a lower pitch. Commander Mars",
          lower_text_field: "explains that the girl is reacting to a <Span>high-pitched</span> sound. Are the vibrations she is hearing fast or slow?"
      },
      10 => {
          upper_text_field: "Commander Mars wants you to try this. Take a large rubber band and loop it around a drawer handle. Pull it tight and pluck it. Now",
          lower_text_field: "loosen it and pluck it again. Can you hear the difference?"
      },
      11 => {
          upper_text_field: "When it’s pulled tight, the rubber band makes a <span>high-pitched</span> sound. Is the rubber band vibrating faster when it’s loose or when",
          lower_text_field: "it’s pulled tight?"
      },
      12 => {
          upper_text_field: "Commander Mars explains that you carry a noisemaker around with you wherever you go. It’s called your <span>larynx</span>, or voice box,",
          lower_text_field: "and it’s in your throat. When you feel your throat and hum, you are feeling the vibration of your <span>larynx</span>."
      },
      13 => {
          upper_text_field: "Commander Mars wants if you know, how your body make a sound? Air travels from your lungs and past your <span>vocal chords</span>,",
          lower_text_field: "which stretch open and shut like two thick rubber bands inside your <span>larynx</span>."
      },
      14 => {
          upper_text_field: "Commander Mars explains how you use muscles to relax or tighten your <span>vocal cords</span>, which changes the pitch of your voice",
          lower_text_field: "from low to high. You use your tongue, teeth and lips to form words."
      },
      15 => {
          upper_text_field: "Mission Activity: Take a look at today’s film then have a go at creating your own sound water whistle.",
          main_video_slide: ""
      },
      16 => {
          upper_text_field: "Please report to your work stations for today’s mission assignment.",
          sixty_count: ""
      },
      17 => {complete_investigation_slide: ""},
      18 => {rocket_word_slide: ""},
      19 => {quiz_slide: ""},
      20 => {outro_slide: ""}
}

end
