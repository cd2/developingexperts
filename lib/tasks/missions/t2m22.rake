desc "Lesson"
task t2m22: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Major Saturn starts to sneeze! Ah-choo! Wow, that was a big sneeze. Well, that’s okay, it was just one and – ah choo! Ah choo!", 
      },
      7 => {
          upper_text_field: "Major Saturn can’t stop sneezing. His nose is runny and he’s starting to feel a little hot. Major Saturn has a tummy ache.",
      },
      8 => {
          upper_text_field: "Everybody feels poorly now and then. It’s no fun. You can feel just terrible. Some <span class='rw_highlight'>germs</span> can make your body unwell. You",
          lower_text_field: "can’t see <span class='rw_highlight'>germs</span>, but you can certainly feel what the bad ones do to you! When your body gets ill, it fights the <span class='rw_highlight'>germs</span>."
      },
      9 => {
          upper_text_field: "Major Saturn explains that sometimes your body is working really hard to fight the <span class='rw_highlight'>germs</span>, you have a fever – which means that your",
          lower_text_field: "body’s temperature rises. Many illnesses, like colds and flu, can be spread from one person to another."
      },
      10 => {
          upper_text_field: "If a friend with a cold sneezes right in your face, his cold <span class='rw_highlight'>germs</span> could get into your body through your nose and mouth.",
          lower_text_field: "Then in a few days you will catch a cold, too. Try not to spread your <span class='rw_highlight'>germs</span>: cover your face when you sneeze or cough."
      },
      11 => {
          upper_text_field: "Major Saturn wants you to use tissues when you blow your nose. Wash your hands after using the toilet and before every meal.",
          lower_text_field: "Your hands can pick up <span class='rw_highlight'>germs</span> without even knowing it. This will help you keep <span class='rw_highlight'>clean</span> and stop germs from spreading."
      },
      12 => {
          upper_text_field: "Sometimes when you’re ill, you go to the doctor. The doctor uses a thermometer to check Major Saturn’s temperature. He asks",
          lower_text_field: "him to open his mouth wide and say ‘Ah’ so he can look at his throat."
      },
      13 => {
          upper_text_field: "The doctor may listen to his heartbeat with a tool called a <span class='rw_highlight'>stethoscope</span>. The doctor may decide that he needs medicine to",
          lower_text_field: "help him get better."
      },
      14 => {
          upper_text_field: "Major Saturn explains that you should never take medicine without permission, and only take as much as you are supposed",
          lower_text_field: "to. Sometimes the doctor may give an injection instead."
      },
      15 => {
          upper_text_field: "Major Saturn explains that you may not like it, but be brave: the injection helps your body fight <span class='rw_highlight'>disease</span>. When you were very",
          lower_text_field: "young, you probably got an injection called a vaccination to help you keep you from falling ill with certain <span class='rw_highlight'>diseases</span>."
      },
      16 => {
          upper_text_field: "Major Saturn explains that <span class='rw_highlight'>Louis Pasteur</span> lived from 1822 -1895. He is famous for his work on the causes and prevention of disease.",
          lower_text_field: "He is well known for inventing a process which stops food and liquid such as milk, making people sick. It’s called Pasteurization."
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment:",
          main_video_slide: ""
      },
      18 => {
          upper_text_field: "Major Saturn announces you have 60 seconds to report to your work stations to complete today’s mission assignment.",
          sixty_second_count: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}



  $rocket_words = {
          "Disease" => {
              description: "An illness that affects a person, animal, or plant, that prevents the body or mind from working well.",
              image: "" },
          "Cleanliness" => {
              description: "Personally neat; careful to keep or make clean.",
              image: "" },
          "Louis Pasteur" => {
              description: "Chemist who lived in the 1800s; was famous for his work on disease causes and prevention.",
              image: "" },
          "Germs" => {
              description: "A very small living thing that causes disease.",
              image: "" },
          "Stethoscope" => {
              description: "An instrument that is used for listening to someone's heart or lungs.",
              image: "" },
  }



end
