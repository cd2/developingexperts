desc "Lesson"
task t5m2: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus discovers that the San Francisco <span>earthquake</span> took place in California on the 18th April 1906.",
      },
      7 => {
          upper_text_field: "Brigadier Venus is saddened to find out that on that terrible morning, San Francisco was shaken by one of the most destructive",
          lower_text_field: "<span>earthquakes</span> ever. Many people died, were crushed by falling buildings or trapped in fires that started when gas lines broke."
      },
      8 => {
          upper_text_field: "Brigadier Venus finds out that more recently, devastating <span>earthquakes</span> have destroyed many buildings in China’s Sichuan",
          lower_text_field: "Province in 2008 and again in 2013 and as well in Haiti in 2010."
      },
      9 => {
          upper_text_field: "Brigadier Venus discovers that <span>earthquakes</span> create a sudden violent shaking of the earth’s crust and can be strong enough to shake",
          lower_text_field: "buildings and bridges off their foundations, to open cracks big enough for cars can fall through, and to cause huge avalanches."
      },
      10 => {
          upper_text_field: "Brigadier Venus is pleased to learn that some <span>earthquakes</span> can be so mild that people don’t even notice them. In fact, an <span>earthquake</span>",
          lower_text_field: "happens somewhere in the world every 30 seconds."
      },
      11 => {
          upper_text_field: "Brigadier Venus finds out that geologists record vibrations from <span>earthquakes</span> with machines called <span>seismographs</span> which measure",
      },
      12 => {
          upper_text_field: "tremors underground and plot them like a graph. Normally, the ground is stable and the <span>seismograph</span> plots a fairly straight",
          lower_text_field: "horizontal line, but during an <span>earthquake</span>, it measures and draws the size of the tremors like in this picture. Can you see Brigadier Venus?"
      },
      13 => {
          upper_text_field: "Brigadier Venus learns that to compare the strength, of different <span>earthquakes</span>, geologists often use a chart called the <span>Richter scale</span>.",
          lower_text_field: "He discovers that the <span>Richter scale</span> gives scientists a way to compare <span>earthquakes</span>."
      },
      14 => {
          upper_text_field: "He finds out that an <span>earthquake</span> measuring 2 on the <span>Richter scale</span> is 10 times stronger than a <span>earthquake</span> rated 1. Fortunately no <span>earthquake</span> has",
          lower_text_field: "ever measured greater than 9 because when an <Span>earthquake</span> which measures 9, is equal to an explosion of 200 million tons of dynamite!"
      },
      15 => {
          upper_text_field: "He learns that the Earth’s crust is like a big, messy jigsaw puzzle puzzle, made of many pieces called plates. The line where two plates",
      },
      16 => {
          upper_text_field: "meet is called a plate boundary. Some plates are locked tightly together where they meet. Others move past each other. Some move",
          lower_text_field: "apart, and can create deep cracks in the Earth’s crust. Other plates will even overlap at their boundaries and lie on top of each other."
      },
      17 => {
          upper_text_field: "Brigadier learns that where ever two plates move past each other, geologists say there is a <span>fault</span>. San Francisco sits right on a <span>fault</span>",
          lower_text_field: "called the San Andreas <span>Fault</span>, which runs the length of California, and is one reason why California experiences so many <span>earthquakes</span>."
      },
      18 => {
          upper_text_field: "Brigadier Venus wonders what makes the plates move? Like rafts on water, the Earth’s plates float on the mantle. When the mantle moves,",
          lower_text_field: "so do the plates. The mantle is always flowing. Hotter rocks rise to the surface, cool, then sink."
      },
      19 => {
          upper_text_field: "Brigadier Venus learns that the sliding of the rock in the Earth’s crust can cause pressure to build up over time, until suddenly –",
          lower_text_field: "CRACK- the rock fractures, stored energy is released, and the ground trembles."
      },
      20 => {
          upper_text_field: "Brigadier Venus is amazed to find out that sometimes, an <span>earthquake</span> happens on the ocean floor. Its energy pushes",
          lower_text_field: "seawater into a giant wave called a <span>tsunami</span>. A <Span>tsunami</span> can travel more than 400 miles per hour. Take a look at this film."
      },
      21 => {
          upper_text_field: "As it approaches a shore, where the water is shallower, it grows taller and taller, pushed by the energy behind it. A <span>tsunami</span> can",
          lower_text_field: "grow as tall as a ten-storey building before the curling wall of water crushes down on land. Can you see Captain Venus?"
      },
      22 => {
          upper_text_field: "Brigadier Venus declares you have 60 seconds to report to your work stations for today’s mission!",
          sixty_count: ""
      },
      23 => {
          upper_text_field: "Today’s Mission Assignment: Make your own <span>earthquake</span> simulator using the materials supplied.",
      },
      24 => {complete_investigation_slide: ""},
      25 => {rocket_word_slide: ""},
      26 => {quiz_slide: ""},
      27 => {outro_slide: ""}
}

end
