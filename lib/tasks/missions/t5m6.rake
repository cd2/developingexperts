desc "Lesson"
task t5m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that if you could go back in time 300 million years – before the time of the dinosaurs – and look",
          lower_text_field: "down from space, the Earth would look very different."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains how <Span>geologists</span> believe that, back then, the <span>continents</span> we know today were crowded together in one",
          lower_text_field: "giant land mass. They have named that land mass ‘<span>Pangaea</span>’ a word made up from the Greek for ‘All the Earth’."
      },
      8 => {
          upper_text_field: "Brigadier Venus explains how scientists believe that over a 100 million years, the super-<span>continents</span> only drifted a few",
          lower_text_field: "centimetres apart per year. They believe that this happened for million of years leading to a lot of changes."
      },
      9 => {
          upper_text_field: "Brigadier Venus explains how <span>geologists</span> call those changes <span>continental drift</span>. The scientist who developed the <span>continental</span>",
          lower_text_field: "<span>drift</span> theory was a gentleman called Alfred Wegener."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains how the Earth looks like a smooth blue and white marble from space. But down here on the planet, we",
          lower_text_field: "know its surface is anything but smooth."
      },
      11 => {
          upper_text_field: "Brigadier Venus explains how the land is wrinkled and bumpy. Mountains poke up from every <Span>continent</span> and from the ocean",
          lower_text_field: "floor. Scientists divide mountains into several categories based on how they were formed."
      },
      12 => {
          upper_text_field: "Brigadier Venus sees a <span>dome-shaped mountain</span> in the distance, which made by volcanoes spitting out piles of lava, cinders",
          lower_text_field: "and ash. Volcanic mountains can also form without an eruption."
      },
      13 => {
          upper_text_field: "Brigadier Venus learns how magma beneath the surface swells and pushes up an mountain-sized bump. Before it finds a vent,",
          lower_text_field: "the magma cools and hardens into <Span>dome-shaped mountains</span>. Ben Nevis, Britain’s highest mountain is a dome mountain."
      },
      14 => {
          upper_text_field: "Today’s Mission Assignment: Have a go at trying this experiment for yourself!",
          main_video_slide: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
