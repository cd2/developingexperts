desc "Lesson"
task t3m9: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that in <span class='rw_highlight'>summer</span>, when the weather is warm and there’s plenty of sunshine, many plants and animals",
          lower_text_field: "grow larger. The little sunflower seedling grows into a mature, adult plant and begins to make seeds."
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains that fruits like <span class='rw_highlight'>strawberries</span> and vegetables like onions grow bigger and bigger to <span class='rw_highlight'>ripen</span>. Trees",
          lower_text_field: "add inches to their branches."
      },
      8 => {
          upper_text_field: "Colonel Jupiter describes how in <span class='rw_highlight'>summer</span>, the baby animals that were born in the spring grow bigger and stronger. The baby birds",
          lower_text_field: "that <span class='rw_highlight'>hatched</span> out of their eggs in the spring grow up and learn to find their own food, and stray further from their parents."
      },
      9 => {
          upper_text_field: "Colonel Jupiter describes how tadpoles grow into adult frogs. Young insects like <span class='rw_highlight'>grasshoppers</span> become adults.",
      },
      10 => {
          upper_text_field: "Today’s Mission Assignment: This experiment reverses the process of ripening.",
          main_video_slide: ""
      },
      11 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      12 => {complete_investigation_slide: ""},
      13 => {rocket_word_slide: ""},
      14 => {quiz_slide: ""},
      15 => {outro_slide: ""}
}

end
