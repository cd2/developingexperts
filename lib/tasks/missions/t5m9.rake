desc "Lesson"
task t5m9: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains that when you look at weather maps in the newspaper or watch the weather report on TV, you can see",
          lower_text_field: "that weather isn’t the same everywhere on earth – it isn’t even the same in different cities or towns across the UK!"
      },
      7 => {
          upper_text_field: "Brigadier Venus describes how the weather is always changing, from warm breezes and gentle rains to <span class='rw_highlight'>hurricanes</span>, blizzards",
          lower_text_field: "and ice storms. It changes because the Earth is heated unevenly, by the heat energy moving around in the Earth’s atmosphere."
      },
      8 => {
          upper_text_field: "Brigadier Venus explains that the sun doesn’t shine with the same intensity on every region of the Earth. It shines more",
          lower_text_field: "directly on the area near the Equator, so the <span class='rw_highlight'>air pressure</span> around the Equator heats up the most."
      },
      9 => {
          upper_text_field: "It shines the least directly at the North and South Poles, so the air at the Poles stays the coldest. These differences in",
          lower_text_field: "temperature make the air in the atmosphere move around."
      },
      10 => {
          upper_text_field: "Brigadier Venus wants to know if you have ever noticed how when you open an oven, hot air rises up in your face, but when",
          lower_text_field: "you open a fridge, cold air spills down to your feet? Hot air rises and cold air sinks."
      },
      11 => {
          upper_text_field: "Brigadier Venus explains that as air is heated, its molecules spread out. It becomes less dense. It is lighter and puts less",
          lower_text_field: "pressure on the earth. Hot air balloons use warm air to rise. Warmer air, is less dense, so it is a low-pressure air system."
      },
      12 => {
          upper_text_field: "Brigadier Venus explains that cooler air contains molecules that are more densely packed together, or closer together. It puts",
          lower_text_field: "more pressure on the Earth, and we call it a high-pressure air system."
      },
      13 => {
          upper_text_field: "Brigadier Venus explains that <span class='rw_highlight'>meteorologists</span> have developed ways to describe wind by measuring its speed and direction.",
          lower_text_field: "For example, you could describe a wind as ‘a north wind at 20 miles per hour’."
      },
      14 => {
          upper_text_field: "Brigadier Venus explains that if you can see a <span class='rw_highlight'>wind sock</span> fluttering, it is probably a breezy day. If it is blown straight out",
          lower_text_field: "like this one, the wind is strong and it might be a gale."
      },
      15 => {
          upper_text_field: "Brigadier Venus explains that a wind that blows 10 or 20 miles per hour is called a breeze. If the winds gusts up to 40 or 50",
          lower_text_field: "miles per hour, it is called a gale. If it travels 75 miles per hour or faster, it qualifies as a <span class='rw_highlight'>hurricane</span>."
      },
      16 => {
          upper_text_field: "To measure wind speed, <span class='rw_highlight'>meteorologists</span> use an instrument called an <span class='rw_highlight'>anemometer</span>. It is a device with three arms and a",
          lower_text_field: "cup at the end of each arm."
      },
      17 => {
          upper_text_field: "Wind blows into the cups and makes the arms spin. By counting its spins during a certain time period, <span class='rw_highlight'>meteorologists</span>",
          lower_text_field: "can measure the speed of the wind."
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Watch the film then complete the experiment with your partner.",
          main_video_slide: ""
      },
      19 => {complete_investigation_slide: ""},
      20 => {rocket_word_slide: ""},
      21 => {quiz_slide: ""},
      22 => {outro_slide: ""}
}


  $rocket_words = {
          "Meteorologist" => {
              description: "A person who studies the weather.",
              image: "" },
          "Air Pressure" => {
              description: "The force applied by the air which pushes down on the Earth.",
              image: "" },
          "Hurricane" => {
              description: "Wind which travels 75 miles per hour or faster, is classified as a <span class='rw_highlight'>hurricane</span>.",
              image: "" },
          "Wind Sock" => {
              description: "A cone of fabric which enables you to see the direction of the wind.",
              image: "" },
          "Anemometer" => {
              description: "An instrument which measures wind speed.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>meteorologist</span>?" => {answer: "A person who studies the weather.",
                           image: "",
                           slide_id: 13},
      "What is <span class='rw_highlight'>air pressure</span>?" => {answer: "The force applied by the air which pushes down on the Earth.",
                                image: "",
                                slide_id: 8},
      "What is a <span class='rw_highlight'>hurricane</span>?" => {answer: "Wind which travels 75 miles per hour or faster, is classified as a <span class='rw_highlight'>hurricane</span>.",
                           image: "",
                           slide_id: 15},
      "What is a <span class='rw_highlight'>wind sock</span>?" => {answer: "A cone of fabric which enables you to see the direction of the wind.",
                              image: "",
                              slide_id: 14},
      "What is an <span class='rw_highlight'>anemometer</span>?" => {answer: "An instrument which measures wind speed.",
                              image: "",
                              slide_id: 16},
  }

end
