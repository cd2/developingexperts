desc "Lesson"
task t6m25: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Mercury explains that when you turn on a light, you are starting up a flow of electrons through an electric <span class='rw_highlight'>circuit</span>. This",
          lower_text_field: "is called an electric current. It all happens with just the flick of a light <span class='rw_highlight'>switch</span>."
          },
      7 => {
          upper_text_field: "Now let’s see what is really happening at the level of electrons. Captain Mercury explains that here is an example of a simple",
          lower_text_field: "<span class='rw_highlight'>circuit</span>, consisting of four parts: 1. A circular copper wire 2. A light bulb of the old kind, such as you might find in a car"
          },
      8 => {
          upper_text_field: "3. A <span class='rw_highlight'>battery</span> and 4. A <span class='rw_highlight'>switch</span>. Captain Mercury explains that the <span class='rw_highlight'>battery</span> is the source of energy. It has a positive and negative",
          lower_text_field: "pole."
          },
      9 => {
          upper_text_field: "Captain Mercury explains that when the <span class='rw_highlight'>switch</span> is turned on, electrons travel through a wire, making the full <span class='rw_highlight'>circuit</span> from the",
          lower_text_field: "negative pole of the <span class='rw_highlight'>battery</span>, through the <span class='rw_highlight'>switch</span> and the light bulb, and back to the positive pole of the <span class='rw_highlight'>battery</span>."
          },
      10 => {
          upper_text_field: "This is called a closed <span class='rw_highlight'>circuit</span>, because the electrons complete a full <span class='rw_highlight'>loop</span>. Captain Mercury explains that when the <span class='rw_highlight'>switch</span> is",
          lower_text_field: "turned off, the <span class='rw_highlight'>circuit</span> is broken and the flow of electrons stops. This is call an open <span class='rw_highlight'>circuit</span>, because there is a gap in the <span class='rw_highlight'>circuit</span>."
          },
      11 => {
          upper_text_field: "So the electrons cannot complete the full <span class='rw_highlight'>loop</span>. Captain Mercury wants you to take a look at an ordinary, older-style clear light",
          lower_text_field: "bulb. Do you see the two wires sticking up? Stretched between them is a very fine wire called a filament."
          },
      12 => {
          upper_text_field: "Captain Mercury explains that when you turn on the <span class='rw_highlight'>switch</span> and create a closed <span class='rw_highlight'>circuit</span>, electrons travel around the <span class='rw_highlight'>circuit</span> and",
          lower_text_field: "through the light bulb. However, compared with the rest of the <span class='rw_highlight'>circuit</span>, the filament is a narrower passageway for electrons."
          },
      13 => {
          upper_text_field: "Captain Mercury explains that it’s as if traffic moving in eight lanes has had to merge onto a one lane bridge, and the result is",
          lower_text_field: "a traffic jam which backs up all the way round the <span class='rw_highlight'>circuit</span>."
          },
      14 => {
          upper_text_field: "Captain Mercury explains that this electrical traffic jam is called resistance – the narrow filament resists the fast flow of",
          lower_text_field: "electrons. And it is the resistance in the filament which causes heat to be produced."
          },
      15 => {
          upper_text_field: "Captain Mercury explains that as the electrons squeeze and jostle their way through the narrow filament, they collide with the",
          lower_text_field: "atoms in it causing them to vibrate, get hot and even glow! Thanks to resistance, light bulbs can turn electricity into light."
          },
      17 => {
          upper_text_field: "Perhaps you have noticed how hot a filament bulb gets when it’s on? In the past, many fingers were burned by impatient people",
          lower_text_field: "who tried to change the light bulb before it had cooled down. Nowadays we don’t see many filament-type light bulbs around."
          },
      18 => {
          upper_text_field: "That’s because we have found more efficient ways of producing light from electricity. Captain Mercury explains that light-emitting",
          lower_text_field: "diodes (<span class='rw_highlight'>LEDs</span>) and fluorescent bulbs are much colder when they’re on, so less electrical energy is wasted as heat."
          },
      19 => {main_video_slide: "",
             upper_text_field: "Today’s Mission Assignment: "},
      21 => {upper_text_field: "You have 60 seconds to report to your work stations for today’s mission assignment. ", sixty_count: ""},
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
  }




  $rocket_words = {
          "Circuit" => {
              description: "A device that provides a path for electrical current to flow.",
              image: "" },
          "Battery" => {
              description: "A type of container that stores energy until it is needed.",
              image: "" },
          "Loop" => {
              description: "A complete <span class='rw_highlight'>circuit</span>.",
              image: "" },
          "LED" => {
              description: "A light-emitting diode (a diode which glows when a voltage is applied).",
              image: "" },
          "Switch" => {
              description: "A device for making and breaking the connection in an electric <span class='rw_highlight'>circuit</span>.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>circuit</span>?" => {answer: "A device that provides a path for electrical current to flow.",
                           image: "",
                           slide_id: 6},
      "What is a <span class='rw_highlight'>battery</span>?" => {answer: "A type of container that stores energy until it is needed.",
                                image: "",
                                slide_id: 8},
      "What is a full <span class='rw_highlight'>loop</span> circuit?" => {answer: "A complete <span class='rw_highlight'>circuit</span>.",
                           image: "",
                           slide_id: 10},
      "What is a <span class='rw_highlight'>LED</span>?" => {answer: "A light-emitting diode (a diode which glows when a voltage is applied).",
                              image: "",
                              slide_id: 18},
      "What is a <span class='rw_highlight'>switch</span>?" => {answer: "A device for making and breaking the connection in an electric <span class='rw_highlight'>circuit</span>.",
                              image: "",
                              slide_id: 6},
  }

end
