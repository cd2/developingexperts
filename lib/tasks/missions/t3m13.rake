desc "Lesson"
task t3m13: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter wants to know what happens when you plant a seed in the ground? With the right combination of soil,",
          lower_text_field: "water and temperature, the seed sprouts and a plant starts growing."
      },
      7 => {
          upper_text_field: "Roots go down and leaves grow up. The plant grows bigger, until it is mature enough to make flowers. Colonel Jupiter",
          lower_text_field: "explains that flowers help the plant reproduce. How?"
      },
      8 => {
          upper_text_field: "Colonel Jupiter explains that often it happens like this. Part of the flower, the <span>anther</span>, makes male <span>pollen</span>. Then the wind, or",
          lower_text_field: "maybe a bee lands on the flower and carries the male <span>pollen</span> to the female part of the flower, called the ‘<span>ovule</span>’."
      },
      9 => {
          upper_text_field: "Colonel Jupiter explains that <span>ovule</span> means ‘little egg’. When this happens, we say that the <Span>ovule</span> has been ‘<span>fertilised</span>’, and now it",
          lower_text_field: "can grow until it becomes a seed."
      },
      10 => {
          upper_text_field: "If you plant that seed in the ground, what happens? The seed sprouts, and a new plant grows. It makes new seeds, and the",
          lower_text_field: "plant’s life cycle goes on."
      },
      11 => {
          upper_text_field: "Today’s film looks at the life cycle of a plant",
          main_video_slide: ""
      },
      12 => {
          upper_text_field: "Today’s Activity: Create a flower using modelling clay. Use the handout to label the parts of the flower.",
      },
      13 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      14 => {complete_investigation_slide: ""},
      15 => {rocket_word_slide: ""},
      16 => {quiz_slide: ""},
      17 => {outro_slide: ""}
}

end
