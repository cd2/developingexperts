desc "Lesson"
task t5m16: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      7 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      8 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      9 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      10 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      11 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      12 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      13 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      14 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      15 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      16 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      17 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      18 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      19 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      20 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      21 => {
          upper_text_field: "",
          lower_text_field: ""
      },
      22 => {
          upper_text_field: "",
      },
      23 => {
          upper_text_field: "",
          ten_count: ""
      },
      24 => {complete_investigation_slide: ""},
      25 => {rocket_word_slide: ""},
      26 => {quiz_slide: ""},
      27 => {outro_slide: ""}
}

  $rocket_words = {
          "Uranus" => {
              description: "The seventh planet from the Sun. It's the first planet discovered with the use of a telescope.",
              image: "" },
          "Neptune" => {
              description: "The eighth planet from the Sun making it the most distant in the solar system.",
              image: "" },
          "Pluto" => {
              description: "Known as the Dwarf Planet, it is the ninth planet from the Sun.",
              image: "" },
          "Jupiter" => {
              description: "The fifth planet from the Sun. It is 2 1⁄2 times larger than all the other planets.",
              image: "" },
          "Saturn" => {
              description: "The sixth planet from the Sun and the second-largest in the Solar System.",
              image: "" },
  }

end
