desc "Lesson"
task t1m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune wonders whether you have ever held a <span class= 'rw_highlight'>kitten</span>? It’s so tiny, it fits inside your hands. It’s soft and",
          lower_text_field: "furry, just like its mother. It has two eyes, four paws and one tail, just like its mother."
      },
      8 => {
          upper_text_field: "Captain Neptune wants to know how a <span class= 'rw_highlight'>kitten</span> is different from its mother? Well, it’s smaller. It may have the same colour and",
          lower_text_field: "markings as its mother, or it may have very different colour and markings."
      },
      9 => {upper_text_field: "Captain Neptune learns how an orange-striped mother cat could have a <span class= 'rw_highlight'>kitten</span> with grey stripes or black and white spots. Or",
            lower_text_field: "she could have orange-striped <span class= 'rw_highlight'>kittens</span>. Even though the <span class= 'rw_highlight'>kittens</span> are definitely cats! Can a mother cat have a <span class= 'rw_highlight'>puppy</span>?"
      },
      10 => {
          upper_text_field: "Captain Neptune says that if you’ve ever seen a mother cat with <span class= 'rw_highlight'>kittens</span> you will know how much attention she pays to her babies.",
      },
      11 => {
          upper_text_field: "She washes them by licking them clean (imagine if you took your bath that way!). She plays with them. She makes sure they can",
          lower_text_field: "get enough food, by letting them drink her milk. She even teaches them how to hunt when they get bigger."
      },
      12 => {
          upper_text_field: "Captain Neptune discovers that many parents take special <span class= 'rw_highlight'>care</span> of their babies. Mother robins bring insects and",
          lower_text_field: "worms back to the nest, and then they help their babies learn to swim down deep and come up for air above the water."
      },
      13 => {
          upper_text_field: "Captain Neptune explains how baby animals need these special lessons because they will have to take <span class= 'rw_highlight'>care</span> of themselves."
      },
      14 => {
          upper_text_field: "Captain Neptune learns that just like baby animals, human babies also need special <span class= 'rw_highlight'>care</span> and attention from their parents.",
      },
      15 => {
          upper_text_field: "Think of all of the ways adults help a <span class= 'rw_highlight'>newborn</span> baby. Discuss with your talk partners, you have 60 seconds!",
          sixty_count: ""
      },
      16 => {
          upper_text_field: "Captain Neptune learns how babies can’t feed themselves. Babies need adults to help them learn how to walk and talk.",
      },
      17 => {
          upper_text_field: "Sometimes you still need help from adults. But you’re not a baby any longer and there’s a lot you can do for yourself.",
      },
      18 => {main_video_slide: "",
             upper_text_field: "Today’s Mission Assignment: The film shows a flycatcher feeding his <span class= 'rw_highlight'>offspring</span>."},
      19 => {upper_text_field: "Today’s Mission Assignment: Use the classroom scrap box to create a nest."},
      20 => {upper_text_field: "Captain Neptune announces that you have 60 seconds to report to your work stations for today’s mission assignment!", sixty_count: ""},
      21 => {complete_investigation_slide: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }

end
