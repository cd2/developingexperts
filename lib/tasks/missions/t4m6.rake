desc "Lesson"
task t4m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Commander Mars learns sound can travel through all kinds of matter: through gases, liquids and solids. Every time you speak,",
          lower_text_field: "you prove that sound travels through gases. The sound of your voice is travelling through air, which is made of gases like oxygen."
      },
      7 => {
          upper_text_field: "Commander Mars wants you to place your fingertips gently on the side of a speaker when the sound is coming out. You can",
          lower_text_field: "try it with an iPod stereo speaker, television or radio. Do you feel the vibrations? What happens when you turn up the <span class='rw_highlight'>volume</span>?"
      },
      8 => {
          upper_text_field: "Commander Mars wants you to think of an example that proves sound travels through liquid. Have you ever heard someone’s",
          lower_text_field: "voice underwater in a swimming pool? It sounds funny, but you can hear it."
      },
      9 => {
          upper_text_field: "Commander Mars learns that sound can travel through a liquid. Some animals, like whales and dolphins, depend on sound that",
          lower_text_field: "travels through the water. Whales sing underwater and can hear each other from more than a mile away."
      },
      10 => {
          upper_text_field: "Commander Mars discovers that at first, you might not think that sound travels through solids. After all, we build walls to keep out sound!",
          lower_text_field: "Did you know that the <span class='rw_highlight'>speed of sound</span> can vary? Sound travels faster through solids than liquids."
      },
      11 => {
          upper_text_field: "However, when you are in your room, you can sometimes hear laughter or talking from the room next door, can’t you? Sounds can also bounce off",
          lower_text_field: "walls and create <span class='rw_highlight'>echoes</span>."
      },
      12 => {
          upper_text_field: "Commander Mars wants you to <span class='rw_highlight'>drum</span> your fingers on a table. Now rest your ear right on the table’s surface and <span class='rw_highlight'>drum</span> your fingers",
          lower_text_field: "on it again. Doesn’t it sound louder? That’s because the sound is travelling through the solid table."
      },
      13 => {
          upper_text_field: "Commander Mars learns that the word for ‘<span class='rw_highlight'>telephone</span>’ comes from two old Greek words: ‘tele’, which means ‘at a distance’ and",
          lower_text_field: "‘phone’, which means ‘sound’. You can make your own <span class='rw_highlight'>telephone</span> that will let you hear a friend’s voice from a distance."
      },
      14 => {
          upper_text_field: "Commander Mars explains that you will need two paper or plastic cups, two paper clips and about five metres of string or strong",
          lower_text_field: "thread. Poke a small hole in the bottom of each cup with a sharp pencil."
      },
      15 => {
          upper_text_field: "Commander Mars explains that you need to stick one end of the string into each hole in the bottom of each cup. Stick one end of",
          lower_text_field: "the string into a hole, and knot it tightly around a paper clip. Take one cup and ask your friend to take the other."
      },
      16 => {
          upper_text_field: "Commander Mars explains that you need to walk apart from one another until the string stretches out straight and tight. Now,",
          lower_text_field: "when you whisper or talk into your ‘<span class='rw_highlight'>telephone</span>’, your friend should be able to hear you through it."
      },
      17 => {
          upper_text_field: "Commander Mars wants to know how this works? Remember, sound can travel through solids. When you talk into the cup, you",
          lower_text_field: "make it vibrate. The vibrating cup makes the strings vibrate. The vibrating string makes the bottom of the other cup vibrate."
      },
      18 => {
          upper_text_field: "Those vibrations go into your friend’s ear, and that’s when your friend hears the sound of your voice.",
      },
      19 => {
          upper_text_field: "Today’s Mission Assignment!",
          main_video_slide: ""
      },
      20 => {
          upper_text_field: "Today’s Mission Assignment: Create your own musical bottles then perform a song with your talk partner. Complete the tests on the handout.",
      },
      21 => {
          upper_text_field: "Today’s Mission Assignment: Now create your own song with supporting lyrics which use your instruments and include today’s Rocket Words.",
      },
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
}

end
