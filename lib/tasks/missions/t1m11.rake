desc "Lesson"
task t1m11: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Captain Neptune explains that it’s hard to live without making a <span class= 'rw_highlight'>mess</span>. When you paint a picture, you make a <span class= 'rw_highlight'>mess</span>. When you",
          lower_text_field: "cook dinner, you make a <span class= 'rw_highlight'>mess</span>."
      },
      7 => {
          upper_text_field: "Captain Neptune learns that when people work in <span class= 'rw_highlight'>factories</span>, making cars or clothes or televisions, they make <span class= 'rw_highlight'>mess</span> too.",
          lower_text_field: "But we all have to learn to clean up our <span class= 'rw_highlight'>mess</span>."
      },
      8 => {upper_text_field: "Captain Neptune learns that some people who work in <span class= 'rw_highlight'>factories</span> are studying the problem of <span class= 'rw_highlight'>mess</span>. They are trying to keep",
            lower_text_field: "dangerous chemicals from going into the streams and <span class= 'rw_highlight'>polluting</span> the water. This would be <span class= 'rw_highlight'>harmful</span> to people and animals."
      },
      9 => {
        upper_text_field: "Captain Neptune explains that we can help too. There’s a place to put our <span class= 'rw_highlight'>rubbish</span>. Not on the floor. Not on the pavement. Not in",
        lower_text_field: "the street. Not in the woods. You know where <span class= 'rw_highlight'>rubbish</span> belongs, right?"
      },
      10 => {
          upper_text_field: "Captain Neptune explains that <span class= 'rw_highlight'>rubbish</span> goes in the <span class= 'rw_highlight'>rubbish</span> bin. People who throw <span class= 'rw_highlight'>rubbish</span> wherever they want are littering.",
          lower_text_field: "Don’t litter: that’s one way to help clean up the Earth."
      },
      11 => {
          upper_text_field: "Captain Neptune explains three things you can do to look after your school or home. Can you add to this list? 1. Turn off water",
          lower_text_field: "taps firmly. 2. Recycle paper, metal cans, glass and plastic. 3. Turn off the lights when you don’t need them."
      },
      12 => {
          upper_text_field: "Today’s Mission Assignment: Recycle the waste products to make a wind chime for the school grounds or home.",
      },
      13 => {
          upper_text_field: "Discuss with your talk partners what is needed to make a good wind chime? Mission Timer: 2 minutes",
      },
      14 => {
        upper_text_field: "Captain Neptune would like you get ready for today’s mission assignment and report to your work stations.", 
        sixty_count: ""},
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""},
  }






end
