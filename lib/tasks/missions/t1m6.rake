desc "Lesson"
task t1m6: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune explains that you <span class= 'rw_highlight'>smell</span> with your <span class= 'rw_highlight'>nose</span>. <span class= 'rw_highlight'>Smells</span> can be pleasant, like the scent of a rose or the <span class= 'rw_highlight'>aroma</span> of bread",
          lower_text_field: "baking in the oven. What <span class= 'rw_highlight'>smells</span> do you like?"
      },
      8 => {
          upper_text_field: "Captain Neptune discovers that some <span class= 'rw_highlight'>smells</span> can be unpleasant, like the smoke from a car’s exhaust or the odour of a rotten",
          lower_text_field: "piece of fruit."
      },
      9 => {upper_text_field: "Captain Neptune wants to know what <span class= 'rw_highlight'>smells</span> are bad for you? You can <span class= 'rw_highlight'>smell</span> a lot of things, but dogs can <span class= 'rw_highlight'>smell</span> even more.",
            lower_text_field: "Have you noticed how dogs like to sniff, sniff, sniff, around almost everything?"
      },
      10 => {
          upper_text_field: "Captain Neptune asks ‘Have you ever <span class= 'rw_highlight'>heard</span> of dogs who help find people who are lost in the woods or the snow?’ Dogs have good",
          lower_text_field: "<span class= 'rw_highlight'>hearing</span> and you might think they use their <span class= 'rw_highlight'>ears</span> to hear people calling for help, but dogs do this by using their sense of <span class= 'rw_highlight'>smell</span>."
      },
      11 => {
          upper_text_field: "Captain Neptune explains how your sense of <span class= 'rw_highlight'>smell</span> also helps you taste things. He wants to know if you have you noticed that",
          lower_text_field: "it’s hard to taste things when you have a bad cold?"
      },
      12 => {
          upper_text_field: "Captain Neptune wants to know why you think that happens? What’s your <span class= 'rw_highlight'>nose</span> like when you have a bad cold?",
      },
      13 => {
          upper_text_field: "Captain Neptune explains that you taste with your tongue. He wants you to look at a mirror and stick out your tongue. Can you",
          lower_text_field: "see how it’s covered with little bumps? Those little bumps have tiny particles that tell you what things taste like."
      },
      14 => {
          upper_text_field: "Captain Neptune explains that the bumps on your tongue tell you whether things are sweet, like ice cream; sour, like lemon; salty,",
          lower_text_field: "like crisps or bitter, like some medicine you might have to take when you’re ill."
      },
      15 => {
          upper_text_field: "Captain Neptune explains that you can touch things and feel things. He wants to know what you feel if you pick up an ice",
          lower_text_field: "cube, pet a cat or dog or rub a piece of sandpaper?"
      },
      16 => {
          upper_text_field: "Captain Neptune wants to know if you have ever stepped barefoot on the grass? How did it feel? He asks, ‘Have you ever",
          lower_text_field: "felt the skin on your arm, how does it feel?’"
      },
      17 => {
          upper_text_field: "Today’s Mission Assignment: With your talk partners taste the foods on the tray blindfolded and guess their flavours and <span class= 'rw_highlight'>smells</span>.",
      },
      18 => {
          upper_text_field: "Today’s Mission Assignment: Complete a sound walk and record what you can <span class= 'rw_highlight'>hear</span>.",
      },
      19 => {upper_text_field: "Captain Neptune would like you get ready for today’s mission assignment and report to your work stations.", sixty_count: ""},
      20 => {complete_investigation_slide: ""},
      21 => {rocket_word_slide: ""},
      22 => {quiz_slide: ""},
      23 => {outro_slide: ""}
  }

end
