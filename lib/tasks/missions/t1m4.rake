desc "Lesson"
task t1m4: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {ten_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {song_slide: ""},
      7 => {
          upper_text_field: "Captain Neptune is wondering if you have ever tried to sit very, very still and watch an animal? Maybe you went to a zoo and saw",
          lower_text_field: "a <span class= 'rw_highlight'>bluejay</span> at a bird feeder, or you watched a mother dog lick her puppies clean?"
      },
      8 => {
          upper_text_field: "Or maybe you sat very quietly and looked at a fly as it landed on your arm. You can learn a lot about animals by watching closely,",
          lower_text_field: "especially if you take care not to scare them."
      },
      9 => {
            upper_text_field: "Captain Neptune learnt that when <span class= 'rw_highlight'>Jane Goodall</span> was a little girl, she loved watching animals. She would crouch in her hen",
            lower_text_field: "house quietly for hours, hoping to see how a chicken laid eggs. She brought earthworms into her bed to see how they move."
      },
      10 => {
          upper_text_field: "Captain Neptune discovered that when <span class= 'rw_highlight'>Jane Goodall</span> read the story of Dr. Dolittle (an imaginary doctor who went to Africa and",
          lower_text_field: "learnt to talk with animals) she decided, ‘That’s what I want to do!’"
      },
      11 => {
          upper_text_field: "Captain Neptune learnt that when she grew up, <span class= 'rw_highlight'>Jane Goodall</span> went to Africa. She was especially interested in apes and",
          lower_text_field: "<span class= 'rw_highlight'>chimpanzees</span>. Since their bodies are so much like human bodies, she thought that we could learn a lot from <span class= 'rw_highlight'>chimpanzees</span>."
      },
      12 => {
          upper_text_field: "Captain Neptune learnt that <span class= 'rw_highlight'>Jane Goodall</span> went to the African country Tanzania. She lived in a part of the country where",
          lower_text_field: "hunters were not allowed and where a group of a hundred <span class= 'rw_highlight'>chimpanzees</span> lived. She was very patient."},
      13 => {
          upper_text_field: "Every morning at five-thirty, she would climb a hill and look through her binoculars, trying to see how the <span class= 'rw_highlight'>chimpanzees</span>",
          lower_text_field: "lived. For months the <span class= 'rw_highlight'>chimpanzees</span> were scared of her, but they slowly came to trust her."
      },
      14 => {
          upper_text_field: "Captain Neptune found out that after a year, the chimps let <span class= 'rw_highlight'>Jane Goodall</span> come close to them, but not close enough to touch.",
          lower_text_field: "After two years they knew her well enough that they would eat the bananas she put out for them near the house."
      },
      15 => {
          upper_text_field: "Captain Neptune discovered that the more the <span class= 'rw_highlight'>chimpanzees</span> trusted <span class= 'rw_highlight'>Jane Goodall</span>, the more she saw how they really lived.",
          lower_text_field: "She learned a lot about <span class= 'rw_highlight'>chimpanzees</span> that people didn’t know before."
      },
      16 => {
          upper_text_field: "Captain Neptune discovered she saw <span class= 'rw_highlight'>chimpanzees</span> holding hands, hugging and kissing. She saw them fighting. She heard the sounds",
          lower_text_field: "and saw the facial expressions they used to communicate with each other."
      },
      17 => {
          upper_text_field: "Captain Neptune was fascinated by <span class= 'rw_highlight'>Jane Goodall’s</span> most important discovery of all, she saw <span class= 'rw_highlight'>chimpanzees</span> making <span class= 'rw_highlight'>tools</span>.",
          lower_text_field: "For example, <span class= 'rw_highlight'>chimpanzees</span> knew how to make <span class= 'rw_highlight'>tools</span> to catch red <span class= 'rw_highlight'>termites</span>, which they loved to eat."
      },
      18 => {upper_text_field: "They broke off long, thin twigs or pieces of grass and poked them into holes where the <span class= 'rw_highlight'>termites</span> lived. The <span class= 'rw_highlight'>termites</span> climbed on,",
             lower_text_field: "then the <span class= 'rw_highlight'>chimpanzees</span> pulled the twigs or grass out of the holes and took off the <span class= 'rw_highlight'>termites</span>."
      },
      19 => {
          upper_text_field: "The chimps had invented something like a fishing pole for <span class= 'rw_highlight'>termites</span>! Before <span class= 'rw_highlight'>Jane Goodall</span> saw this, no one believed that",
          lower_text_field: "<span class= 'rw_highlight'>chimpanzees</span> made <span class= 'rw_highlight'>tools</span>. <span class= 'rw_highlight'>Jane Goodall</span> has now spent almost forty years living in Africa and watching <span class= 'rw_highlight'>chimpanzees</span>."  
      },
      20 => {upper_text_field: "Today’s Mission Assignment: Using plasticine, create a model of a <span class= 'rw_highlight'>chimpanzee</span> using a <span class= 'rw_highlight'>tool</span> to catch <span class= 'rw_highlight'>termites</span>."},
      21 => {upper_text_field: "Captain Neptune announces that you have 60 seconds to report to your work stations for today’s mission assignment!", sixty_count: ""},
      22 => {rocket_word_slide: ""},
      23 => {quiz_slide: ""},
      24 => {outro_slide: ""}
  }

end
