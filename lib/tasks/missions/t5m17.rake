desc "Lesson"
task t5m17: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Brigadier Venus explains as the Moon orbits Earth, it occasionally moves between Earth and the Sun. Then the Moon",
          lower_text_field: "blocks our view of the Sun and casts a shadow on Earth. When that happens, we call it a <span class='rw_highlight'>solar eclipse</span>."
      },
      7 => {
          upper_text_field: "Brigadier Venus explains that as a <span class='rw_highlight'>solar eclipse</span> begins, it looks as if a disc is creeping slowly across the face of the Sun. The",
          lower_text_field: "disc – which is the Moon – seems just as big as the Sun, but that’s because the Moon is so much closer to the Earth than the Sun."
      },
      8 => {
          upper_text_field: "As more and more of the Moon blocks the light of the Sun, day seems to turn to night, no matter what time it is. The sky darkens.",
          lower_text_field: "Stars become visible. A <span class='rw_highlight'>solar eclipse</span> lasts only a few minutes. The Moon moves out of its position between the Earth and the Sun."
      },
      9 => {
          upper_text_field: "Then the sky brightens again. Hundreds of years ago, people were terrified by <span class='rw_highlight'>solar eclipses</span>. They didn’t understand why the Sun",
          lower_text_field: "seemed to be getting darker in the middle of the day."
      },
      10 => {
          upper_text_field: "Brigadier Venus explains that the Moon does not make its own light. It just reflects the light of the Sun. If the Earth blocks",
          lower_text_field: "sunlight from reaching the Moon, Earth will cast a shadow on the Moon so it will become visible for just a moment."
      },
      11 => {
          upper_text_field: "Brigadier Venus explains that when that happens, it’s called a lunar eclipse. Even when you’re studying the Sun, never look",
          lower_text_field: "directly at it, either with your eyes alone or through binoculars or a telescope. You could damage your eyes or even blind yourself."
      },
      12 => {
          upper_text_field: "You can view a <span class='rw_highlight'>solar eclipse</span> by poking a little hole in an index card. Hold it about a metre above a white piece of paper. A little",
          lower_text_field: "image of the sun will be projected by the hole onto the paper. Planets travel around the Sun in fixed paths called orbits."
      },
      13 => {
          upper_text_field: "Brigadier Venus explains that as the planets orbit the Sun, they also <span class='rw_highlight'>rotate</span>. Earth orbits the Sun and <span class='rw_highlight'>rotates</span>. We say the Earth",
          lower_text_field: "<span class='rw_highlight'>rotates</span> around an <span class='rw_highlight'>axis</span>, which is an imaginary line running through the planet from the North Pole to the South Pole."
      },
      14 => {
          upper_text_field: "Brigadier Venus explains that if you were to spend a day keeping track of the position of the Sun in the sky, it might appear as",
          lower_text_field: "though the Sun were moving. Get up early one morning and observe where you see the <span class='rw_highlight'>sunrise</span>. Where is the Sun?"
      },
      15 => {
          upper_text_field: "A few hours later Earth has <span class='rw_highlight'>rotated</span> so that it looks as though the Sun has moved to a different place in the sky. But really, the Sun",
          lower_text_field: "isn’t moving. It only appears to move because of the Earth’s <span class='rw_highlight'>rotation</span> on its <span class='rw_highlight'>axis</span>."
      },
      16 => {
          upper_text_field: "When evening comes, observe the <span class='rw_highlight'>sunset</span>. It always sets in the west and rises in the east. We talk about the Sun ‘rising’ and",
          lower_text_field: "‘setting’ because that’s what the Sun appears to do. But remember: it only looks that way. Earth is moving, not the Sun."
      },
      17 => {
          upper_text_field: "Brigadier Venus explains that it takes a day for the Earth to make one complete spin around its <span class='rw_highlight'>axis</span>. When the place where you live",
          lower_text_field: "is turned towards the Sun, it is day for you, while it is night for people on the opposite side of the Earth."
      },
      18 => {
          upper_text_field: "When the place you live is turned towards the Sun, it is day for you and night for people on the opposite side of the Earth.",
          lower_text_field: "When the Earth tilts towards the Sun, it’s our hottest time of year as the sunlight shines more directly on us."
      },
      19 => {
          upper_text_field: "When people in Europe are enjoying sunny summer days, people in New Zealand are shivering because it is the middle of winter.",
          lower_text_field: "Brigadier Venus explains that it takes 365 days for the Earth to orbit the Sun.."
      },
      20 => {
          upper_text_field: "",
          main_video_slide: ""
      },
      21 => {
          upper_text_field: "",
          ten_count: ""
      },
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}



  $rocket_words = {
          "Solar Eclipse" => {
              description: "When the sun is obscured by the moon.",
              image: "" },
          "Rotation" => {
              description: "The act of rotating or turning as if on an axis.",
              image: "" },
          "Axis" => {
              description: "The line about which a rotating body, such as the earth, turns.",
              image: "" },
          "Sunset" => {
              description: "The time in the evening when the sun disappears or daylight fades.",
              image: "" },
          "Sunrise" => {
              description: "The rise of the sun above the horizon in the morning.",
              image: "" },
  }

  $questions = {
      "What is a <span class='rw_highlight'>solar eclipse</span>?" => {answer: "When the sun is obscured by the moon.",
                           image: "",
                           slide_id: 6},
      "What does <span class='rw_highlight'>rotation</span> mean?" => {answer: "The act of rotating or turning as if on an axis.",
                                image: "",
                                slide_id: 13},
      "What is an <span class='rw_highlight'>axis</span>?" => {answer: "The line about which a rotating body, such as the earth, turns.",
                           image: "",
                           slide_id: 13},
      "What is <span class='rw_highlight'>sunset</span>?" => {answer: "The time in the evening when the sun disappears or daylight fades.",
                              image: "",
                              slide_id: 16},
      "What is a <span class='rw_highlight'>sunrise</span>?" => {answer: "The rise of the sun above the horizon in the morning.",
                              image: "",
                              slide_id: 14},
  }

end
