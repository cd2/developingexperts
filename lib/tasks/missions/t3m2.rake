desc "Lesson"
task t3m2: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter learns that plants are made of <span class='rw_highlight'>cells</span>. A <span class='rw_highlight'>cell</span> is a building block. All plants and animals are built from <span class='rw_highlight'>cells</span>. He",
          lower_text_field: "discovers that when special male and female <span class='rw_highlight'>cells</span> (gametes) join together, new plants are made, this is called <span class='rw_highlight'>reproduction</span>."
      },
      7 => {
          upper_text_field: "Colonel Jupiter explains that mosses <span class='rw_highlight'>reproduce</span> by making <span class='rw_highlight'>spores</span>. Moss starts to grow when a moss <span class='rw_highlight'>spore</span> lands on a moist,",
          lower_text_field: "nutrient-rich spot."
      },
      8 => {
          upper_text_field: "But where do these <span class='rw_highlight'>spores</span> come from? Well, Colonel Jupiter says you need to listen very carefully as something",
          lower_text_field: "AMAZING happens in the life of a moss plant. The <span class='rw_highlight'>spore</span> grows into a soft green moss. Then little stalks begin to grow."
      },
      9 => {
          upper_text_field: "Buds start to form at the end of the stalk. The bud contains gametes or sex <span class='rw_highlight'>cells</span>. Colonel Jupiter discovers that",
          lower_text_field: "if the buds make <span class='rw_highlight'>gametes</span> that are eggs, the moss plant is female."
      },
      10 => {
          upper_text_field: "If the bud make gametes that can swim, the moss plant is male. These gametes are called <span class='rw_highlight'>sperm</span>. When a <span class='rw_highlight'>sperm</span> and egg are",
          lower_text_field: "close together, and there is some water present, the <span class='rw_highlight'>sperm</span> swims to an egg and <span class='rw_highlight'>fertilises</span> it."
      },
      11 => {
          upper_text_field: "Colonel Jupiter discovers that this <Span>fertilised</span> egg grows into a stalk on top of the female moss plant. A capsule full of <span class='rw_highlight'>spores</span>",
          lower_text_field: "forms at the tip of each stalk and when it is fully grown something AMAZING happens."
      },
      12 => {
          upper_text_field: "The capsule BURSTS open and releases <span class='rw_highlight'>spores</span>. They germinate and the process starts all over again.",
      },
      13 => {
          upper_text_field: "Today’s Mission Assignment: Watch today’s film then try the exercise which models what happens when moss <span class='rw_highlight'>reproduces</span>.",
          main_video_slide: ""
      },
      14 => {
          upper_text_field: "Colonel Jupiter activates the 60 seconds countdown clock. Please report to your work stations.",
          sixty_second_count: ""
      },
      15 => {complete_investigation_slide: ""},
      16 => {rocket_word_slide: ""},
      17 => {quiz_slide: ""},
      18 => {outro_slide: ""}
}

end
