desc "Lesson"
task t3m3: :environment do

  $slides = {
      1 => {intro_slide: ""},
      2 => {sixty_count_test: ""},
      3 => {previous_rocket_word_slide: ""},
      4 => {ten_count_launch: ""},
      5 => {rocket_word_slide: ""},
      6 => {
          upper_text_field: "Colonel Jupiter explains that animals such as whales, dolphins and human beings have to eat food to keep their energy levels up.",
          lower_text_field: "Plants, on the other hand, are able to make food from carbon dioxide and water, using energy from sunlight."
      },
      7 => {
          upper_text_field: "Colonel Jupiter discovers that all plants contain <span class='rw_highlight'>chlorophyll</span>, which makes some or all of their cells appear green.",
          lower_text_field: "The word <span class='rw_highlight'>chlorophyll</span> comes from two Greek words – chlor, meaning ‘green’ and phyllo, meaning ‘leaf’."
      },
      8 => {
          upper_text_field: "Colonel Jupiter learns that plants use sunlight, carbon dioxide and <span class='rw_highlight'>chlorophyll</span> to make sugar. This sugar gives the plant energy",
          lower_text_field: "to grow. The process they use to make sugar (food) is called <span class='rw_highlight'>photosynthesis</span>."
      },
      9 => {
          upper_text_field: "Colonel Jupiter discovers that during the night plants breathe in oxygen, this is called <span class='rw_highlight'>respiration</span>. The plant releases any water",
          lower_text_field: "it doesn’t need, into the air. This is called <span class='rw_highlight'>transpiration</span>. Let’s start by finding out how water and carbon dioxide get into a celery leaf."
      },
      10 => {
          upper_text_field: "<b>Step One</b>: <span class='rw_highlight'>Transporting</span> water and nutrients. Cars, buses and underground trains are all means of <span class='rw_highlight'>transport</span>. We use them to",
          lower_text_field: "move from one place to another. Many plants <span class='rw_highlight'>transport</span> things inside them too, in order to make and store food."
      },
      11 => {
          upper_text_field: "Look at the celery plant which takes in water and nutrients from the soil through tiny hairs on its roots. The water and nutrients",
          lower_text_field: "are <span class='rw_highlight'>transported</span> through the root hairs to the roots and up the stem (or ‘stalk’) of the celery, through tubes arranged in bundles."
      },
      12 => {
          upper_text_field: "Colonel Jupiter reminds us that there are two types of tubes in plants: xylem and phloem. Xylem tubes carry water and nutrients",
          lower_text_field: "from the soil, up the stem to the leaves where most <span class='rw_highlight'>photosynthesis</span> occurs."
      },
      13 => {
          upper_text_field: "<b>Step Two</b>: Getting light energy from the sun. Look at the cross section of the celery leaf. Colonel Jupiter wants to know if you",
          lower_text_field: "can see the chloroplast cells just below the top surface of the leaf? These cells contain chloroplasts, where <span class='rw_highlight'>chlorophyll</span> is found."
      },
      14 => {
          upper_text_field: "Colonel Jupiter explains that sunlight shines through the top of the leaf, and the light energy is trapped by the <span class='rw_highlight'>chlorophyll</span> and",
          lower_text_field: "stored in special molecules for later use. Remarkably, leaf cells can change themselves into root cells."
      },
      15 => {
          upper_text_field: "<b>Step Three</b>: Taking carbon dioxide from the air. Colonel Jupiter explains how food is made. Air passes in through tiny holes on the",
          lower_text_field: "bottom surface of the leaf called stomata (a single hole is called a stoma; many holes are called stomata)."
      },
      16 => {
          upper_text_field: "Colonel Jupiter learns that carbon dioxide from the air reaches the cells where <span class='rw_highlight'>chlorophyll</span> has trapped energy from sunlight. This",
          lower_text_field: "energy causes a chemical reaction that combines water and carbon dioxide to make oxygen and sugars."
      },
      17 => {
          upper_text_field: "<b>Step Four</b>: <span class='rw_highlight'>Transportation</span>. Colonel Jupiter explains that the sugars created in the leaves are <span class='rw_highlight'>transported</span> down the celery",
          lower_text_field: "plant in phloem tubes, to be stored as sugar or very large molecules called starch in other parts of the plant."
      },
      18 => {
          upper_text_field: "The plant’s cells use this food later to grow and do work. The sugars and starches stored in a plant are necessary for the plant",
          lower_text_field: "to survive, but they can also make an apple taste good to humans and animals. Can you see where the phloem cells are?"
      },
      19 => {
          upper_text_field: "The sweetness in an apple comes from the sugars which the apple tree has stored in its fruit – sugars created by",
          lower_text_field: "<span class='rw_highlight'>photosynthesis</span>. Many plants such as potatoes and maize change the sugar they make into starch, before it is stored."
      },
      20 => {
          upper_text_field: "Today’s mission assignment: Create your own celery <span class='rw_highlight'>transporters</span>!",
          main_video_slide: ""
      },
      21 => {
          upper_text_field: "Colonel Jupiter activates the 60 second countdown clock. Please report to your work stations!",
          sixty_second_count: ""
      },
      22 => {complete_investigation_slide: ""},
      23 => {rocket_word_slide: ""},
      24 => {quiz_slide: ""},
      25 => {outro_slide: ""}
}

  $rocket_words = {
          "Photosynthesis" => {
              description: "How <b>plants convert light energy</b>, from the Sun, into chemical energy (fuel).",
              image: "" },
          "Transporting" => {
              description: "<b>Moving something</b> from one location to another.",
              image: "" },
          "Transpiration" => {
              description: "Is when a plant releases any water it does not need through its leaves.",
              image: "" },
          "Chlorophyll" => {
              description: "<b>Green</b> pigments found in photosynthetic organisms.",
              image: "" },
          "Respiration" => {
              description: "Is when plants breathe in oxygen during the night.",
              image: "" },
  }



end
