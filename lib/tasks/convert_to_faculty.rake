task convert_to_faculty: :environment do

  User.where(type: 'User::Teacher').update_all(type: 'User::Faculty')
  User.where(type: 'User::Administrator').update_all(type: 'User::Faculty', administrator: true)

end
