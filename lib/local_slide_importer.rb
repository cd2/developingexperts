module LocalSlideImporter

  def self.create_slide
    $slides.each do |k, v|
      $slide = $presentation.slides.new

      #first slide
      if v[:intro_slide]
        $slide.slide_type = Slide::INTRO

      #introduction to the course (slide 2 of year first unit)
      elsif v[:course_intro_slide]
        $slide.slide_type = Slide::COURSE_INTRO

      #ten second countdown to start the lesson
      elsif v[:ten_count_launch]
        $slide.slide_type = Slide::TEN_COUNT_LAUNCH

      #include a 60 second countdown video in the slide
      elsif v[:sixty_count]
        $slide.slide_type = Slide::SIXTY_COUNT

      #include the rocket words on the slide
      elsif v[:rocket_word_slide]
        $slide.slide_type = Slide::ROCKET_WORDS

      #assessing last weeks rws
      elsif v[:sixty_count_test ]
        $slide.slide_type = Slide::SIXTY_COUNT_TEST

      #giving the answers of last week
      elsif v[:previous_rocket_word_slide]
        $slide.slide_type = Slide::PREVIOUS_ROCKET_WORD

      #slide with the integrated quiz
      elsif v[:quiz_slide]
        $slide.slide_type = Slide::QUIZ

      #slide with the song
      elsif v[:song_slide]
        $slide.slide_type = Slide::SONG

      #slide includes a custom video
      elsif v[:main_video_slide]
        $slide.slide_type = Slide::VIDEO

      #slide instructing on the writeup
      elsif v[:complete_investigation_slide]
        $slide.slide_type = Slide::INVESTIGATION

      #ending slide
      elsif v[:outro_slide]
        $slide.slide_type = Slide::OUTRO
      end

      $slide.upper_text = v[:upper_text_field].gsub("<span class='rw'>", "<u>").gsub("</span>", "</u>") if v[:upper_text_field]
      $slide.lower_text = v[:lower_text_field].gsub("<span class='rw'>", "<u>").gsub("</span>", "</u>") if v[:lower_text_field]
      $slide.slide_text = v[:slide_text] if v[:slide_text]

      $slide.save!

      $slide.update_attributes(background_image: File.open(File.join(Rails.root,"/seed_information/#{$mission.machine_name}/#{k}.jpg")))


    end
    create_words if $rocket_words
    create_quiz if $questions
  end

  def self.create_words

    $rocket_words.each do |k, v|
      @word = $mission.rocket_words.create!(
                              word: k,
                              description: v[:description]
      )
      @word.update_attributes(image: File.open(File.join(Rails.root,"/seed_information/rocket_words/#{k.downcase}.jpg")))
    end
    $rocket_words = nil
  end

  def self.create_quiz
    $questions.each do |k, v|
      q = $mission.formative_questions.create!(question: k)
      @a = q.create_answer(answer: v[:answer])
    end
    $questions = nil
  end

  $slides = nil

end