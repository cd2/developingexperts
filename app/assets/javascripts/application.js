// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require moment
//= require highcharts
//= require jquery-unslider.min
//= require_tree .

$(function() {

    (function(){
        var $cont = $('.pupil_mission_thumbnail a');
        if (($el = $cont.find('h4')).length>0) {
            if ($el.outerHeight(true)+$el.position().top > $cont.parent().height()) {
                $el.css({top: 0})
            }
            if ($el.outerHeight(true)+$el.position().top > $cont.parent().height()) {
                $el.css({margin: '5px'})
            }
            if ($el.outerHeight(true)+$el.position().top > $cont.parent().height()) {
                $el.css({'font-size': '1.1em'})
            }
        }
    })();

    $('[name=filter_specified_class]').on('change', function(){
        val = $(this).val();
        $('.specified_class_column').each(function(){
            var $row = $(this).closest('tr');
            if ($(this).text() != val && val!='') {
                $row.hide();
            } else {
                $row.show();
            }
        });
    }).trigger('change');

    $('[data-auto-submit]').on('change', function(){
        $(this).closest('form').trigger('submit')
    });


    $('[data-submit-answer]').on('click', function(){
        $(this).find('input[type=submit]').on('click', function(e){
            e.stopPropagation();
        }).click()
    });


    $( "#course_selector_active, #course_selector_units" ).sortable({
        connectWith: ".connectedSortable",
        update: function(){
            var order = $('#course_selector_active').sortable("toArray");
            $('input#course_arrangement').val(order)
            //$.post($(this).data('update-url'), $(this).sortable('serialize'));
        }
    }).disableSelection();

    $('[data-date-picker]').each(function(){
        var date = new Date(this.value)
        $(this)
            .datepicker({ dateFormat: 'dd-mm-yy'})
            .datepicker( "setDate", date );
    });


    $('#course_selection_container').sortable().disableSelection();

    $(".div_link").click(function() {
        window.document.location = $(this).data("href");
    });

    $(".menu_activator").on('click', function(){
        $('body').toggleClass('menu_activation')
    })


    $('#slide_admin_sidebar').sortable({
        axis: 'y',
        handle: 'i',
        update: function(){
            $.post($(this).data('update-url'), $(this).sortable('serialize'));
        }
    });

    update_slide()

});

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    //>=, not <=
    if (scroll >= 1200) {
        //clearHeader, not clearheader - caps H
        $("html").addClass("scrolled");
    } else {
        $("html").removeClass("scrolled");
    }
}); //missing );

function entrance_pupil_mission(name) {

    history.pushState({x:1}, "", '#' + name)
    window.onpopstate = function(){
        $('.modal_exit').trigger('click')
    }

    setTimeout(function(){
        $("#lesson_show_panel_container").removeClass('entrance')
    }, 250);
    $('.modal_exit').on('click', function(){
        $("#lesson_show_panel_container").addClass('entrance')
        setTimeout(function(){
            $('#lesson_show_panel').remove()
        }, 250);

    });
}
$(function(){
    if (window.hash != '') {
        $('[data-mid=' + window.hash + ']').trigger('click');
    }

    $('form[data-autosave]').find('input, select').on('change', function(){
        var $form = $(this).closest('form');
        var data = $form.serialize();
        var action = $form.attr('action');
        var method = $form.attr('method');
        $.ajax({
            url: action,
            method: method,
            data: data,
            success: function(data) {
                console.log('SAVED')
                $('html').addClass('saved')
                setTimeout(
                    function(){  $('html').removeClass('saved') }, 1000
                )
            },
            error: function(data){
                console.error('THERE WAS AN ERROR')
            }
        });
    });

});

function update_slide() {
    if ($('#lower_text').length > 0) {
        console.log("HI")
        $('#upper_text').columnize({
            columns: 1,
            height: 76,
            buildOnce: false,
            overflow: {
                height: 94,
                id: "#lower_text",
                buildOnce: false,
                doneFunc: function(){
                    console.log("done with page");
                }
            }
        });
    }
    $("[data-highres]").each(function(){
        var _this = this;
        if ($(_this).is("img")) {
            var callback = function () {
                $(_this).attr("src", this.src);

            }
        } else {
            var callback = function(){
                $(_this).css("background-image", 'url(' + this.src + ')');
            }

        }
        $("<img src='" + $(this).data('highres')  + "'>").load(callback);
        console.log($(this).data('highres'))
    });

    if ($('.slide_thumb.current_slide').length > 0) {
        var offset_top = $('.slide_thumb.current_slide').offset().top - 300
        $('#navigation_thumbs').scrollTop(offset_top)
    }
    if ($('.active.quiz_progress_item').length > 0) {
        console.log("HI")
        var offset_top = $('.active.quiz_progress_item').offset().top - 300
        $('#second_sidebar').scrollTop(offset_top)
    }
}


$(window).on('load page:load', function(){

    $('a[data-safe-delete]').on('click', function(e){
        $(this).prev().show()
        $(this).hide()
        e.preventDefault();
    });
    $('a[data-safe-delete-cancel]').on('click', function(e){
        $(this).parent().hide().next().show()
        e.preventDefault();
    });


    $('input[type=checkbox][data-select-all]').on('change', function(){
        console.log(this.value)
        if ($(this).is(':checked')) {
            $(this).closest('table').find('input[type=checkbox]:visible').prop('checked', true)
        } else {
            $(this).closest('table').find('input[type=checkbox]').prop('checked', false)
        }
    });
    $('input[type=checkbox][data-select-all]')
        .closest('table').find('input[type=checkbox]').not('[data-select-all]')
        .on('change', function(){
            if (!$(this).is(':checked')) {
                $(this).closest('table').find('[data-select-all]').prop('checked', false)
            }
        });


});
