(function($, window){

  $(window).on('new_notification', attachListeners);
  $(attachListeners)
  function attachListeners() {
    $('.flash').on('click', '.flash_close', removeFlash)
  }


  function removeFlash() {
    $flash = $(this).closest('.flash');
    $flash.addClass('flash_hidden');
    setTimeout(function(){
      $flash.remove();
      if ($('.flash').length == 0) $('#flash_container').remove()
        $('html').removeClass('notification_present')
    }, 200);
  }

  $(function(){
    $('.flash.flash_hidden').each(function(i){
      var _this = this;
      setTimeout(function(){
          $(_this).removeClass('flash_hidden')
          $('html').addClass('notification_present')
      }, (i + 6)*250);
    });
  });

})(jQuery, window)