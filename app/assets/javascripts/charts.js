(function($){

  google.charts.load('current', {
    packages: ['corechart', 'table', 'line', 'bar'],
    callback: drawDashboard
  });


  function drawDashboard() {

    if (($cont = $("#klass_show_graph")).length > 0){

      var options = {
        animation:{
          "startup": true,
          duration: 1000,
          easing: 'out',
        },
        chart: {
          title: 'Class Performance',
        },
        hAxis: {
          textPosition: 'none',             
          title: "Pupil Names",
        },
        vAxis: {
          viewWindow:{
            max:100,
          }
        },
        bars: 'vertical'
      };

      createAndDrawChart($cont[0], 'Bar', options)

    }

    if (($cont = $("#pupil_welcome_graph")).length > 0){

      var options = {
        animation:{
          "startup": true,
          duration: 1000,
          easing: 'out',
        },
        chart: {
          title: 'My Performance',
        },
        vAxis: {
          viewWindow:{
            max:100,
          }
        },
        bars: 'vertical'
      };

      createAndDrawChart($cont[0], 'Line', options)

    }

    if (($cont = $("#klass_index_graph")).length > 0){

      var options = {
        animation:{
          "startup": true,
          duration: 1000,
          easing: 'out',
        },
        chart: {
          title: 'Class Performance',
        },
        vAxis: { 
          viewWindow:{
            max:100,
          }
        },
        bars: 'vertical'
      };

      createAndDrawChart($cont[0], 'Bar', options)

    }

    if (($cont = $("#pupil_show_graph")).length > 0){
      var options = {
        animation:{
          "startup": true,
          duration: 1000,
          easing: 'out',
        },
        chart: {
          title: 'Pupil Performance',
        },
        vAxis: { 
          viewWindow:{
            max:100,
          }
        },
        bars: 'vertical'
      };

      createAndDrawChart($cont[0], 'Line', options)
    }


  }

  function createAndDrawChart(elem, type, options) {
    var url = $(elem).data('chart-url');
    var chart = new google.charts[type](elem)

    google.visualization.events.addListener(chart, 'error', function(googleError) {
      google.visualization.errors.removeError(googleError.id);
      console.error(googleError)
      $(elem).remove()
    })


    // options = google.charts[type].convertOptions(options)

    $.ajax({
      url: url,
      complete: function(response) {
        console.log(response.responseText)
        data = new google.visualization.DataTable(response.responseText);       
        
        drawChart(chart, data, options)
        $(window).on('resize', function(){
          drawChart(chart, data, options)
        });

      }
    });
  }

  function drawChart(chart, data, options) {
    chart.draw(data, options);
  }



})(jQuery);
