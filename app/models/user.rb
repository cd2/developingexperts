
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:school_id, :email]

  has_many :messages, class_name: 'Message', foreign_key: 'recipient_id'

  after_create :generate_hash
  
  extend FriendlyId
  friendly_id :hash_id

  attr_accessor :type_selector

  def administrator?
    User::Faculty.administrators.include? self
  end

  def system_admin?
    self.is_a? User::SystemAdministrator
  end

  def teacher?
    User::Faculty.teachers.include? self
  end

  def pupil?
    self.is_a? User::Pupil
  end

  def parent?
    self.is_a? User::Parent
  end

  def self.find_for_database_authentication(warden_conditions)
    if warden_conditions[:school_id] == '-1'
      User::SystemAdmin.find_by(email: warden_conditions[:email])
    else
      super
    end
  end

  private

    def generate_hash
      hashids = Hashids.new("developingexperts", 12)
      self.hash_id = hashids.encode(self.id)
      self.save
    end

end
