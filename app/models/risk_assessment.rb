class RiskAssessment < ApplicationRecord

  has_many :risk_assessment_apparatus
  has_many :risk_assessment_safeties

end
