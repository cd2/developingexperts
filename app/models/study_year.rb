class StudyYear < ApplicationRecord

  validates :name, :year_number, :sidebar_background, :lesson_outro_video, :lesson_intro_mascot, :rocket_thumbnail, :sixty_count_video, :ten_count_video, :mascot_name, :certificate, presence: true

  default_scope -> { order year_number: :asc }

  has_many :units
  has_many :lesson_templates, through: :units

  mount_uploader :sidebar_background, ImageUploader
  mount_uploader :lesson_intro_mascot, ImageUploader
  mount_uploader :rocket_thumbnail, ImageUploader
  mount_uploader :certificate, ImageUploader

  mount_uploader :lesson_outro_video, VideoUploader
  mount_uploader :sixty_count_video, VideoUploader
  mount_uploader :ten_count_video, VideoUploader

end
