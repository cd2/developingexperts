class LessonResult < ApplicationRecord

  belongs_to :pupil, class_name: 'User::Pupil'
  belongs_to :lesson
  has_one :attempt, class_name: 'Quiz::Attempt'

  POSSIBLE_MARKS = ['Working Below', 'Working Towards', 'Expected Standard', 'Greater Depth']




  def attempt
    super || create_attempt!
  end

  def score
    if attempt.started?
      (attempt.score / attempt.total_questions.to_f * 100).to_i
    else
      nil
    end
  end

  def normalized_in_class_mark
    if in_class_mark.present?
      (1+in_class_mark)*25
    else
      nil
    end
  end

  def assessed_score_percentage
    if assessed_score?
      (assessed_score.to_f / 5 * 100).to_i
    else
      nil
    end
  end

  def total
    scores = [score, normalized_in_class_mark].compact
    if scores.length>0
      scores.inject(:+).to_f / scores.size
    else
      0
    end
  end

end
