class LessonTemplate < ApplicationRecord

  scope :ordered, -> {order machine_name: :asc}

  belongs_to :unit
  delegate :study_year, to: :unit, allow_nil: true

  #lesson integration
  has_one :presentation
  has_one :lesson_plan
  has_one :risk_assessment
  has_many :slides, through: :presentation
  has_many :rocket_words
  has_many :resources

  has_many :course_lessons
  has_many :courses, through: :course_lessons

  has_one :quiz, class_name: 'Quiz::Quiz', foreign_key: 'lesson_template_id'
  has_many :questions, class_name: 'Quiz::Question', through: :quiz
  has_many :formative_questions, -> { formative }, class_name: 'Quiz::Question', through: :quiz
  has_many :summative_questions, -> { summative }, class_name: 'Quiz::Question', through: :quiz

  scope :active, -> { where(published: true) }

  mount_uploader :cover_image, ImageUploader
  mount_uploader :song, VideoUploader

  after_create :create_presentation
  alias_attribute :lesson_number, :machine_name



  def display_name_array
    body[1..-2].split(']//[') rescue nil
  end

  def display_name
    arr = Array(display_name_array).join(', ') rescue nil
    arr.first.upcase + arr[1..-1] rescue nil
  end

  def rocket_words_present?
    rocket_words.count == 5
  end

  def questions_present?
    formative_questions.count == 5
  end

  def image_thumbnails_present?
    rocket_words.where("image <> ''").count == 5
  end

  def quiz_route
    [self, formative_questions.first]

  end

  def unit_lesson_number
    number = unit.lesson_templates
    number.map(&:id).index(id) + 1
  end

  def narration_slides_count

  end

  def rocket_word_count
    @rw_count ||= rocket_words.count
  end

  def current_position_in_mission_order
    LessonTemplate.ordered.pluck(:id).index(self.id)
  end

  def previous
    LessonTemplate.find(LessonTemplate.ordered.pluck(:id)[current_position_in_mission_order - 1])
  end

  private

    def create_presentation
      self.create_presentation!
    end
    
end
