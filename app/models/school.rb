class School < ApplicationRecord

  #validations
  validates :name, :subdomain, presence: true

  #relationships
  has_many :courses
  has_many :unassigned_courses, -> { includes(:klass).where(klasses:{id: nil}) }, class_name: 'Course'

  has_many :teachers, -> {teachers}, class_name: 'User::Faculty', dependent: :destroy
  has_many :administrators, -> {administrators}, class_name: 'User::Faculty', dependent: :destroy
  has_many :faculty_members, class_name: 'User::Faculty', dependent: :destroy
  has_many :pupils, class_name: 'User::Pupil', dependent: :destroy

  has_many :notifications

  has_many :klasses

  #extensions
  mount_uploader :logo, ImageUploader
  mount_uploader :sidebar_image, ImageUploader

  def self.matching_subdomain subdomain
    find_by(subdomain: subdomain)
  end

  def display_logo
    logo.blank? ? 'default_logo.jpg' : logo.formatted
  end

  def notify message
    notifications.create(message: message)
  end

  def unassigned_pupils
    pupils.includes(:enrollments).where(enrollments: {pupil_id: nil})
  end

  def specified_classes
    pupils.pluck(:specified_class).uniq
  end

end
