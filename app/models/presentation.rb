class Presentation < ApplicationRecord

  belongs_to :lesson_template
  has_one :unit, through: :lesson_template
  has_one :study_year, through: :unit

  has_many :slides
  has_many :student_slides, -> { where(slide_type: valid_child_slides) }, class_name: 'Slide'
  has_many :story_slides, -> { where(slide_type: Slide::STORY) }, class_name: 'Slide'
  has_many :video_slides, -> { where(slide_type: Slide::VIDEO) }, class_name: 'Slide'

  def sidebar_background
    study_year.sidebar_background
  end

  def mascot_name
    study_year.mascot_name
  end

  def current_position_in_mission_order
    LessonTemplate.ordered.pluck(:id).index(lesson_template.id)
  end

  def previous_presentation
    LessonTemplate.find(LessonTemplate.ordered.pluck(:id)[current_position_in_mission_order - 1]).presentation
  end

  def slide_images_present?
    story_slides.where("background_image <> ''").count == story_slides.count
  end

  def slide_videos_present?
    video_slides.where("video <> ''").count == video_slides.count
  end

  def from_slide_number num
    slides.first(num).last
  end

end

