class Unit < ApplicationRecord

  default_scope -> { order id: :asc }

  belongs_to :study_year
  has_many :lesson_templates, -> {order machine_name: :asc}

  def unit_year
    "#{study_year&.name} - #{name}"
  end
  
end
