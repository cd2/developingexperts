class RocketWord < ApplicationRecord

  default_scope -> { order id: :asc }

  validates :word, presence: true
  mount_uploader :image, ImageUploader

end
