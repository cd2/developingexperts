class Quiz::Question < ApplicationRecord

  belongs_to :quiz, class_name: 'Quiz::Quiz'

  belongs_to :slide
  belongs_to :rocket_word

  mount_uploader :answer_image, ImageUploader

  enum question_type: [:formative, :summative]

  def get_weighted_value_for user
    #TODO do method
    1
  end

end
