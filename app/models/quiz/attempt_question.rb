class Quiz::AttemptQuestion < ActiveRecord::Base

  belongs_to :attempt

  belongs_to :question_object, class_name: 'Quiz::Question', foreign_key: 'question_id'
  belongs_to :answer_object, class_name: 'Quiz::Question', foreign_key: 'answer_id'
    
  delegate :answer_image, to: :question_object
  delegate :slide, to: :question_object

  default_scope -> { order(question_number: :asc)}


  def self.completed
    where.not(answer_id: nil)
  end

  def self.current
    where(answer_id: nil).first
  end

  def correct?
    self.question_id == self.answer_id
  end

  def incorrect?
    !correct?
  end

  def answered?
    self.answer_id.present?
  end


  def question
    question_object&.question
  end

  def answer
    question_object&.answer
  end


  def given_answer
    answer_object&.answer
  end

  
end
