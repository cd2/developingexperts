class Quiz::Attempt < ApplicationRecord
  #pulls the 5 questions from the quiz and 5 questions from previous quizzes


  has_many :questions, class_name: 'Quiz::AttemptQuestion'
  belongs_to :lesson_result
  
  has_one :lesson, through: :lesson_result
  has_one :quiz, through: :lesson

  has_one :pupil, through: :lesson_result, class_name: 'User::Pupil'

  def score
    questions.collect(&:correct?).count(true)
  end

  def total_questions
    questions.count
  end

  def duration
    end_time - start_time
  end

  def start!
    reset if started? and !completed?
    generate_questions
    self.start_time = Time.now
    save!
  end

  def started?
    start_time.present?
  end

  def reset
    update(start_time: nil, end_time: nil, completed: false)
    questions.destroy_all
  end

  def finish!
    update(end_time: Time.now, completed: true)
  end

  def next_question
    questions.current
  end

  def generate_questions
    #pick the 5 questions belonging to this quiz
    question_ids = quiz.question_ids
    
    #pick 5 random questions from all previous quizzes
    previous_questions = lesson.all_previous.flat_map(&:questions)

    weighted_question_ids = {}
    previous_questions.each do |question|
      weighted_question_ids[question.id] = question.get_weighted_value_for pupil
    end

    #normalize weights
    total_weight = weighted_question_ids.values.sum.to_f
    normalized_weights = weighted_question_ids.map do |id, weight|
      [id, (weight/total_weight)]
    end.to_h

    
    ids = normalized_weights.map { |id, w| [id, -rand**(1.0/w)] }.sort_by(&:last).map(&:first).first(5)


    question_ids.concat ids
    
    #randomize all the ids
    question_ids.shuffle!

    #build the questions
    attempt_question_attributes = question_ids.each_with_index.map do |id, i|
      {question_id: id, question_number: i+1}
    end
    questions.build(attempt_question_attributes)
  end


  def fun_start!
    update(start_time: nil, end_time: nil, completed: false)
    self.start_time = Time.now
    save!
  end


  def generate_answers_for question
    #get the current question answers id
    answers = [question]

    #select 4 other answers from the selected questions
    answers += questions.where.not(id: question.id).sample(4)

    #return 5 random questions where one is the current quiestion
    answers.shuffle
  end

  def fun_questions_for lesson
    question_ids = lesson.quiz.question_ids
    attempt_question_attributes = question_ids.each_with_index.map do |id, i|
      {question_id: id, question_number: i+1}
    end
    questions.build(attempt_question_attributes)
  end

end
