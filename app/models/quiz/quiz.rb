class Quiz::Quiz < ActiveRecord::Base

  has_many :questions

  has_many :formative_questions, -> { formative }, class_name: 'Question'
  has_many :summative_questions, -> { summative }, class_name: 'Question'
  
  has_many :attempts

  belongs_to :lesson_template

  def formative_questions_count
    @fq_count ||= formative_questions.count
  end

  def summative_questions_count
    @sq_count ||= summative_questions.count
  end

end
