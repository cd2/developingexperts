class Klass < ApplicationRecord

  validates :name, :school, presence: true
  
  belongs_to :school
  belongs_to :teacher, class_name: 'User::Faculty', foreign_key: :faculty_id
  belongs_to :course

  has_many :lessons, through: :course

  has_many :enrollments
  has_many :pupils, through: :enrollments, class_name: 'User::Pupil'

  has_many :class_results

  def available_courses
    courses = school.unassigned_courses
    if course
      courses << course
    end
    courses
  end

  def mastery_percentage
    rand(80..100)
  end

  def average_quiz_mark
    marks = lessons.flat_map(&:average_quiz_mark)
    marks.reject!(&:nil?)
    if marks.length>0
      marks.sum/marks.length.to_f
    else
      0
    end
  end

  def average_in_class_mark
    marks = lessons.flat_map &:average_in_class_mark
    marks.reject!(&:nil?)
    if marks.length > 0
      marks.sum/marks.length.to_f
    else
      0
    end
  end


  def average_assessed_mark
    marks = lessons.flat_map(&:average_assessed_mark)
    marks.reject!(&:nil?)
    if marks.length>0
      marks.sum/marks.length.to_f
    else
      0
    end
  end
  def average_total_mark
    marks = [average_quiz_mark, average_in_class_mark].compact
    if marks.length>0
      marks.sum/marks.length.to_f
    else
      0
    end
  end
end
