class PupilAvatar < ApplicationRecord

  belongs_to :pupil, class_name: 'User::Pupil'
  belongs_to :avatar

end
