class Course < ApplicationRecord

  validates :name, presence: true

  #relationships
  belongs_to :school
  
  has_one :klass
  has_one :teacher, through: :klass
  has_many :pupils, through: :klass, class_name: 'User::Pupil'

  has_many :lessons, -> { selected }, autosave: true
  accepts_nested_attributes_for :lessons


  before_create do
    #set lesson numbers and and start dates
    inc = 0
    lessons.each do |lesson|
      unless lesson.disabled? || lesson.lesson_template.nil?
        inc += 1
        lesson.lesson_number = inc
        lesson.start_date = self.start_date + ((lesson.lesson_number-1)*7).days
      end
    end
  end

  def update_lessson_start_dates _start_date=nil, lesson=nil
    _start_date ||= self.start_date
    if lesson
      _lessons = [lesson] + lesson.all_next
    else
      _lessons = self.lessons
    end
    inc = 0
    _lessons.each do |lesson|
      unless lesson.disabled?
        lesson.update(start_date: _start_date + (inc*7).days)
        inc += 1
      end
    end
  end

  def lessons_attributes=(val)
    super val
    inc = 0
    lessons.each {|l| l.lesson_number=(inc+=1) unless l.disabled?}
  end

  def build_lessons_for_unit unit
    new_lesson_ids = unit.lesson_template_ids - lessons.collect(&:lesson_template_id)
    new_lesson_ids = new_lesson_ids.map { |id| {lesson_template_id: id}}
    lessons.build(new_lesson_ids)
    lessons.select { |lesson| unit.lesson_template_ids.include? lesson.lesson_template_id }
  end

  def build_lessons_for_year year
    year.units.each do |unit|
      build_lessons_for_unit unit
    end
    self
  end

  def units
    lessons.collect(&:unit).uniq
  end

  def get_lessons_for_date date
    lessons.where(start_date: date)
  end

  def start_date
    lessons.first.start_date || Date.today
  end

  def current_lesson
    lessons.active
  end

  def current_lesson_number
    lessons.available.count
  end

end
