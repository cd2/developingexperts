class LessonPlan < ApplicationRecord

  belongs_to :lesson_template
  delegate :unit, to: :lesson_template, allow_nil: true
  delegate :study_year, to: :unit, allow_nil: true

end
