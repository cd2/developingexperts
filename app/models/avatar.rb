class Avatar < ApplicationRecord

  mount_uploader :image, ImageUploader

  has_many :pupil_avatars, dependent: :destroy

  attr_accessor :pupil
  attr_accessor :new_avatar

  def self.all_for pupil
    avatars = all
    avatars.each do |a|
      a.pupil = pupil
      a.check_for_completion
    end
    avatars
  end

  def current_score
    case category
    when 'complete_tasks'
      @pupil.lesson_results.count
    when 'perfect_tasks'
      @pupil.lesson_results.select{|r| (r.score || 0) >= 100 }.count
    when 'lessons'
      @pupil.lessons.available.count
    when 'presentations'
      0
    else
      0
    end
  end

  def total_score
    self[:total_score] || case category
    when 'complete_tasks'
      @pupil.lessons.count
    when 'perfect_tasks'
      @pupil.lessons.count
    when 'lessons'
      @pupil.lessons.count
    when 'presentations'
      @pupil.lessons.count
    else
      0
    end
  end

  def progress
    current_score / total_score.to_f
  end

  def progress_text
    text = self[:progress_text] || ''
    text.gsub! '{{current_score}}', current_score.to_s
    text.gsub '{{total_score}}', total_score.to_s
  end

  def completed?
    progress >= 1
  end

  def check_for_completion
    if (PupilAvatar.find_by(avatar_id: self.id, pupil_id: @pupil.id).blank?) && completed?
      @pupil.avatars << self
      self.new_avatar = true
    end
  end

  def to_partial_path
    if @pupil.has? self
      'avatars/achieved'
    else
      'avatars/unachieved'
    end
  end

end
