module Faculty
  extend ActiveSupport::Concern

  included do

    has_many :klasses
    has_many :courses, through: :klasses
    has_many :lessons, through: :courses
    has_many :pupils, through: :klasses
    belongs_to :school

    def this_weeks_lesson
      lessons.where('lessons.start_date >= ? AND lessons.start_date < ? ', Date.today.at_beginning_of_week, Date.today.at_end_of_week).first
    end

    def next_lesson
      lessons.where('lessons.start_date > ?', Date.yesterday).first
    end

    enum gender: [:male, :female, :other]
    enum working_days: ['0.2', '0.4', '0.6', '0.8', '1']


  end

  class_methods do

  end

end