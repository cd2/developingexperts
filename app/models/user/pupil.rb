class User::Pupil < User
  # devise :database_authenticatable, :authentication_keys => [:pupil_identifier]

  attr_accessor :klass

  default_scope -> { order name: :asc}

  belongs_to :school
  has_paper_trail

  has_many :enrollments

  has_many :klasses, through: :enrollments
  has_many :courses, through: :klasses
  has_many :lessons, ->{ selected }, through: :courses

  has_many :lesson_results

  has_many :pupil_avatars
  has_many :avatars, through: :pupil_avatars
  belongs_to :avatar, foreign_key: 'pupil_avatar_id'

  validates :name, presence: true
  validates :school_id, presence: true
  validates :pupil_identifier, presence: true, uniqueness: { scope: :school_id }

  before_validation :assign_pupil_identifier

  enum gender: [:male, :female, :other]

  def password_required?
    false
  end

  def email_required?
    false
  end


  def avatar_image
    avatar&.image || 'locked_avatar'
  end

  def enroll_in klass
    enrollments.create(klass_id: klass.id) unless in?(klass)
  end

  def leave klass
    enrollments.find_by(klass_id: klass.id)&.destroy
  end

  def in? klass
    klasses.include? klass
  end

  def has? avatar
    avatars.find_by(id: avatar.id)
  end


  def performance_graph
    lesson_results.map do |lesson_result|
      [lesson_result.lesson&.lesson_number, lesson_result.score, lesson_result.normalized_in_class_mark, lesson_result.total]
    end
  end

  def average_in_class_mark
    marks = lesson_results.collect &:normalized_in_class_mark
    marks.select! &:present?
    if marks.length > 0
      marks.sum / marks.length
    end
  end

  def average_quiz_mark
    marks = lesson_results.collect &:score
    marks.select! &:present?
    if marks.length > 0
      marks.sum / marks.length
    end
  end
  def average_total
    marks = lesson_results.collect &:total
    marks.select! &:present?
    if marks.length > 0
      marks.sum / marks.length
    end
  end
  

  def lesson_mark lesson
    if lesson = lesson_results.find_by(lesson_id: lesson.id)
      lesson.in_class_mark
    end
  end

  def lesson_comment lesson
    if lesson = lesson_results.find_by(lesson_id: lesson.id)
      lesson.teacher_comments
    end
  end

  def absent lesson
    if lesson = lesson_results.find_by(lesson_id: lesson.id)
      lesson.absent
    end
  end


  def self.import_csv school, file
    require 'csv'
    ActiveRecord::Base.transaction do
      CSV.foreach(file.path, headers: true) do |row|
        school.pupils.create!(
            name: row['Name'],
            email: (row['Email'] || ''),
            age: row['Age'],
            dob: row['Date of Birth'],
            study_year_id: row['Year'],
            gender: row['Gender'],
            specified_class: row['Class'],
            pupil_identifier: row['Pupil ID'],
        )
      end
    end
  end


  belongs_to :q_fun_quiz_attempt, class_name: 'Quiz::Attempt', foreign_key: :fun_quiz_attempt_id
  def fun_quiz_attempt lesson
    unless q_fun_quiz_attempt && fun_quiz_lesson_id == lesson.id
      a = build_q_fun_quiz_attempt 
      a.fun_questions_for lesson
      update(fun_quiz_lesson_id: lesson.id)
      a.save!
    end
    q_fun_quiz_attempt
  end




  private

    def assign_pupil_identifier
      loop do
        num = rand(1..999999)
        @identifier = num.to_s.rjust(6, '0')
        break unless school.pupils.find_by(pupil_identifier: @identifier)
      end
      self.pupil_identifier = @identifier if self.pupil_identifier.blank?
    end

    def assign_pupil_email
      self.email = "#{pupil_identifier}-pupil@#{school.subdomain}.school" if self.email.blank?
    end

end
