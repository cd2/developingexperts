class User::Faculty < User

  has_many :klasses
  has_many :courses, through: :klasses
  has_many :lessons, through: :courses
  has_many :pupils, through: :klasses
  belongs_to :school

  enum gender: [:male, :female, :other]
  enum working_days: ['0.2', '0.4', '0.6', '0.8', '1']

  scope :teachers, -> { where(administrator: false) }
  scope :administrators, -> { where(administrator: true) }

  validates :name, presence: true
  validates :school_id, presence: true

  def this_weeks_lesson
    lessons.where('lessons.start_date >= ? AND lessons.start_date < ? ', Date.today.at_beginning_of_week, Date.today.at_end_of_week).first
  end

  def next_lesson
    lessons.where('lessons.start_date > ?', Date.yesterday).first
  end


end
