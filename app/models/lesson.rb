class Lesson < ApplicationRecord
  
  default_scope {order lesson_number: :asc}  

  scope :selected, -> { where(disabled: [false, nil]) }

  scope :available, -> { selected.where('lessons.start_date <= ?', Date.today) }

  belongs_to :lesson_template
  belongs_to :course

  has_one :presentation, through: :lesson_template

  has_one :quiz, through: :lesson_template
  has_many :questions, through: :quiz, class_name: 'Quiz::Question'

  has_one :unit, through: :lesson_template

  has_many :pupils, through: :course, class_name: 'User::Pupil'
  has_many :quiz_attempts, foreign_key: 'lesson_id'

  has_many :lesson_results
  accepts_nested_attributes_for :lesson_results

  delegate :machine_name, to: :lesson_template
  delegate :cover_image, to: :lesson_template
  delegate :display_name, to: :lesson_template
  delegate :rocket_words, to: :lesson_template
  delegate :presentation, to: :lesson_template
  delegate :resources, to: :lesson_template
  delegate :study_year, to: :lesson_template
  delegate :lesson_plan, to: :lesson_template
  delegate :quiz, to: :lesson_template


  def name
    lesson_template.display_name
  end

  def self.active
    where('lessons.start_date <= ?', Date.today).last
  end

  def available?
    self.start_date <= Date.today
  end

  def active?
    self == course.lessons.available.last
  end

  def previous
    if (self.lesson_number > 1)
      self.class.where(lesson_number: self.lesson_number-1, course_id: self.course_id).first
    end
  end

  def next
    self.class.where(lesson_number: self.lesson_number+1, course_id: self.course_id).first
  end

  def all_previous
    self.class.where(lesson_number: 1..self.lesson_number-1, course_id: self.course_id)
  end

  def all_next
    self.class.where(lesson_number: self.lesson_number+1..Float::INFINITY, course_id: self.course_id)
  end

  def average_quiz_mark
    lesson_results.collect(&:score) 
  end

  def average_in_class_mark
    lesson_results.collect(&:normalized_in_class_mark) 
  end

  def average_assessed_mark
    lesson_results.collect(&:assessed_score_percentage) 
  end

  def average_total_mark
    lesson_results.collect(&:total) 
  end


  def get_result_for pupil
    if (lesson_result = lesson_results.find_by(pupil_id: pupil.id))
      return lesson_result
    else
      lesson_results.create!(pupil_id: pupil.id) if pupils.include?(pupil)
    end
  end

  def has_result_for? pupil
    lesson_results.find_by(pupil_id: pupil.id)
  end

  def build_all_lesson_results
    generate_all_lesson_results
    pupils.each do |pupil|
      lesson_results.build(pupil: pupil) unless has_result_for? pupil
    end
    lesson_results.includes(:pupil).order("users.name asc")
  end

  def generate_all_lesson_results
    pupils.each { |p| get_result_for(p) }
  end

  def new_start_date
    self.start_date
  end

  def new_start_date= val
    val = DateTime.parse(val) if val.is_a? String
    c = course
    c.update_lessson_start_dates val, self
    c.save!
  end

end
