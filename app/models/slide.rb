class Slide < ApplicationRecord

  default_scope {order slide_number: :asc}
  
  belongs_to :presentation

  mount_uploader :background_image, ImageUploader
  mount_uploader :on_slide_image, ImageUploader

  mount_uploader :video, VideoUploader
  mount_uploader :narration_video, VideoUploader

  enum slide_type: [:story,
                    :intro,
                    :outro,
                    :quiz,
                    :course_intro,
                    :ten_count_launch,
                    :sixty_count,
                    :rocket_words,
                    :previous_rocket_word,
                    :sixty_count_test,
                    :song,
                    :video,
                    :investigation]

  def self.valid_child_slides
    [0, 1, 2, 3, 4, 7, 8, 10, 11, 12]
  end

  def slide_type_name
    slide_type.classify
  end

  before_create do
    #set the slide number so each presentation starts at slide 1
    self.slide_number = presentation.slides.count + 1
  end


end
