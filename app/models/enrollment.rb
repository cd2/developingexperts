class Enrollment < ApplicationRecord

  belongs_to :klass
  belongs_to :pupil, class_name: 'User::Pupil'

end
