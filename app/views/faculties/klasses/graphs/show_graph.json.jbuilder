
json.cols [
  {'id': '', 'label': 'MARK', type: 'string'},
  {'id': '', 'label': 'Average Quiz Mark', type: 'number'},
  {'id': '', 'label': 'Average Assessed Mark', type: 'number'},
  {'id': '', 'label': 'Final Score', type: 'number'}
], :label, :type, :id

json.rows @klass.pupils do |pupil|
  json.c [:name, :average_quiz_mark, :average_in_class_mark, :average_total] do |v|
    json.v pupil.send(v)
  end
end

