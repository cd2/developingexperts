
json.cols [
  {'id': 'y', 'label': 'Lesson Number', type: 'string'},
  {'id': 'y', 'label': 'Test Score', type: 'number'},
  {'id': 's', 'label': 'Class Score', type: 'number'},
  {'id': 'e', 'label': 'Final Score', type: 'number'}
]

json.rows @pupil.performance_graph do |row|
  json.c row do |v|
    json.v v
  end
end

