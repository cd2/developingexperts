
module TabsHelper

  def tab_controller control_name, checked=nil
    checked ||= true if !content_for?("tab_controllers_#{@tab_group_name.last}")
    content = radio_button_tag("tab_#{@tab_group_name.last}", control_name, (checked || params[:tab]==control_name.to_s), class: 'tab_controller')
    content_for "tab_controllers_#{@tab_group_name.last}", content
  end


  def tab_head control_name, text=nil
    text ||= control_name.to_s.humanize
    content_for "tab_heads_#{@tab_group_name.last}", label_tag("tab_#{@tab_group_name.last}_#{control_name}", text, class: control_name)
  end

  def tab control_name, *args, &block
    opts = args.extract_options!

    tab_name = opts.delete(:tab_head) { args[0] }
    checked = opts.delete(:checked) { args[1] }
    condition = opts.delete(:if) { args[2] }

    return if condition.present? && condition.call==false

    if checked.nil? && !!tab_name==tab_name
      checked, tab_name = tab_name, nil
    end

    tab_controller control_name, checked
    tab_head control_name, tab_name

    opts[:class] ||= []
    opts[:class] = [opts[:class]].flatten(1)
    opts[:class].push('tab_panel')

    content = capture(&block) if block
    content_for "tab_panels_#{@tab_group_name.last}", content_tag(:div, content, opts)
  end

  def tabs group_name=nil, &block
    @tab_group_name ||= []
    @tab_group_name.push group_name || rand
    content = capture &block if block

    tab_controllers = @view_flow.content.delete "tab_controllers_#{@tab_group_name.last}"
    tab_heads = @view_flow.content.delete "tab_heads_#{@tab_group_name.last}"
    tab_panels = @view_flow.content.delete "tab_panels_#{@tab_group_name.last}"

    @tab_group_name.pop

    return unless tab_controllers

    content_tag :div, class: 'tabs_container' do
      tab_controllers +
      content_tag(:div, tab_heads, class: 'tab_heads_container') +
      content_tag(:div, tab_panels, class: 'tab_panels_container')
    end
  end

end
