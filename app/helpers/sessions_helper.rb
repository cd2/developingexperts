module SessionsHelper

  def signed_in_as_faculty?
    signed_in_as_administrator? || signed_in_as_teacher?
  end

  def signed_in_as_administrator?
    signed_in? && current_user.administrator?
  end

  def signed_in_as_parent?
    signed_in? && current_user.parent?
  end

  def signed_in_as_teacher?
    signed_in? && current_user.teacher?
  end

  def signed_in_as_pupil?
    signed_in? && current_user.pupil?
  end


end

