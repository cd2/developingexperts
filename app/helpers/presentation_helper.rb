module PresentationHelper

  def slide_group
    signed_in? && current_user.pupil? ? @presentation.student_slides : @presentation.slides
  end
  #tracks the current slide as a sessions
  def current_slide_number
    current_slide.slide_number
  end

  def current_slide_index_number
    @slides.find_index(current_slide)+1
  end

  #calculated the current slide in the DB
  def current_slide
    @slide || @presentation.slides.first
  end

  #returns the previous slide number
  def previous_slide
    if current_slide_index_number == 1
      false
    else
      current_slide_index_number - 1
    end
  end

  #returns the next slide number
  def next_slide
    if current_slide_index_number == @presentation.slides.count
      false
    else
      current_slide_index_number + 1
    end
  end

  #places the sidebar background
  def sidebar
    img = (@current_school && @current_school.sidebar_image.url.present?) ? @current_school.sidebar_image.url : @lesson.presentation&.sidebar_background&.url
    #img = @lesson.presentation&.sidebar_background&.url
    content_tag :div, '', data: {highres: img }
  end

  def upper_text
    text = case current_slide.slide_type.to_sym
      when :intro
        "Mission #{@lesson.lesson_number}<br>Unit: #{@presentation.unit.name}".html_safe
      when :outro
        "See you on my next mission!<br>#{@lesson.study_year.mascot_name} signing out!".html_safe
      when :quiz
        if signed_in? && current_user.pupil?
          "Time for the quiz"
        else
          "Mission Quiz Master: Tell your talk partners the answers.<br>Mission Timer: 2 minutes".html_safe
        end
      when :ten_count_launch
        "You have 10 seconds to move to the launch pad mat for today’s mission briefing.".html_safe
      when :rocket_words
        "Today’s Rocket Words:<br>Your 5 words and meanings to learn".html_safe
      when :previous_rocket_word
        "Previous Rocket Words:".html_safe
      when :sixty_count_test
        "Today’s Rocket Words and Meaning Test".html_safe
      when :song
        "Today’s Rocket Song.".html_safe
      when :investigation
        "Writing up your experiment:<br>Complete the investigation sheet.".html_safe
      else
        current_slide.upper_text
    end
    unless text.blank?
      content_tag :div, id: 'upper_text', class: "#{'with_narration' if session[:toggle]}" do
        content_tag :p, class: 'upper_text_inner' do
          raw text
        end
      end
    end
  end

  def main_slide
    if current_slide.background_image&.formatted&.url.present?
      content_tag :div, id: 'main_slide_panel', class: 'trans', data: {highres: current_slide.background_image.url}, style: "background-image: url(#{current_slide.background_image&.formatted})" do
        (narration_video + main_slide_sliding).html_safe
      end
    else
      content_tag :div, id: 'main_slide_panel', class: 'trans' do
        (narration_video + main_slide_sliding).html_safe
      end
    end
  end

  def main_slide_sliding
    case current_slide.slide_type.to_sym
      when :intro
        render 'presentations/presentation_intro'
      when :outro
        content_tag(:div, video_tag(@presentation.study_year.lesson_outro_video, autoplay: true, controls: true), class: 'count_video')
      when :quiz
        signed_in? && current_user.pupil? ? render('presentations/home_quiz') : render('presentations/class_quiz')
      when :course_intro
        render 'presentations/course_intro'
      when :ten_count_launch
        content_tag(:div, video_tag(@presentation.study_year.ten_count_video, autoplay: true, controls: true), class: 'count_video')
      when :sixty_count
        content_tag(:div, video_tag(@presentation.study_year.sixty_count_video, autoplay: true, controls: true), class: 'count_video')
      when :rocket_words
        render 'presentations/rocket_words', lesson: @lesson
      when :previous_rocket_word
        if @course
          begin
            render 'presentations/rocket_words', lesson: @course.lessons.find_by(lesson_template_id: @lesson.id).previous
          rescue
            render 'presentations/rocket_words', lesson: @lesson.previous
          end
        else
          render 'presentations/rocket_words', lesson: @lesson.previous
        end
      when :sixty_count_test
        content_tag(:div, video_tag(@presentation.study_year.sixty_count_video, autoplay: true, controls: true), class: 'count_video')
      when :song
        content_tag(:div, video_tag(@presentation.lesson_template.song, autoplay: true, controls: true), class: 'slide_video')
      when :video
        content_tag(:div, video_tag(current_slide.video, autoplay: true, controls: true), class: 'slide_video')
      else
        ''
    end
  end

  def lower_text
    content_tag :div, id: 'lower_text' do
    end
  end

  def narration_video
    if session[:toggle] && !current_slide.narration_video.blank?
      content_tag(:div, id: 'narration_video_wrapper') do
        content_tag :div, id: 'narration_video' do
          video_tag(current_slide.narration_video, autoplay:true)
        end
      end
    else
      ''
    end
  end

  def thumbnails

  end

  def slide_thumb_link(slide, link_num, current_slide=nil)
    img = case slide.slide_type.to_sym
      when :outro
        asset_url 'nav_thumbs/outro_thumb.jpg'
      when :quiz
        asset_url 'nav_thumbs/quiz_thumb.jpg'
      when :course_intro
        asset_url 'nav_thumbs/certificate_thumb.jpg'
      when :ten_count_launch
        asset_url 'nav_thumbs/ten_count_thumb.jpg'
      when :sixty_count
        asset_url 'nav_thumbs/sixty_count_thumb.jpg'
      when :rocket_words
        asset_url 'nav_thumbs/rocket_words_thumb.jpg'
      when :previous_rocket_word
        asset_url 'nav_thumbs/rocket_words_thumb.jpg'
      when :sixty_count_test
        asset_url 'nav_thumbs/sixty_count_thumb.jpg'
      when :song
        asset_url 'nav_thumbs/song_thumb.jpg'
      when :video
        asset_url 'nav_thumbs/video_thumb.jpg'
      else
        slide.background_image.square.url
          end
    if @course
      link_to course_presentation_slide_path(@course, @presentation, link_num), class: ['slide_thumb', ('current_slide' if current_slide==slide)] do
        image_tag("thumb_placeholder.gif", data: {highres: img}) +
            content_tag(:span, "#{link_num}")
      end
    else
      link_to presentation_slide_path(@presentation, link_num), class: ['slide_thumb', ('current_slide' if current_slide==slide)] do
        image_tag("thumb_placeholder.gif", data: {highres: img}) +
        content_tag(:span, "#{link_num}")
      end
    end

  end

end
