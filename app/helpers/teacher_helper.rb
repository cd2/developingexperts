module TeacherHelper


  def classes_display
    "This will show either the class if they have 1 class or the list of classes if they have more than one"
  end

  def teacher_lesson_summary_panel
    content_tag :div, class: 'panel', id: 'summary_panel' do
      if (mission = @teacher.next_mission)
        lesson_preview_hero(mission) +
        lesson_quick_actions(mission)
      else
        content_tag :div, class: 'panel_inner' do
          content_tag :p, 'You currently have no classes assigned.', class: 'no_assigned_classes'
        end
      end
    end
  end

  def lesson_preview_hero(mission)
    content_tag :div, class: 'lesson_panel_hero', style: "background-image: url(#{mission.cover_image})" do
      content_tag :h3, mission.mission
    end
  end

  def lesson_quick_actions mission
    content_tag :div, class: 'lesson_panel_actions' do
      link_to 'Begin Mission', mission.presentation_route, class: 'teacher_start_lesson_link'
    end
  end

end
