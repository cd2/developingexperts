module ApplicationHelper
  ActionView::Base.default_form_builder = FormBuilder

  def current_subdomain
    request.subdomain
  end

  def subdomain?
    current_subdomain.present?
  end

  def user_bar_user_info
    content_tag :p, current_user.name
  end

  def flash_messages
    content_tag :div, id: :flash_messages do
      flash.each.collect { |k, v| content_tag(:div, v, class: k) }.join.html_safe
    end
  end

  def errors obj, f
    if obj.errors.any?
      render 'common/errors', object: f.object
    end
  end

  def link_to_protected_delete text, href, msg='Are you sure?', opts={}
    opts[:data] ||= {}
    opts[:data].merge!(safe_delete: true)
    content_tag :div, class: 'safe_delete_container' do
      """
      #{content_tag :div, class: 'safe_delete_options', style: 'display: none' do
              link_to('Confirm', href, data: {method: :delete, confirm: msg}, class: 'safe_delete_confirm') +
              link_to('Cancel', '#', data: {safe_delete_cancel: true}, class: 'safe_delete_cancel')
            end}
      #{link_to(text, '#', opts)}
      """.html_safe
    end
  end

end
