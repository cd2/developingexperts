module AdminHelper

  def sidebar_section name=nil, &block
    content = capture(&block)
    if content.present?
      content_tag :div, class: 'sidebar_section' do
        if name
          content_tag(:h4, name) +
              content
        else
          content
        end
      end
    end
  end

  def ensure_navigation
    @navigation ||= [ { :title => 'Home', :url => '/' } ]
  end

  def navigation_add(title, url)
    ensure_navigation << { :title => title, :url => url }
  end

  def render_navigation(partial = nil)
    render :partial => partial || 'bread_crumbs', :locals => { :nav => ensure_navigation }
  end

end
