module PupilHelper


  def pupil_mission_thumbnail mission
    content_tag :div, class: 'pupil_mission_thumbnail_container', data: {mid: mission.machine_name} do
      content_tag :div, class: 'pupil_mission_thumbnail' do
        link_to(image_tag(mission.cover_image.pupil_thumbnail), pupil_mission_path(mission), remote: true)
      end
    end
  end

  def greeting
    now = Time.now
    today = Date.today.to_time

    morning = today.beginning_of_day
    noon = today.noon
    evening = today.change( hour: 17 )
    night = today.change( hour: 20 )
    tomorrow = today.tomorrow

    message = case now
    when morning..noon
      'Good Morning'
    when noon..evening
      'Good Afternoon'
    when evening..night
      'Good Evening'
    when night..tomorrow
      'Good Night'
    end
  end

end
