class Admin::RocketWordsController < AdminController
  before_action :set_lesson
  before_action :set_word, only: [:edit, :update, :destroy]

  def index
    @rocket_words = @lesson.rocket_words
  end

  def new
    @word = @lesson.rocket_words.build
  end

  def create
    @word = @lesson.rocket_words.new(rocket_word_params)
    if @word.save
      redirect_to admin_lesson_template_rocket_words_path(@lesson)
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @word.update(rocket_word_params)
      redirect_to admin_lesson_template_rocket_words_path(@lesson)
    else
      render :edit
    end
  end

  def destroy
    @word.destroy
    redirect_to admin_lesson_template_rocket_words_path(@lesson)
  end

  private

  def set_lesson
    @lesson = LessonTemplate.find(params[:lesson_template_id])
  end

  def set_word
    @word = RocketWord.find(params[:id])
  end

  def rocket_word_params
    params.require(:rocket_word).permit(:word, :image, :description)
  end
  
end