class Admin::SlidesController < AdminController
  before_action :set_lesson
  before_action :set_slides
  before_action :set_presentation

  def index
  end

  def new
    @slide = @lesson.slides.build
  end

  def create
    @slide = @lesson.presentation.slides.new(slide_params)
    if @slide.save
      redirect_to edit_admin_lesson_template_slide_path(@lesson, @slide)
    else
      render :edit
    end
  end

  def edit
    @slide = Slide.find(params[:id])
  end

  def update
    @slide = Slide.find(params[:id])
    if @slide.update(slide_params)
      redirect_to edit_admin_lesson_template_slide_path(@lesson, @slide)
    else
      render :edit
    end
  end

  def sort
    params[:slide].each_with_index do |id, index|
      Slide.where(id:id).update_all({slide_number: index+1})
    end
    head :created, location: admin_lesson_template_slides_path(LessonTemplate.find(params[:lesson_template_id]))
  end

  def destroy
    @slide = Slide.find(params[:id])
    @slide.destroy
    redirect_to admin_lesson_template_slides_path(@lesson)
  end

  private

  def set_slides
    @slides = @lesson.slides
  end

  def set_lesson
    @lesson = LessonTemplate.find(params[:lesson_template_id])
  end

  def rocket_word_params
    params.require(:slide).permit(:word, :image, :description)
  end

  def slide_params
    params.require(:slide).permit(:new_video_id, :video_quality_override, :presentation_id, :slide_number, :slide_type, :upper_text, :lower_text, :on_slide_text, :background_image, :on_slide_image, :video, :narration_video, :class_only)
  end

  def set_presentation
    @presentation = @lesson.presentation || @lesson.create_presentation
  end
  
end