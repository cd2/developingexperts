class Admin::SchoolsController < AdminController
  before_action :set_school, only: [:edit, :update, :show]

  def index
    @schools = School.all
  end

  def new
    @school = School.new
  end

  def create
    @school = School.new(school_params)
    if @school.save
      redirect_to admin_school_path(@school)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @school.update(school_params)

      redirect_to admin_school_path(@school)
    else
      render :edit
    end
  end

  def show
    @users = @school.administrators
  end

  private

    def school_params
      params.require(:school).permit(:name, :subdomain, :sidebar_image)
    end

    def set_school
      @school = School.find(params[:id])
    end

end
