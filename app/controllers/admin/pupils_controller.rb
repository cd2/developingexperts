class Admin::PupilsController < AdminController
  before_action :set_cupil, only: [:edit, :update]

  def new
    @pupil = User::Pupil.administrators.new
  end

  def edit
  end

  def create
    @pupil = User::Pupil.administrators.new(user_params)
    if @pupil.save
      redirect_to [:admin, :pupils]
    else
      render :new
    end
  end

  def update
    if @pupil.update(user_params)
      redirect_to [:edit, :admin, @pupil]
    else
      render :edit
    end
  end

  def index
    @pupils = User::Pupil.all
  end

  private

    def set_pupil
      @pupil = User::Pupil.find(params[:id])
    end

    def user_params
      params.require(:user_faculty).permit(:name, :email, :password, :password_confirmation, :school_id)
    end

end
