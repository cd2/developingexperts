class Admin::LessonTemplatesController < AdminController
  before_action :set_lesson, only: [:edit, :update, :formative_questions, :summative_questions]

  def index
    @lessons_setup = LessonTemplate.includes(:quiz, :presentation, :lesson_plan, :risk_assessment).all.ordered
    @lessons = @lessons_setup.paginate(:page => params[:page])
  end

  def new
    @lesson = LessonTemplate.new
  end

  def create
    @lesson = LessonTemplate.new(lesson_params)
    if @lesson.save
      redirect_to admin_lesson_templates_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @lesson.update(lesson_params)
      redirect_to admin_lesson_templates_path
    else
      render :new
    end
  end

  private

    def set_lesson
      @lesson = LessonTemplate.find(params[:id])
    end

    def lesson_params
      params.require(:lesson_template).permit(:lesson, :machine_name, :unit_id, :song, :cover_image, :published, :body)
    end

end