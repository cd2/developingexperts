class Admin::ResourcesController < AdminController
  before_action :set_resource, only: [:show, :edit, :update, :destroy]
  before_action :set_lesson

  def index
    @resources = @lesson.resources
  end

  def show
  end

  def new
    @resource = @lesson.resources.build
  end

  def edit
  end

  def create
    @resource = @lesson.resources.build(resource_params)
    if @resource.save
      redirect_to admin_lesson_template_resources_path(@lesson)
    else
      render :new
    end
  end

  def update
    if @resource.update(resource_params)
      redirect_to admin_lesson_template_resources_path(@lesson)
    else
      render :edit
    end
  end

  def destroy
    @resource.destroy
    redirect_to admin_lesson_template_resources_path(@lesson)
  end

  private

    def set_lesson
      @lesson = LessonTemplate.find(params[:lesson_template_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:resource).permit(:file, :name, :lesson_template_id)
    end
end
