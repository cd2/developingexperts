class Admin::CoursesController < AdminController
  before_action :set_course, only: [:show, :edit, :update]

  def new
    @course = Course.administrators.new
  end

  def show
  end

  def edit
  end

  def create
    @course = Course.administrators.new(user_params)
    if @course.save
      redirect_to [:admin, :courses]
    else
      render :new
    end
  end

  def update
    if @course.update(user_params)
      redirect_to [:edit, :admin, @course]
    else
      render :edit
    end
  end

  def index
    @courses = Course.all
  end

  private

    def set_course
      @course = Course.find(params[:id])
    end

    def user_params
      params.require(:user_faculty).permit(:name, :email, :password, :password_confirmation, :school_id)
    end

end
