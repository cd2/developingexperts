class Admin::UserFacultiesController < AdminController
  before_action :set_user, only: [:edit, :update]

  def new
    @teacher = User::Faculty.new
  end

  def edit
  end

  def create
    @teacher = User::Faculty.new(user_params)
    if @teacher.save
      redirect_to [:admin, :user_faculties]
    else
      render :new
    end
  end

  def update
    if @teacher.update(user_params)
      redirect_to [:edit, :admin, @teacher]
    else
      render :edit
    end
  end

  def index
    @teachers = User::Faculty.all
  end

  private

    def set_user
      @teacher = User::Faculty.find(params[:id])
    end

    def user_params
      params.require(:user_faculty).permit(:name, :email, :password, :password_confirmation, :school_id, :administrator)
    end

end
