class Admin::QuizQuestionsController < AdminController
  before_action :set_lesson
  before_action :set_question, only: [:edit, :update, :destroy]

  def new
    @question = @lesson.questions.build
  end

  def index
  end

  def create
    @quiz = @lesson.quiz
    unless @quiz
      @quiz = @lesson.create_quiz
    end
    @question = @lesson.quiz.questions.new(question_params)
    if @question.save
      if @question.formative?
        redirect_to formative_questions_admin_lesson_template_path(@lesson)
      else
        redirect_to summative_questions_admin_lesson_template_path(@lesson)
      end
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @question.update(question_params)
      if @question.formative?
        redirect_to formative_questions_admin_lesson_template_path(@lesson)
      else
        redirect_to summative_questions_admin_lesson_template_path(@lesson)
      end
    else
      render :edit
    end
  end

  def destroy
    @question.destroy
    if @question.formative?
      redirect_to formative_questions_admin_lesson_template_path(@lesson)
    else
      redirect_to summative_questions_admin_lesson_template_path(@lesson)
      end
  end

  private

  def set_lesson
    @lesson = LessonTemplate.find(params[:lesson_template_id])
  end

  def set_question
    @question = Quiz::Question.find(params[:id])
  end

  def question_params
    params.require(:quiz_question).permit(:question, :slide_id, :question_type, :answer, :rocket_word_id)
  end

end
