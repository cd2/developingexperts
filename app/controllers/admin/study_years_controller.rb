class Admin::StudyYearsController < AdminController

  def index
    @study_years = StudyYear.all
  end

  def new
    @study_year = StudyYear.new
  end

  def create
    @study_year = StudyYear.new(study_year_params)
    if @study_year.save
      redirect_to admin_study_years_path
    else
      render :edit
    end
  end

  def edit
    @study_year = StudyYear.find(params[:id])
  end

  def update
    @study_year = StudyYear.find(params[:id])
    if @study_year.update(study_year_params)
      redirect_to admin_study_years_path
    else
      render :edit
    end
  end

  private

  def study_year_params
    params.require(:study_year).permit(:name, :year_number, :sidebar_background, :lesson_outro_video, :lesson_intro_mascot, :rocket_thumbnail, :sixty_count_video, :ten_count_video, :mascot_name, :certificate)
  end

end

