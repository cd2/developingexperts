class Admin::KlassesController < AdminController
  before_action :set_klass, only: [:edit, :update]

  def new
    @klass = Klass.administrators.new
  end

  def edit
  end

  def create
    @klass = Klass.administrators.new(user_params)
    if @klass.save
      redirect_to [:admin, :klasss]
    else
      render :new
    end
  end

  def update
    if @klass.update(user_params)
      redirect_to [:edit, :admin, @klass]
    else
      render :edit
    end
  end

  def index
    @klasses = Klass.all
  end

  private

    def set_klass
      @klass = Klass.find(params[:id])
    end

    def user_params
      params.require(:user_faculty).permit(:name, :email, :password, :password_confirmation, :school_id)
    end

end
