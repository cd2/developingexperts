class Admin::RiskAssessmentsController < AdminController
  before_action :set_risk_assessment, only: [:show, :edit, :update]
  before_action :set_lesson

  def edit
  end

  def update
    if @risk_assessment.update(risk_assessment_params)
      edit_admin_lesson_template_risk_assessment_url(@lesson, @risk_assessment)
    else
      render :edit
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_risk_assessment
      @risk_assessment = RiskAssessment.find(params[:id])
    end

    def set_lesson
      @lesson = LessonTemplate.find(params[:lesson_template_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def risk_assessment_params
      params.require(:risk_assessment).permit(:lesson_template_id, :practical_number, :practical_outline, :day, :date, :period, :room, :teacher, :year_group, :lesson_number, :class_practical,
      :fume_cupboard,
      :working_in_pairs,
      :demo,
      :safety_screen,
      :individual_assessment)
    end
end
