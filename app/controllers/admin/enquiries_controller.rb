class Admin::EnquiriesController < AdminController

  def index
    @enquiries = Enquiry.all
  end

  def destroy
    Enquiry.find(params[:id]).destroy
    redirect_to admin_enquiries_path
  end

end
