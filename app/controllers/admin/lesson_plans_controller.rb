class Admin::LessonPlansController < AdminController
  before_action :set_lesson
  before_action :set_lesson_plan, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @lesson_plan = LessonPlan.new
  end

  def create
    @lesson_plan = @lesson.build_lesson_plan(lesson_plan_params)
    if @lesson_plan.save
      redirect_to admin_lesson_template_lesson_plan_path(@lesson, @lesson_plan)
    else
      render :new
    end
  end

  def edit
  end

  def update

    if @lesson_plan.update(lesson_plan_params)

      redirect_to admin_lesson_template_lesson_plan_path(@lesson, @lesson_plan)
    else
      render :edit
    end
  end

  def destroy
  end

  private

    def set_lesson
      @lesson = LessonTemplate.find(params[:lesson_template_id])
    end

    def set_lesson_plan
      @lesson_plan = LessonPlan.find(params[:id])
    end

    def lesson_plan_params
      params.require(:lesson_plan).permit(:resources, :sentence_structure, :activities, :assessment, :learning_outcomes, :teacher_mastery, :pupil_mastery)
    end

  end