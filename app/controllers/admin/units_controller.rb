class Admin::UnitsController < AdminController

  def index
    @units = Unit.all
  end

  def new
    @unit = Unit.new
  end

  def create
    @unit = Unit.new(unit_params)
    if @unit.save
      redirect_to admin_units_path
    else
      render :edit
    end
  end

  def edit
    @unit = Unit.find(params[:id])
  end

  def update
    @unit = Unit.find(params[:id])
    if @unit.update(unit_params)
      redirect_to admin_units_path
    else
      render :edit
    end
  end

  private

  def unit_params
    params.require(:unit).permit(:name, :ib, :study_year_id)
  end

end