class Admin::RiskAssessmentSafetiesController < AdminController
  before_action :set_risk_assessment_safety, only: [:show, :edit, :update, :destroy]
  before_action :set_lesson
  before_action :set_risk_assessment

  def show
  end

  def new
    @risk_assessment_safety = @risk_assessment.risk_assessment_safeties.build
  end

  def edit
  end

  def create
    @risk_assessment_safety = @risk_assessment.risk_assessment_safeties.build(risk_assessment_safety_params)

    if @risk_assessment_safety.save
      redirect_to edit_admin_lesson_template_risk_assessment_url(@lesson, @risk_assessment)
    else
      render :new
    end
  end

  def update
    if @risk_assessment_safety.update(risk_assessment_safety_params)
      redirect_to edit_admin_lesson_template_risk_assessment_url(@lesson, @risk_assessment)
    else
      render :edit
    end
  end

  def destroy
    @risk_assessment_safety.destroy
    redirect_to edit_admin_lesson_template_risk_assessment_url(@lesson, @risk_assessment)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_risk_assessment_safety
      @risk_assessment_safety = RiskAssessmentSafety.find(params[:id])
    end

    def set_lesson
      @lesson = LessonTemplate.find(params[:lesson_template_id])
    end

    def set_risk_assessment
      @risk_assessment = RiskAssessment.find(params[:risk_assessment_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def risk_assessment_safety_params
      params.require(:risk_assessment_safety).permit(:procedure, :hazard, :level_of_risk, :severity, :number_affected, :safety_points)
    end
end
