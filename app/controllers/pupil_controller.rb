class PupilController < ApplicationController

  layout 'pupil_application'

  before_action :set_school
  before_action :require_pupil
  before_action :send_demo_to_library

  private

  def set_school
    @school = School.find_by(subdomain: current_subdomain)
  end

  def require_pupil
    if signed_in? && current_user.pupil?
      @pupil = current_user
    else
      flash[:error] = "You do not have permission to access that page. Please sign in to continue"
      redirect_to login_path
    end
  end

  def send_demo_to_library
    if @pupil.demo
      redirect_to pupil_my_library_path
    end
  end

end
