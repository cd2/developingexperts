class Pupil::AchievementsController < PupilController

  def index
    @avatars = Avatar.all_for current_user
  end

  def set_avatar
    current_user.update(avatar: current_user.avatars.find(params[:id]))
    redirect_to pupil_root_path
  end


end
