class Pupils::ResultsController < ApplicationController

  def index
    @results = current_user.lesson_results
  end

  def show
    @attempt = current_user.lesson_results.find(params[:id]).attempt
  end

end
