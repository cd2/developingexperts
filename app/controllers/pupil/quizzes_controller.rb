class Pupil::QuizzesController < PupilController
  layout 'presentation'

  before_action :set_lesson
  before_action :set_attempt
  before_action :confirm_started, except: :start_quiz

  def start_quiz
    if @completed
      @attempt.fun_start!
    else
      @attempt.start!
    end
    redirect_to question_pupil_lesson_path(@lesson)
  end

  def question
    unless @question = @attempt.questions.current
      if @completed
        current_user.update(fun_quiz_lesson_id: nil)
        redirect_to pupil_lesson_path(@lesson, fun: true)
      else
        @attempt.finish!
        redirect_to pupil_lesson_path(@lesson)
      end
    end
  end

  def submit_answer
    current_question = @attempt.questions.current
    #questions numbers match
    #they could be caused to mismatch by opening the quiz in multiple tabs
    #thus you could progress on one screen (advanving the current question)
    #then submit a second answer on the other
    if current_question && current_question.question_number.to_s == params[:question_number]
      current_question.update(answer_id: params[:answer_id])
      if current_question.incorrect?
        @slide = current_question.slide
        @presentation = @slide.presentation
        @slides = @presentation.student_slides
        @questions = @attempt.questions
        @question = current_question
        render 'presentations/show'
      else

        redirect_to question_pupil_lesson_path(@lesson)
      end
    else
      redirect_to question_pupil_lesson_path(@lesson)
    end
  end



  private

    def set_lesson
      @lesson = Lesson.find(params[:id])
    end

    def set_attempt
      lesson_result = @lesson.get_result_for current_user
      @attempt = lesson_result.attempt

      if @attempt.completed?
        @completed = true
        @attempt = current_user.fun_quiz_attempt(@lesson)
      end
    end

    def confirm_started
      unless @attempt.started?
        start_quiz
      end
    end

end
