class Pupil::PupilController < PupilController


  def home
    @lesson = current_user.courses.first&.lessons&.active
  end

  def library
  end

  def set_avatar
    current_user.update(avatar: current_user.avatars.find(params[:avatar_id]))
    redirect_to pupil_root_path
  end

  def charts
    @pupil = current_user
    respond_to do |format|
      format.json {render "pupil/charts/#{params[:chart_type]}"}
    end
  end

  def my_class
    @pupils = @pupil.klasses.first.pupils.sort { |pupil| pupil.lesson_results.sum(&:total) }.reverse
  end

end
