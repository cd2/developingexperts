class Pupil::LessonsController < PupilController
  skip_before_action :send_demo_to_library, only: [:library, :big_preview]

  def index
    @results = current_user.lesson_results
  end

  def show
    if params[:fun]
      @result = current_user.q_fun_quiz_attempt
      @fun = true
    else
      @result = current_user.lessons.find(params[:id]).get_result_for current_user
    end
  end

  def big_preview
    @lesson = Lesson.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def library
    @lessons = @pupil.lessons
  end


end
