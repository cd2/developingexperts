class PresentationsController < ApplicationController
  layout 'presentation'
  before_action :set_presentation
  skip_before_action :authenticate_user!

  def show
    @course = Course.find_by(id: params[:course_id])
    @presentation = Presentation.find(params[:presentation_id])
    if current_user.is_a? User::Pupil
      @slides = @presentation.student_slides
    else
      @slides = @presentation.slides
    end
    if params[:slide_number].present?
      @slide = @slides[params[:slide_number].to_i-1]
    else
      @slide = @slides.first
    end
    @lesson = @presentation.lesson_template
    session[:toggle] = params[:toggle_narration] == '1' if params[:toggle_narration]
  end

  def slide_path
    @slide = Slide.find(params[:id])
    redirect_to presentation_path(@slide.presentation, @slide.slide_number)
  end


  private

    def set_presentation

    end

end
