class SessionsController < Devise::SessionsController
  # skip_before_action :no_subdomain_if_anonymous
  before_action :validate_subdomain, except: :choose_school
  before_action :no_subdomain, only: :choose_school
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    # self.resource = resource_class.new(sign_in_params)
    render 'new_teacher' if params[:user] && params[:user][:teacher]
    render 'new_pupil' if params[:user] && params[:user][:pupil]
  end

  def create
    if params[:user] && params[:user][:teacher]
      super
    else
      if pupil = @school.pupils.find_by(pupil_identifier: params[:user][:pupil_identifier])
        sign_in :user, pupil
        redirect_to root_url
      else
        @error = 'Sorry we couldn\'t find that number, please try again.'
        render 'new_pupil'
      end
    end
  end

  def choose_school
    if School.find_by(subdomain: params[:school_sub])
      redirect_to who_are_you_url(subdomain: params[:school_sub])
    end
  end

  def who_are_you
  end

  private

    def validate_subdomain
      unless request.subdomain.present? && ((@school = School.find_by(subdomain: request.subdomain)) || request.subdomain == 'admin')
        redirect_to choose_school_url(subdomain: false)
      end
    end

    def no_subdomain
      if request.subdomain.present? && School.find_by(subdomain: request.subdomain)
        redirect_to who_are_you_path
      end
    end


  protected

    def configure_permitted_parameters
      added_attrs = [:pupil_identifier, :email, :password, :school_id]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end

end
