class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # before_action :no_subdomain_if_anonymous
  
  # before_action :subdomain_redirection
  
  include ApplicationHelper
  include SessionsHelper

  before_action :authenticate_user!
  before_action :set_current_school

  before_action do
    $request = request
  end

  #school detection through subdomains
  def subdomain_redirection
    if subdomain? && !signed_in? && current_subdomain != 'www' && current_subdomain != 'admin' && current_subdomain != 'api' && current_subdomain != 'developingexperts-staging'
      if School.matching_subdomain current_subdomain
        unless signed_in_as_teacher? || signed_in_as_pupil? || action_name == 'choose_login' || controller_name == 'sessions'
          redirect_to choose_login_path
        end
      else
        redirect_to no_school_url(subdomain: false, sub: current_subdomain)
      end
    end
  end

  def set_current_school
    @current_school = School.matching_subdomain current_subdomain
    # if subdomain?
    # end
  end

  def authenticated_user_redirection
  end

  def no_subdomain_if_anonymous
    redirect_to subdomain: false if request.subdomain.present? && !signed_in?
  end

end
