class Faculties::KlassesController < FacultyController
  before_action :set_class, only: [:show, :edit, :update, :destroy, :show_graph_json]
  before_action :verify_administrator, except: [:index, :graph_json, :show_graph_json]


  def index
    if signed_in_as_administrator?
      @q = @school.klasses.ransack(params[:q])
    else
      @q = current_user.klasses.ransack(params[:q])
    end
    @classes = @q.result(distinct: true)
  end

  def new
    @klass = @school.klasses.new
  end

  def show
  end

  def create
    @klass = @school.klasses.build(klass_params)
    if @klass.save
      @school.notify("#{view_context.link_to(@klass.name, faculty_klass_path(@klass))} has been created")
      flash[:success] = 'Class Created'
      redirect_to faculty_klass_path(@klass)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if params[:commit] == 'add_mass_to_klass'
      if params[:add_to_klass]
        params[:add_to_klass].each do |id|
          @school.unassigned_pupils.find(id).enroll_in(@klass)
        end
      end
      render :edit
    else
      if @klass.update(klass_params)
        @school.notify("#{view_context.link_to(@klass.name, faculty_klass_path(@klass))} has been updated")
        flash[:success] = 'Class Updated'
        redirect_to faculty_klass_path(@klass)
      else
        render :edit
      end
    end
  end

  def destroy
    @klass.destroy
    redirect_to faculty_klasses_path
  end

  def graph_json
    @klasses = @school.klasses
    respond_to do |format|
      format.json {render "faculty/klasses/graphs/marks"}
    end
  end

  def show_graph_json
    respond_to do |format|
      format.json {render "faculty/klasses/graphs/show_graph"}
    end
  end

  private

    def set_class
      @klass = Klass.find(params[:id])
    end

    def klass_params
      params.require(:klass).permit(:name, :faculty_id, :course_id)
    end

    def verify_administrator
      redirect_to faculty_root_path unless signed_in_as_administrator? || current_user.klasses.include?(@klass)
    end

end
