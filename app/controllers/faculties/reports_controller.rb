class Faculties::ReportsController < FacultyController

  before_action :verify_administrator

  def index

  end

  private

    def verify_administrator
      redirect_to faculty_root_path unless signed_in_as_administrator?
    end

end
