class Faculties::TeachersController < FacultyController
  before_action :verify_administrator, except: [:show, :edit, :update]
  before_action :set_teacher, only: [:show, :update, :edit, :destroy]
  before_action :verify_self, only: [:show, :edit, :update]
  before_action :require_valid_user, only: [:edit, :update]
  layout 'teacher_application'

  def index
    @q = @school.faculty_members.ransack(params[:q])
    @teachers = @q.result(distinct: true)
  end

  def show
  end

  def new
    @teacher = @school.faculty_members.build
  end

  def create
    @teacher = @school.faculty_members.build(teacher_params)
    if @teacher.save
      flash[:notice] = 'Teacher created'
      @school.notify("#{view_context.link_to(@teacher.name, faculty_teacher_path(@teacher))} has been added as a new #{view_context.link_to('teacher', faculty_teachers_path)}")
      redirect_to faculty_teachers_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @teacher.update(teacher_params)
      flash[:notice] = 'Teacher update'
      @school.notify("#{view_context.link_to(@teacher.name, faculty_teacher_path(@teacher))} has been updated")
      redirect_to faculty_teachers_path
    else
      render :edit
    end
  end

  def destroy
    @teacher.destroy
    redirect_to faculty_teachers_path
  end

  private

    def set_teacher
      @teacher = User.friendly.find(params[:id])
    end

    def teacher_params
      params.require(:user_faculty).permit(:name, :email, :gender, :working_days, :password, :password_confirmation)
    end

    def verify_administrator
      redirect_to faculty_root_path unless signed_in_as_administrator?
    end

    def verify_self
      redirect_to faculty_root_path unless @teacher == current_user || signed_in_as_administrator?
    end

    def require_valid_user
      redirect_to faculty_root_path unless @teacher.id == current_user.id || signed_in_as_administrator?
    end

end
