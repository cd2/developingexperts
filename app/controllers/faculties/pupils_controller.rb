class Faculties::PupilsController < FacultyController
  before_action :verify_administrator, only: [:new, :create, :edit, :update, :destroy, :add_to_class, :leave_class]
  before_action :set_pupil, only: [:show, :update, :edit, :add_to_class, :leave_class, :destroy, :graph_json]

  def show
  end

  def edit
  end

  def index
    pupil_driver = signed_in_as_administrator? ? @school : current_user
    @q = pupil_driver.pupils.ransack(params[:q])
    @pupils = @q.result(distinct: true)
  end

  def new
    @pupil = @school.pupils.build
  end

  def create
    @pupil = @school.pupils.build(pupil_params)
    if @pupil.save
      unless params[:user_pupil][:klass].blank?
        @pupil.enroll_in Klass.find(params[:user_pupil][:klass])
      end
      flash[:notice] = 'Pupil created'
      @school.notify("#{view_context.link_to(@pupil.name, faculty_pupil_path(@pupil))} has been added as a new #{view_context.link_to('pupil', faculty_pupils_path)}")
      redirect_to faculty_pupil_path(@pupil)
    else
      render :new
    end
  end

  def update
    if @pupil.update(pupil_params)
      unless params[:user_pupil][:klass].blank?
        @pupil.enroll_in Klass.find(params[:user_pupil][:klass])
      end
      flash[:notice] = 'Pupil updated'
      @school.notify("#{view_context.link_to(@pupil.name, faculty_pupil_path(@pupil))} has been updated")
      redirect_to faculty_pupil_path(@pupil)
    else
      render :edit
    end
  end

  def add_to_class
    klass = Klass.find(params[:klass_id])
    @pupil.enroll_in(klass)
    redirect_to edit_faculty_klass_path(klass, tab: 'manage')
  end

  def leave_class
    klass = Klass.find(params[:klass_id])
    @pupil.leave(klass)
    redirect_to edit_faculty_klass_path(klass, tab: 'manage')
  end

  def import
    file = params[:csv_file]
    unless File.extname(file.original_filename) == ".csv"
      flash[:error] = "File format is not supported"
      return redirect_to faculty_pupils_url
    end
    begin
      User::Pupil.import_csv(@school, file)
    rescue => e
      flash[:error] = e.message
    else
      flash[:success] = "Pupils Imported"
    ensure
      redirect_to faculty_pupils_url
    end
  end

  def destroy
    @pupil.destroy
    redirect_to faculty_pupils_path
  end

  def graph_json
    respond_to do |format|
      format.json {render "faculty/pupils/graphs/#{params[:chart_type]}"}
    end
  end


  private

    def set_pupil
      @pupil = User.friendly.find(params[:id])
    end

    def pupil_params
      params.require(:user_pupil).permit(:name, :email, :age, :gender, :pupil_identifier, :dob, :study_year_id, :klass, :specified_class)
    end

    def verify_administrator
      redirect_to faculty_root_path unless signed_in_as_administrator?
    end


end
