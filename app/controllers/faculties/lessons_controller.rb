class Faculties::LessonsController < FacultyController

  before_action :set_lesson

  def show
    respond_to do |format|
      format.html
      format.pdf do
        pdf = LessonPlanPdf.new(@lesson)
        send_data pdf.render, filename: "#{@lesson.display_name}.pdf", type: 'application/pdf'
      end
    end
  end

  def mark
    @pupils = @lesson.pupils
  end

  def update_marks
    if @lesson.update(lesson_params)
      respond_to do |format|
        format.html { redirect_to mark_faculty_lesson_path(@lesson) }
        format.js {render nothing: true, status: 200}
      end
    else
      respond_to do |format|
        format.html { render :update_marks }
        format.js {render nothing: true, status: 500}
      end
    end
  end

  def edit
  end

  def update
    if @lesson.update(lesson_date_params)
      redirect_to faculty_course_path(@lesson.course)
    else
      render :edit
    end
  end

  private

    def set_lesson
      @lesson = Lesson.find(params[:id])
    end

    def lesson_params
      params.require(:lesson).permit(lesson_results_attributes: [:id, :in_class_mark, :teacher_comments, :absent])
    end

    def lesson_date_params
      params.require(:lesson).permit(:new_start_date)
    end

end
