class Faculties::CoursesController < FacultyController
  before_action :set_course, only: [:show, :edit, :update, :duplicate, :destroy]

  def index
    if signed_in_as_administrator?
      @q = @school.courses.ransack(params[:q])
    else
      @q = current_user.courses.ransack(params[:q])
    end
    @courses = @q.result(distinct: true)
  end

  def show
    respond_to do |format|
      format.html
      format.json
    end
  end

  def select_years
  end

  def new
    @year = StudyYear.find(params[:year])
    @course = @school.courses.build
  end

  def create
    @year = StudyYear.find(params[:course][:year])
    @course = @school.courses.new(course_params)
    if @course.save
      @school.notify("#{view_context.link_to(@course.name, faculty_course_path(@course))} has been created")
      flash[:success] = 'Course Created'
      redirect_to edit_faculty_course_path(@course)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @course.update(course_params)
      @school.notify("#{view_context.link_to(@course.name, faculty_course_path(@course))} has been updated")
      flash[:success] = 'Course Updated'
      redirect_to [:edit, :faculty, @course]
    else
      render :edit
    end
  end

  def duplicate
    new_course = @course.dup
    new_course.lessons << @course.lessons.collect {|l| l.dup }
    new_course.save
    render :edit
  end

  def destroy
    @course.destroy
    redirect_to faculty_courses_path
  end



  private

    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :start_date, lessons_attributes: [:lesson_template_id, :disabled])
    end

end
