class PagesController < ApplicationController
  layout :set_layout

  skip_before_action :authenticate_user!

  def show
    @page = Page.friendly.find(params[:id])
  end

  def home
    if signed_in?
      if signed_in_as_administrator?
        redirect_to faculty_klasses_path
      elsif signed_in_as_teacher?
        redirect_to faculty_root_path
      elsif signed_in_as_pupil?
        redirect_to pupil_root_path
      elsif current_user.admin?
        redirect_to admin_path
      end
    end
    @enquiry = Enquiry.new
  end

  def no_school
  end

  def product
  end

  def research
  end

  def blogs
  end

  def sample_missions
    @presentations = Presentation.where(id: [1, 86, 116, 158, 180, 234])
  end


  private

    def set_layout
      if signed_in?
        if current_user.teacher?
          'teacher_application'
        elsif current_user.pupil?
          'pupil_application'
        end
      end
    end

end
