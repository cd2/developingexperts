module Permissions

  def require_user
    redirect_to login_path unless signed_in?
  end

end