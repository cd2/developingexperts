class FacultyController < ApplicationController

  layout 'teacher_application'

  before_action :set_school
  before_action :require_faculty

  private

  def set_school
    @school = School.find_by(subdomain: current_subdomain)
  end

  def require_faculty
    if signed_in_as_faculty?
      @teacher = current_user
    else
      flash[:error] = "You do not have permission to access that page. Please sign in to continue"
      redirect_to sign_in_teachers_path
    end
  end

end
