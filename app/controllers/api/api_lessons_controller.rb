class Api::ApiLessonsController < ApiController

  def index
    render json: LessonTemplate.all
  end

  def show
    @mission = LessonTemplate.find_by(machine_name: params[:id])

    render @mission.map { |mission|

      {
          attr: {
              name: mission.display_name,
              machine_name: mission.machine_name,
              body: mission.body,
              remote_cover_image_url: mission.cover_image.url,
          },
          rocket_words: mission.rocket_words.map {|word|
            {
                word: word.word,
                remote_image_url: word.image.url,
                description: word.description,
            }
          },
          slides: mission.slides.map {|slide|
            {
                slide_type: slide.slide_type,
                body: slide.upper_text,
                remote_image_url: slide.background_image.url,
                jwplayer_media_id: slide.new_video_id,
                jwplayer_media_source_id: (slide.video_quality_override rescue nil),
                class_only: slide.class_only,
            }
          },
          questions: mission.formative_questions.map {|question|
            {
                question: question.question,
                answer: question.answer,
                image: question.answer_image.url,
                slide_id: question.slide_id,
                rocket_word_id: question.rocket_word_id,
            }
          }
      }

    }
  end

end