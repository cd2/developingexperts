class Api::ApiUnitsController < ApiController

  def index
    render json: Unit.all.map { |unit|
      {
        attr: {
            name: unit.name,
            mascot_name: unit.study_year&.mascot_name,
            legacy_id: unit.id,
        },
        extras: {
            song_video: unit.song.url
        },
        study_year:
          {
              name: unit.study_year&.name,
              number: unit.study_year&.year_number,
              legacy_id: unit.study_year.id,
          },
        ib: {
            name: unit.ib
        },
        missions: {
          mission: unit.lesson_templates.map {|mission|
            {
              attr: {
                name: mission.display_name,
                machine_name: mission.machine_name,
                body: mission.body,
                remote_cover_image_url: mission.cover_image.url,
                legacy_id: mission.id
              },
              template: {
                  resources: mission.lesson_plan&.resources,
                  sentence_structure: mission.lesson_plan&.sentence_structure,
                  activities: mission.lesson_plan&.activities,
                  assessment: mission.lesson_plan&.assessment,
                  learning_outcomes: mission.lesson_plan&.learning_outcomes,
                  teacher_mastery: mission.lesson_plan&.teacher_mastery,
                  pupil_mastery: mission.lesson_plan&.pupil_mastery,
                  legacy_id: mission.lesson_plan.id
              },
              rocket_words: mission.rocket_words.map {|word|
                {
                  word: word.word,
                  remote_image_url: word.image.url,
                  description: word.description,
                  legacy_id: word.id
                }
              },
              slides: mission.slides.map {|slide|
                {
                  slide_type: slide.slide_type,
                  body: slide.upper_text,
                  remote_image_url: slide.background_image.url,
                  remote_video_url: slide.video.url,
                  remote_narration_url: slide.narration_video.url,
                  class_only: slide.class_only,
                  legacy_id: slide.id
                }
              }
            }
          }
        }
      }
    }
  end

  def show
    @unit = Unit.find(params[:id])
    render json: Unit.where(id: @unit.id).map { |unit|
      {
        attr: {
            name: unit.name,
            mascot_name: unit.study_year&.mascot_name,
            legacy_id: unit.id
        },

        study_year:
            {
                name: unit.study_year&.name,
                number: unit.study_year&.year_number,
                legacy_id: unit.study_year.id,
            },
        ib: {
            name: unit.ib
        },
        missions: {
            mission: unit.lesson_templates.map {|mission|
              {
                attr: {
                  name: mission.display_name,
                  machine_name: mission.machine_name,
                  body: mission.body,
                  remote_cover_image_url: mission.cover_image.url,
                  resources: mission.lesson_plan&.resources,
                  sentence_structure: mission.lesson_plan&.sentence_structure,
                  activities: mission.lesson_plan&.activities,
                  assessment: mission.lesson_plan&.assessment,
                  learning_outcomes: mission.lesson_plan&.learning_outcomes,
                  teacher_mastery: mission.lesson_plan&.teacher_mastery,
                  pupil_mastery: mission.lesson_plan&.pupil_mastery,
                  legacy_id: mission.id
                },
                extras: {
                    song_video: mission.song.url
                },
                rocket_words: mission.rocket_words.map {|word|
                  {
                    word: word.word,
                    remote_image_url: word.image.url,
                    description: word.description,
                    legacy_id: word.id
                  }
                },
                slides: mission.slides.map {|slide|
                  {
                    slide_type: slide.slide_type,
                    body: slide.upper_text,
                    remote_image_url: slide.background_image.url,
                    legacy_video_url: slide.video.url,
                    legacy_narration_video_url: slide.narration_video.url,
                    class_only: slide.class_only,
                    legacy_id: slide.id
                  }
                },
                questions: mission.formative_questions.map {|question|
                  {
                    question: question.question,
                    answer: question.answer,
                    image: question.answer_image.url,
                    legacy_slide_id: question.slide_id,
                    legacy_rocket_word_id: question.rocket_word_id,
                    legacy_id: question.id
                  }
                },
                resources: mission.resources.map {|resource|
                 {
                   remote_document_url: resource.file.url,
                   name: resource.name,
                   legacy_id: resource.id
                 }
                }
              }
            }
        }
      }
    }
  end

end