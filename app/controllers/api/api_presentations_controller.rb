class Api::ApiPresentationsController < ApiController

  def index
    render json: Presentation.all
  end

end