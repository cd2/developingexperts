class Api::ApiStudyYearsController < ApiController

  def index
    render json: StudyYear.all.map do |s|
      {
        year: s,
        units: s.units
      }
    end
  end

  def show
    @study_year = StudyYear.find(params[:id])
    render json: @study_year, except: [:created_at, :updated_at]
  end

end