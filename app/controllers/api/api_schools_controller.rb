class Api::ApiSchoolsController < ApiController

  def index
    render json: School.all.map { |school|
      {
        attr: {
          name: school.name,
          subdomain: school.subdomain,
          logo: school.logo,
          twitter: school.twitter,
          facebook: school.facebook,
          legacy_id: school.id
        },
        teachers: school.teachers.map { |teacher|
          {
            name: teacher.name,
            age: teacher.age,
            gender: teacher.gender,
            working_days: teacher.working_days,
            email: teacher.email,
            administrator: teacher.administrator,
            legacy_id: teacher.id,
            legacy_school_id: school.id
          }
        },
        pupils: school.pupils.map { |pupil|
          {
            name: pupil.name,
            identifier: pupil.pupil_identifier,
            legacy_id: pupil.id,
            legacy_school_id: school.id
          }
        },
        courses: school.courses.map { |course|
          {
              attr: {
                  name: course.name,
                  legacy_id: course.id,
                  legacy_school_id: school.id
              },
              lessons: course.lessons.includes(:lesson_template).map { |lesson|
                {
                    name: lesson.lesson_template&.machine_name,
                    legacy_id: lesson.id,
                    legacy_course_id: course.id,
                    legacy_school_id: school.id
                }
              }
          }
        },
        forms: school.klasses.map { |klass|
          {
            attr: {
              name: klass.name,
              legacy_id: klass.id,
              legacy_school_id: school.id
            },
            rel: {
                course: klass.course&.id,
                teacher: klass.teacher&.id,
            }

          }

        },

      }
    }
  end

  def show

    render json: School.where(id: params[:id]).map { |school|
      {
          attr: {
              name: school.name,
              subdomain: school.subdomain,
              logo: school.logo,
              twitter: school.twitter,
              facebook: school.facebook,
              legacy_id: school.id
          },
          teachers: school.teachers.map { |teacher|
            {
                name: teacher.name,
                age: teacher.age,
                gender: teacher.gender,
                working_days: teacher.working_days,
                email: teacher.email,
                administrator: teacher.administrator,
                legacy_id: teacher.id,
                legacy_school_id: school.id
            }
          },
          pupils: school.pupils.map { |pupil|
            {
                name: pupil.name,
                identifier: pupil.pupil_identifier,
                legacy_id: pupil.id,
                legacy_school_id: school.id
            }
          },
          courses: school.courses.map { |course|
            {
                attr: {
                    name: course.name,
                    legacy_id: course.id,
                    legacy_school_id: school.id
                },
                lessons: course.lessons.includes(:lesson_template).map { |lesson|
                  {
                      name: lesson.lesson_template&.machine_name,
                      legacy_id: lesson.id,
                      legacy_course_id: course.id,
                      legacy_school_id: school.id
                  }
                }
            }
          },
          forms: school.klasses.map { |klass|
            {
                attr: {
                    name: klass.name,
                    legacy_id: klass.id,
                    legacy_school_id: school.id
                },
                rel: {
                    course: klass.course&.id,
                    teacher: klass.teacher&.id,
                }

            }

          },

      }
    }
  end

  def enrollments
    render json: Enrollment.includes(:klass, :pupil).all.map { |enrollment|
      {
          form_id: enrollment.klass&.id,
          pupil_id: enrollment.pupil&.id,
      }
    }
  end

end