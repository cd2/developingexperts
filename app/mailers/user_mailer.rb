class UserMailer < ApplicationMailer

  def send_details(user)
    @user = user
    mail to: 'josh@developingexperts.com', from: 'web@developingexperts.com', cc: 'sarah@developingexperts.com', subject: 'New person in the sample lessons'
  end

end
