class LessonPlanPdf < Prawn::Document

  def initialize(lesson_plan)
    super()

    font_families.update("the_font" => {
      normal: (Rails.root + 'public/fonts/Raleway-Regular.ttf').to_s,
      bold: (Rails.root + 'public/fonts/Raleway-Bold.ttf').to_s
     })

    font('the_font')

    @lesson_plan = lesson_plan
    @lesson = @lesson_plan.lesson_template
    header
    resource_list_table
    summary
    mission_knowledge_table
    vocab_table
    mission_overview_table
  end

  def header
    text @lesson.display_name, size: 20, style: :bold
  end

  def resource_list_table
    table_content = [['Resources']]
    @lesson.resources.each{|r| table_content << [r.name]}
    table table_content, :width => 540 do
      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
    end
  end

  def summary
    table_content = [['Year Group:', @lesson.study_year.name],
                  ['Unit:', @lesson.unit&.name],
                  ['Mission Number:', @lesson.lesson_number],
                  ['Mission Objective:', @lesson.display_name]]
    table table_content, :width => 540 do
      column(0).borders = [:right]
      column(0).border_width = 2
      column(0).font_style = :bold
    end
  end

  def mission_knowledge_table
    table_content = [['Mission Knowledge and Expertise to Master:',
    "#{@lesson.display_name}. Key Words: #{@lesson.rocket_words.collect{|w| w.word}.join(', ')}"]]
    table table_content, :width => 540 do
      column(0).borders = [:right]
      column(0).border_width = 2
      column(0).font_style = :bold
    end
  end

  def vocab_table
    table_content = [['Vocabulary to Master:',
    'Key Resources:',
    'Sentence Structures:'],

    [@lesson.rocket_words.collect{|w| w.word}.join(', '),
    @lesson.lesson_plan&.resources,
    @lesson.lesson_plan&.sentence_structure],

     ['Activities for Learning:',
    'Key Questions:',
    'Assessment:'],

    [@lesson.lesson_plan&.activities,
    @lesson.quiz.formative_questions.collect{|q| q.question.strip}.join("\n"),
    @lesson.lesson_plan&.assessment],

    [{content: @lesson.lesson_plan&.learning_outcomes, colspan: 3}]]

    table table_content, :width => 540 do
      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
      row(2).borders = [:bottom]
      row(2).border_width = 2
      row(2).font_style = :bold
    end

  end

  def mission_overview_table
    table_content = [['Mission Overview: Rocket Words and Meanings Facts Test:',
    'Content:'],
    ['Issue Rocket words sheets to those students without access to computer at home. Homework is to complete a minimum of 5 minutes daily test practice.',
    'Students enter the classroom in silence. Key words and meanings to be reviewed on board. Results from week’s performance can be shown on board. A starter test can be sat. Using tablets is the easiest option. (Paper tests are available.) Appoint mission timer and issue stopwatch keeper.'],
    ['Transition:', ''],
    ['Start countdown clock',
    'Explain to the pupils about the count down clock system. They have ten seconds to move to the story mat. (Pupils are to count down from 10 – 1 then say ‘blast off’.)',],
    ['Knowledge:',''],
    ['','Read through today’s mission. Ask the class debriefer to manage the slide show.',],
    ['Transition:',''],
    ['60 second countdown clock',
    'Explain to the pupils about the count down clock system. This time pupils are to collect equipment in silence during time.',],
    ['Discussion Task:',''],
    ['','Pupils to answer questions during presentation.',],
    ['Mastering the Knowledge Task:',''],
    [@lesson.lesson_plan&.teacher_mastery, @lesson.lesson_plan&.pupil_mastery],
    ['Transition:',''],
    ['','Explain to the pupils about the count down clock system. Countdown clock to be used to time reentry into classroom to settle down an record experiment write up.',],
    ['Independent Task:',''],
    ['','To complete the handout writing up the process of your experiment by completing the statement bank. Activate the timer clock for pupils to clear away their workstations (10 minutes). Pupils are to count down from 10 – 1 seconds. Place your log book into either the Discovery tray (green) or Explorer tray (red). Explain that the green tray means that the pupil has understood the lesson well and the red tray means that more time is needed to practise.',],
    ['Mission Debrief:',''],
    ['','Pupils are to discuss with talk partners the questions on board. Complete a question and answer session selecting some pupils to share their answers.',]]
    table table_content, :width => 540 do
      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
      row(2).borders = [:bottom]
      row(2).border_width = 2
      row(2).font_style = :bold
    end
  end
end
